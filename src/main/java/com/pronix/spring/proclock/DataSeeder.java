package com.pronix.spring.proclock;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.pronix.spring.proclock.dao.CompanyDAO;
import com.pronix.spring.proclock.dao.ModuleDAO;
import com.pronix.spring.proclock.dao.ProjectDAO;
import com.pronix.spring.proclock.dao.RefRolesDAO;
import com.pronix.spring.proclock.dao.RolesModuleDAO;
import com.pronix.spring.proclock.dao.UserContactsDAO;
import com.pronix.spring.proclock.dao.UserDetailsDAO;
import com.pronix.spring.proclock.dao.UserPasswordDAO;
import com.pronix.spring.proclock.models.Company;
import com.pronix.spring.proclock.models.Modules;
import com.pronix.spring.proclock.models.Project;
import com.pronix.spring.proclock.models.RefRoles;
import com.pronix.spring.proclock.models.RolesModules;
import com.pronix.spring.proclock.models.UserContacts;
import com.pronix.spring.proclock.models.UserDetails;
import com.pronix.spring.proclock.models.UserPassword;
import com.pronix.spring.proclock.taskmanagement.DAO.TaskStatusDAO;
import com.pronix.spring.proclock.taskmanagement.models.TaskStatus;

@Component
public class DataSeeder {

	@Autowired
	private ModuleDAO moduleDAO;

	@Autowired
	private UserDetailsDAO userDetailsDAO;

	@Autowired
	private UserContactsDAO userContactsDAO;

	@Autowired
	private UserPasswordDAO userPasswordDAO;

	@Autowired
	private RefRolesDAO refRolesDAO;

	@Autowired
	private RolesModuleDAO rolesModuleDAO;

	@Autowired
	private ProjectDAO projecrDao;

	@Autowired
	private CompanyDAO companyDAO;
	
	@Autowired
	private TaskStatusDAO taskStatusDAO;

	@EventListener
	public void moduleSeeder(ApplicationReadyEvent event) {
		Optional<Modules> checkFirstOne = Optional.ofNullable(moduleDAO.findByValue("Leave Management"));
		if (checkFirstOne.isPresent()) {
			System.out.println("Data already their");
		} else {
			moduleDAO.save(new Modules("Leave Management", "Leave access for manager level"));
		}
		Optional<Modules> checkFirstSecond = Optional.ofNullable(moduleDAO.findByValue("Leaves"));
		if (checkFirstSecond.isPresent()) {
		} else {
			moduleDAO.save(new Modules("Leaves", "Leave access for Employee level"));
		}
		Optional<Modules> checkFirstThird = Optional.ofNullable(moduleDAO.findByValue("TimeSheet Management"));
		if (checkFirstThird.isPresent()) {
		} else {
			moduleDAO.save(new Modules("TimeSheet Management", "TimeSheet access for Manager level"));
		}
		Optional<Modules> checkFirstFourth = Optional.ofNullable(moduleDAO.findByValue("TimeSheet"));
		if (checkFirstFourth.isPresent()) {
		} else {
			moduleDAO.save(new Modules("TimeSheet", "TimeSheet access for Employee level"));
		}
		Optional<Modules> checkFirstFifth = Optional.ofNullable(moduleDAO.findByValue("Asset"));
		if (checkFirstFifth.isPresent()) {
		} else {
			moduleDAO.save(new Modules("Asset", "Asset"));
		}
		Optional<Modules> checkFirstSix = Optional.ofNullable(moduleDAO.findByValue("Onboard"));
		if (checkFirstSix.isPresent()) {
		} else {
			moduleDAO.save(new Modules("Onboard", "Onboard"));
		}
		Optional<Modules> checkFirstSeven = Optional.ofNullable(moduleDAO.findByValue("Projects"));
		if (checkFirstSeven.isPresent()) {
		} else {
			moduleDAO.save(new Modules("Projects", "Creting project and assiging emplpyee to project"));
		}
		Optional<Modules> checkFirstEigth = Optional.ofNullable(moduleDAO.findByValue("Employees"));
		if (checkFirstEigth.isPresent()) {
		} else {
			moduleDAO.save(new Modules("Employees", "Creting Employees and updating employees"));
		}
		Optional<Modules> checkFirstNine = Optional.ofNullable(moduleDAO.findByValue("Service Request main"));
		if (!checkFirstNine.isPresent()) {
			moduleDAO.save(new Modules("Service Request main", "Service request configuration access"));
		}
		Optional<Modules> checkFirstTen = Optional.ofNullable(moduleDAO.findByValue("Service Request Approver"));
		if (!checkFirstTen.isPresent()) {
			moduleDAO.save(new Modules("Service Request Approver", "Service request Approval access"));
		}
		Optional<Modules> checkFirstExecutor = Optional.ofNullable(moduleDAO.findByValue("Service Request Executor"));
		if (!checkFirstExecutor.isPresent()) {
			moduleDAO.save(new Modules("Service Request Executor", "Service request Executor access"));
		}

		Optional<Modules> checkHolidayManagement = Optional.ofNullable(moduleDAO.findByValue("Holiday Management"));
		if (checkHolidayManagement.isPresent()) {
		} else {
			moduleDAO.save(new Modules("Holiday Management", "Holiday Management"));
		}
		
		Optional<Modules> checkExpense = Optional.ofNullable(moduleDAO.findByValue("Expense Management"));
		if (checkExpense.isPresent()) {
		} else {
			moduleDAO.save(new Modules("Expense Management", "Expense Management"));
		}
		
		Optional<Modules> checkTaskManagement = Optional.ofNullable(moduleDAO.findByValue("Task Management"));
		if (checkTaskManagement.isPresent()) {
		} else {
			moduleDAO.save(new Modules("Task Management", "Task Management"));
		}
	}
	

	@EventListener
	public void RoleSeeder(ApplicationReadyEvent event) {

		Optional<RefRoles> checkRole = Optional.ofNullable(refRolesDAO.findByValue("SuperAdmin"));
		if (!checkRole.isPresent()) {
			refRolesDAO.save(new RefRoles("SuperAdmin", "SuperAdmin Always have full access"));
			RefRoles getRole = refRolesDAO.findByValue("SuperAdmin");
			List<Modules> allModules = moduleDAO.findAll();
			allModules.forEach(module -> {
				rolesModuleDAO.save(new RolesModules(module, getRole, true));
			});
			UserDetails userdetails = new UserDetails("Super", "Admin", "kshdhi789s7dsjbskj", true, getRole);
			userDetailsDAO.save(userdetails);
			userContactsDAO.save(new UserContacts("SuperAdmin@testmail.com", "00000000", userdetails));
			userPasswordDAO.save(new UserPassword("SuperAdmin", getMd5("Password"), userdetails));
		}

		Optional<RefRoles> checkRoleManagement = Optional.ofNullable(refRolesDAO.findByValue("Management"));
		if (!checkRoleManagement.isPresent()) {
			RefRoles getRole = refRolesDAO
					.save(new RefRoles("Management", "This is role is for Who is having high level access"));
			List<Modules> allModules = moduleDAO.findAll();
			allModules.forEach(module -> {
				rolesModuleDAO.save(new RolesModules(module, getRole, true));
			});
		}
		
//		Optional<RefRoles> checkRoleRM = Optional.ofNullable(refRolesDAO.findByValue("Reporting Manager"));
//		if (!checkRoleRM.isPresent()) {
//			RefRoles getRole = refRolesDAO
//					.save(new RefRoles("Reporting Manager", "This is role is for Who is having high level access"));
//			List<Modules> allModules = moduleDAO.findAll();
//			allModules.forEach(module -> {
//				rolesModuleDAO.save(new RolesModules(module, getRole, true));
//			});
//		}

	}

	public static String getMd5(String input) {
		try {
			// Static getInstance method is called with hashing MD5
			MessageDigest md = MessageDigest.getInstance("MD5");
			// digest() method is called to calculate message digest
			// of an input digest() return array of byte
			byte[] messageDigest = md.digest(input.getBytes());
			// Convert byte array into signum representation
			BigInteger no = new BigInteger(1, messageDigest);
			// Convert message digest into hex value
			String hashtext = no.toString(16);
			while (hashtext.length() < 32) {
				hashtext = "0" + hashtext;
			}
			return hashtext;
		}
		// For specifying wrong message digest algorithms
		catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	@EventListener
	public void defaultProjectseeder(ApplicationReadyEvent event) {
		Optional<Project> checkProject = projecrDao.findById((long) 1);
		if (!checkProject.isPresent()) {
			//Optional<Company> company = companyDAO.findById((long) 1);
			projecrDao.save(new Project("Default Task", "TASK", "SmarTRM", "2020-01-01", "Default", "2030-01-01",
				null));
		}
	}
	
	
	@EventListener
	public void defaultTaskStatusseeder(ApplicationReadyEvent event) {
		Optional<TaskStatus> checkTaskStatusOne = taskStatusDAO.findById((long) 1);
		if (!checkTaskStatusOne.isPresent()) {
			taskStatusDAO.save(new TaskStatus("New", "New"));
		}
		
		Optional<TaskStatus> checkTaskStatusTwo = taskStatusDAO.findById((long) 2);
		if (!checkTaskStatusTwo.isPresent()) {
			taskStatusDAO.save(new TaskStatus("Resolved", "Resolved"));
		}
		
		Optional<TaskStatus> checkTaskStatusThree = taskStatusDAO.findById((long) 3);
		if (!checkTaskStatusThree.isPresent()) {
			taskStatusDAO.save(new TaskStatus("In Progress", "In Progress"));
		}
		
		Optional<TaskStatus> checkTaskStatusFour = taskStatusDAO.findById((long) 4);
		if (!checkTaskStatusFour.isPresent()) {
			taskStatusDAO.save(new TaskStatus("Closed", "Closed"));
		}
		
		Optional<TaskStatus> checkTaskStatusFive = taskStatusDAO.findById((long) 5);
		if (!checkTaskStatusFive.isPresent()) {
			taskStatusDAO.save(new TaskStatus("Re Open", "Re Open"));
		}
	}
}
