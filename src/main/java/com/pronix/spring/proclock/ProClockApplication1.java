package com.pronix.spring.proclock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

//BootStrap Class
@SpringBootApplication
@EnableTransactionManagement
@EnableScheduling
@EnableAsync
public class ProClockApplication1 {

	public static void main(String[] args) {
		SpringApplication.run(ProClockApplication1.class, args);
	}	
}