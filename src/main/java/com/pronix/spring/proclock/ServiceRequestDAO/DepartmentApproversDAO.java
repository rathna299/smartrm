package com.pronix.spring.proclock.ServiceRequestDAO;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pronix.spring.proclock.servicerequestModel.Departmentapprovers;

public interface DepartmentApproversDAO extends JpaRepository<Departmentapprovers, Long> {

	@Query(value = "select * from department_approvers where service_department_id = ?2 and user_id =?1",nativeQuery = true)
	Optional<Departmentapprovers> findbyApproverIdAndDepartmentId(Long approver_id,Long department_id);

	@Query(value = "select * from  department_approvers where service_department_id = ?1",nativeQuery = true)
	List<Departmentapprovers> findbyDepartmentId(Long departmentId);

}
