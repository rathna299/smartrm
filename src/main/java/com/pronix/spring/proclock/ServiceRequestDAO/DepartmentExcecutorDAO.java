package com.pronix.spring.proclock.ServiceRequestDAO;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pronix.spring.proclock.servicerequestModel.DepartmentExecutors;

public interface DepartmentExcecutorDAO extends JpaRepository<DepartmentExecutors, Long> {

	@Query(value = "select * from department_executors where user_id=?1 and service_department_id=?2",nativeQuery = true)
	Optional<DepartmentExecutors> findByExecutorAndDepartment(Long user_id,Long service_department_id);

	@Query(value = "select * from department_executors where service_department_id=?1",nativeQuery = true)
	List<DepartmentExecutors> findByDepartment(Long departmentId);

}
