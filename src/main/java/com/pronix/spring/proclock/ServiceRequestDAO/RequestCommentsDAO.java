package com.pronix.spring.proclock.ServiceRequestDAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pronix.spring.proclock.servicerequestModel.RequestComments;

public interface RequestCommentsDAO extends JpaRepository<RequestComments, Long> {

	@Query(value = "select * from request_comments where service_request_id =?1",nativeQuery = true)
	List<RequestComments> findByIdServiceRequestId(Long service_request_id);

}
