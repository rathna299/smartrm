package com.pronix.spring.proclock.ServiceRequestDAO;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.pronix.spring.proclock.servicerequestModel.RequestTracker;
import com.pronix.spring.proclock.servicerequestModel.ServiceRequest;

public interface RequestTrackerDAO extends JpaRepository<RequestTracker, Long> {

	@Transactional
	@Modifying
	@Query(value = "update request_tracker set approver_one_status =?1, approver_one_approvedon =?2, executor_id =?3, request_status=?4, status_identity=?5 where request_tracker_id =?6", nativeQuery = true)
	void updateExecutor(String approver_one_status, String date, Long executor_id, String request_status,long status_identity,Long request_tracker_id);

	@Transactional
	@Modifying
	@Query(value = "update request_tracker set approver_one_status =?1, approver_one_approvedon =?2, approver_two_id =?3, approver_two_status=?4, status_identity=?5 where request_tracker_id =?6", nativeQuery = true)
	void updateApprover(String approver_one_status, String date, Long approver_two_id, String approver_two_status,long status_identity,Long request_tracker_id);
	
	@Transactional
	@Modifying
	@Query(value = "update request_tracker set approver_two_status =?1, approver_two_approvedon =?2, executor_id =?3, request_status=?4, status_identity=?5 where request_tracker_id =?6", nativeQuery = true)
	void updateExecutorBySecondApprover(String approver_one_status, String date, Long executor_id, String request_status,long status_identity,Long request_tracker_id);
	
	@Transactional
	@Modifying
	@Query(value = "update request_tracker set approver_one_status =?1, approver_one_approvedon =?2, status_identity=?3 where request_tracker_id =?4", nativeQuery = true)
	void declineByApproverOne(String approver_one_status, String date, long status_identity,Long request_tracker_id);
	
	@Transactional
	@Modifying
	@Query(value = "update request_tracker set approver_two_status =?1, approver_two_approvedon =?2, status_identity=?3 where request_tracker_id =?4", nativeQuery = true)
	void declineByApproverTwo(String approver_one_status, String date, long status_identity,Long request_tracker_id);
	@Transactional
	@Modifying
	@Query(value = "update request_tracker set request_status=?1, executor_updatedon =?2, status_identity=?3 where request_tracker_id =?4", nativeQuery = true)
	void closedByExecutor(String request_status, String executor_updatedon, long statusIndentity, Long request_tracker_id);

	@Query(value = "select * from request_tracker where approver_one_id=?1",nativeQuery = true)
	List<RequestTracker> findbyApproverOneId(Long approverID);
	
	@Query(value = "select * from request_tracker where approver_two_id=?1",nativeQuery = true)
	List<RequestTracker> findbyApproverTwoId(Long approverID);
	
	@Query(value = "select * from request_tracker where approver_one_id=?1 and approver_one_status =?2",nativeQuery = true)
	List<RequestTracker> findByApproverOneAndStatus(Long approverID, String status);

	@Query(value = "select * from request_tracker where approver_two_id=?1 and approver_two_status =?2",nativeQuery = true)
	List<RequestTracker> findbyApproverTwoAndStatus(Long approverID, String status);

	@Query(value = "select * from request_tracker where executor_id=?1 and request_status =?2",nativeQuery = true)
	List<RequestTracker> findByExecutorAndStatus(Long executorId, String status);
	
	RequestTracker findByServiceRequest(ServiceRequest serviceRequest);
	
	@Query(value = "select * from request_tracker where service_request_id=?1 and request_status =?2",nativeQuery = true)
	Optional<RequestTracker> findbyServiceRequestAndRequestStatus(Long service_request_id,String status);
	
	@Query(value = "select count(*) from request_tracker where approver_one_id=?1 and approver_one_status =?2",nativeQuery = true)
	Long findByApproverOneAndStatusCount(Long approverID, String status);
	
	@Query(value = "select count(*) from request_tracker where approver_two_id=?1 and approver_two_status =?2",nativeQuery = true)
	Long findbyApproverTwoAndStatusCount(Long approverID, String status);
	
	@Query(value = "select count(*) from request_tracker where executor_id=?1 and request_status =?2",nativeQuery = true)
	Long findByExecutorAndStatusCount(Long executorId, String status);
	
	@Query(value = "select * from request_tracker where executor_id=?1",nativeQuery = true)
	List<RequestTracker> findbyExecutorId(Long executorId);
}
