package com.pronix.spring.proclock.ServiceRequestDAO;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pronix.spring.proclock.servicerequestModel.RequestType;

public interface RequestTypeDAO extends JpaRepository<RequestType, Long> {

	@Query(value = "select * from request_type where requesttype_id =?1 and service_department_id =?2",nativeQuery = true)
	Optional<RequestType> findByDepartmentAndRequestId(Long request_id, Long department_id);

	@Query(value = "select * from request_type where request_name =?1",nativeQuery = true)
	Optional<RequestType> findbyRequstName(String request_name);

	@Query(value = "select * from request_type where service_department_id =?1",nativeQuery = true)
	List<RequestType> findByDepartment(Long departmentId);

	
	
}
