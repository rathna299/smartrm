package com.pronix.spring.proclock.ServiceRequestDAO;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pronix.spring.proclock.servicerequestModel.ServiceDepartment;

public interface ServiceDepartmentDAO extends JpaRepository<ServiceDepartment, Long> {

	@Query(value = "select * from service_department where department_name = ?1",nativeQuery = true)
	Optional<ServiceDepartment> findByDepartmentName(String department_name);
	

}
