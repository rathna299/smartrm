package com.pronix.spring.proclock.ServiceRequestDAO;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.pronix.spring.proclock.servicerequestModel.ServiceRequest;

public interface ServiceRequestDAO extends JpaRepository<ServiceRequest, Long> {

	@Transactional
	@Modifying
	@Query(value = "update service_request set status =?1, closed_date=?2 where service_request_id=?3",nativeQuery = true)
	void updateStatus(String status, Date date, Long service_request_id);
	
	
	@Query(value = "select * from service_request where emp_id=?1",nativeQuery = true)
    List<ServiceRequest> findByEmp_id(Long emp_id);
}
