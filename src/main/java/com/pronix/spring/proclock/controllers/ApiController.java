package com.pronix.spring.proclock.controllers;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.pronix.spring.proclock.dao.UserPersonalInfoDao;
import com.pronix.spring.proclock.models.Assets;
import com.pronix.spring.proclock.models.Assets_Employees;
import com.pronix.spring.proclock.models.Company;
import com.pronix.spring.proclock.models.JobTitles;
import com.pronix.spring.proclock.models.LeaveConfiguration;
import com.pronix.spring.proclock.models.LeaveDetails;
import com.pronix.spring.proclock.models.LeaveMaster;
import com.pronix.spring.proclock.models.Logs;
import com.pronix.spring.proclock.models.Project;
import com.pronix.spring.proclock.models.RefRoles;
import com.pronix.spring.proclock.models.UserDetails;
import com.pronix.spring.proclock.models.UserPersonalInfo;
import com.pronix.spring.proclock.pojos.AssetsEmployeePojo;
import com.pronix.spring.proclock.pojos.AssetsPojo;
import com.pronix.spring.proclock.pojos.CompanyPojo;
import com.pronix.spring.proclock.pojos.DashboardPojo;
import com.pronix.spring.proclock.pojos.Employee;
import com.pronix.spring.proclock.pojos.EmployeeMultiple;
import com.pronix.spring.proclock.pojos.LeaveMasterPojo;
import com.pronix.spring.proclock.pojos.LoginDetailsPojo;
import com.pronix.spring.proclock.pojos.ProjectPojo;
import com.pronix.spring.proclock.pojos.RemoveContentPojo;
import com.pronix.spring.proclock.pojos.ResetPassword;
import com.pronix.spring.proclock.pojos.StatusRes;
import com.pronix.spring.proclock.pojos.UserDetailsPojo;
import com.pronix.spring.proclock.services.LeaveIncrementServiceImpl;
import com.pronix.spring.proclock.services.UserServices;
import com.pronix.spring.proclock.utils.Constants;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/proClock")
public class ApiController {

	private static final Logger log = LoggerFactory.getLogger(ApiController.class);

	@Autowired
	private UserServices userServices;

	@Autowired
	private UserPersonalInfoDao userPersonalInfoDao;

	@Autowired
	private LeaveIncrementServiceImpl leaveIncrementServiceImpl;

	// Admin ADD Assets in Portal
	@PostMapping("/addAssetslist")
	public Object saveAssets(@RequestBody List<AssetsPojo> assetsList) {
		return userServices.addProductslist(assetsList);
	}

	// Admin Update Assets in Portal
	@PostMapping("/updateAssets")
	public Object updateProductslist(@RequestBody AssetsPojo assets) {
		return userServices.updateProduct(assets);
	}

	// User Assets View in Portal
	@GetMapping("/viewAssets/{id}")
	public List<Assets_Employees> viewProduct(@PathVariable("id") Long userId) {
		return userServices.viewProducts(userId);
	}

	// Admin VIEW ALL Assets in Portal
	@GetMapping("/viewAssets")
	public List<Assets> getALLAssets(@RequestParam(value = "status", required = false) String status) {
		return userServices.viewListProducts(status);
	}

	// Admin Delete Assets in Portal
	@DeleteMapping("/deleteByAssets/{id}")
	public StatusRes deleteByAssets(@PathVariable("id") long assetId) {
		return userServices.deleteByAssets(assetId);
	}

	@PostMapping("/assetAssigntoUser")
	public Object saveAssetAssigntoUser(@RequestBody AssetsEmployeePojo assetsemp) {
		return userServices.assetAssigntoUser(assetsemp);
	}

	@GetMapping("/viewByEmployee/{id}")
	public Object viewEmployeeList(Long userId) {
		return userServices.viewEmployeeList(userId);
	}

	@GetMapping("/listofEmployeeView")
	public List<UserDetails> viewListofEmployee() {
		return userServices.viewListofEmployee();
	}

	@GetMapping("/viewProductTypeList")
	public List<Assets> viewProductTypeList() {
		return userServices.viewProductTypeList();
	}

	// Registration URL For the Admin to Register
	@PostMapping(value = "/register")
	public Object registerAdmin(@RequestBody UserDetailsPojo userDetailsPojo) {
		return userServices.registerUser(userDetailsPojo);
	}

	@GetMapping("/confirmEmail/{verificationToken}")
	public String confirmEmail(@PathVariable("verificationToken") String verificationToken) {
		if (userServices.confirmEmail(verificationToken) == "success") {
			return "<body>\r\n"
					+ "<table id=\"Table_01\" style=\"border: 2px solid #062f4f; margin: 10px auto; border-radius: 8px;\" width=\"621\" cellspacing=\"0\" cellpadding=\"0\">"
					+ "<tbody>" + "<tr style=\"background: #062f4f; color: white; height: 39px;\">"
					+ "<td style=\"padding: 20px 10px; font-size: 30px; height: 39px;\">SmarTRM</td>" + "</tr>"
					+ "<tr style=\"height: 36px;\">" + "<td style=\"padding: 10px 7px; height: 20px;\"> "
					+ "  <p style=\"padding: 10px 0px;height: 15px;\"> Email Confirmation Successful.</p>\r\n"
					+ "  <a href=" + Constants.hostnameUI + " style=\"background-color: #4CAF50;\r\n"
					+ "  color: white;width:100px;\r\n" + "  padding: 15px 25px;\r\n"
					+ "  text-align: center;display: inline-block;text-decoration: none;display: block;\r\n"
					+ "  margin-left: auto;\r\n" + "  margin-right: auto;\">Click to Login</a>\r\n" + "</td>" + "</tr>"
					+ "<tr style=\"height: 33px;\">"
					+ "<td style=\"padding: 20px 40px; text-align: right; height: 33px;\">Thanks and Regards<br /> SmarTRM Team</td>"
					+ "</tr>" + "</tbody>" + "</table>" + "</body>";

			// new
			// ModelAndView("redirect:"+Constants.hostnameUI+Constants.genearetePasswordUI);
		} else {
			return "Error";
		}
	}

	@RequestMapping("/getRoles")
	public List<RefRoles> getRoles() {
		return userServices.getRoles();
	}

	@PostMapping(value = "/login")
	public Object login(@RequestBody LoginDetailsPojo loginDetailsPojo, HttpServletRequest request) {
		return userServices.login(loginDetailsPojo, request);
	}

	/*
	 * @RequestMapping(method = RequestMethod.POST, value = "/test") public Object
	 * test(@RequestBody LoginDetailsPojo lotestginDetailsPojo) { return new
	 * StatusRes("Test Connected"); }
	 */

	@PostMapping(value = "/forgotPassword")
	public StatusRes forgotPassword(@RequestBody LoginDetailsPojo forgotPasswordObj) {
		return userServices.forgotPassword(forgotPasswordObj.getEmailId());
	}

	@PostMapping(value = "/resetPassword")
	public StatusRes resetPassword(@RequestBody ResetPassword resetPassword) {
		return userServices.resetPassword(resetPassword);
	}

	@PostMapping(value = "/changePassword")
	public StatusRes changePassword(@RequestBody ResetPassword changePassword) {
		return userServices.changePassword(changePassword);
	}

	@GetMapping("/viewCompanies")
	public List<Company> viewCompanies() {
		return userServices.viewCompanies();
	}

	@PostMapping(value = "/addCompany")
	public StatusRes addCompany(@RequestBody CompanyPojo company) {
		return userServices.addCompany(company);
	}

	@PostMapping(value = "/updateCompany")
	public StatusRes updateCompany(@RequestBody CompanyPojo company) {
		return userServices.updateCompany(company);
	}

	@PostMapping(value = "/addProject")
	public StatusRes addProject(@RequestBody ProjectPojo project) {
		return userServices.addProject(project);
	}

	@PostMapping(value = "/updateProject")
	public StatusRes updateProject(@RequestBody ProjectPojo project) {
		return userServices.updateProject(project);
	}

	// admin view Project in portal
	@GetMapping("/viewProject")
	public List<Project> viewProject() {
		return userServices.viewProject();
	}

	@GetMapping("/viewProjects/{id}")
	public List<Project> viewProjects(@PathVariable("id") Long companyId) {
		return userServices.viewProjects(companyId);
	}

	@PostMapping(value = "/addEmployee")
	public List<StatusRes> addEmployee(@RequestBody EmployeeMultiple employee) {
		return userServices.addEmployee(employee);
	}

	@PostMapping(value = "/updateEmployee")
	public List<StatusRes> updateEmployee(@RequestBody EmployeeMultiple employee) {
		return userServices.updateEmployee(employee);
	}

	@GetMapping("/viewEmployeesBycreatedUser/{id}")
	public List<Employee> viewEmployees(@PathVariable("id") Long userId) {
		return userServices.viewEmployees(userId);
	}

	// list of Enabled Employee
	@GetMapping("/viewEmployees")
	public List<Employee> viewEmployees(@RequestParam(value = "name", required = false) String name) {
		return userServices.viewEmployees(name);
	}

	// LEAVE MODULE
	@GetMapping("/listofEnabledEmployee")
	public List<UserDetails> listofEnabledEmployee(boolean enabled) {
		return userServices.listofEnabledEmployee(enabled);
	}

	@PostMapping("/applyLeave")
	public List<StatusRes> applyLeave(@RequestBody LeaveMasterPojo leaveMasterPojo) throws Exception {
		return leaveIncrementServiceImpl.applyLeave(leaveMasterPojo);
	}

	@PostMapping("/leaveApplicationApproval/{leaveMasterId}")
	public List<StatusRes> leaveApplicationApproval(@PathVariable Long leaveMasterId,
			@RequestParam(value = "comment", required = false, defaultValue = "") String comment) {
		return leaveIncrementServiceImpl.approveLeave(leaveMasterId, comment);
	}

	@PostMapping("/leaveApplicationDeclined/{leaveMasterId}")
	public List<StatusRes> leaveApplicationDecline(@PathVariable Long leaveMasterId,
			@RequestParam(value = "comment", required = true, defaultValue = "") String comment) {
		return leaveIncrementServiceImpl.DeclineLeave(leaveMasterId, comment);
	}

	// Admin ADD JobTitles in Portal
	@PostMapping("/addJobTitles")
	public Object saveJobTitles(@RequestBody List<JobTitles> jobTitles) {
		return userServices.addJobTitle(jobTitles);
	}

	// Admin Update JobTitles in Portal
	@PostMapping("/updateJobTitles")
	public Object updateJobTitles(@RequestBody JobTitles jobTitles) {
		return userServices.updateJobTitles(jobTitles);
	}

	// admin view JobTitles in portal
	@GetMapping("/viewJobTitles")
	public List<JobTitles> getJobTitles() {
		return userServices.viewJobTitles();
	}

	@GetMapping("LeaveDetaildByUserId/{userID}")
	public LeaveDetails getUserLeaveDetails(@PathVariable Long userID) {
		return leaveIncrementServiceImpl.LeaveDetailsById(userID);
	}

	@GetMapping("AppliedLeaveDetailsByIdAndStatus/{userId}/{status}")
	public List<LeaveMaster> userAppliedLeaves(@PathVariable Long userId, @PathVariable String status) {
		return leaveIncrementServiceImpl.UserAppliedLeaves(userId, status);
	}

	@GetMapping("leavesByStatus/{status}")
	public List<LeaveMaster> leavesByStatus(@PathVariable String status) {
		return leaveIncrementServiceImpl.leavesByStatus(status);
	}

	@PostMapping("leaveApplicationCancel/{leavemastedId}")
	public Object cancel(@PathVariable Long leavemastedId,
			@RequestParam(value = "comment", required = false, defaultValue = "") String comment) {
		return leaveIncrementServiceImpl.CancelLeave(leavemastedId, comment);
	}

	@GetMapping("ReportingManagerList/{PresentUserID}")
	public List<Employee> ReportingManagerList(@PathVariable Long PresentUserID) {
		return userServices.ReportingManagerList(PresentUserID);
	}

	@PostMapping("addDashboardContent")
	public Object addDashboardContent(@RequestBody DashboardPojo dashboardContent) {
		return userServices.addDashboardContent(dashboardContent);
	}

	@DeleteMapping("/RemoveDashboardContent")
	public Object RemoveDashboardContent(@RequestBody RemoveContentPojo contentId) {
		return userServices.removeDashboardContent(contentId);
	}

	@PostMapping("/userPersonalInfo")
	public Object userPersonalInfo(@ModelAttribute UserPersonalInfo userPersonalInfo,
			@RequestParam("imagefile") MultipartFile file) throws IOException {
		return userServices.userPersonalInfos(userPersonalInfo, file);
	}

	@GetMapping("/userPersonalInfosView/{id}")
	public Object userPersonalInfosView(@PathVariable("id") Long userId) {
		return userServices.userPersonalInfosView(userId);
	}

	@GetMapping("/userPersonalInfosDownloadImage/{fileId}")
	public ResponseEntity<ByteArrayResource> downloadFile(@PathVariable Long fileId) {
		// Load file from database
		UserPersonalInfo dbFile = userPersonalInfoDao.findByUserId(fileId);

		return ResponseEntity.ok().contentType(MediaType.parseMediaType(dbFile.getFileType()))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + dbFile.getFileName() + "\"")
				.body(new ByteArrayResource(dbFile.getData()));
	}

	@PostMapping("/assetUserReturn")
	public Object assetUserReturn(@RequestBody AssetsEmployeePojo assetsemp) {
		return userServices.assetUserReturn(assetsemp);
	}

	// view projects By employee
	@GetMapping("/viewProjectsByEmployees/{id}")
	public List<Employee> viewProjectsByEmployees(@PathVariable("id") Long userId) {
		return userServices.viewProjectsByEmployees(userId);
	}

	@GetMapping("leavesRemaining/{userId}")
	public Optional<Double> leavesRemaining(@PathVariable Long userId) {
		return leaveIncrementServiceImpl.leavesRemaining(userId);
	}

	@GetMapping("leavesUtilized/{userId}")
	public Optional<Double> leavesUtilized(@PathVariable Long userId) {
		return leaveIncrementServiceImpl.leavesUtilized(userId);
	}

	// update UserPersonal Infos in Portal
	/*
	 * @PostMapping("/updateUserPersonalInfos") public Object
	 * updateUserPersonalInfos(@RequestBody UserPersonalInfoPojo
	 * personalInfo,@PathP) { return
	 * userServices.updateUserPersonalInfos(personalInfo); }
	 */

	@PostMapping("/updateUserPersonalInfos")
	public Object updateUserPersonalInfos(@ModelAttribute UserPersonalInfo userPersonalInfo,
			@RequestParam(value = "imagefile", required = false) MultipartFile file) throws IOException {
		return userServices.updateUserPersonalInfos(userPersonalInfo, file);
	}

	// Admin user Personal InfosList in Portal
	@GetMapping("/userPersonalInfosList")
	public List<UserPersonalInfo> userPersonalInfosList() {
		return userServices.userPersonalInfosList();
	}

	@GetMapping("leaveDetailsByManager/{managerId}")
	public List<LeaveMaster> leaveDetailsByManager(@PathVariable Long managerId) {
		return leaveIncrementServiceImpl.leaveDetailsbyManager(managerId);
	}

	@GetMapping("leavesCountByManager/{managerId}")
	public Object leavesCountByManager(@PathVariable Long managerId) {
		return leaveIncrementServiceImpl.LeavesCountByManager(managerId);
	}

	@PostMapping("/addLeaveRule")
	public Object addLeaveRule(@RequestBody LeaveConfiguration configuration) {
		return leaveIncrementServiceImpl.addLeaveRule(configuration);
	}

	@PostMapping("/updateLeaveRule")
	public Object updateLeaveRule(@RequestBody LeaveConfiguration configuration) {
		return leaveIncrementServiceImpl.updateleaveRule(configuration);
	}

	@PostMapping("/deleteLeaveRule/{LeaveConfigId}")
	public Object deleteLeaveRule(@PathVariable Long LeaveConfigId) {
		return leaveIncrementServiceImpl.deleteLeaveRule(LeaveConfigId);
	}

	@GetMapping("/viewLeaveRules")
	public Object viewLeaveRules() {
		return leaveIncrementServiceImpl.viewLeaveRules();
	}

	// Admin view assigned list in Portal
	@GetMapping("/viewAssignedList")
	public List<Assets_Employees> viewassignedlist() {
		return userServices.viewassignedlist();
	}

	// Admin view assigned list in Portal
	@GetMapping("/loginLogsUser")
	public List<Logs> loginLogsUser() {
		return userServices.loginLogsUser();
	}

	@GetMapping("/approvedAndPendingLeavesByUserId/{userID}")
	public Object approvedAndPendingLeavesByUserId(@PathVariable Long userID) {
		return leaveIncrementServiceImpl.approvedAndPendingLeavesByUserId(userID);
	}
	
	@GetMapping("/listofEmployeeRM")
	public List<UserDetails> listofEmployeeRM() {
		return userServices.listofEmployeeRM();
	}
}