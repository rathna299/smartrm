package com.pronix.spring.proclock.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pronix.spring.proclock.pojos.UserTimeSheetCountPojo;
import com.pronix.spring.proclock.services.DashboardService;
import com.pronix.spring.proclock.services.LeaveIncrementServiceImpl;
import com.pronix.spring.proclock.services.UserServices;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/dashboard")
public class DashboardController {

	@Autowired
	private DashboardService dashboardService;

	@Autowired
	private LeaveIncrementServiceImpl leaveIncrementServiceImpl;

	@GetMapping("/EmplpoyeeCount")
	public Object EmployeeCount() {
		return dashboardService.EmployeeCount();
	}

	@GetMapping("/LeaveCountUserIdList/{userId}")
	public Object getByLeaveStatus(@PathVariable("userId") Long userId) {
		return dashboardService.getByLeaveStatus(userId);
	}

	@GetMapping("/LeaveCountsManagerList/{managerId}")
	public Object getByLeaveStatusManagerList(@PathVariable("managerId") Long managerId) {
		return dashboardService.getByLeaveStatusManagerList(managerId);
	}

}
