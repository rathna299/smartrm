package com.pronix.spring.proclock.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.pronix.spring.proclock.expenses.models.AddTrip;
import com.pronix.spring.proclock.expenses.models.Category;
import com.pronix.spring.proclock.expenses.models.Expenses;
import com.pronix.spring.proclock.expenses.models.PaymentMode;
import com.pronix.spring.proclock.models.Assets_Employees;
import com.pronix.spring.proclock.services.ExpensesService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/expenses")
public class ExpensesController {

	@Autowired
	private ExpensesService expensesService;

	@PostMapping("/addTripDetails")
	public Object addTripDetails(@RequestBody AddTrip addTrip) {
		return expensesService.addTripDetails(addTrip);
	}

	@PostMapping("/updateAddTrip")
	public Object updateAddTrip(@RequestBody AddTrip addTrip) {
		return expensesService.updateAddTrip(addTrip);
	}

	@GetMapping("/getTripList")
	public Object getTripList() {
		return expensesService.getTripList();
	}

	@PostMapping("/addCategory")
	public Object addCategory(@RequestBody Category category) {
		return expensesService.addCategory(category);
	}

	@PostMapping("/updateCategory")
	public Object updateCategory(@RequestBody Category category) {
		return expensesService.updateCategory(category);
	}

	@GetMapping("/getCategoryList")
	public Object getCategoryList() {
		return expensesService.getCategoryList();
	}

	@PostMapping("/addPayment")
	public Object addPayment(@RequestBody PaymentMode paymentMode) {
		return expensesService.addPayment(paymentMode);
	}

	@PostMapping("/updatePayment")
	public Object updatePayment(@RequestBody PaymentMode paymentMode) {
		return expensesService.updatePayment(paymentMode);
	}

	@GetMapping("/getPaymentList")
	public Object getPaymentList() {
		return expensesService.getPaymentList();
	}

	@PostMapping("/addExpensesDetails")
	public Object addExpensesDetails(@ModelAttribute Expenses expenses,
			@RequestParam(value = "imagefile", required = false) MultipartFile imagefile) throws IOException {
		return expensesService.addExpensesDetails(expenses, imagefile);
	}

	@PostMapping("/updateExpensesDetails")
	public Object updateExpensesDetails(@ModelAttribute Expenses expenses,
			@RequestParam(value = "imagefile", required = false) MultipartFile imagefile) throws IOException {
		return expensesService.updateExpensesDetails(expenses, imagefile);
	}

	@GetMapping("/getAllExpensesList")
	public Object getAllExpensesList() {
		return expensesService.getAllExpensesList();
	}

//	@GetMapping("/viewbyUserExpenses/{user_id}")
//	public Object viewbyUserExpenses(@PathVariable Long user_id) {
//		return expensesService.viewbyUserExpenses(user_id);
//	}

	// User Expense View
	@GetMapping("/viewbyUserExpenses/{user_id}")
	public List<Expenses> viewbyUserExpenses(@PathVariable Long user_id) {
		return expensesService.viewbyUserExpenses(user_id);
	}

//	@GetMapping("/reportingManagerExpensesView/{id}")
//	public List<Expenses> reportingManagerExpensesView(@PathVariable("id") Long userId) {
//		//System.out.println("reportingManagerTimeSheetView " + userId);
//		return expensesService.reportingManagerExpensesView(userId);
//	}

	@GetMapping("/getExpensesByManager/{reporting_manager_id}")
	public Object getExpensesByManager(@PathVariable Long reporting_manager_id) {
		return expensesService.getExpensesByManager(reporting_manager_id);
	}

	@PostMapping("/approveExpenses/{expenses_id}")
	public Object approveExpenses(@PathVariable Long expenses_id,
			@RequestParam(value = "comment", required = true) String comment) {
		return expensesService.approveExpenses(expenses_id, comment);
	}

	@PostMapping("/declineExpenses/{expenses_id}")
	public Object declineExpenses(@PathVariable Long expenses_id,
			@RequestParam(value = "comment", required = true) String comment) {
		return expensesService.declineExpenses(expenses_id, comment);
	}

	@GetMapping("/fileManager/{Id}")
	public ResponseEntity<byte[]> getfile(@PathVariable Long Id) {
		return expensesService.getFile(Id);
	}
	
	@GetMapping("/downloadZip/{expenses_id}")
	public @ResponseBody void zipOut(@PathVariable Long expenses_id, HttpServletResponse response) {
		expensesService.downloadZip(expenses_id, response);
	}

}
