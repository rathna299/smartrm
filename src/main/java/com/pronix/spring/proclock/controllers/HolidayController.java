package com.pronix.spring.proclock.controllers;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pronix.spring.proclock.models.Holidaydates;
import com.pronix.spring.proclock.models.HolidaysGroup;
import com.pronix.spring.proclock.onboard.models.Candidate_details;
import com.pronix.spring.proclock.services.HolidayService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/holidays")
public class HolidayController {

	@Autowired
	private HolidayService holidayService;

	@PostMapping("/createHolidayGroup")
	public Object createGroup(@RequestBody HolidaysGroup holidaysGroup) {
		return holidayService.CreateGroup(holidaysGroup);
	}

	@PostMapping("/updateHolidayGroup")
	public Object udpateGroup(@RequestBody HolidaysGroup holidaysGroup) {
		return holidayService.updategroup(holidaysGroup);
	}

	@GetMapping("/holidayGroups")
	public Object getAllholidayGroups() {
		return holidayService.getAllHolidayGroup();
	}

	@PostMapping("/addholiday")
	public Object AddHoliday(@RequestBody Holidaydates holidaydates) throws ParseException {
		return holidayService.addholidays(holidaydates);
	}

	@GetMapping("/getAllHolidayList")
	public Object getAllHolidayList() {
		return holidayService.getAllHolidayList();
	}

	@GetMapping("/holidayList")
	public Object getHolidayList(@RequestParam(value = "year", required = false) String year,
			@RequestParam(value = "group_id", required = false) Long group_id) {
		return holidayService.getHolidayList(year, group_id);
	}

	@GetMapping("/holidayListForEmployee/{businessunit_id}/{year}")
	public Object holidayListForEmployee(@PathVariable Long businessunit_id, @PathVariable String year) {
		return holidayService.getHolidayListForEmployee(businessunit_id, year);
	}

	@PostMapping("/mappingHolidayGroupAndBusinessUnit/{HolidayGroupId}/{businessUnitId}")
	public Object mappingHolidayGroupAndBusinessUnit(@PathVariable Long HolidayGroupId,
			@PathVariable Long businessUnitId) {
		return holidayService.mappingHolidayGroupToBusinessUnit(HolidayGroupId, businessUnitId);
	}

	@PostMapping("mappingUpdateHolidayGroupAndBusinessUnit/{hbId}/{HolidayGroupId}/{businessUnitId}")
	public Object updateHolidayGroupAndBusinessUnit(@PathVariable Long hbId, @PathVariable Long HolidayGroupId,
			@PathVariable Long businessUnitId) {
		return holidayService.updateHolidayGroupAndBusinessUnit(hbId, HolidayGroupId, businessUnitId);
	}

	@GetMapping("/mappingList")
	public Object mappingHolidayGroupToBusinessUnitList() {
		return holidayService.mappingHolidayGroupToBusinessUnitList();
	}
	
	@PostMapping("/updateholidays")
	public Object updateholidays(@RequestBody Holidaydates holidaydates) {
		return holidayService.updateholidays(holidaydates);
	}

}
