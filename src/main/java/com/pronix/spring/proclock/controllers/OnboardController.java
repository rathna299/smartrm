package com.pronix.spring.proclock.controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.pronix.spring.proclock.onboard.models.CandidateLoginPojo;
import com.pronix.spring.proclock.onboard.models.Candidate_details;
import com.pronix.spring.proclock.onboard.models.Candidate_education_details;
import com.pronix.spring.proclock.onboard.models.Candidate_job_details;
import com.pronix.spring.proclock.onboard.models.Candidate_personal_details;
import com.pronix.spring.proclock.onboard.models.OfferConfirmPojo;
import com.pronix.spring.proclock.services.OnboardService;
import com.pronix.spring.proclock.services.ReportService;

import net.sf.jasperreports.engine.JRException;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/onboard")
public class OnboardController {

	@Autowired
	private OnboardService onboardService;

	@Autowired
	private ReportService reportService;

	@PostMapping("/addCandidate")
	public Object addCandidate(@RequestBody Candidate_details candidate_details) {
		return onboardService.addCandidateDetails(candidate_details);
	}

	@PostMapping("/updateCandidate")
	public Object editCandidate(@RequestBody Candidate_details candidate_details) {
		return onboardService.updateCandidate(candidate_details);
	}

	@GetMapping("/viewCandidateList")
	public Object viewCandidateList(@RequestParam(value = "status", required = false) String status) {
		return onboardService.viewCandidateLists(status);
	}

	@PostMapping("/candidateLogin")
	public Object candidateLogin(@RequestBody CandidateLoginPojo candidateLoginPojo) {
		return onboardService.candidatelogin(candidateLoginPojo);
	}

	@PostMapping("/addPersonalDetails")
	public Object addPersonalDetails(@ModelAttribute Candidate_personal_details candidate_personal_details,
			@RequestParam(value = "aadhar", required = true) MultipartFile aadhar,
			@RequestParam(value = "pancard", required = true) MultipartFile pancard,
			@RequestParam(value = "passport", required = false) MultipartFile passport) throws IOException {
		return onboardService.addPersonal(candidate_personal_details, aadhar, pancard, passport);
	}

	@PostMapping("/updatePersonalDetails")
	public Object updatePersonalDetails(@ModelAttribute Candidate_personal_details candidate_personal_details,
			@RequestParam(value = "aadhar", required = false) MultipartFile aadhar,
			@RequestParam(value = "pancard", required = false) MultipartFile pancard,
			@RequestParam(value = "passport", required = false) MultipartFile passport) throws IOException {
		return onboardService.updatePersonal(candidate_personal_details, aadhar, pancard, passport);
	}

	@GetMapping("/viewPersonalDetails/{candidate_Id}")
	public Object viewPersonalDetails(@PathVariable Long candidate_Id) {
		return onboardService.viewPersonalDetails(candidate_Id);
	}

	@PostMapping("/addEducationDetails")
	public Object addEducationDetails(@ModelAttribute Candidate_education_details candidate_education_details,
			@RequestParam(value = "marksheet", required = true) MultipartFile marksheet,
			@RequestParam(value = "certificate", required = false) MultipartFile certificate) throws IOException {
		return onboardService.addEducation(candidate_education_details, marksheet, certificate);
	}

	@PostMapping("/updateEducationDetails")
	public Object updateEducationDetails(Candidate_education_details candidate_education_details,
			@RequestParam(value = "marksheet", required = false) MultipartFile marksheet,
			@RequestParam(value = "certificate", required = false) MultipartFile certificate) throws IOException {
		return onboardService.updateEducation(candidate_education_details, marksheet, certificate);
	}

	@GetMapping("/viewEducationDetails/{candidate_Id}")
	public Object viewEducationDetails(@PathVariable Long candidate_Id) {
		return onboardService.viewEducationDetailsList(candidate_Id);
	}

	@PostMapping("/addJobDetails")
	public Object addJobDetails(@ModelAttribute Candidate_job_details candidate_job_details,
			@RequestParam(value = "offerletter", required = true) MultipartFile offerletter,
			@RequestParam(value = "relivelingletter", required = true) MultipartFile relievingletter,
			@RequestParam(value = "payslip_one", required = false) MultipartFile payslip_one,
			@RequestParam(value = "payslip_two", required = false) MultipartFile payslip_two,
			@RequestParam(value = "payslip_three", required = false) MultipartFile payslip_three) throws IOException {
		return onboardService.addJobDetails(candidate_job_details, offerletter, relievingletter, payslip_one,
				payslip_two, payslip_three);
	}

	@PostMapping("/updateJobdetails")
	public Object updateJobdetails(@ModelAttribute Candidate_job_details candidate_job_details,
			@RequestParam(value = "offerletter", required = false) MultipartFile offerletter,
			@RequestParam(value = "relivelingletter", required = false) MultipartFile relievingletter,
			@RequestParam(value = "payslip_one", required = false) MultipartFile payslip_one,
			@RequestParam(value = "payslip_two", required = false) MultipartFile payslip_two,
			@RequestParam(value = "payslip_three", required = false) MultipartFile payslip_three) throws IOException {
		return onboardService.updatejob(candidate_job_details, offerletter, relievingletter, payslip_one, payslip_two,
				payslip_three);
	}

	@GetMapping("/viewJobdetails/{candidate_Id}")
	public Object viewJobdetails(@PathVariable Long candidate_Id) {
		return onboardService.viewJobDetailslist(candidate_Id);
	}

	@GetMapping("/fileManager/{file}/{pId}")
	public ResponseEntity<byte[]> getfile(@PathVariable Long pId, @PathVariable String file) {
		return onboardService.getFile(pId, file);
	}

	@PostMapping("/submitApplication/{candidate_Id}")
	public Object submitApplication(@PathVariable Long candidate_Id) {
		return onboardService.submitApplication(candidate_Id);
	}

	@PostMapping("/confirmOffer")
	public Object offerConfirm(@RequestBody OfferConfirmPojo confirmPojo) {
		return onboardService.approveOffer(confirmPojo);
	}

	@PostMapping("/reminder/{candidate_Id}")
	public Object sendReminders(Long candidate_Id) {
		return onboardService.reminder(candidate_Id);
	}

	@PostMapping("/offerDecline/{candidate_Id}")
	public Object declineOffer(@PathVariable Long candidate_Id,
			@RequestParam(value = "comment", required = true) String comment) {
		return onboardService.decline(candidate_Id, comment);
	}

	@PostMapping("/resubmissionRequest/{candidate_Id}")
	public Object resubmittRequest(@PathVariable Long candidate_Id,
			@RequestParam(value = "comment", required = true) String comment) {
		return onboardService.resubmission(candidate_Id, comment);
	}

	@GetMapping("/downloadZip/{candidate_Id}")
	public @ResponseBody void zipOut(@PathVariable Long candidate_Id, HttpServletResponse response) {
		onboardService.downloadZip(candidate_Id, response);
	}

	@GetMapping("/onbordcount")
	public Object onboarCount() {
		return onboardService.OnbordCount();
	}

	@GetMapping("/offerletter/{id}")
	public @ResponseBody void generateRmTimesheetReport(@PathVariable Long id, HttpServletResponse response)
			throws JRException, IOException {
		onboardService.generateOfferletter(id, response);
	}

}
