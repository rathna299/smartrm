package com.pronix.spring.proclock.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.pronix.spring.proclock.pojos.AssetReport;
import com.pronix.spring.proclock.pojos.EmployeeReport;
import com.pronix.spring.proclock.services.ReportService;

import net.sf.jasperreports.engine.JRException;

@RestController
@RequestMapping("/reports")
public class ReportController {

	@Autowired
	private ReportService reportService;

	@GetMapping("/employees/{enable}")
	public List<EmployeeReport> EmployeeReportlist(@PathVariable boolean enable) {
		return reportService.EmployeeReport(enable);
	}

	@GetMapping("/employees/{enable}/{reportFormat}")
	public @ResponseBody void generateEmployee(@PathVariable boolean enable, @PathVariable String reportFormat,
			HttpServletResponse response) throws JRException, IOException {
		reportService.generateEmployeeReport(enable, reportFormat, response);
	}

	@GetMapping("/leaves/{fromdate}/{todate}/{status}")
	public Object leaveReport(@PathVariable String fromdate, @PathVariable String todate,
			@PathVariable String status) {
		return reportService.Leavelist(fromdate, todate, status);
	}

	@GetMapping("/leaves/{fromdate}/{todate}/{status}/{reportFormat}")
	public @ResponseBody void generateLeavesReport(@PathVariable String fromdate, @PathVariable String todate,
			@PathVariable String status, @PathVariable String reportFormat, HttpServletResponse response)
			throws JRException, IOException {
		reportService.generateleavereport(fromdate, todate, status, reportFormat, response);

	}

	@GetMapping("/assetreport")
	public List<AssetReport> assetrport() {
		return reportService.assetreport();
	}

	@GetMapping("/assetreport/{reportFormat}")
	public @ResponseBody void generateAssetreport(@PathVariable String reportFormat, HttpServletResponse response)
			throws JRException, IOException {
		reportService.generateAssetreport(reportFormat, response);
	}

	//emp leave list
	@GetMapping("/userleaveslist/{fromdate}/{todate}/{status}/{userid}")
	public Object userLeavelist(@PathVariable String fromdate, @PathVariable String todate,
			@PathVariable String status, @PathVariable Long userid) {
		return reportService.userLeavelist(fromdate, todate, status, userid);
	}
	
	//emp leave report
	@GetMapping("/userleavesreport/{fromdate}/{todate}/{status}/{userid}/{reportFormat}")
	public @ResponseBody void generateuserLeavesReport(@PathVariable String fromdate, @PathVariable String todate,
			@PathVariable String status,@PathVariable Long userid, @PathVariable String reportFormat, HttpServletResponse response)
			throws JRException, IOException {
		reportService.generateuserleavereport(fromdate, todate, status,userid, reportFormat, response);
	}
	
	//rm leave list
	@GetMapping("/rmLeavelist/{fromdate}/{todate}/{status}/{userid}")
	public Object rmLeavelist(@PathVariable String fromdate, @PathVariable String todate,
			@PathVariable String status, @PathVariable Long userid) {
		return reportService.rmLeavelist(fromdate, todate, status, userid);
	}
	
	//rm leave report
	@GetMapping("/rmleavesreport/{fromdate}/{todate}/{status}/{userid}/{reportFormat}")
	public @ResponseBody void generateRmleavereport(@PathVariable String fromdate, @PathVariable String todate,
			@PathVariable String status,@PathVariable Long userid, @PathVariable String reportFormat, HttpServletResponse response)
			throws JRException, IOException {
		reportService.generateRmleavereport(fromdate, todate, status,userid, reportFormat, response);
	}
	
}
