package com.pronix.spring.proclock.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pronix.spring.proclock.models.BusinessUnits;
import com.pronix.spring.proclock.models.Department;
import com.pronix.spring.proclock.models.Modules;
import com.pronix.spring.proclock.models.Project_Employees;
import com.pronix.spring.proclock.models.RefRoles;
import com.pronix.spring.proclock.models.UserRoles;
import com.pronix.spring.proclock.pojos.AssetsPojo;
import com.pronix.spring.proclock.pojos.BusinessUnitsPojo;
import com.pronix.spring.proclock.pojos.DepartmentPojo;
import com.pronix.spring.proclock.pojos.EmpProjectPojo;
import com.pronix.spring.proclock.pojos.ProjectAssignmentEDPojo;
import com.pronix.spring.proclock.pojos.RoleModulePojo;
import com.pronix.spring.proclock.pojos.UpdateRoleAndModules;
import com.pronix.spring.proclock.pojos.UserRoleViewPojo;
import com.pronix.spring.proclock.services.RolesService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/proClock")
public class RolesController {

	//private static final Logger log = LoggerFactory.getLogger(RolesController.class);
	@Autowired
	private RolesService rolesService;

	//adding role and assigning modules
	@PostMapping("/addRolesAssignModule")
	public Object addRolesAssignModule(@RequestBody RoleModulePojo roleModulePojo) {
		return rolesService.AddingRoles(roleModulePojo);
	}
	@GetMapping("/roleAndModulelist/{role_id}")
	public Object roleAndModulelist(@PathVariable Long role_id) {
		return rolesService.getRoleModule(role_id);
	}
	@PostMapping("/updateRoleModules")
	public Object updateRoleModules(@RequestBody UpdateRoleAndModules updateRoleAndModules) {
		return rolesService.updateRoleAndModule(updateRoleAndModules);
	}

	@GetMapping("/userRolesView")
	public List<UserRoles> viewUserRoles() {
		return rolesService.viewUserRoles();
	}


	@GetMapping("/userRolesConfig")
	public UserRoleViewPojo userRolesConfig() {
		return rolesService.userRolesConfig();
	}

	//admin view roles in portal
	@GetMapping("/viewRolesList")
	public List<RefRoles> getRefRoles() {
		return rolesService.viewRolesList();
	}

	// Admin ADD Modules in Portal
	@PostMapping("/addModuleslist")
	public Object saveModules(@RequestBody List<Modules> modules) {
		return rolesService.addModules(modules);
	}

	//admin view Modules in portal
	@GetMapping("/viewModuleList")
	public List<Modules> getModule() {
		return rolesService.viewModuleList();
	}

	// Admin Update Modules in Portal
	@PostMapping("/updateModules")
	public Object updateModules(@RequestBody Modules modules) {
		return rolesService.updateModule(modules);
	}


	// Admin ADD BusinessUnits in Portal
	@PostMapping("/addBusinessUnits")
	public Object addBusinessUnits(@RequestBody List<BusinessUnitsPojo> businessUnitsPojo) {
		return rolesService.addBusinessUnits(businessUnitsPojo);
	}

	// Admin Update BusinessUnits  in Portal
	@PostMapping("/updateBusinessUnits")
	public Object updateBusinessUnits(@RequestBody BusinessUnitsPojo businessUnitsPojo) {
		return rolesService.updateBusinessUnits(businessUnitsPojo);
	}

	//admin view BusinessUnits in portal
	@GetMapping("/viewBusinessUnits")
	public List<BusinessUnits> getBusinessUnits() {
		return rolesService.viewBusinessUnit();
	}

	@PostMapping("/addDepartment")
	public Object addDepartment(@RequestBody List<DepartmentPojo> departmentPojo) {
		return rolesService.addDepartment(departmentPojo);
	}

	@PostMapping("/updateDepartment")
	public Object updateDepartment(@RequestBody DepartmentPojo departmentPojo) {
		return rolesService.updateDepartment(departmentPojo);
	}

	//admin view Dartments in portal
	@GetMapping("/viewDepartments")
	public List<Department> getDepartments() {
		return rolesService.viewDepartments();
	}

	@GetMapping("/viewBusinessUnits/{companyId}")
	public List<BusinessUnits> viewBusinessUnits(@PathVariable("companyId") Long companyId) {
		return rolesService.viewBusinessUnits(companyId);
	}

	@GetMapping("/viewDepartment/{businessUnitsId}")
	public List<Department> viewDepartment(@PathVariable("businessUnitsId") Long businessUnitsId) {
		return rolesService.viewDepartment(businessUnitsId);
	}

	@PostMapping("/assignEmpoyeeToProject")
	public Object assignEmployeeToProject(@RequestBody EmpProjectPojo empPojo) {
		return rolesService.assignEmployeeToProject(empPojo);
	}

	@PostMapping("/employeeProjectED/{pId}")
	public Object employeeProjectED(@PathVariable long pId,@RequestBody boolean status) {
		return rolesService.employeeProjectED(pId, status);
	}
	
	
	@GetMapping("/assignEmployeeToProjectView")
	public List<Project_Employees> ViewAssignEmployeeToProject() {
		return rolesService.assignEmployeeToProjectView();
	}
	
	@PostMapping("/employeeEDProject")
	public Object employeeEDProject(@RequestBody ProjectAssignmentEDPojo paedpojo) {
		return rolesService.employeeEDProject(paedpojo);
	}

}
