package com.pronix.spring.proclock.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pronix.spring.proclock.servicerequestModel.RequestType;
import com.pronix.spring.proclock.servicerequestModel.ServiceDepartment;
import com.pronix.spring.proclock.servicerequestModel.TicketApprovalOnePojo;
import com.pronix.spring.proclock.servicerequestModel.TicketPojo;
import com.pronix.spring.proclock.services.ServiceRequestServices;


@RestController
@CrossOrigin
@RequestMapping("/ticket")
public class ServiceRequestController {

	@Autowired
	private ServiceRequestServices serviceRequestServices;

	@PostMapping("/createTicket")
	public Object raiseARequest(@RequestBody TicketPojo ticket) {
		return serviceRequestServices.raiseARequest(ticket);
	}
	@PostMapping("/addServiceDepartment")
	public Object addServiceDepartment(@RequestBody ServiceDepartment department) {
		return serviceRequestServices.addServiceDepartment(department);
	}
	@PostMapping("/updateServiceDepartment")
	public Object updateServiceDepartment(@RequestBody ServiceDepartment department) {
		return serviceRequestServices.updateServiceDepartment(department);
	}
	@PostMapping("/addServiceRequestType")
	public Object addServiceRequestType(@RequestBody RequestType requestType) {
		return serviceRequestServices.addRequestType(requestType);
	}
	
	@PostMapping("/mapApproversToDepartment/{approver_id}/{department_id}")
	public Object mapApproversToDepartment(@PathVariable Long approver_id,@PathVariable Long department_id) {
		return serviceRequestServices.mapApprovers(approver_id, department_id);
	}
	@PostMapping("/mapExecutorToDepartment/{executor_id}/{department_id}")
	public Object mapExecutorToDepartment(@PathVariable Long executor_id,@PathVariable Long department_id) {
		return serviceRequestServices.mapExecutors(executor_id, department_id);
	}
	@PostMapping("/approveTicket")
	public Object approveTicket(@RequestBody TicketApprovalOnePojo approvalOne) {
		return serviceRequestServices.approveTicket(approvalOne);
	}
	@PostMapping("/declineTicket/{trackingId}/{comment}")
	public Object declineTicket(@PathVariable Long trackingId,@PathVariable String comment) {
		return serviceRequestServices.declineRequest(trackingId, comment);
	}
	@PostMapping("/closeticket/{ticketTrackingId}/{comment}")
	public Object CloseTicketByExecutor(@PathVariable Long ticketTrackingId,@PathVariable String comment) {
		return serviceRequestServices.CloseTicketByExecutor(ticketTrackingId, comment);
	}
	@GetMapping("/ticketListByApprover/{approverID}/{status}")
	public Object ticketList(@PathVariable Long approverID,@PathVariable String status) {
		return serviceRequestServices.ticketslistByApprover(approverID, status);
	}
	@GetMapping("/getDepartments")
	public Object getDepartments() {
		return serviceRequestServices.getServiceDepartments();
	}
	@GetMapping("/getRequestTypes/{departmentId}")
	public Object getRequestTypes(@PathVariable Long departmentId) {
		return serviceRequestServices.getRequestTypes(departmentId);
	}
	@GetMapping("/getRequestTypeList")
	public Object getRequestTypeList() {
		return serviceRequestServices.getRequestTypeList();
	}
	@PostMapping("/updateServiceRequestType")
	public Object updateRequestType(@RequestBody RequestType requestType){
		return serviceRequestServices.updateRequestType(requestType);
	}
	@GetMapping("/getApprovers/{departmentId}")
	public Object getApprovers(@PathVariable Long departmentId) {
		return serviceRequestServices.getApprovers(departmentId);
	}
	@GetMapping("/getExecutors/{departmentId}")
	public Object getExecutor(@PathVariable Long departmentId) {
		return serviceRequestServices.getExecutors(departmentId);
	}
	@GetMapping("/ticketListByExecutor/{executorId}/{status}")
	public Object ticketListByExecutor(@PathVariable Long executorId,@PathVariable String status) {
		return serviceRequestServices.ticketListByExecutors(executorId, status);
	}
	@GetMapping("/ticketlist/{empId}")
	public Object ticketList(@PathVariable Long empId) {
		return serviceRequestServices.ticketList(empId);
	}
	@GetMapping("/ticketCount/{empId}")
	public Object ticketCount(@PathVariable Long empId) {
		return serviceRequestServices.ticketCount(empId);
	}
	@GetMapping("/ticketCountByApprover/{ApproverId}")
	public Object ticketCountByApprover(@PathVariable Long ApproverId) {
		return serviceRequestServices.tickectCountByApprover(ApproverId);
	}
	@GetMapping("/ticketCountByExecutor/{executorId}")
	public Object ticketCountByExecutor(@PathVariable Long executorId) {
		return serviceRequestServices.ticketCountByExecutor(executorId);
	}
	@GetMapping("/ExecutorList")
	public Object executorList() {
		return serviceRequestServices.executorList();
	}
	@GetMapping("/ApproverList")
	public Object approverList() {
		return serviceRequestServices.ApproverList();
	}
}




















