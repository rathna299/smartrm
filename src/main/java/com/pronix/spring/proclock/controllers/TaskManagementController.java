package com.pronix.spring.proclock.controllers;

import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.pronix.spring.proclock.pojos.TaskDetailTrackerPojo;
import com.pronix.spring.proclock.pojos.TaskDetailsPojo;
import com.pronix.spring.proclock.services.TaskManagementService;
import com.pronix.spring.proclock.taskmanagement.models.TaskDetails;
import com.pronix.spring.proclock.taskmanagement.models.TaskStatus;
import com.pronix.spring.proclock.taskmanagement.models.TaskTrackerPojo;
import com.pronix.spring.proclock.taskmanagement.models.TaskType;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/taskmanagement")
public class TaskManagementController {

	@Autowired
	private TaskManagementService taskManagementService;

	@PostMapping("/addTaskType")
	public Object addTaskType(@RequestBody TaskType taskType) {
		return taskManagementService.addTaskType(taskType);
	}

	@PostMapping("/updateTaskType")
	public Object updateTaskType(@RequestBody TaskType taskType) {
		return taskManagementService.updateTaskType(taskType);
	}

	@GetMapping("/getTasktypeList")
	public Object getTasktypeList() {
		return taskManagementService.getTasktypeList();
	}

	@PostMapping("/addTaskStatus")
	public Object addTaskStatus(@RequestBody TaskStatus taskstatus) {
		return taskManagementService.addTaskStatus(taskstatus);
	}

	@PostMapping("/updateTaskStatus")
	public Object updateTaskStatus(@RequestBody TaskStatus taskstatus) {
		return taskManagementService.updateTaskStatus(taskstatus);
	}

	@GetMapping("/getTaskStatusList")
	public Object getTaskStatusList() {
		return taskManagementService.getTaskStatusList();
	}

	@GetMapping("/getTaskStatusLists")
	public Object getTaskStatusLists() {
		return taskManagementService.getTaskStatusLists();
	}

	@PostMapping("/addTaskDetails")
	public Object addTaskDetails(@ModelAttribute TaskDetailsPojo taskDetails, MultipartHttpServletRequest imagefile)
			throws IOException {
		return taskManagementService.addTaskDetails(taskDetails, imagefile);
	}

	@PostMapping("/updateTaskDetails")
	public Object updateTaskDetails(@ModelAttribute TaskDetailsPojo taskDetails, MultipartHttpServletRequest imagefile)
			throws IOException {
		return taskManagementService.updateTaskDetails(taskDetails, imagefile);
	}

	@PostMapping("/updateTaskDetailsTracker")
	public Object updateTaskDetailsTracker(@ModelAttribute TaskTrackerPojo taskDetailsTracker,
			MultipartHttpServletRequest imagefile) throws IOException {
		return taskManagementService.updateTaskDetailsTracker(taskDetailsTracker, imagefile);
	}

	@PostMapping("/resolvedTaskDetailsTracker")
	public Object resolvedTaskDetailsTracker(@ModelAttribute TaskDetailTrackerPojo taskDetailsTracker,
			MultipartHttpServletRequest imagefile) throws IOException {
		return taskManagementService.resolvedTaskDetailsTracker(taskDetailsTracker, imagefile);
	}

	@GetMapping("/taskDetailsList")
	public Object taskDetailsList() {
		return taskManagementService.taskDetailsList();
	}

	// User Tasks View
	@GetMapping("/viewbyUserTasks/{assigned_to}")
	public List<TaskDetails> viewbyUserTasks(@PathVariable Long assigned_to) {
		return taskManagementService.viewbyUserTasks(assigned_to);
	}

	@GetMapping("/taskDetailsTrackerList")
	public Object taskDetailsTrackerList() {
		return taskManagementService.taskDetailsTrackerList();
	}

	@GetMapping("/taskDetailsByID/{taskID}")
	public Object taskDetailsByID(@PathVariable Long taskID) {
		return taskManagementService.taskDetailsByID(taskID);
	}

	@GetMapping("/taskDetailsTrackerByID/{taskDetailsTrackerID}")
	public Object taskDetailsTrackerByID(@PathVariable Long taskDetailsTrackerID) {
		return taskManagementService.taskDetailsTrackerByID(taskDetailsTrackerID);
	}

	@GetMapping("/getTaskDetailsFile/{Id}")
	public ResponseEntity<byte[]> getTaskDetailsFile(@PathVariable Long Id) {
		return taskManagementService.getTaskDetailsFile(Id);
	}

	@GetMapping("/getTaskDetailsTrackerFile/{Id}")
	public ResponseEntity<byte[]> getTaskDetailsTrackerFile(@PathVariable Long Id) {
		return taskManagementService.getTaskDetailsTrackerFile(Id);
	}

	@GetMapping("/downloadZip/{taskdetailsfileId}")
	public @ResponseBody void zipOut(@PathVariable Long taskdetailsfileId, HttpServletResponse response) {
		taskManagementService.downloadZip(taskdetailsfileId, response);
	}

//	@GetMapping("/taskDetailsTrackerByIDs/{taskDetailsID}")
//	public Object taskDetailsTrackerByIDs(@PathVariable Long taskDetailsID) {
//		return taskManagementService.taskDetailsTrackerByIDs(taskDetailsID);
//	}
//	
//	@GetMapping("/taskDetailsByIDs/{taskID}")
//	public Object taskDetailsByIDs(@PathVariable Long taskID) {
//		return taskManagementService.taskDetailsByIDs(taskID);
//	}

	@DeleteMapping("/deleteByFiles/{id}")
	public Object deleteByFiles(@PathVariable("id") Long taskdetailsfileId) {
		return taskManagementService.deleteByFiles(taskdetailsfileId);
	}

	@DeleteMapping("/deleteByTaskFiles/{id}")
	public Object deleteByTaskFiles(@PathVariable("id") List<Long> taskdetailsfileId) {
		return taskManagementService.deleteByTaskFiles(taskdetailsfileId);
	}
}
