package com.pronix.spring.proclock.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pronix.spring.proclock.services.TimeSheetServices;
import com.pronix.spring.proclock.timesheet.models.Timesheet_details;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/timesheet")
public class TimeSheetController {

	@Autowired
	private TimeSheetServices timeSheetServices;

	@PostMapping("/addDayTimesheet")
	public Object insertDayTimesheet(@RequestBody List<Timesheet_details> timesheet_details) {
		return timeSheetServices.insertDayTimesheet(timesheet_details);
	}

	@PostMapping("/updateTimesheet")
	public Object updateTimesheet(@RequestBody List<Timesheet_details> timesheet_details) {
		return timeSheetServices.updateDayTimeSheet(timesheet_details);
	}

	@GetMapping("/getAllUsersTimesheetlist/{year}/{month}")
	public Object getAllTimesheetlist(@PathVariable long year, @PathVariable long month,
			@RequestParam(value = "status", required = true) String status) {
		return timeSheetServices.timeSheetList(status, year, month);
	}

	@GetMapping("/getTimesheetByManager/{reporting_manager_id}/{year}/{month}")
	public Object getTimesheetByManager(@PathVariable Long reporting_manager_id, @PathVariable long year,
			@PathVariable long month, @RequestParam(value = "status", required = true) String status) {
		return timeSheetServices.getTimeSheetByManager(reporting_manager_id, status, year, month);
	}

	@GetMapping("/getMonthTimeSheetByUser/{user_id}/{year}/{month}")
	public Object getMonthTimeSheetByUser(@PathVariable Long user_id, @PathVariable long year, @PathVariable long month,
			@RequestParam(value = "status", required = false) String status) {
		return timeSheetServices.getMonthTimeSheetByUser(user_id, status, year, month);
	}

	@GetMapping("/getWeekTimeSheetByUser/{user_id}/{year}/{month}/{week}")
	public Object getWeekTimeSheetByUser(@PathVariable Long user_id, @PathVariable long year, @PathVariable long month,
			@PathVariable long week) {
		return timeSheetServices.getWeekTimeSheetByUser(user_id, year, month, week);
	}

	@GetMapping("getTimesheetCountByManager/{reporting_manager_id}/{year}/{month}")
	public Object getTimeSheetCountByManager(@PathVariable Long reporting_manager_id, @PathVariable Long year,
			@PathVariable Long month) {
		return timeSheetServices.getCountByReportingManager(reporting_manager_id, year, month);
	}

	@GetMapping("getTimesheetCountByAllUsers/{year}/{month}")
	public Object getTimeSheetCountByAllUsers(@PathVariable Long year, @PathVariable Long month) {
		return timeSheetServices.getCountByAllUsers(year, month);
	}

}
