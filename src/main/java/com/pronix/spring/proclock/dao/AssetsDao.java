package com.pronix.spring.proclock.dao;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.pronix.spring.proclock.models.Assets;
import com.pronix.spring.proclock.models.UserDetails;

public interface AssetsDao extends JpaRepository<Assets, Long> {

	@Transactional
	@Modifying
	void deleteByProductId(String productId);	

	Assets findByassetId(long assetId);

	Assets findByproductId(String productId);

	public static final String findByUserId = "Select * from assets_list where user_id = ?1";

	@Query(value = findByUserId, nativeQuery = true)
	List<Assets> findProductsbyUserId(Long userId);

	Assets findByProductTypeAndProductNumberAndProductCreateAndProductDescAndProductIdAndProductModelAndProductMakeAndProductModifyDateAndUserDetails(
			String productType, String productNumber, Date productCreate, String productDesc, String productId,
			String productModel, String productMake, Date productModifyDate, UserDetails userDetails);

	Assets findByProductTypeAndProductNumberAndProductCreateAndProductDescAndProductIdAndProductModelAndProductMakeAndProductModifyDate(
			String productType, String productNumber, Date productCreate, String productDesc, String productId,
			String productModel, String productMake, Date productModifyDate);

	Assets findByProductNumberAndProductId(String productNumber, String productId);

	Assets findByProductNumber(String productNumber);

	Assets findByProductId(String productId);

	//List<Assets> findByAssignToUser(long assignToUser);

	@Transactional
	@Modifying
	@Query(value = "update assets_list set product_id=?3,product_number=?2,product_desc=?4 where asset_id=?1", nativeQuery = true)
	void updateAssets(Long id, String productNumber, String productId, String productDesc);

	// assign To User List
	@Query(value = "	SELECT al.*,ud.first_name,ud.last_name FROM user_details ud\r\n"
			+ "	inner JOIN assets_list al ON al.assign_to_user = ud.user_id where assign_to_user=?1", nativeQuery = true)
	public List<Assets> findByAssignToUser(Long assignToUser);

}
