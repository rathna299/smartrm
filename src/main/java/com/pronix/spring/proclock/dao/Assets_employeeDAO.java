package com.pronix.spring.proclock.dao;

import java.util.List;

import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import com.pronix.spring.proclock.models.Assets_Employees;
import com.pronix.spring.proclock.models.UserDetails;

public interface Assets_employeeDAO extends JpaRepository<Assets_Employees, Long> {
	
	public static final String findByUserId = "Select * from assets_employees where user_id = ?1";
	@Query(value = findByUserId, nativeQuery = true)
	List<Assets_Employees> findByUserid(Long userId);
	
	//List<Assets_Employees> findByAssignToUser(Long assignToUser);
		
	@Query(value ="select * from assets_employees where asset_id=?1" , nativeQuery = true)
	List<Assets_Employees> findByAssets(long assets);
}
