package com.pronix.spring.proclock.dao;


import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.pronix.spring.proclock.models.BusinessUnits;
import com.pronix.spring.proclock.models.Company;

public interface BusinessUnitsDAO  extends CrudRepository<BusinessUnits, Long>{

	BusinessUnits findByBusinessNameAndBusinessCodeAndBusinessStartDateAndCountryAndCityAndCompany(String businessName,String businessCode, String businessStartDate, String country, String state, Company company);
		
	BusinessUnits findByBusinessCode(String businessCode);
	
	BusinessUnits findByBusinessName(String businessName);
	
	BusinessUnits findByBusinessCodeAndBusinessName(String businessCode, String businessName);
	
	BusinessUnits findByBusinessCodeAndBusinessUnitsId(String businessCode,Long businessUnitsId );
	
	List<BusinessUnits> findBusinessUnitsByCompany(Company company);
		
	@Transactional
	@Modifying
	@Query(value = "update business_units set business_name=?3,business_code=?2 where business_units_id=?1",nativeQuery = true)
	void updateBusinessUnits(Long id,String businessCode,String businessName);
		
	@Transactional
	@Modifying
	@Query(value = "update business_units set business_name=?2 where business_units_id=?1",nativeQuery = true)
	void updateBusinessUnit(Long id,String businessName);
	
	@Transactional
	@Modifying
	@Query(value = "update business_units set business_name=?3,business_code=?2,company_id=?4 where business_units_id=?1",nativeQuery = true)
	void updateBusinessUnit(Long id,String businessCode,String businessName,Long company);
	
	
	@Transactional
	@Modifying
	@Query(value = "update business_units set business_name=?3,business_code=?2,company_id=?4,business_start_date=?5,country=?6,state=?7,city=?8 where business_units_id=?1",nativeQuery = true)
	void updateBusinessUnits(Long id,String businessCode,String businessName,Long company,String businessStartDate,String country,String state,String city);
	
	
}
