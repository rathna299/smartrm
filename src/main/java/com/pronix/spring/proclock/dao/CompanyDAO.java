package com.pronix.spring.proclock.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.google.type.Date;
import com.pronix.spring.proclock.models.Company;
import com.pronix.spring.proclock.models.UserDetails;

public interface CompanyDAO extends CrudRepository<Company, Long>{

	public static final String findByUserId = 
			"Select * from company where user_id = ?1";
	
	@Query(value = findByUserId, nativeQuery = true)
	List<Company> findCompaniesbyUserId(Long userId);

	
	Company findByCompanyNameAndCountryAndAddressLine1AndAddressLine2AndCityAndStateAndPostalCodeAndPhoneNumberAndEmailIdAndWebsiteAndDomainAndStartedDateAndUserDetails(String companyName, String country, String addressLine1, String addressLine2, String city, String state, long postalCode,long phoneNumber, String emailID,String website, String domain,Date startedDate, UserDetails userDetails);
	
	Company findByCompanyNameAndCountryAndAddressLine1AndAddressLine2AndCityAndStateAndPostalCodeAndPhoneNumberAndEmailIdAndWebsiteAndDomainAndStartedDate(String companyName, String country, String addressLine1, String addressLine2, String city, String state, long postalCode, long phoneNumber, String emailID,String website, String domain,String startedDate);
	

//	@Modifying
//	@Transactional
	@Query(value="select company_name FROM company where company_name=?",nativeQuery=true)
	public String findByCompanyName(String companyName);
	
	@Query(value="select email_id FROM company where email_id=?",nativeQuery=true)
	public List<String> findByEmailId(String emailId);
	
	Company findByAddressLine1(String addressLine1);
	
	Company findByAddressLine1AndCompanyId(String addressLine1,Long companyId);
	
	@Query(value="select phone_number FROM company where phone_number=?",nativeQuery=true)
	public List<Long> findByPhoneNumber(long phoneNumber);
}
