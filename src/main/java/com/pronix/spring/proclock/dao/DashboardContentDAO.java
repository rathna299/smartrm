package com.pronix.spring.proclock.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pronix.spring.proclock.models.DashboardContent;
import com.pronix.spring.proclock.models.UserDetails;

public interface DashboardContentDAO extends JpaRepository<DashboardContent, Long> {

	@Query(value = "select * from dashboard_content where module_id=?1 and user=?2",nativeQuery = true)
	DashboardContent findBymodule_id(long module_id,long user);
	
	List<DashboardContent> findByuserdetails(UserDetails userdetails);

}
