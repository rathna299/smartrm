package com.pronix.spring.proclock.dao;


import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.pronix.spring.proclock.models.Assets;
import com.pronix.spring.proclock.models.BusinessUnits;
import com.pronix.spring.proclock.models.Department;




public interface DepartmentDAO  extends JpaRepository<Department, Long>{

	
	Department findByDepartmentNameAndDepartmentCodeAndBusinessUnits(String departmentName,String departmentCode, BusinessUnits businessUnits);
	
	
	Department findByDepartmentNameAndDepartmentCode(String departmentName,String departmentCode);
	
	Department findByDepartmentCodeAndBusinessUnits(String departmentCode,Long businessUnits);
	
	Department findByBusinessUnits(String departmentCode);
	
	List<Department> findDepartmentByBusinessUnits(BusinessUnits businessUnits);


	Department findByDepartmentCode(String departmentCode);
	
	Department findByDepartmentName(String departmentName);
	
	
//	public static final String findByDepartmentCodeAndBusinessUnits = 
//			"Select department_code,business_units_id from department where department_id = ?1";
//	
//	@Query(value = findByDepartmentCodeAndBusinessUnits, nativeQuery = true)
//	List<Department> findByDepartmentId(Long departmentId);
	
	
	
	@Transactional
	@Modifying
	@Query(value = "update department set department_name=?3,department_code=?2,business_units_id=?4 where department_id=?1",nativeQuery = true)
	void updateDepartment(Long id,String departmentCode,String departmentName,Long businessUnits);
	
}
