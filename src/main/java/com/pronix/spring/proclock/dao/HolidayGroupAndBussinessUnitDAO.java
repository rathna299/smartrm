package com.pronix.spring.proclock.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.pronix.spring.proclock.models.HolidayGroupAndBusinnesUnit;

public interface HolidayGroupAndBussinessUnitDAO extends JpaRepository<HolidayGroupAndBusinnesUnit, Long> {
	
	@Query(value = "select * from holydaygroup_businessunit where businessunit_id = ?1 and holidaygroup_id =?2",nativeQuery = true)
	HolidayGroupAndBusinnesUnit findbyHolidayGroupAndBusinnesUnit(Long businessunit_id,Long holidaygroup_id);
	
	@Transactional
	@Modifying
	@Query(value = "update holydaygroup_businessunit set businessunit_id = ?1,holidaygroup_id=?2 where id=?3",nativeQuery = true)
	void updateHolidayGroupAndBusinnesUnit(Long businessunit_id,Long holidaygroup_id,Long id);

	@Query(value = "select * from holydaygroup_businessunit where businessunit_id = ?1",nativeQuery = true)
	HolidayGroupAndBusinnesUnit findbyBusinessUnit(Long businessunit_id);

}
