package com.pronix.spring.proclock.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pronix.spring.proclock.models.Holidaydates;
import com.pronix.spring.proclock.models.HolidaysGroup;
import com.pronix.spring.proclock.models.UserDetails;

public interface HolidaydatesDAO extends JpaRepository<Holidaydates, Long> {

	@Query(value = "select * from holidaydates where holidaydate = ?1 and group_id = ?2", nativeQuery = true)
	Holidaydates findByholidaydateAndholidayyear(String holidaydate, Long group_id);

	@Query(value = "select * from holidaydates where group_id = ?1", nativeQuery = true)
	List<Holidaydates> findByGroupId(Long group_id);

	@Query(value = "select * from holidaydates where holidayyear Like %?1%", nativeQuery = true)
	List<Holidaydates> findByHolidayyear(String year);

	@Query(value = "select * from holidaydates where group_id=?1 ", nativeQuery = true)
	List<Holidaydates> findByHolidaygroup(Long group_id);
	
	@Query(value = "select * from holidaydates where holidayname=?1 and group_id=?2 and holidayyear=?3", nativeQuery = true)
	Optional<Holidaydates> findByHolidaynameAndHolidaygroupAndHolidayyears(String holidayname,Long group_id,String year);
	
	@Query(value = "select * from holidaydates where holidaydate=?1 and group_id=?2 and holidayyear=?3", nativeQuery = true)
	Optional<Holidaydates> findByHolidaydateAndHolidaygroupAndHolidayyear(String holidaydate,Long group_id,String year);

}
