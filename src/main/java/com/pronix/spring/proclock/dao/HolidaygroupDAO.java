package com.pronix.spring.proclock.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.pronix.spring.proclock.models.HolidaysGroup;

public interface HolidaygroupDAO extends JpaRepository<HolidaysGroup, Long> {

	HolidaysGroup findByGroupName(String groupName);

	@Transactional
	@Modifying
	@Query(value = "update holidays_group set group_name =?1,description=?2,enabled = ?3,modifieddate=?4 where holidaygroupid =?5", nativeQuery = true)
	void updategroup(String group_name, String description, boolean enabled, String modifieddate, Long groupid);

}
