package com.pronix.spring.proclock.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.pronix.spring.proclock.models.JobTitles;
import com.pronix.spring.proclock.models.RefRoles;



public interface JobTitlesDAO  extends CrudRepository<JobTitles, Long>{

	JobTitles findByJobtitleAndJobtitlecode(String jobtitle,String jobtitlecode);
	
	JobTitles findByJobtitlecode(String jobtitlecode);
	
	JobTitles findByJobtitlecodeAndJobTitlesId(String jobtitlecode, Long jobTitlesId);
	
	@Transactional
	@Modifying
	@Query(value = "update job_titles set jobtitle=?3,jobtitlecode=?2,jobdescription=?4 where job_titles_id=?1",nativeQuery = true)
	void updateTitles(Long id,String jobtitlecode,String jobtitle,String jobdescription);
	
	
//	public static final String findByJobtitlecode = 
//			"Select * from job_titles where jobtitle = ?1";
//	
////	@Query(value = findByJobtitlecode, nativeQuery = true)
////	JobTitles findByJobtitlecode1(String jobtitlecode);
//	
//	@Query(value = findByJobtitlecode, nativeQuery = true)
	JobTitles findByJobtitle(String jobtitle);
	
}
