package com.pronix.spring.proclock.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pronix.spring.proclock.models.LeaveConfiguration;

public interface LeaveConfigurationDAO extends JpaRepository<LeaveConfiguration, Long> {

	@Query(value = "select * from leave_configuration where business_units_id =?1 and role_id =?2",nativeQuery = true)
	Optional<LeaveConfiguration> findbyBusinessUnitAndRole(long businessUnitsId, long roleId);

}
