package com.pronix.spring.proclock.dao;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.pronix.spring.proclock.models.LeaveDetails;
import com.pronix.spring.proclock.models.UserDetails;



public interface LeaveDetailsDAO extends JpaRepository<LeaveDetails, Long> {

public static final String updateLeavese = "update leave_details set remaining_annual_leaves =?2 , utilised_annual_leaves =?3 where leave_detail_id = ?1";
public static final String updateSickLeavese = "update leave_details set remaining_sickleaves =?2 , utilizedsick_leaves =?3 where leave_detail_id = ?1";

	
	@Query(value = "select l from LeaveDetails l where l.userDetails.userId=:userId AND l.year=:year")
    LeaveDetails findByLeaveDetails(@Param("userId") Long userId,@Param("year") Integer year);
	
	@Query(value = "select * from leave_details where user_id=?1",nativeQuery = true)
	Optional<LeaveDetails> findByUserId(long userid);

	@Transactional
	@Modifying
	@Query(value = updateLeavese, nativeQuery = true)
	void updateLeavese(Long leavedetailid, double remainingAnnualLeaves,double utilisedAnnualLeaves);
	
	@Transactional
	@Modifying
	@Query(value = updateSickLeavese, nativeQuery = true)
	void updateSickLeavese(Long leavedetailid, double remainingSickleaves,double utilizedsickLeaves);
	
	@Query(value ="SELECT sum(remaining_annual_leaves+remaining_sickleaves) FROM leave_details where user_id=?1", nativeQuery = true)
	Optional<Double> findByUserId1(long userid);
	
	
	@Query(value ="SELECT sum(utilised_annual_leaves+utilizedsick_leaves) FROM leave_details where user_id=?1", nativeQuery = true)
	Optional<Double> findByUserId2(long userid);
	
}
