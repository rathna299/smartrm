package com.pronix.spring.proclock.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.pronix.spring.proclock.models.LeaveMaster;

public interface LeaveMasterDAO extends JpaRepository<LeaveMaster, Long> {
	

	
public static final String updateStatus = "update leave_master set status =?2, comment =?3  where leave_master_id = ?1";
	
	@Transactional	
	@Modifying
	@Query(value=updateStatus,nativeQuery = true)
	void updateStatus(Long leave_master_id,String Status,String comment);
	
    @Query(value="select * from leave_master where user_id=?1 and status=?2",nativeQuery = true)
    List<LeaveMaster> findByUserId(Long user_id,String status);

    @Query(value="select * from leave_master where status=?1",nativeQuery = true)
    List<LeaveMaster> findByStatus(String status);
    
//    @Query(value="SELECT count(*) FROM leave_master WHERE status IN ('Approved', 'pending', 'Decliened') GROUP BY status;",nativeQuery=true)
//	public Object[] findAllByStatus();
	
	@Query(value="select count(*) FROM leave_master WHERE status=?2 and user_id=?1",nativeQuery = true)
    public Long findByStatus(Long UserId,String Status);
	
	@Query(value="SELECT status,count(*) FROM leave_master WHERE user_id=?1 AND status IN ('Approved', 'Pending', 'Decliened') GROUP BY status",nativeQuery=true)
    public List<Object[]> findByStatus2(Long UserId);
	
	@Query(value="select count(*) FROM .leave_master WHERE status=?2 and manager_id=?1",nativeQuery = true)
    public Long findByManagerId(Long ManagerId,String Status);
	
	@Query(value="SELECT status,count(*) FROM leave_master WHERE manager_id=?1 AND status IN ('Approved', 'Pending', 'Decliened') GROUP BY status",nativeQuery=true)
    public List<Object[]> findByManagerId2(Long UserId);
    
    @Query(value="select * from leave_master where from_date=?1 and status=?2",nativeQuery = true)
    public List<LeaveMaster> findbyfromdateAndStatus(String fromdate,String status);
    
	@Query(value="select * FROM leave_master where manager_id=?1",nativeQuery = true)
	 public List<LeaveMaster> findByManager(Long ManagerId);
	
	@Query(value="select * from leave_master where from_date=?1 and status=?2 and user_id=?3",nativeQuery = true)
    public List<LeaveMaster> findbyfromdateAndStatusAndUserDetails(String fromdate,String status,Long UserId);

	@Query(value="select * from leave_master where status In ('Approved','Pending') and user_id=?1",nativeQuery = true)
	List<LeaveMaster> findByUserDetails(long userId);
}
