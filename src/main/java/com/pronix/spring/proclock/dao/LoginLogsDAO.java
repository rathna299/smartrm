package com.pronix.spring.proclock.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.pronix.spring.proclock.models.Logs;


public interface LoginLogsDAO extends JpaRepository<Logs, Long>{
	
	
	@Query(value = "SELECT * FROM logs order by login_time desc",nativeQuery = true)
	List<Logs> findAll();
	
}
