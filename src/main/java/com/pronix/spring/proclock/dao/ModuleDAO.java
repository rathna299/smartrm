package com.pronix.spring.proclock.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.pronix.spring.proclock.models.Modules;

public interface ModuleDAO extends JpaRepository<Modules, Long> {

	public static final String findByValue = "Select * from modules where value = ?1";

	@Query(value = findByValue, nativeQuery = true)
	Modules findByEnabled(String refRole);

	Modules findByDescriptionAndValue(String description, String value);

	@Transactional
	@Modifying
	@Query(value = "update modules set enabled=?4,description=?3,value=?2 where modules_id=?1", nativeQuery = true)
	void updateModules(Long id, String value, String description, boolean enabled);

	Modules findByDescription(String description);

	Modules findByValue(String value);

}
