package com.pronix.spring.proclock.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.pronix.spring.proclock.models.Project;
import com.pronix.spring.proclock.models.Project_Employees;
import com.pronix.spring.proclock.models.UserDetails;

public interface Project_employeeDAO extends JpaRepository<Project_Employees, Long> {

	@Transactional
	@Modifying
	@Query(value = "delete from project_employees where project_id=?1 and employee_id=?2", nativeQuery = true)
	void RemoveEmployeeFromProject(Long project_id, Long employee_id);

	@Query(value = "select * from project_employees where employee_id=?1 and project_id=?2", nativeQuery = true)
	List<Project_Employees> findByEmployee(Long employee_id, Long project_id);

	Project_Employees findByUserDetailsAndProject(UserDetails userDetails, Project project);

	List<Project_Employees> findAllByUserDetails(UserDetails userDetails);

	@Query(value = "SELECT count(*) FROM project_employees where employee_id=?1 and enable=1 group by employee_id", nativeQuery = true)
	Long findCount(Long employee_id);

	@Query(value = "select * from project_employees where employee_id=?1 and enable=?2", nativeQuery = true)
	List<Project_Employees> findByEmployeeAndEnable(Long employee_id, boolean enable);

	@Query(value = "select * from project_employees where employee_id=?1 and project_id=?2", nativeQuery = true)
	Project_Employees findByEmployeeAndProject(Long employee_id, Long project_id);
}
