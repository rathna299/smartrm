package com.pronix.spring.proclock.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pronix.spring.proclock.models.RefRoles;

public interface RefRolesDAO  extends JpaRepository<RefRoles, Long>{

	public static final String findByValue = 
			"Select * from ref_roles where value = ?1";
	
	@Query(value = findByValue, nativeQuery = true)
	RefRoles findByValue(String refRole);
	
	
	RefRoles findByDescriptionAndValue(String description,String value);

}
