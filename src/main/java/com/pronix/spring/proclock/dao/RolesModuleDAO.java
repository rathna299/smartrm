package com.pronix.spring.proclock.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.pronix.spring.proclock.models.Modules;
import com.pronix.spring.proclock.models.RefRoles;
import com.pronix.spring.proclock.models.RolesModules;



public interface RolesModuleDAO extends JpaRepository<RolesModules, Long>{

		
	RolesModules findByModulesAndRefRoles(Modules modules, RefRoles refRoles);
	
	@Query(value = "select * from roles_modules where role_id=?1",nativeQuery = true)
	List<RolesModules> findByroleId(Long role_id);
	
	@Transactional
	@Modifying
	@Query(value = "update roles_modules set enable=?2 where roles_modules_id=?1",nativeQuery = true)
	void updateRoleModule(Long roles_modules_id,boolean enable);
	
	@Query(value = "select * from roles_modules where role_id=?1 and enable=?2",nativeQuery = true)
	List<RolesModules> findByRoleWithEnables(Long role_id,boolean enable);
	
	@Query(value = "select * from roles_modules where modules_id=?1 and enable =?2",nativeQuery = true)
	List<RolesModules> findByModuleActiveStatus(Long moduleId, boolean enable);
}
