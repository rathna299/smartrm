package com.pronix.spring.proclock.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pronix.spring.proclock.models.TimeSheetUnitIds;

public interface TimeSheetUnitqueDAO extends JpaRepository<TimeSheetUnitIds, Long> {

}
