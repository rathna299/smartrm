package com.pronix.spring.proclock.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.pronix.spring.proclock.models.UserContacts;
import com.pronix.spring.proclock.models.UserDetails;

public interface UserContactsDAO extends CrudRepository<UserContacts, Long>{
	
	public static final String updateEmail = "update user_contacts set email_id=?1 where id=?2";

	UserContacts findByEmailId(String emailId);
	
	UserContacts findByPhoneNo(String phoneNo);

	UserContacts findByUserDetails(UserDetails userDetails);
	
	
	@Query(value = updateEmail,nativeQuery = true)
	void updateEmail(String emailId, long id);

}
