package com.pronix.spring.proclock.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.pronix.spring.proclock.models.RolesModules;
import com.pronix.spring.proclock.models.UserDetails;

public interface UserDetailsDAO extends CrudRepository<UserDetails, Long> {

	UserDetails findByVerificationToken(String verificationToken);

	public static final String updateToken = "update user_details set verification_token=?2 where user_id=?1";

	public static final String findByUserId = "Select * from user_details where user_id = ?1";

	/*
	 * public static final String findByUserId =
	 * "SELECT user_id FROM pro_clock.user_details";
	 */

	@Query(value = findByUserId, nativeQuery = true)
	List<UserDetails> findbyUserId(Long userId);

	@Transactional
	@Modifying
	@Query(value = updateToken, nativeQuery = true)
	void updateToken(long userId, String md5);

	@Query(value = "select usr from UserDetails usr where usr.enabled=:enabled")
	List<UserDetails> findByEnabled(@Param("enabled") boolean enabled);

	@Query(value = "SELECT * FROM user_details where reporting_manager_user_id=?1", nativeQuery = true)
	List<UserDetails> findByManagerID(Long ManagerId);
	

	@Query(value = "select count(*) from user_details where enabled =?1",nativeQuery = true)
	Long findbyCount(boolean enabled);
	
	
	@Query(value="SELECT ut.*,ud.reporting_manager_user_id FROM user_details ud \r\n" + 
			"inner JOIN user_timesheet ut ON ut.user_id = ud.user_id where reporting_manager_user_id=?1",nativeQuery=true)
	public List<UserDetails> findByTempUserDetails(long tempUserDetails);
	
	@Query(value = "select * from user_details where role_id =?1 and enabled =?2",nativeQuery = true)
	List<UserDetails> findByRoleAndActiveStatus(Long roleId,boolean enabled);

	@Query(value = "select * from user_details where role_id =?1 and business_id = ?2 and enabled =?3",nativeQuery = true)
	List<UserDetails> findByRoleAndBusinessUnitActiveStatus(long roleId, long businessUnitsId, boolean b);
	

	@Query(value = "select * from user_details where first_name Like %?1% or last_name Like %?1% or concat(first_name,' ',last_name) Like %?1%",nativeQuery = true)
	List<UserDetails> findByFirstNameAndLastName(String name);
	
	@Query(value = "SELECT * FROM user_details where reporting_manager_user_id=?1 and enabled =?2", nativeQuery = true)
	List<UserDetails> findByManagerIdAndActive(Long ManagerId,boolean enable);
	
	@Query(value = "SELECT * FROM user_details where role_id in(2,3)",nativeQuery = true)
	List<UserDetails> findByroleId();
}
