package com.pronix.spring.proclock.dao;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import com.pronix.spring.proclock.models.UserPersonalInfo;

public interface UserPersonalInfoDao extends JpaRepository<UserPersonalInfo, Long> {

	@Query(value = "select * from user_personal_info where id=?1", nativeQuery = true)
	List<UserPersonalInfo> findByEmployeeById(int id);

	UserPersonalInfo findByEmail(String emailId);

	List<UserPersonalInfo> findByDobEndsWith(String dob);

	@Query(value = "SELECT upi.user_id,uc.email_id,ud.first_name,ud.last_name\r\n" + "FROM user_details ud \r\n"
			+ "inner JOIN user_contacts uc ON uc.user_id = ud.user_id \r\n"
			+ "inner JOIN user_personal_info upi ON ud.user_id = upi.user_id where upi.user_id=?1", nativeQuery = true)
	public List<Long> findByUserId1(long userId);

	UserPersonalInfo findByFileType(String fileType);

	public static final String findByUserId = "Select * from user_personal_info where user_id = ?1";

	@Query(value = findByUserId, nativeQuery = true)
	UserPersonalInfo findByUserIds(Long userId);

	UserPersonalInfo findByUserId(long userId);

	@Transactional
	@Modifying
	@Query(value = "update user_personal_info set gender=?2,email=?3,mobile_number=?4,emergency_number=?5,permanent_address=?6,"
			+ "present_addresss=?7,martial_status=?8,pancard=?9,adhaarcard=?10,passport=?11,father_name=?12,mother_name=?13,"
			+ "parent_contact=?14,dob=?15 where user_personalinfo_id=?1", nativeQuery = true)

	void updateUserPersonalInfo(Long userPersonalInfo, String gender, String email, String mobileNumber,
			String emergencyNumber, String permanentAddress, String presentAddress, String martialStatus,
			String pancard, String adhaarcard, String passport, String fatherName, String motherName,
			String parentContact, String dob);

}
