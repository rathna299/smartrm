package com.pronix.spring.proclock.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.pronix.spring.proclock.models.RefRoles;
import com.pronix.spring.proclock.models.UserDetails;
import com.pronix.spring.proclock.models.UserRoles;

public interface UserRolesDAO extends CrudRepository<UserRoles, Long>{

	UserRoles findByUserDetails(UserDetails userDetails);
	
	UserRoles findByIdAndRefRolesAndUserDetails(long id, RefRoles refRoles, UserDetails userDetails);
	
	@Transactional
	@Modifying
	@Query(value = "update user_roles set role_id=?2 where id=?1",nativeQuery = true)
	void updateRoles(Long id,Long role_id);

}
