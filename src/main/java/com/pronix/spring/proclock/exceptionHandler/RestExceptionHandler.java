package com.pronix.spring.proclock.exceptionHandler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler {

	@ExceptionHandler
	public ResponseEntity<CustomError> hanlder(Exception exe) {

		CustomError error = new CustomError(HttpStatus.BAD_REQUEST.value(), exe.getMessage(),
				System.currentTimeMillis());

		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
	@ExceptionHandler
	public ResponseEntity<CustomError> handler2(CustomNotFoundException exe){
		
		CustomError error = new CustomError(HttpStatus.NOT_FOUND.value(), exe.getMessage(),
				System.currentTimeMillis());
		
		return new ResponseEntity<>(error,HttpStatus.NOT_FOUND);
	}

}
