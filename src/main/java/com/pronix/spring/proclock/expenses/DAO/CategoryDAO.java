package com.pronix.spring.proclock.expenses.DAO;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pronix.spring.proclock.expenses.models.Category;

public interface CategoryDAO extends JpaRepository<Category, Long> {
	
	Optional<Category> findByCategoryName(String categoryName);
	

}
