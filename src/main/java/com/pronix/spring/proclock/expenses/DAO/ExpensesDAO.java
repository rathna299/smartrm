package com.pronix.spring.proclock.expenses.DAO;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.pronix.spring.proclock.expenses.models.Expenses;

public interface ExpensesDAO extends JpaRepository<Expenses, Long> {
	
	Optional<Expenses> findByExpensesName(String expensesName);
	
//	@Query(value = "select * from expenses where user_id =?1",nativeQuery = true)
//	Optional<Expenses> findByUser_id(Long user_id);
		
	
	@Query(value = "select * from expenses where status =?1 and user_id =?2", nativeQuery = true)
	List<Expenses> findByUser_id(String status, Long user_id);
	
	
	@Query(value = "select * from expenses where user_id =?1", nativeQuery = true)
	List<Expenses> findByUser_ids(Long user_id);
	

	@Query(value = "Select * from expenses where user_id =?1", nativeQuery = true)
	List<Expenses> findByUser_id(Long userId);

}
