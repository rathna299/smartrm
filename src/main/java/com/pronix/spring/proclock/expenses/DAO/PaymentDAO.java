package com.pronix.spring.proclock.expenses.DAO;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pronix.spring.proclock.expenses.models.PaymentMode;

public interface PaymentDAO extends JpaRepository<PaymentMode, Long> {
	
	Optional<PaymentMode> findByPaymentType(String paymentType);

}
