package com.pronix.spring.proclock.expenses.DAO;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import com.pronix.spring.proclock.expenses.models.AddTrip;

public interface TripDAO extends JpaRepository<AddTrip, Long> {
	
	Optional<AddTrip> findByTripName(String tripName);

}
