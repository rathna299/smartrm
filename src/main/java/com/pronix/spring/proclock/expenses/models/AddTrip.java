package com.pronix.spring.proclock.expenses.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.pronix.spring.proclock.models.Project;
import com.pronix.spring.proclock.models.UserDetails;

@Entity
@Table(name = "trip")
public class AddTrip {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long trip_id;

	private String tripName;

	private String tripFromDate;

	private String tripToDate;

	private String description;
	
	
	public AddTrip() {
		super();
	}

	public Long getTrip_id() {
		return trip_id;
	}

	public void setTrip_id(Long trip_id) {
		this.trip_id = trip_id;
	}

	public String getTripName() {
		return tripName;
	}

	public void setTripName(String tripName) {
		this.tripName = tripName;
	}

	public String getTripFromDate() {
		return tripFromDate;
	}

	public void setTripFromDate(String tripFromDate) {
		this.tripFromDate = tripFromDate;
	}

	public String getTripToDate() {
		return tripToDate;
	}

	public void setTripToDate(String tripToDate) {
		this.tripToDate = tripToDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	

	
}
