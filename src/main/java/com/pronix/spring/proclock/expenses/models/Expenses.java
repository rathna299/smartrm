package com.pronix.spring.proclock.expenses.models;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pronix.spring.proclock.models.Project;
import com.pronix.spring.proclock.models.UserDetails;

@Entity
@Table(name = "expenses")
public class Expenses {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long expenses_id;

	private String expensesName;

	private String expensesDate;

	private String expensesAmount;

	private String expensesAdvancedAmount;

	private String paymentReference;

	private String description;

	private String fileName;

	private String fileType;
	

	@JsonIgnore
	@Lob
	private byte[] data;

	private String reimbursable;
	
	private LocalDateTime createddate;
	
	private LocalDateTime modifydate;
	
	private String status;

	@ManyToOne
	@JoinColumn(name = "category_id", referencedColumnName = "category_id")
	private Category category;
	
	@ManyToOne
	@JoinColumn(name = "payment_id", referencedColumnName = "payment_id")
	private PaymentMode paymentMode;
	
	@ManyToOne
	@JoinColumn(name = "trip_id", referencedColumnName = "trip_id")
	private AddTrip addTrip;	

	private Long project_id;
	
	private String comment;

	private Long user_id;


	public Expenses() {
		super();
	}

	public Long getExpenses_id() {
		return expenses_id;
	}

	public void setExpenses_id(Long expenses_id) {
		this.expenses_id = expenses_id;
	}

	public String getExpensesName() {
		return expensesName;
	}

	public void setExpensesName(String expensesName) {
		this.expensesName = expensesName;
	}

	public String getExpensesDate() {
		return expensesDate;
	}

	public void setExpensesDate(String expensesDate) {
		this.expensesDate = expensesDate;
	}

	public String getExpensesAmount() {
		return expensesAmount;
	}

	public void setExpensesAmount(String expensesAmount) {
		this.expensesAmount = expensesAmount;
	}

	public String getExpensesAdvancedAmount() {
		return expensesAdvancedAmount;
	}

	public void setExpensesAdvancedAmount(String expensesAdvancedAmount) {
		this.expensesAdvancedAmount = expensesAdvancedAmount;
	}

	public String getPaymentReference() {
		return paymentReference;
	}

	public void setPaymentReference(String paymentReference) {
		this.paymentReference = paymentReference;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public String getReimbursable() {
		return reimbursable;
	}
	
	public void setReimbursable(String reimbursable) {
		this.reimbursable = reimbursable;
	}
	
	public LocalDateTime getCreateddate() {
		return createddate;
	}

	public void setCreateddate(LocalDateTime createddate) {
		this.createddate = createddate;
	}
	
	public LocalDateTime getModifydate() {
		return modifydate;
	}

	public void setModifydate(LocalDateTime modifydate) {
		this.modifydate = modifydate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public PaymentMode getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(PaymentMode paymentMode) {
		this.paymentMode = paymentMode;
	}

	public AddTrip getAddTrip() {
		return addTrip;
	}

	public void setAddTrip(AddTrip addTrip) {
		this.addTrip = addTrip;
	}

	public Long getProject_id() {
		return project_id;
	}

	public void setProject_id(Long project_id) {
		this.project_id = project_id;
	}
	
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Long getUser_id() {
		return user_id;
	}

	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}

}
