package com.pronix.spring.proclock.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity

@Table(name = "assets_list",uniqueConstraints = @UniqueConstraint(columnNames={"productNumber", "productId"}))
public class Assets {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "asset_id")
	private long assetId;
	
	private String productType;
	private String productNumber;
	private Date productCreate;
	private String productDesc;
	private String productId;
	private String productModel;
	private String productMake;
	private Date productModifyDate;
	private boolean enable;
	
	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "user_id")
	private UserDetails userDetails;


	public Assets() {
		super();
	}	

	//Update constructor
	public Assets(long assetId, String productType, Date productCreate, String productNumber, String productDesc, String productId,
			String productModel, String productMake,boolean enable,UserDetails userDetails) {
		super();
		this.assetId = assetId;
		this.productType = productType;
		this.productCreate = productCreate;
		this.productNumber = productNumber;
		this.productDesc = productDesc;
		this.productId = productId;
		this.productModel = productModel;
		this.productMake = productMake;
		this.productModifyDate = new Date();
		this.enable = enable;
		this.userDetails = userDetails;
	}



	//Add constructor
	public Assets(String productType, String productNumber, String productDesc, String productId,
			String productModel, String productMake,boolean enable, UserDetails userDetails) {
		super();
		this.productType = productType;
		this.productNumber = productNumber;
		this.productCreate = new Date();
		this.productDesc = productDesc;
		this.productId = productId;
		this.productModel = productModel;
		this.productMake = productMake;
		this.enable = enable;
		this.userDetails = userDetails;
	}
	

	public Assets(String productNumber, String productId, UserDetails userDetails) {
		super();
		this.productNumber = productNumber;
		this.productId = productId;
		this.userDetails = userDetails;
	}



	public long getAssetId() {
		return assetId;
	}

	public void setAssetId(long assetId) {
		this.assetId = assetId;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getProductNumber() {
		return productNumber;
	}

	public void setProductNumber(String productNumber) {
		this.productNumber = productNumber;
	}

	public Date getProductCreate() {
		return productCreate;
	}

	public void setProductCreate() {
		this.productCreate = new Date();
	}

	public String getProductDesc() {
		return productDesc;
	}

	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductModel() {
		return productModel;
	}

	public void setProductModel(String productModel) {
		this.productModel = productModel;
	}

	public String getProductMake() {
		return productMake;
	}

	public void setProductMake(String productMake) {
		this.productMake = productMake;
	}

	public Date getProductModifyDate() {
		return productModifyDate;
	}

	public void setProductModifyDate() {
		this.productModifyDate = new Date();
	}

	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}
	

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	@Override
	public String toString() {
		return "Assets [assetId=" + assetId + ", productType=" + productType + ", productNumber=" + productNumber
				+ ", productDesc=" + productDesc + ", productId=" + productId + ", productModel=" + productModel
				+ ", productMake=" + productMake + ", userDetails=" + userDetails
				+ "]";
	}


}