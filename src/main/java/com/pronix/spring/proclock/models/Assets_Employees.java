package com.pronix.spring.proclock.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "assets_employees")
public class Assets_Employees {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long assets_employees_id;
	
	private String assignToUserDate;
	
	private String assetsUserReturnDate;
	
	private String comment;
	
	@ManyToOne
	@JoinColumn(name = "asset_id",referencedColumnName = "asset_id")
	private Assets assets;
	
	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "user_id")
	private UserDetails userDetails;
	
	
	

	public Assets_Employees() {
		super();
	}
	
	
	//Assets assign to User
	public Assets_Employees(String assignToUserDate, Assets assets, UserDetails userDetails) {
		super();
		this.assignToUserDate = assignToUserDate;
		this.assets = assets;
		this.userDetails = userDetails;
	}
	

	//assets User Return to admin
	public Assets_Employees(Long assets_employees_id, String assignToUserDate, String assetsUserReturnDate,
			String comment, Assets assets, UserDetails userDetails) {
		super();
		this.assets_employees_id = assets_employees_id;
		this.assignToUserDate = assignToUserDate;
		this.assetsUserReturnDate = assetsUserReturnDate;
		this.comment = comment;
		this.assets = assets;
		this.userDetails = userDetails;
	}


	public Long getAssets_employees_id() {
		return assets_employees_id;
	}

	public void setAssets_employees_id(Long assets_employees_id) {
		this.assets_employees_id = assets_employees_id;
	}

	public String getAssignToUserDate() {
		return assignToUserDate;
	}

	public void setAssignToUserDate(String assignToUserDate) {
		this.assignToUserDate = assignToUserDate;
	}

	public String getAssetsUserReturnDate() {
		return assetsUserReturnDate;
	}

	public void setAssetsUserReturnDate(String assetsUserReturnDate) {
		this.assetsUserReturnDate = assetsUserReturnDate;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Assets getAssets() {
		return assets;
	}

	public void setAssets(Assets assets) {
		this.assets = assets;
	}


	public UserDetails getUserDetails() {
		return userDetails;
	}


	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}


	


	
	
	
	
	
	
}
