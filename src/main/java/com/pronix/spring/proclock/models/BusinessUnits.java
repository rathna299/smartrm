package com.pronix.spring.proclock.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "business_units")
public class BusinessUnits {
	
	@Id
	@Column(name = "business_units_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long businessUnitsId;
	
	@NotNull
	@Column(nullable = false)
	private String businessName;
	
	@NotNull
	@Column(nullable = false)
	private String businessCode;
	
	
	@Column()
	private String businessStartDate;
	
	@Column(nullable = false)
	private String country;
	
	@Column(nullable = false)
	private String state;
	
	@Column(nullable = false)
	private String city;
	
	
	@ManyToOne
	@JoinColumn(name = "company_id", referencedColumnName = "company_id")
	private Company company;
	
	
	
	
	


	public BusinessUnits() {
		super();
	}


	
	







	public BusinessUnits(long businessUnitsId, @NotNull String businessName, @NotNull String businessCode,
			@NotNull String businessStartDate, String country, String state, String city, Company company) {
		super();
		this.businessUnitsId = businessUnitsId;
		this.businessName = businessName;
		this.businessCode = businessCode;
		this.businessStartDate = businessStartDate;
		this.country = country;
		this.state = state;
		this.city = city;
		this.company = company;
	}











	public BusinessUnits(@NotNull String businessName, @NotNull String businessCode, @NotNull String businessStartDate,
			String country, String state, String city, Company company) {
		super();
		this.businessName = businessName;
		this.businessCode = businessCode;
		this.businessStartDate = businessStartDate;
		this.country = country;
		this.state = state;
		this.city = city;
		this.company = company;
	}




	




	public long getBusinessUnitsId() {
		return businessUnitsId;
	}





	public void setBusinessUnitsId(long businessUnitsId) {
		this.businessUnitsId = businessUnitsId;
	}





	public String getBusinessName() {
		return businessName;
	}


	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}


	public String getBusinessCode() {
		return businessCode;
	}


	public void setBusinessCode(String businessCode) {
		this.businessCode = businessCode;
	}


	public String getBusinessStartDate() {
		return businessStartDate;
	}


	public void setBusinessStartDate(String businessStartDate) {
		this.businessStartDate = businessStartDate;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public Company getCompany() {
		return company;
	}


	public void setCompany(Company company) {
		this.company = company;
	}
	
	
	
	

	
	
	
	



}
