package com.pronix.spring.proclock.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "company", uniqueConstraints = @UniqueConstraint(columnNames = "addressLine1"))
public class Company {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "company_id")
	private long companyId;

	@Column(nullable = false)
	private String companyName;

	@Column(nullable = false)
	private String country;

	//@Column(nullable = false)
	private String addressLine1;
	
	//@Column(nullable = false)
	private String addressLine2;
	
	@Column(nullable = false)
	private String city;
	
	@Column(nullable = false)
	private String state;
	
	@Column(nullable = false)
	private long postalCode;
	
	
	@Column(nullable = false)
	private long phoneNumber;
	
	@Column(nullable = false)
	private String emailId;
	
	private String website;
	private String domain;
	private String startedDate;
	
	
	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "user_id")
	private UserDetails userDetails;
	
	

	public Company() {
		super();
	}
	

	public Company(long companyId, String companyName, String country, String addressLine1, String addressLine2,
			String city, String state, long postalCode, long phoneNumber, String emailId, String website, String domain,
			String startedDate, UserDetails userDetails) {
		super();
		this.companyId = companyId;
		this.companyName = companyName;
		this.country = country;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.city = city;
		this.state = state;
		this.postalCode = postalCode;
		this.phoneNumber = phoneNumber;
		this.emailId = emailId;
		this.website = website;
		this.domain = domain;
		this.startedDate = startedDate;
		this.userDetails = userDetails;
	}


	public Company(String companyName, String country, String addressLine1, String addressLine2, String city,
			String state, long postalCode, long phoneNumber, String emailId, String website, String domain,
			String startedDate, UserDetails userDetails) {
		super();
		this.companyName = companyName;
		this.country = country;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.city = city;
		this.state = state;
		this.postalCode = postalCode;
		this.phoneNumber = phoneNumber;
		this.emailId = emailId;
		this.website = website;
		this.domain = domain;
		this.startedDate = startedDate;
		this.userDetails = userDetails;
	}





	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public long getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(long postalCode) {
		this.postalCode = postalCode;
	}
	
	
	public long getPhoneNumber() {
		return phoneNumber;
	}


	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public String getEmailId() {
		return emailId;
	}


	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	
	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}


	public String getStartedDate() {
		return startedDate;
	}

	public void setStartedDate(String startedDate) {
		this.startedDate = startedDate;
	}


	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}

	
	
	
	

}