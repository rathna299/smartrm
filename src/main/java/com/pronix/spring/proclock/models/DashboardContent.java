package com.pronix.spring.proclock.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "dashboard_content")
public class DashboardContent {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "d_id")
	private long dashboard_id;
	
	private long module_id;
	
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "user", referencedColumnName  = "user_id")
	private UserDetails userdetails;

	public DashboardContent() {
		super();
	}


	public DashboardContent(long module_id, UserDetails userdetails) {
		super();
		this.module_id = module_id;
		this.userdetails = userdetails;
	}


	public long getDashboard_id() {
		return dashboard_id;
	}

	public void setDashboard_id(long dashboard_id) {
		this.dashboard_id = dashboard_id;
	}

	
	public long getModule_id() {
		return module_id;
	}

	public void setModule_id(long module_id) {
		this.module_id = module_id;
	}

	public UserDetails getUserdetails() {
		return userdetails;
	}

	public void setUserdetails(UserDetails userdetails) {
		this.userdetails = userdetails;
	}
	

	

}
