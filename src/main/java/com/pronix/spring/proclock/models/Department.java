package com.pronix.spring.proclock.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "department")
public class Department {
	
	@Id
	@Column(name = "Department_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long departmentId;
	
	@NotNull
	@Column(nullable = false)
	private String departmentName;
	
	@NotNull
	@Column(nullable = false)
	private String departmentCode;
	
	
	@ManyToOne
	@JoinColumn(name = "business_units_id", referencedColumnName = "business_units_id")
	private BusinessUnits businessUnits;
	
	
	
	


	public Department() {
		super();
	}





	public Department(long departmentId, @NotNull String departmentName, @NotNull String departmentCode,
			BusinessUnits businessUnits) {
		super();
		this.departmentId = departmentId;
		this.departmentName = departmentName;
		this.departmentCode = departmentCode;
		this.businessUnits = businessUnits;
	}
	
	
	


	public Department(@NotNull String departmentName, @NotNull String departmentCode, BusinessUnits businessUnits) {
		super();
		this.departmentName = departmentName;
		this.departmentCode = departmentCode;
		this.businessUnits = businessUnits;
	}





	public long getDepartmentId() {
		return departmentId;
	}


	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}


	public String getDepartmentName() {
		return departmentName;
	}


	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}


	public String getDepartmentCode() {
		return departmentCode;
	}


	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}


	public BusinessUnits getBusinessUnits() {
		return businessUnits;
	}


	public void setBusinessUnits(BusinessUnits businessUnits) {
		this.businessUnits = businessUnits;
	}
	

}
