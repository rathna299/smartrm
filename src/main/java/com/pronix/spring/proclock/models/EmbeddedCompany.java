package com.pronix.spring.proclock.models;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class EmbeddedCompany implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1029376836423974949L;

	@Column(nullable = false)
	private String companyName;
	
	@Column(nullable = false)
	private String companyAddress;

	public EmbeddedCompany() {
		super();
	}

	public EmbeddedCompany(String companyName, String companyAddress) {
		super();
		this.companyName = companyName;
		this.companyAddress = companyAddress;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}
	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EmbeddedCompany)) return false;
        EmbeddedCompany that = (EmbeddedCompany) o;
        return Objects.equals(getCompanyName(), that.getCompanyName()) &&
                Objects.equals(getCompanyAddress(), that.getCompanyAddress());
    }
 
    @Override
    public int hashCode() {
        return Objects.hash(getCompanyName(), getCompanyAddress());
    }

}
