package com.pronix.spring.proclock.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "holydaygroup_businessunit")
public class HolidayGroupAndBusinnesUnit {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "businessunit_id",referencedColumnName = "business_units_id")
	private BusinessUnits businessUnits;
	
	@ManyToOne
	@JoinColumn(name = "holidaygroup_id",referencedColumnName = "holidaygroupid")
	private HolidaysGroup holidaysGroup;
	
	

	public HolidayGroupAndBusinnesUnit() {
		super();
	}

	public HolidayGroupAndBusinnesUnit(BusinessUnits businessUnits, HolidaysGroup holidaysGroup) {
		super();
		this.businessUnits = businessUnits;
		this.holidaysGroup = holidaysGroup;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BusinessUnits getBusinessUnits() {
		return businessUnits;
	}

	public void setBusinessUnits(BusinessUnits businessUnits) {
		this.businessUnits = businessUnits;
	}

	public HolidaysGroup getHolidaysGroup() {
		return holidaysGroup;
	}

	public void setHolidaysGroup(HolidaysGroup holidaysGroup) {
		this.holidaysGroup = holidaysGroup;
	}
	
	
	
}
