package com.pronix.spring.proclock.models;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "holidaydates")
public class Holidaydates {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long holiday_id;

	private String holidayname;

	private String holidaydate;

	private String holidayyear;

	private String description;

	private String createddate;

	private String modifieddate;

	@ManyToOne
	@JoinColumn(name = "group_id", referencedColumnName = "holidaygroupid")
	private HolidaysGroup holidaygroup;

	private boolean enabled;

	public Holidaydates() {

	}

	public Long getHoliday_id() {
		return holiday_id;
	}

	public void setHoliday_id(Long holiday_id) {
		this.holiday_id = holiday_id;
	}

	public String getHolidayname() {
		return holidayname;
	}

	public void setHolidayname(String holidayname) {
		this.holidayname = holidayname;
	}

	public String getHolidaydate() {
		return holidaydate;
	}

	public void setHolidaydate(String holidaydate) {
		this.holidaydate = holidaydate;
	}

	public String getHolidayyear() {
		return holidayyear;
	}

	public void setHolidayyear(String holidayyear) {
		this.holidayyear = holidayyear;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreateddate() {
		return createddate;
	}

	public void setCreateddate(String createddate) {
		this.createddate = new SimpleDateFormat("dd/MM/yyyy_HH:mm:ss").format(Calendar.getInstance().getTime());
	}

	public String getModifieddate() {
		return modifieddate;
	}

	public void setModifieddate(String modifieddate) {
		this.modifieddate = modifieddate;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public HolidaysGroup getHolidaygroup() {
		return holidaygroup;
	}

	public void setHolidaygroup(HolidaysGroup holidaygroup) {
		this.holidaygroup = holidaygroup;
	}

}
