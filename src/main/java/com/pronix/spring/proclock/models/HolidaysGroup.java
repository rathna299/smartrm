package com.pronix.spring.proclock.models;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "holidays_group")
public class HolidaysGroup {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long holidaygroupid;

	private String groupName;

	private String description;

	private String createddate;

	private String modifieddate;

	private boolean enabled;

	public HolidaysGroup() {
		super();
	}

	public HolidaysGroup(String groupName, String description, String createddate, boolean enabled) {
		super();
		this.groupName = groupName;
		this.description = description;
		this.createddate = createddate;
		this.enabled = enabled;
	}

	public HolidaysGroup(Long holidaygroupid, String groupName, String description, boolean enabled) {
		super();
		this.holidaygroupid = holidaygroupid;
		this.groupName = groupName;
		this.description = description;
		this.enabled = enabled;
		this.modifieddate = new SimpleDateFormat("dd/MM/yyyy_HH:mm:ss").format(Calendar.getInstance().getTime());
	}

	public Long getHolidaygroupid() {
		return holidaygroupid;
	}

	public void setHolidaygroupid(Long holidaygroupid) {
		this.holidaygroupid = holidaygroupid;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreateddate() {
		return createddate;
	}

	public void setCreateddate(String createddate) {
		this.createddate = new SimpleDateFormat("dd/MM/yyyy_HH:mm:ss").format(Calendar.getInstance().getTime());
	}

	public String getModifieddate() {
		return modifieddate;
	}

	public void setModifieddate(String modifieddate) {
		this.modifieddate = modifieddate;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
