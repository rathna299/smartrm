package com.pronix.spring.proclock.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "job_titles")
public class JobTitles {
	
	@Id
	@Column(name = "job_titles_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long jobTitlesId;
	
	@NotNull
	@Column(nullable = false)
	private String jobtitlecode;
	
	@NotNull
	@Column(nullable = false)
	private String jobtitle;
	
	@NotNull
	@Column(nullable = false)
	private String jobdescription;
	
	
	
	


	

	public JobTitles() {
		super();
	}


	public JobTitles(long jobTitlesId, @NotNull String jobtitlecode, @NotNull String jobtitle,
			@NotNull String jobdescription) {
		super();
		this.jobTitlesId = jobTitlesId;
		this.jobtitlecode = jobtitlecode;
		this.jobtitle = jobtitle;
		this.jobdescription = jobdescription;
	}



	public JobTitles(@NotNull String jobtitlecode, @NotNull String jobtitle, @NotNull String jobdescription) {
		super();
		this.jobtitlecode = jobtitlecode;
		this.jobtitle = jobtitle;
		this.jobdescription = jobdescription;
	}



	public long getJobTitlesId() {
		return jobTitlesId;
	}


	public void setJobTitlesId(long jobTitlesId) {
		this.jobTitlesId = jobTitlesId;
	}


	public String getJobtitlecode() {
		return jobtitlecode;
	}

	public void setJobtitlecode(String jobtitlecode) {
		this.jobtitlecode = jobtitlecode;
	}

	public String getJobtitle() {
		return jobtitle;
	}

	public void setJobtitle(String jobtitle) {
		this.jobtitle = jobtitle;
	}

	public String getJobdescription() {
		return jobdescription;
	}

	public void setJobdescription(String jobdescription) {
		this.jobdescription = jobdescription;
	}

	
	


}
