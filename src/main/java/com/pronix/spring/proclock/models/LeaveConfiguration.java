package com.pronix.spring.proclock.models;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "leave_configuration")
public class LeaveConfiguration {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long leave_configuration_id;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "Business_units_id", referencedColumnName = "business_units_id")
	private BusinessUnits BusinessUnits;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "role_id", referencedColumnName = "role_id")
	private RefRoles role;

	@NotNull
	private Long leaves;

	@NotNull
	private Long months;

	private LocalDateTime created_date;

	private LocalDateTime updated_on;

	private Long createdby;

	private Long updatedby;

	public LeaveConfiguration() {
		super();
	}

	public LeaveConfiguration(@NotNull BusinessUnits businessUnits,
			@NotNull RefRoles role, @NotNull Long leaves, @NotNull Long months, LocalDateTime created_date,
			LocalDateTime updated_on, Long createdby) {
		super();
		BusinessUnits = businessUnits;
		this.role = role;
		this.leaves = leaves;
		this.months = months;
		this.created_date = created_date;
		this.updated_on = updated_on;
		this.createdby = createdby;
	}

	public Long getLeave_configuration_id() {
		return leave_configuration_id;
	}

	public BusinessUnits getBusinessUnits() {
		return BusinessUnits;
	}

	public RefRoles getRole() {
		return role;
	}

	public Long getLeaves() {
		return leaves;
	}

	public Long getMonths() {
		return months;
	}

	public void setLeave_configuration_id(Long leave_configuration_id) {
		this.leave_configuration_id = leave_configuration_id;
	}

	public void setBusinessUnits(BusinessUnits businessUnits) {
		BusinessUnits = businessUnits;
	}

	public void setRole(RefRoles role) {
		this.role = role;
	}

	public void setLeaves(Long leaves) {
		this.leaves = leaves;
	}

	public void setMonths(Long months) {
		this.months = months;
	}

	public LocalDateTime getCreated_date() {
		return created_date;
	}

	public LocalDateTime getUpdated_on() {
		return updated_on;
	}

	public void setCreated_date(LocalDateTime created_date) {
		this.created_date = created_date;
	}

	public void setUpdated_on(LocalDateTime updated_on) {
		this.updated_on = updated_on;
	}

	public Long getCreatedby() {
		return createdby;
	}

	public Long getUpdatedby() {
		return updatedby;
	}

	public void setCreatedby(Long createdby) {
		this.createdby = createdby;
	}

	public void setUpdatedby(Long updatedby) {
		this.updatedby = updatedby;
	}

}
