package com.pronix.spring.proclock.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "leave_details")
public class LeaveDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "leave_detail_id")
	private long leavedetailId;

	private Integer year;

	private double sickLeaves;
	private double remainingSickleaves;
	private double utilizedsickLeaves;

	private double actualAnnualLeaves;
	private double remainingAnnualLeaves;
	private double utilisedAnnualLeaves;

	private double lossOfPay;

	private LocalDateTime updated_on;

	@OneToOne
	@JoinColumn(name = "user_id", referencedColumnName = "user_id")
	private UserDetails userDetails;

	public LeaveDetails() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LeaveDetails(long leavedetailId, Integer year, double sickLeaves, double remainingSickleaves,
			double utilizedsickLeaves, double actualAnnualLeaves, double remainingAnnualLeaves,
			double utilisedAnnualLeaves, double lossOfPay) {
		super();
		this.leavedetailId = leavedetailId;
		this.year = year;
		this.sickLeaves = sickLeaves;
		this.remainingSickleaves = remainingSickleaves;
		this.utilizedsickLeaves = utilizedsickLeaves;
		this.actualAnnualLeaves = actualAnnualLeaves;
		this.remainingAnnualLeaves = remainingAnnualLeaves;
		this.utilisedAnnualLeaves = utilisedAnnualLeaves;
		this.lossOfPay = lossOfPay;
	}

	public LeaveDetails(long leavedetailId, Integer year, double sickLeaves, double remainingSickleaves,
			double utilizedsickLeaves, double actualAnnualLeaves, double remainingAnnualLeaves,
			double utilisedAnnualLeaves, double lossOfPay, UserDetails userDetails) {
		super();
		this.leavedetailId = leavedetailId;
		this.year = year;
		this.sickLeaves = sickLeaves;
		this.remainingSickleaves = remainingSickleaves;
		this.utilizedsickLeaves = utilizedsickLeaves;
		this.actualAnnualLeaves = actualAnnualLeaves;
		this.remainingAnnualLeaves = remainingAnnualLeaves;
		this.utilisedAnnualLeaves = utilisedAnnualLeaves;
		this.lossOfPay = lossOfPay;
		this.userDetails = userDetails;
	}

	public LeaveDetails(long leavedetailId, double remainingAnnualLeaves, double utilisedAnnualLeaves) {
		super();
		this.leavedetailId = leavedetailId;
		this.remainingAnnualLeaves = remainingAnnualLeaves;
		this.utilisedAnnualLeaves = utilisedAnnualLeaves;
	}

	public long getLeavedetailId() {
		return leavedetailId;
	}

	public void setLeavedetailId(long leavedetailId) {
		this.leavedetailId = leavedetailId;
	}

	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public double getSickLeaves() {
		return sickLeaves;
	}

	public void setSickLeaves(double sickLeaves) {
		this.sickLeaves = sickLeaves;
	}

	public double getRemainingSickleaves() {
		return remainingSickleaves;
	}

	public void setRemainingSickleaves(double remainingSickleaves) {
		this.remainingSickleaves = remainingSickleaves;
	}

	public double getUtilizedsickLeaves() {
		return utilizedsickLeaves;
	}

	public void setUtilizedsickLeaves(double utilizedsickLeaves) {
		this.utilizedsickLeaves = utilizedsickLeaves;
	}

	public double getActualAnnualLeaves() {
		return actualAnnualLeaves;
	}

	public void setActualAnnualLeaves(double actualAnnualLeaves) {
		this.actualAnnualLeaves = actualAnnualLeaves;
	}

	public double getRemainingAnnualLeaves() {
		return remainingAnnualLeaves;
	}

	public void setRemainingAnnualLeaves(double remainingAnnualLeaves) {
		this.remainingAnnualLeaves = remainingAnnualLeaves;
	}

	public double getUtilisedAnnualLeaves() {
		return utilisedAnnualLeaves;
	}

	public void setUtilisedAnnualLeaves(double utilisedAnnualLeaves) {
		this.utilisedAnnualLeaves = utilisedAnnualLeaves;
	}

	public double getLossOfPay() {
		return lossOfPay;
	}

	public void setLossOfPay(double lossOfPay) {
		this.lossOfPay = lossOfPay;
	}

	public LocalDateTime getUpdated_on() {
		return updated_on;
	}

	public void setUpdated_on(LocalDateTime updated_on) {
		this.updated_on = updated_on;
	}

}
