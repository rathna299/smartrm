package com.pronix.spring.proclock.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "leave_master")
public class LeaveMaster {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "leave_master_id")
	private long leaveapplyId;

	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "user_id")
	private UserDetails userDetails;

	public String LeaveType;

	public String FromDate;
	public String ToDate;
	public double Days;

	public String Status;
	public String Reason;

	public Long ManagerId;

	public String comment;
	public LeaveMaster() {

	}

	public LeaveMaster(UserDetails userDetails, String leaveType, String fromDate, String toDate, double days,
			String status, String reason, Long managerId) {
		super();
		this.userDetails = userDetails;
		LeaveType = leaveType;
		FromDate = fromDate;
		ToDate = toDate;
		Days = days;
		Status = status;
		Reason = reason;
		ManagerId = managerId;
	}

	public LeaveMaster(long leaveapplyId, String status) {
		super();
		this.leaveapplyId = leaveapplyId;
		Status = status;
	}

	public long getLeaveapplyId() {
		return leaveapplyId;
	}

	public void setLeaveapplyId(long leaveapplyId) {
		this.leaveapplyId = leaveapplyId;
	}

	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}

	public String getLeaveType() {
		return LeaveType;
	}

	public void setLeaveType(String leaveType) {
		LeaveType = leaveType;
	}

	public String getFromDate() {
		return FromDate;
	}

	public void setFromDate(String fromDate) {
		FromDate = fromDate;
	}

	public String getToDate() {
		return ToDate;
	}

	public void setToDate(String toDate) {
		ToDate = toDate;
	}

	public double getDays() {
		return Days;
	}

	public void setDays(double days) {
		Days = days;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getReason() {
		return Reason;
	}

	public void setReason(String reason) {
		Reason = reason;
	}

	public Long getManagerId() {
		return ManagerId;
	}

	public void setManagerId(Long managerId) {
		ManagerId = managerId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	

}
