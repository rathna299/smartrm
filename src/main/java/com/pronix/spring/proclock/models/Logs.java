package com.pronix.spring.proclock.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity

@Table(name = "logs")
public class Logs {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "logs_id")
	private long LogsId;

	private String name;
	private String emailId;
	private String Role;
	private Date loginTime;

	private String ipAddress;

	public Logs() {
		super();
	}

	public Logs(String name, String emailId, String role, String ipAddress) {
		super();
		this.name = name;
		this.emailId = emailId;
		Role = role;
		this.loginTime = new Date();
		this.ipAddress = ipAddress;
	}
	

	public long getLogsId() {
		return LogsId;
	}

	public void setLogsId(long logsId) {
		LogsId = logsId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getRole() {
		return Role;
	}

	public void setRole(String role) {
		Role = role;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = new Date();
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	

}