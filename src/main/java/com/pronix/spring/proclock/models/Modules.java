package com.pronix.spring.proclock.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "modules")
public class Modules {
	
	@Id
	@Column(name = "modules_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long modulesId;
	
	@NotNull
	@Column(nullable = false)
	private String value;
	
	@NotNull
	@Column(nullable = false)
	private String description;

	private boolean enabled;
	
	public Modules() {
		super();
	}

	public Modules(@NotNull String value, @NotNull String description) {
		super();
		/*this.roleId = roleId;*/
		this.value = value;
		this.description = description;
	}
	

	public Modules(long modulesId, @NotNull String value, @NotNull String description) {
		super();
		this.modulesId = modulesId;
		this.value = value;
		this.description = description;
	}

	public long getModulesId() {
		return modulesId;
	}

	public void setModulesId(long modulesId) {
		this.modulesId = modulesId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}


}
