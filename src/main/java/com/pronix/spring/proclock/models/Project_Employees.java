package com.pronix.spring.proclock.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "project_employees")
public class Project_Employees {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long project_employee_id;
	
	private String projectAssignedDate;
	
	@ManyToOne
	@JoinColumn(name = "project_id",referencedColumnName = "project_id")
	private Project project;
	
	@ManyToOne
	@JoinColumn(name = "employee_id",referencedColumnName = "user_id")
	private UserDetails userDetails;
	
	private boolean enable;

	public Project_Employees() {
		super();
	}

		
	public Project_Employees(String projectAssignedDate, Project project, UserDetails userDetails, boolean enable) {
		super();
		this.projectAssignedDate = projectAssignedDate;
		this.project = project;
		this.userDetails = userDetails;
		this.enable = enable;
	}

	public Project_Employees(Project project, UserDetails userDetails) {
		super();
		this.project = project;
		this.userDetails = userDetails;
	}
	
	

	public Project_Employees(Long project_employee_id, boolean enable) {
		super();
		this.project_employee_id = project_employee_id;
		this.enable = enable;
	}

	//Enable-Disable Constructor
	public Project_Employees(Long project_employee_id, String projectAssignedDate, Project project,
			UserDetails userDetails, boolean enable) {
		super();
		this.project_employee_id = project_employee_id;
		this.projectAssignedDate = projectAssignedDate;
		this.project = project;
		this.userDetails = userDetails;
		this.enable = enable;
	}


	public Long getProject_employee_id() {
		return project_employee_id;
	}

	public void setProject_employee_id(Long project_employee_id) {
		this.project_employee_id = project_employee_id;
	}
	

	public String getProjectAssignedDate() {
		return projectAssignedDate;
	}

	public void setProjectAssignedDate(String projectAssignedDate) {
		this.projectAssignedDate = projectAssignedDate;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}


	@Override
	public String toString() {
		return "Project_Employees [project_employee_id=" + project_employee_id + ", projectAssignedDate="
				+ projectAssignedDate + ", project=" + project + ", userDetails=" + userDetails + ", enable=" + enable
				+ "]";
	}
	
	
	
	
	
}
