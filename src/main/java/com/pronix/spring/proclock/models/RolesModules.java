package com.pronix.spring.proclock.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "roles_modules")
public class RolesModules {
	
	@Id
	@Column(name = "roles_modules_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	
	@ManyToOne
    @JoinColumn(name = "modules_id", referencedColumnName = "modules_id")
	private Modules modules;
	
	
	@ManyToOne
    @JoinColumn(name = "role_id", referencedColumnName = "role_id")
	private RefRoles refRoles;
	
	private boolean enable;
	

	public RolesModules() {
		super();
	}




	public RolesModules(Modules modules, RefRoles refRoles, boolean enable) {
		super();
		this.modules = modules;
		this.refRoles = refRoles;
		this.enable = enable;
	}




	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public Modules getModules() {
		return modules;
	}


	public void setModules(Modules modules) {
		this.modules = modules;
	}


	public RefRoles getRefRoles() {
		return refRoles;
	}


	public void setRefRoles(RefRoles refRoles) {
		this.refRoles = refRoles;
	}


	public boolean isEnable() {
		return enable;
	}


	public void setEnable(boolean enable) {
		this.enable = enable;
	}


	
}
