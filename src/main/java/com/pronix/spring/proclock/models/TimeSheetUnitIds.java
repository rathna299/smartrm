package com.pronix.spring.proclock.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "timesheet_ids")
public class TimeSheetUnitIds {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long unique_id;

	public Long getUnique_id() {
		return unique_id;
	}

	public void setUnique_id(Long unique_id) {
		this.unique_id = unique_id;
	}

	public TimeSheetUnitIds() {
		super();
	}
	
	
	
}
