package com.pronix.spring.proclock.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "user_details")
public class UserDetails {
	
	@Id
	@Column(name = "user_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long userId;
	
	@NotNull
	@Column(nullable = false)
	private String firstName;
	
	@NotNull
	@Column(nullable = false)
	private String lastName;
	
	@NotNull
	@Column(nullable = false)
	private String verificationToken;
	
	@NotNull
	@Column(nullable = false)
	private boolean enabled;
	
	@ManyToOne
	@JoinColumn(name = "role_id",referencedColumnName = "role_id")
	private RefRoles tempRefRoles;
	
	@ManyToOne
	@JoinColumn(name = "company_id",referencedColumnName = "company_id")
	private Company tempCompany;
	
	@ManyToOne
	@JoinColumn(name = "Business_id",referencedColumnName = "business_units_id")
	private BusinessUnits businessUnits;
	
	@ManyToOne
	@JoinColumn(name = "Department_id",referencedColumnName = "department_id")
	private Department department;
	
	@ManyToOne
	@JoinColumn(name = "reporting_manager_user_id",referencedColumnName = "user_id")
	private  UserDetails tempUserDetails;
	
	@ManyToOne
	@JoinColumn(name = "job_title_id",referencedColumnName = "job_titles_id")
	private JobTitles jobTitles;
	
	
	
	private String dateOfJoining;
	
	public UserDetails() {
		super();
	}

	public UserDetails(@NotNull String firstName, @NotNull String lastName, @NotNull String verificationToken,
			@NotNull boolean enabled) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.verificationToken = verificationToken;
		this.enabled = enabled;
		
	}
	
	
	public UserDetails(@NotNull String firstName, @NotNull String lastName, @NotNull String dateOfJoining, @NotNull String verificationToken,
			@NotNull boolean enabled) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateOfJoining = dateOfJoining;
		this.verificationToken = verificationToken;
		this.enabled = enabled;
		
	}

	//add employee
	public UserDetails(@NotNull String firstName, @NotNull String lastName, @NotNull String verificationToken,
			@NotNull boolean enabled, RefRoles tempRefRoles, Company tempCompany, BusinessUnits businessUnits,
			Department department, UserDetails tempUserDetails, JobTitles jobTitles, String dateOfJoining) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.verificationToken = verificationToken;
		this.enabled = enabled;
		this.tempRefRoles = tempRefRoles;
		this.tempCompany = tempCompany;
		this.businessUnits = businessUnits;
		this.department = department;
		this.tempUserDetails = tempUserDetails;
		this.jobTitles = jobTitles;
		this.dateOfJoining = dateOfJoining;
	}
	
	
	

	public UserDetails(long userId, @NotNull String firstName, @NotNull String lastName,
			@NotNull String verificationToken, @NotNull boolean enabled, RefRoles tempRefRoles, Company tempCompany,
			BusinessUnits businessUnits, Department department, UserDetails tempUserDetails, JobTitles jobTitles,
			String dateOfJoining) {
		super();
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.verificationToken = verificationToken;
		this.enabled = enabled;
		this.tempRefRoles = tempRefRoles;
		this.tempCompany = tempCompany;
		this.businessUnits = businessUnits;
		this.department = department;
		this.tempUserDetails = tempUserDetails;
		this.jobTitles = jobTitles;
		this.dateOfJoining = dateOfJoining;
	}

	public UserDetails(@NotNull String firstName, @NotNull String lastName, @NotNull String verificationToken,
			@NotNull boolean enabled, RefRoles tempRefRoles, String dateOfJoining) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.verificationToken = verificationToken;
		this.enabled = enabled;
		this.tempRefRoles = tempRefRoles;
		this.dateOfJoining = dateOfJoining;
	}
	
	//Super Admin constructor
	public UserDetails(@NotNull String firstName, @NotNull String lastName, @NotNull String verificationToken,
			@NotNull boolean enabled, RefRoles tempRefRoles) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.verificationToken = verificationToken;
		this.enabled = enabled;
		this.tempRefRoles = tempRefRoles;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getVerificationToken() {
		return verificationToken;
	}

	public void setVerificationToken(String verificationToken) {
		this.verificationToken = verificationToken;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public RefRoles getTempRefRoles() {
		return tempRefRoles;
	}

	public void setTempRefRoles(RefRoles tempRefRoles) {
		this.tempRefRoles = tempRefRoles;
	}

	public Company getTempCompany() {
		return tempCompany;
	}

	public void setTempCompany(Company tempCompany) {
		this.tempCompany = tempCompany;
	}

	public BusinessUnits getBusinessUnits() {
		return businessUnits;
	}

	public void setBusinessUnits(BusinessUnits businessUnits) {
		this.businessUnits = businessUnits;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public UserDetails getTempUserDetails() {
		return tempUserDetails;
	}

	public void setTempUserDetails(UserDetails tempUserDetails) {
		this.tempUserDetails = tempUserDetails;
	}

	public JobTitles getJobTitles() {
		return jobTitles;
	}

	public void setJobTitles(JobTitles jobTitles) {
		this.jobTitles = jobTitles;
	}

	public String getDateOfJoining() {
		return dateOfJoining;
	}

	public void setDateOfJoining(String dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}


	
}
