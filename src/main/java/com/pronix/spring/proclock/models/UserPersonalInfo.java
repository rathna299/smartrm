package com.pronix.spring.proclock.models;

import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user_personal_info")
public class UserPersonalInfo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "user_personalinfo_id")
	private long userPersonalInfoId;


	@Column(name = "gender")
	private String gender;

	@Column(name = "email")
	private String email;

	@Column(name = "mobile_number")
	private String mobileNumber;

	@Column(name = "emergency_number")
	private String emergencyNumber;

	@Column(name = "permanent_address")
	private String permanentAddress;

	@Column(name = "present_addresss")
	private String presentAddress;

	@Column(name = "martial_status")
	private String martialStatus;

	@Column(name = "pancard")
	private String pancard;

	@Column(name = "adhaarcard")
	private String adhaarcard;

	@Column(name = "passport")
	private String passport;
	
	private String image;
	
	private String fatherName;
	
	private String motherName;
	
	private String parentContact;
	
	private String fileName;

    private String fileType;

    @Lob
    private byte[] data;
	
    private String dob;
    
    private long userId;

	
	public UserPersonalInfo() {

	}



	public UserPersonalInfo(long userPersonalInfoId, String gender, String email, String mobileNumber,
			String emergencyNumber, String permanentAddress, String presentAddress, String martialStatus,
			String pancard, String adhaarcard, String passport, String image, String fatherName, String motherName,
			String parentContact, String fileName, String fileType, byte[] data, String dob, long userId) {
		super();
		this.userPersonalInfoId = userPersonalInfoId;
		this.gender = gender;
		this.email = email;
		this.mobileNumber = mobileNumber;
		this.emergencyNumber = emergencyNumber;
		this.permanentAddress = permanentAddress;
		this.presentAddress = presentAddress;
		this.martialStatus = martialStatus;
		this.pancard = pancard;
		this.adhaarcard = adhaarcard;
		this.passport = passport;
		this.image = image;
		this.fatherName = fatherName;
		this.motherName = motherName;
		this.parentContact = parentContact;
		this.fileName = fileName;
		this.fileType = fileType;
		this.data = data;
		this.dob = dob;
		this.userId = userId;
	}

	public UserPersonalInfo(String gender, String email, String mobileNumber, String emergencyNumber,
			String permanentAddress, String presentAddress, String martialStatus, String pancard, String adhaarcard,
			String passport, String image, String fatherName, String motherName, String parentContact, String fileName,
			String fileType, byte[] data, String dob, long userId) {
		super();
		this.gender = gender;
		this.email = email;
		this.mobileNumber = mobileNumber;
		this.emergencyNumber = emergencyNumber;
		this.permanentAddress = permanentAddress;
		this.presentAddress = presentAddress;
		this.martialStatus = martialStatus;
		this.pancard = pancard;
		this.adhaarcard = adhaarcard;
		this.passport = passport;
		this.image = image;
		this.fatherName = fatherName;
		this.motherName = motherName;
		this.parentContact = parentContact;
		this.fileName = fileName;
		this.fileType = fileType;
		this.data = data;
		this.dob = dob;
		this.userId = userId;
	}





	public long getUserPersonalInfoId() {
		return userPersonalInfoId;
	}



	public void setUserPersonalInfoId(long userPersonalInfoId) {
		this.userPersonalInfoId = userPersonalInfoId;
	}



	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getMobileNumber() {
		return mobileNumber;
	}


	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}


	public String getEmergencyNumber() {
		return emergencyNumber;
	}


	public void setEmergencyNumber(String emergencyNumber) {
		this.emergencyNumber = emergencyNumber;
	}


	public String getPermanentAddress() {
		return permanentAddress;
	}


	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}


	public String getPresentAddress() {
		return presentAddress;
	}


	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}


	public String getMartialStatus() {
		return martialStatus;
	}


	public void setMartialStatus(String martialStatus) {
		this.martialStatus = martialStatus;
	}


	public String getPancard() {
		return pancard;
	}


	public void setPancard(String pancard) {
		this.pancard = pancard;
	}


	public String getAdhaarcard() {
		return adhaarcard;
	}


	public void setAdhaarcard(String adhaarcard) {
		this.adhaarcard = adhaarcard;
	}


	public String getPassport() {
		return passport;
	}


	public void setPassport(String passport) {
		this.passport = passport;
	}


	public String getImage() {
		return image;
	}


	public void setImage(String image) {
		this.image = image;
	}


	public String getFatherName() {
		return fatherName;
	}


	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}


	public String getMotherName() {
		return motherName;
	}


	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}


	public String getParentContact() {
		return parentContact;
	}


	public void setParentContact(String parentContact) {
		this.parentContact = parentContact;
	}
	
	


	public String getFileName() {
		return fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public String getFileType() {
		return fileType;
	}


	public void setFileType(String fileType) {
		this.fileType = fileType;
	}


	
	public byte[] getData() {
		return data;
	}


	public void setData(byte[] data) {
		this.data = data;
	}


	



	public String getDob() {
		return dob;
	}






	public void setDob(String dob) {
		this.dob = dob;
	}






	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}



	@Override
	public String toString() {
		return "UserPersonalInfo [userPersonalInfoId=" + userPersonalInfoId + ", gender=" + gender + ", email=" + email
				+ ", mobileNumber=" + mobileNumber + ", emergencyNumber=" + emergencyNumber + ", permanentAddress="
				+ permanentAddress + ", presentAddress=" + presentAddress + ", martialStatus=" + martialStatus
				+ ", pancard=" + pancard + ", adhaarcard=" + adhaarcard + ", passport=" + passport + ", image=" + image
				+ ", fatherName=" + fatherName + ", motherName=" + motherName + ", parentContact=" + parentContact
				+ ", fileName=" + fileName + ", fileType=" + fileType + ", data=" + Arrays.toString(data) + ", dob="
				+ dob + ", userId=" + userId + "]";
	}



		
}