package com.pronix.spring.proclock.onboard.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pronix.spring.proclock.onboard.models.Candidate_education_details;

public interface Candidate_Education_detailsDAO extends JpaRepository<Candidate_education_details, Long> {

	@Query(value = "select * from candidate_education_details where candidate_id = ?1",nativeQuery = true)
	List<Candidate_education_details> findbyCandidateId(Long candidate_Id);

}
