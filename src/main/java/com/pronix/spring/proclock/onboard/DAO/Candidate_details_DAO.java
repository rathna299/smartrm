package com.pronix.spring.proclock.onboard.DAO;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pronix.spring.proclock.onboard.models.Candidate_details;

public interface Candidate_details_DAO extends JpaRepository<Candidate_details, Long> {

	Optional<Candidate_details> findByEmail(String email);

	@Query(value = "select count(*) from candidate_details where status =?1",nativeQuery = true)
	long findByStatus(String string);
}
