package com.pronix.spring.proclock.onboard.DAO;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pronix.spring.proclock.onboard.models.Candidate_personal_details;

public interface Candidate_personal_detailsDAO extends JpaRepository<Candidate_personal_details, Long> {

	@Query(value = "select * from candidate_personal_details where candidate_id =?1",nativeQuery = true)
	Optional<Candidate_personal_details> findByCandidateId(Long candidate_Id);

}
