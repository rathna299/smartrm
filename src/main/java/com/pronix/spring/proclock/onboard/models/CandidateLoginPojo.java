package com.pronix.spring.proclock.onboard.models;

public class CandidateLoginPojo {
	
	private String UserName;
	
	private String password;

	public String getUserName() {
		return UserName;
	}

	public String getPassword() {
		return password;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	

}
