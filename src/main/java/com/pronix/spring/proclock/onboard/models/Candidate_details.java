package com.pronix.spring.proclock.onboard.models;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;

@Entity
@Table(name = "candidate_details")
public class Candidate_details {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long candidate_id;

	private String candidate_name;

	@Email
	private String email;

	private Long candidate_phone;

	private String designation;

	private String status;

	private Long createdby;

	private LocalDateTime createddate;

	private LocalDateTime expirationdate;

	private LocalDateTime submittedon;

	private String verificationtoken;

	private String date_of_joining;

	private String ctc;

	private String decline_comment;

	private String resubmission_comment;
	
	private String experience;

	public Candidate_details() {
		super();
	}

	public Long getCandidate_id() {
		return candidate_id;
	}

	public String getCandidate_name() {
		return candidate_name;
	}

	public String getEmail() {
		return email;
	}

	public Long getCandidate_phone() {
		return candidate_phone;
	}

	public String getDesignation() {
		return designation;
	}

	public String getStatus() {
		return status;
	}

	public Long getCreatedby() {
		return createdby;
	}

	public LocalDateTime getCreateddate() {
		return createddate;
	}

	public LocalDateTime getExpirationdate() {
		return expirationdate;
	}

	public void setCandidate_id(Long candidate_id) {
		this.candidate_id = candidate_id;
	}

	public void setCandidate_name(String candidate_name) {
		this.candidate_name = candidate_name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setCandidate_phone(Long candidate_phone) {
		this.candidate_phone = candidate_phone;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setCreatedby(Long createdby) {
		this.createdby = createdby;
	}

	public void setCreateddate(LocalDateTime createddate) {
		this.createddate = createddate;
	}

	public void setExpirationdate(LocalDateTime expirationdate) {
		this.expirationdate = expirationdate;
	}

	public String getVerificationtoken() {
		return verificationtoken;
	}

	public void setVerificationtoken(String verificationtoken) {
		this.verificationtoken = verificationtoken;
	}

	public String getDate_of_joining() {
		return date_of_joining;
	}

	public String getCtc() {
		return ctc;
	}

	public void setDate_of_joining(String date_of_joining) {
		this.date_of_joining = date_of_joining;
	}

	public void setCtc(String ctc) {
		this.ctc = ctc;
	}

	public LocalDateTime getSubmittedon() {
		return submittedon;
	}

	public void setSubmittedon(LocalDateTime submittedon) {
		this.submittedon = submittedon;
	}

	public String getDecline_comment() {
		return decline_comment;
	}

	public String getResubmission_comment() {
		return resubmission_comment;
	}

	public void setDecline_comment(String decline_comment) {
		this.decline_comment = decline_comment;
	}

	public void setResubmission_comment(String resubmission_comment) {
		this.resubmission_comment = resubmission_comment;
	}

	public String getExperience() {
		return experience;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}
	

}
