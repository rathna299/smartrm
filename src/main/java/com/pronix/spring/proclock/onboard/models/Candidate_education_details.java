package com.pronix.spring.proclock.onboard.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "candidate_education_details")
public class Candidate_education_details {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long educational_details_id;

	private String highest_qualification;

	private String university;

	private String branch;

	private String percentage;

	private Long pass_out_year;

	private String marksheetfile;

	private String marksheettype;
	@JsonIgnore
	@Lob
	private byte marksheetbyte[];

	private String certificates;

	private String certificatestype;
	@JsonIgnore
	@Lob
	private byte certificatesbye[];

	@ManyToOne
	@JoinColumn(name = "candidate_id", referencedColumnName = "candidate_id")
	private Candidate_details candidate_details;

	public Candidate_education_details() {
		super();
	}

	public Long getEducational_details_id() {
		return educational_details_id;
	}

	public String getHighest_qualification() {
		return highest_qualification;
	}

	public String getUniversity() {
		return university;
	}

	public String getBranch() {
		return branch;
	}

	public String getPercentage() {
		return percentage;
	}

	public String getMarksheetfile() {
		return marksheetfile;
	}

	public String getMarksheettype() {
		return marksheettype;
	}

	public byte[] getMarksheetbyte() {
		return marksheetbyte;
	}

	public String getCertificates() {
		return certificates;
	}

	public String getCertificatestype() {
		return certificatestype;
	}

	public byte[] getCertificatesbye() {
		return certificatesbye;
	}

	public void setEducational_details_id(Long educational_details_id) {
		this.educational_details_id = educational_details_id;
	}

	public void setHighest_qualification(String highest_qualification) {
		this.highest_qualification = highest_qualification;
	}

	public void setUniversity(String university) {
		this.university = university;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}

	public void setMarksheetfile(String marksheetfile) {
		this.marksheetfile = marksheetfile;
	}

	public void setMarksheettype(String marksheettype) {
		this.marksheettype = marksheettype;
	}

	public void setMarksheetbyte(byte[] marksheetbyte) {
		this.marksheetbyte = marksheetbyte;
	}

	public void setCertificates(String certificates) {
		this.certificates = certificates;
	}

	public void setCertificatestype(String certificatestype) {
		this.certificatestype = certificatestype;
	}

	public void setCertificatesbye(byte[] certificatesbye) {
		this.certificatesbye = certificatesbye;
	}

	public Candidate_details getCandidate_details() {
		return candidate_details;
	}

	public void setCandidate_details(Candidate_details candidate_details) {
		this.candidate_details = candidate_details;
	}

	public Long getPass_out_year() {
		return pass_out_year;
	}

	public void setPass_out_year(Long pass_out_year) {
		this.pass_out_year = pass_out_year;
	}

}
