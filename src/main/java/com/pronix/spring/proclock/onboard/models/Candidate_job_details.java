package com.pronix.spring.proclock.onboard.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "candidate_job_details")
public class Candidate_job_details {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long job_details_id;

	private String companyname;

	private String designation;

	private String fromdate;

	private String todate;

	private Long employee_referance_number;

	private Long manager_number;

	private String offerletterfile;

	private String offerlettertype;
	@JsonIgnore
	@Lob
	private byte[] offerletterbyte;

	private String relievingletterfile;

	private String relievinglettertype;
	@JsonIgnore
	@Lob
	private byte[] relievingletterbyte;

	private String payslipfile_one;

	private String payslipfiletype_one;
	@JsonIgnore
	@Lob
	private byte[] payslipfilebyte_one;

	private String payslipfile_two;

	private String payslipfiletype_two;
	@JsonIgnore
	@Lob
	private byte[] payslipfilebyte_two;

	private String payslipfile_three;

	private String payslipfiletype_three;
	@JsonIgnore
	@Lob
	private byte[] payslipfilebyte_three;

	@ManyToOne
	@JoinColumn(name = "candidate_id", referencedColumnName = "candidate_id")
	private Candidate_details candidate_details;

	public Candidate_job_details() {
		super();
	}

	public Long getJob_details_id() {
		return job_details_id;
	}

	public String getCompanyname() {
		return companyname;
	}

	public String getDesignation() {
		return designation;
	}

	public String getFromdate() {
		return fromdate;
	}

	public String getTodate() {
		return todate;
	}

	public Long getEmployee_referance_number() {
		return employee_referance_number;
	}

	public Long getManager_number() {
		return manager_number;
	}

	public String getOfferletterfile() {
		return offerletterfile;
	}

	public String getOfferlettertype() {
		return offerlettertype;
	}

	public byte[] getOfferletterbyte() {
		return offerletterbyte;
	}

	public String getRelievingletterfile() {
		return relievingletterfile;
	}

	public String getRelievinglettertype() {
		return relievinglettertype;
	}

	public byte[] getRelievingletterbyte() {
		return relievingletterbyte;
	}

	public String getPayslipfile_one() {
		return payslipfile_one;
	}

	public String getPayslipfiletype_one() {
		return payslipfiletype_one;
	}

	public byte[] getPayslipfilebyte_one() {
		return payslipfilebyte_one;
	}

	public String getPayslipfile_two() {
		return payslipfile_two;
	}

	public String getPayslipfiletype_two() {
		return payslipfiletype_two;
	}

	public byte[] getPayslipfilebyte_two() {
		return payslipfilebyte_two;
	}

	public String getPayslipfile_three() {
		return payslipfile_three;
	}

	public String getPayslipfiletype_three() {
		return payslipfiletype_three;
	}

	public byte[] getPayslipfilebyte_three() {
		return payslipfilebyte_three;
	}

	public Candidate_details getCandidate_details() {
		return candidate_details;
	}

	public void setJob_details_id(Long job_details_id) {
		this.job_details_id = job_details_id;
	}

	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public void setFromdate(String fromdate) {
		this.fromdate = fromdate;
	}

	public void setTodate(String todate) {
		this.todate = todate;
	}

	public void setEmployee_referance_number(Long employee_referance_number) {
		this.employee_referance_number = employee_referance_number;
	}

	public void setManager_number(Long manager_number) {
		this.manager_number = manager_number;
	}

	public void setOfferletterfile(String offerletterfile) {
		this.offerletterfile = offerletterfile;
	}

	public void setOfferlettertype(String offerlettertype) {
		this.offerlettertype = offerlettertype;
	}

	public void setOfferletterbyte(byte[] offerletterbyte) {
		this.offerletterbyte = offerletterbyte;
	}

	public void setRelievingletterfile(String relievingletterfile) {
		this.relievingletterfile = relievingletterfile;
	}

	public void setRelievinglettertype(String relievinglettertype) {
		this.relievinglettertype = relievinglettertype;
	}

	public void setRelievingletterbyte(byte[] relievingletterbyte) {
		this.relievingletterbyte = relievingletterbyte;
	}

	public void setPayslipfile_one(String payslipfile_one) {
		this.payslipfile_one = payslipfile_one;
	}

	public void setPayslipfiletype_one(String payslipfiletype_one) {
		this.payslipfiletype_one = payslipfiletype_one;
	}

	public void setPayslipfilebyte_one(byte[] payslipfilebyte_one) {
		this.payslipfilebyte_one = payslipfilebyte_one;
	}

	public void setPayslipfile_two(String payslipfile_two) {
		this.payslipfile_two = payslipfile_two;
	}

	public void setPayslipfiletype_two(String payslipfiletype_two) {
		this.payslipfiletype_two = payslipfiletype_two;
	}

	public void setPayslipfilebyte_two(byte[] payslipfilebyte_two) {
		this.payslipfilebyte_two = payslipfilebyte_two;
	}

	public void setPayslipfile_three(String payslipfile_three) {
		this.payslipfile_three = payslipfile_three;
	}

	public void setPayslipfiletype_three(String payslipfiletype_three) {
		this.payslipfiletype_three = payslipfiletype_three;
	}

	public void setPayslipfilebyte_three(byte[] payslipfilebyte_three) {
		this.payslipfilebyte_three = payslipfilebyte_three;
	}

	public void setCandidate_details(Candidate_details candidate_details) {
		this.candidate_details = candidate_details;
	}

}
