package com.pronix.spring.proclock.onboard.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "candidate_personal_details")
public class Candidate_personal_details {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long personal_details_id;

	private String first_name;

	private String middle_name;

	private String last_name;

	private String email;

	private Long mobile;

	private String alternate_number;

	private String father_name;

	private Long father_number;

	private String mother_name;

	private String present_address;

	private String permanent_address;

	private String martial_status;

	private String nationality;

	private String gender;

	private String aadhar_file;

	private String adhartype;
	@JsonIgnore
	@Lob
	private byte[] aadharbyte;

	private String pancard_file;

	private String pantype;
	@JsonIgnore
	@Lob
	private byte[] panbyte;

	private String passport_file;

	private String passporttype;
	@JsonIgnore
	@Lob
	private byte[] passportbyte;

	@ManyToOne
	@JoinColumn(name = "candidate_id", referencedColumnName = "candidate_id")
	private Candidate_details candidate_details;

	public Candidate_personal_details() {
		super();
	}

	public Candidate_personal_details(Long personal_details_id, String first_name, String middle_name, String last_name,
			String alternate_number, String father_name, Long father_number, String mother_name, String present_address,
			String permanent_address, String martial_status, String nationality) {
		super();
		this.personal_details_id = personal_details_id;
		this.first_name = first_name;
		this.middle_name = middle_name;
		this.last_name = last_name;
		this.alternate_number = alternate_number;
		this.father_name = father_name;
		this.father_number = father_number;
		this.mother_name = mother_name;
		this.present_address = present_address;
		this.permanent_address = permanent_address;
		this.martial_status = martial_status;
		this.nationality = nationality;
	}

	public Long getPersonal_details_id() {
		return personal_details_id;
	}

	public String getFirst_name() {
		return first_name;
	}

	public String getMiddle_name() {
		return middle_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public String getEmail() {
		return email;
	}

	public Long getMobile() {
		return mobile;
	}

	public String getFather_name() {
		return father_name;
	}

	public Long getFather_number() {
		return father_number;
	}

	public String getMother_name() {
		return mother_name;
	}

	public String getPresent_address() {
		return present_address;
	}

	public String getPermanent_address() {
		return permanent_address;
	}

	public String getMartial_status() {
		return martial_status;
	}

	public String getNationality() {
		return nationality;
	}

	public String getAadhar_file() {
		return aadhar_file;
	}

	public String getAdhartype() {
		return adhartype;
	}

	public byte[] getAadharbyte() {
		return aadharbyte;
	}

	public String getPancard_file() {
		return pancard_file;
	}

	public String getPantype() {
		return pantype;
	}

	public byte[] getPanbyte() {
		return panbyte;
	}

	public String getPassport_file() {
		return passport_file;
	}

	public String getPassporttype() {
		return passporttype;
	}

	public byte[] getPassportbyte() {
		return passportbyte;
	}

	public Candidate_details getCandidate_details() {
		return candidate_details;
	}

	public void setPersonal_details_id(Long personal_details_id) {
		this.personal_details_id = personal_details_id;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setMobile(Long mobile) {
		this.mobile = mobile;
	}

	public String getAlternate_number() {
		return alternate_number;
	}

	public void setAlternate_number(String alternate_number) {
		this.alternate_number = alternate_number;
	}

	public void setFather_name(String father_name) {
		this.father_name = father_name;
	}

	public void setFather_number(Long father_number) {
		this.father_number = father_number;
	}

	public void setMother_name(String mother_name) {
		this.mother_name = mother_name;
	}

	public void setPresent_address(String present_address) {
		this.present_address = present_address;
	}

	public void setPermanent_address(String permanent_address) {
		this.permanent_address = permanent_address;
	}

	public void setMartial_status(String martial_status) {
		this.martial_status = martial_status;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public void setAadhar_file(String aadhar_file) {
		this.aadhar_file = aadhar_file;
	}

	public void setAdhartype(String adhartype) {
		this.adhartype = adhartype;
	}

	public void setAadharbyte(byte[] aadharbyte) {
		this.aadharbyte = aadharbyte;
	}

	public void setPancard_file(String pancard_file) {
		this.pancard_file = pancard_file;
	}

	public void setPantype(String pantype) {
		this.pantype = pantype;
	}

	public void setPanbyte(byte[] panbyte) {
		this.panbyte = panbyte;
	}

	public void setPassport_file(String passport_file) {
		this.passport_file = passport_file;
	}

	public void setPassporttype(String passporttype) {
		this.passporttype = passporttype;
	}

	public void setPassportbyte(byte[] passportbyte) {
		this.passportbyte = passportbyte;
	}

	public void setCandidate_details(Candidate_details candidate_details) {
		this.candidate_details = candidate_details;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

}
