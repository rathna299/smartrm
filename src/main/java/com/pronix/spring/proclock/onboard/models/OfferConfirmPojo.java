package com.pronix.spring.proclock.onboard.models;

public class OfferConfirmPojo {

	private Long candidate_Id;
	
	private String date_of_joining;
	
	private String ctc;

	public Long getCandidate_Id() {
		return candidate_Id;
	}

	public String getDate_of_joining() {
		return date_of_joining;
	}

	public String getCtc() {
		return ctc;
	}

	public void setCandidate_Id(Long candidate_Id) {
		this.candidate_Id = candidate_Id;
	}

	public void setDate_of_joining(String date_of_joining) {
		this.date_of_joining = date_of_joining;
	}

	public void setCtc(String ctc) {
		this.ctc = ctc;
	}
	
	
}
