package com.pronix.spring.proclock.onboard.models;

public class OfferLetter {

	public String fullname;

	public String email;

	public String phone;

	public String salary;

	public String desingation;

	public String doj;

	public String getFullname() {
		return fullname;
	}

	public String getEmail() {
		return email;
	}

	public String getPhone() {
		return phone;
	}

	public String getSalary() {
		return salary;
	}

	public String getDesingation() {
		return desingation;
	}

	public String getDoj() {
		return doj;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

	public void setDesingation(String desingation) {
		this.desingation = desingation;
	}

	public void setDoj(String doj) {
		this.doj = doj;
	}

}
