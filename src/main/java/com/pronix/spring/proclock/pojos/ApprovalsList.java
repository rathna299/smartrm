package com.pronix.spring.proclock.pojos;

public class ApprovalsList {
	
	private String companyName;
	private String projectName;
	private String employeeName;
	private String week;
	private String noOfHoursWorked;
	private String approvedBy;
	private String approvedStatus;
	
	public ApprovalsList() {
		super();
	}
	
	public ApprovalsList(String companyName, String projectName, String employeeName, String week,
			String noOfHoursWorked, String approvedBy,String approvedStatus) {
		super();
		this.companyName = companyName;
		this.projectName = projectName;
		this.employeeName = employeeName;
		this.week = week;
		this.noOfHoursWorked = noOfHoursWorked;
		this.approvedBy = approvedBy;
		this.approvedStatus = approvedStatus;
	}
	
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getWeek() {
		return week;
	}
	public void setWeek(String week) {
		this.week = week;
	}
	public String getNoOfHoursWorked() {
		return noOfHoursWorked;
	}
	public void setNoOfHoursWorked(String noOfHoursWorked) {
		this.noOfHoursWorked = noOfHoursWorked;
	}
	public String getApprovedBy() {
		return approvedBy;
	}
	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}
	public String getApprovedStatus() {
		return approvedStatus;
	}
	public void setApprovedStatus(String approvedStatus) {
		this.approvedStatus = approvedStatus;
	}

}
