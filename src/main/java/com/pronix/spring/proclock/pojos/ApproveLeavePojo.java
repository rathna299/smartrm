package com.pronix.spring.proclock.pojos;

public class ApproveLeavePojo {

	
	public double Days;
	public String LeaveType;
	public String Status;
	private long userId;
	private long leaveApplId;
	public ApproveLeavePojo(double days, String leaveType, String status, long userId, long leaveApplId) {
		super();
		Days = days;
		LeaveType = leaveType;
		Status = status;
		this.userId = userId;
		this.leaveApplId = leaveApplId;
	}
	public double getDays() {
		return Days;
	}
	public void setDays(double days) {
		Days = days;
	}
	public String getLeaveType() {
		return LeaveType;
	}
	public void setLeaveType(String leaveType) {
		LeaveType = leaveType;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getLeaveApplId() {
		return leaveApplId;
	}
	public void setLeaveApplId(long leaveApplId) {
		this.leaveApplId = leaveApplId;
	}
	
	
	
	
	
}
