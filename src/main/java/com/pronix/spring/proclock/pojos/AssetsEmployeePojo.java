package com.pronix.spring.proclock.pojos;

import java.util.Date;

public class AssetsEmployeePojo {

	private Long assets_employees_id;
	private String assignToUserDate;
	private String assetsUserReturnDate;
	private String comment;	
	private long assetId;	
	private long userId;
	
	
	
	public Long getAssets_employees_id() {
		return assets_employees_id;
	}
	public void setAssets_employees_id(Long assets_employees_id) {
		this.assets_employees_id = assets_employees_id;
	}
	public String getAssignToUserDate() {
		return assignToUserDate;
	}
	public void setAssignToUserDate(String assignToUserDate) {
		this.assignToUserDate = assignToUserDate;
	}
	public String getAssetsUserReturnDate() {
		return assetsUserReturnDate;
	}
	public void setAssetsUserReturnDate(String assetsUserReturnDate) {
		this.assetsUserReturnDate = assetsUserReturnDate;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public long getAssetId() {
		return assetId;
	}
	public void setAssetId(long assetId) {
		this.assetId = assetId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	
	
}