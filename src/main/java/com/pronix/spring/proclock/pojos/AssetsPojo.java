package com.pronix.spring.proclock.pojos;

import java.util.Date;

public class AssetsPojo {

	private long assetId;
	private String productType;
	private String productNumber;
	private String productDesc;	
	private String productId;	
	private String productModel;	
	private String productMake;	
	private long userId;
	
	
	public AssetsPojo() {
		super();
	}
	
	
	
	public long getAssetId() {
		return assetId;
	}



	public void setAssetId(long assetId) {
		this.assetId = assetId;
	}



	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getProductNumber() {
		return productNumber;
	}



	public void setProductNumber(String productNumber) {
		this.productNumber = productNumber;
	}



	public String getProductDesc() {
		return productDesc;
	}



	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}



	public String getProductId() {
		return productId;
	}



	public void setProductId(String productId) {
		this.productId = productId;
	}



	public String getProductModel() {
		return productModel;
	}



	public void setProductModel(String productModel) {
		this.productModel = productModel;
	}



	public String getProductMake() {
		return productMake;
	}



	public void setProductMake(String productMake) {
		this.productMake = productMake;
	}
	

	public long getUserId() {
		return userId;
	}



	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	@Override
	public String toString() {
		return "AssetsPojo [assetId=" + assetId + ", productType=" + productType + ", productNumber=" + productNumber
				+ ", productDesc=" + productDesc + ", productId=" + productId + ", productModel=" + productModel
				+ ", productMake=" + productMake + ",  userId=" + userId + "]";
	}
	
}