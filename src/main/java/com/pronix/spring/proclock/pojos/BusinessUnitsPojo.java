package com.pronix.spring.proclock.pojos;

public class BusinessUnitsPojo {
	
	
	
	private long businessUnitsId;
	private String businessName;
	private String businessCode;
	private String businessStartDate;
	private String country;
	private String state;
	private String city;
	private Long companyId;
	
	
	
	
	public long getBusinessUnitsId() {
		return businessUnitsId;
	}
	public void setBusinessUnitsId(long businessUnitsId) {
		this.businessUnitsId = businessUnitsId;
	}
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public String getBusinessCode() {
		return businessCode;
	}
	public void setBusinessCode(String businessCode) {
		this.businessCode = businessCode;
	}
	public String getBusinessStartDate() {
		return businessStartDate;
	}
	public void setBusinessStartDate(String businessStartDate) {
		this.businessStartDate = businessStartDate;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	
	
	
	

	
	
}
