package com.pronix.spring.proclock.pojos;

public class CompanyPojo {

	private long companyId;
	private String companyName;
	private String country;
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String state;
	private long postalCode;
	private long phoneNumber;
	private String emailId;
	private String website;
	private String domain;
	private String startedDate;
	
	private long userId;
	
	


	public CompanyPojo() {
		super();
	}



	

	

	@Override
	public String toString() {
		return "CompanyPojo [companyId=" + companyId + ", companyName=" + companyName + ", country=" + country
				+ ", addressLine1=" + addressLine1 + ", addressLine2=" + addressLine2 + ", city=" + city + ", state="
				+ state + ", postalCode=" + postalCode + ", phoneNumber=" + phoneNumber + ", emailId=" + emailId
				+ ", website=" + website + ", domain=" + domain + ", startedDate=" + startedDate + ", userId=" + userId
				+ "]";
	}







	public CompanyPojo(long companyId, String companyName, String country, String addressLine1, String addressLine2,
			String city, String state, long postalCode, long phoneNumber, String emailId, String website, String domain,
			String startedDate, long userId) {
		super();
		this.companyId = companyId;
		this.companyName = companyName;
		this.country = country;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.city = city;
		this.state = state;
		this.postalCode = postalCode;
		this.phoneNumber = phoneNumber;
		this.emailId = emailId;
		this.website = website;
		this.domain = domain;
		this.startedDate = startedDate;
		this.userId = userId;
	}
	
	
	

	public CompanyPojo(String companyName, String country, String addressLine1, String addressLine2, String city,
			String state, long postalCode, long phoneNumber, String emailId, String website, String domain,
			String startedDate, long userId) {
		super();
		this.companyName = companyName;
		this.country = country;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.city = city;
		this.state = state;
		this.postalCode = postalCode;
		this.phoneNumber = phoneNumber;
		this.emailId = emailId;
		this.website = website;
		this.domain = domain;
		this.startedDate = startedDate;
		this.userId = userId;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public long getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(long postalCode) {
		this.postalCode = postalCode;
	}

	public long getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}



	public String getStartedDate() {
		return startedDate;
	}

	public void setStartedDate(String startedDate) {
		this.startedDate = startedDate;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

}
