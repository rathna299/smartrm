package com.pronix.spring.proclock.pojos;

import java.util.List;

public class DashboardPojo {
	
	private List<Long> module_id;
	
	private Long user_id;



	public List<Long> getModule_id() {
		return module_id;
	}

	public void setModule_id(List<Long> module_id) {
		this.module_id = module_id;
	}

	public Long getUser_id() {
		return user_id;
	}

	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}
	
	

}
