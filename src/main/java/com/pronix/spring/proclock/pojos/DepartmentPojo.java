package com.pronix.spring.proclock.pojos;

public class DepartmentPojo {
	
	
	
	private long departmentId;
	private String departmentName;
	private String departmentCode;
	private Long businessUnitsId;
	
	
	
	
	public long getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public String getDepartmentCode() {
		return departmentCode;
	}
	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}
	public Long getBusinessUnitsId() {
		return businessUnitsId;
	}
	public void setBusinessUnitsId(Long businessUnitsId) {
		this.businessUnitsId = businessUnitsId;
	}
	
	
	
	
	
	
	

	
	
}
