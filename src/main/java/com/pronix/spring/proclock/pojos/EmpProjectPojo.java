package com.pronix.spring.proclock.pojos;

import java.util.ArrayList;
import java.util.List;

public class EmpProjectPojo {
	
	private Long project_id;
	
	private String projectAssignedDate;
	
	private List<Long> employeeList = new ArrayList<>();

	
	
	public Long getProject_id() {
		return project_id;
	}

	public void setProject_id(Long project_id) {
		this.project_id = project_id;
	}
	

	public String getProjectAssignedDate() {
		return projectAssignedDate;
	}

	public void setProjectAssignedDate(String projectAssignedDate) {
		this.projectAssignedDate = projectAssignedDate;
	}

	public List<Long> getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(List<Long> employeeList) {
		this.employeeList = employeeList;
	}

}
