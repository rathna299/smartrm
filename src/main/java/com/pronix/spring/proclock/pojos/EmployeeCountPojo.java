package com.pronix.spring.proclock.pojos;

public class EmployeeCountPojo {

	
	private Long totalEmployees;
	
	private Long totalActiveEmployees;
	
	private Long totalInactiveEmployee;

	public Long getTotalEmployees() {
		return totalEmployees;
	}

	public void setTotalEmployees(Long totalEmployees) {
		this.totalEmployees = totalEmployees;
	}

	public Long getTotalActiveEmployees() {
		return totalActiveEmployees;
	}

	public void setTotalActiveEmployees(Long totalActiveEmployees) {
		this.totalActiveEmployees = totalActiveEmployees;
	}

	public Long getTotalInactiveEmployee() {
		return totalInactiveEmployee;
	}

	public void setTotalInactiveEmployee(Long totalInactiveEmployee) {
		this.totalInactiveEmployee = totalInactiveEmployee;
	}

	
	
	
	
}
