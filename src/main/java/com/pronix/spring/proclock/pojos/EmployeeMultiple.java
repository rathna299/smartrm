package com.pronix.spring.proclock.pojos;

public class EmployeeMultiple {
	private Long userId;
	private String firstName;
	private String lastName;
	private String emailId;
	private String phoneNo;
	private Long companyId;
//	private String companyName;
	private Long role_id;
	private Long businessUnit_id;
	private Long department_id;
	private Long job_title_id;
//	private List<Long> projectId;
//	private List<String> projectName;
//	private String vendorName;
	private Long managerId;
//	private List<String> managerName;
	private Long createdBy;
	private String dateOfJoining;

	public EmployeeMultiple() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EmployeeMultiple(Long userId, String firstName, String lastName, String emailId, String phoneNo,
			Long companyId, Long role_id, Long businessUnit_id, Long department_id, Long job_title_id, Long managerId,
			Long createdBy, String dateOfJoining) {
		super();
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.emailId = emailId;
		this.phoneNo = phoneNo;
		this.companyId = companyId;
		this.role_id = role_id;
		this.businessUnit_id = businessUnit_id;
		this.department_id = department_id;
		this.job_title_id = job_title_id;
		this.managerId = managerId;
		this.createdBy = createdBy;
		this.dateOfJoining = dateOfJoining;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Long getRole_id() {
		return role_id;
	}

	public void setRole_id(Long role_id) {
		this.role_id = role_id;
	}

	public Long getBusinessUnit_id() {
		return businessUnit_id;
	}

	public void setBusinessUnit_id(Long businessUnit_id) {
		this.businessUnit_id = businessUnit_id;
	}

	public Long getDepartment_id() {
		return department_id;
	}

	public void setDepartment_id(Long department_id) {
		this.department_id = department_id;
	}

	public Long getJob_title_id() {
		return job_title_id;
	}

	public void setJob_title_id(Long job_title_id) {
		this.job_title_id = job_title_id;
	}

	public Long getManagerId() {
		return managerId;
	}

	public void setManagerId(Long managerId) {
		this.managerId = managerId;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getDateOfJoining() {
		return dateOfJoining;
	}

	public void setDateOfJoining(String dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}


}
