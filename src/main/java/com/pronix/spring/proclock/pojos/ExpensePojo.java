package com.pronix.spring.proclock.pojos;

import java.time.LocalDateTime;
import java.util.Date;

public class ExpensePojo {

	private Long expenses_id;
	private String expensesName;

	private String expensesDate;

	private String expensesAmount;

	private String expensesAdvancedAmount;

	private String paymentReference;

	private String description;

	private String status;
	private String comment;

	private String categoryName;
	private String paymentType;

	private String projectName;
	private String userName;

	public Long getExpenses_id() {
		return expenses_id;
	}

	public void setExpenses_id(Long expenses_id) {
		this.expenses_id = expenses_id;
	}

	public String getExpensesName() {
		return expensesName;
	}

	public void setExpensesName(String expensesName) {
		this.expensesName = expensesName;
	}

	public String getExpensesDate() {
		return expensesDate;
	}

	public void setExpensesDate(String expensesDate) {
		this.expensesDate = expensesDate;
	}

	public String getExpensesAmount() {
		return expensesAmount;
	}

	public void setExpensesAmount(String expensesAmount) {
		this.expensesAmount = expensesAmount;
	}

	public String getExpensesAdvancedAmount() {
		return expensesAdvancedAmount;
	}

	public void setExpensesAdvancedAmount(String expensesAdvancedAmount) {
		this.expensesAdvancedAmount = expensesAdvancedAmount;
	}

	public String getPaymentReference() {
		return paymentReference;
	}

	public void setPaymentReference(String paymentReference) {
		this.paymentReference = paymentReference;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}