package com.pronix.spring.proclock.pojos;

public class HrOfferDashboardCount {
	
	
	private Long totalPending;
	
	private Long totalSubmitted;
	
	private Long totalApproved;
	
	private Long totalDecliened;

	

	public Long getTotalPending() {
		return totalPending;
	}

	public void setTotalPending(Long totalPending) {
		this.totalPending = totalPending;
	}

	public Long getTotalSubmitted() {
		return totalSubmitted;
	}

	public void setTotalSubmitted(Long totalSubmitted) {
		this.totalSubmitted = totalSubmitted;
	}

	public Long getTotalApproved() {
		return totalApproved;
	}

	public void setTotalApproved(Long totalApproved) {
		this.totalApproved = totalApproved;
	}

	public Long getTotalDecliened() {
		return totalDecliened;
	}

	public void setTotalDecliened(Long totalDecliened) {
		this.totalDecliened = totalDecliened;
	}
	
	

}
