package com.pronix.spring.proclock.pojos;

public class JobTitlesPojo {
	
	
	
	private long JobTitlesId;
	private String jobtitlecode;
	private String jobtitle;
	private String jobdescription;
	
	
	
	
	public long getJobTitlesId() {
		return JobTitlesId;
	}
	public void setJobTitlesId(long jobTitlesId) {
		JobTitlesId = jobTitlesId;
	}
	public String getJobtitlecode() {
		return jobtitlecode;
	}
	public void setJobtitlecode(String jobtitlecode) {
		this.jobtitlecode = jobtitlecode;
	}
	public String getJobtitle() {
		return jobtitle;
	}
	public void setJobtitle(String jobtitle) {
		this.jobtitle = jobtitle;
	}
	public String getJobdescription() {
		return jobdescription;
	}
	public void setJobdescription(String jobdescription) {
		this.jobdescription = jobdescription;
	}
	


	
	
}
