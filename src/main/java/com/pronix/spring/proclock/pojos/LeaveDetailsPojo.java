package com.pronix.spring.proclock.pojos;

public class LeaveDetailsPojo {
	
	
	private long leavedetailId;
	private long userId;
	private Integer year;
    private double sickLeaves;
	private double remainingSickleaves;
	private double utilizedsickLeaves;
	private double actualAnnualLeaves;
	private double remainingAnnualLeaves;
	private double utilisedAnnualLeaves;  
	private double lossOfPay;
	
	
	
	
	public long getLeavedetailId() {
		return leavedetailId;
	}
	public void setLeavedetailId(long leavedetailId) {
		this.leavedetailId = leavedetailId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public double getSickLeaves() {
		return sickLeaves;
	}
	public void setSickLeaves(double sickLeaves) {
		this.sickLeaves = sickLeaves;
	}
	public double getRemainingSickleaves() {
		return remainingSickleaves;
	}
	public void setRemainingSickleaves(double remainingSickleaves) {
		this.remainingSickleaves = remainingSickleaves;
	}
	public double getUtilizedsickLeaves() {
		return utilizedsickLeaves;
	}
	public void setUtilizedsickLeaves(double utilizedsickLeaves) {
		this.utilizedsickLeaves = utilizedsickLeaves;
	}
	public double getActualAnnualLeaves() {
		return actualAnnualLeaves;
	}
	public void setActualAnnualLeaves(double actualAnnualLeaves) {
		this.actualAnnualLeaves = actualAnnualLeaves;
	}
	public double getRemainingAnnualLeaves() {
		return remainingAnnualLeaves;
	}
	public void setRemainingAnnualLeaves(double remainingAnnualLeaves) {
		this.remainingAnnualLeaves = remainingAnnualLeaves;
	}
	public double getUtilisedAnnualLeaves() {
		return utilisedAnnualLeaves;
	}
	public void setUtilisedAnnualLeaves(double utilisedAnnualLeaves) {
		this.utilisedAnnualLeaves = utilisedAnnualLeaves;
	}
	public double getLossOfPay() {
		return lossOfPay;
	}
	public void setLossOfPay(double lossOfPay) {
		this.lossOfPay = lossOfPay;
	}
	
	
	
	

}
