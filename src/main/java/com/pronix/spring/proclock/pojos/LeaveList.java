package com.pronix.spring.proclock.pojos;

public class LeaveList {
	
	
	private Long pendingLeaves;
	private Long approvedLeaves;
	private Long declienedLeaves;
	private Long cancelled;
	
	
	public Long getPendingLeaves() {
		return pendingLeaves;
	}
	public void setPendingLeaves(Long pendingLeaves) {
		this.pendingLeaves = pendingLeaves;
	}
	public Long getApprovedLeaves() {
		return approvedLeaves;
	}
	public void setApprovedLeaves(Long approvedLeaves) {
		this.approvedLeaves = approvedLeaves;
	}
	public Long getDeclienedLeaves() {
		return declienedLeaves;
	}
	public void setDeclienedLeaves(Long declienedLeaves) {
		this.declienedLeaves = declienedLeaves;
	}
	public Long getCancelled() {
		return cancelled;
	}
	public void setCancelled(Long cancelled) {
		this.cancelled = cancelled;
	}
	
	
	
	

}
