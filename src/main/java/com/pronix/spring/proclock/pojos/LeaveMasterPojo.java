package com.pronix.spring.proclock.pojos;

public class LeaveMasterPojo {

	public String fromDate;
	public String toDate;
	public double days;
	public String leaveType;
	public String status;
	public String reason;
	private long userId;

	public LeaveMasterPojo() {
		super();
	}

	public LeaveMasterPojo(String fromDate, String toDate, double days, String leaveType, String status, String reason,
			long userId) {
		super();
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.days = days;
		this.leaveType = leaveType;
		this.status = status;
		this.reason = reason;
		this.userId = userId;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public double getDays() {
		return days;
	}

	public void setDays(double days) {
		this.days = days;
	}

	public String getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	



	


}
