package com.pronix.spring.proclock.pojos;

public class LeaveReport {
	
	private Long eid;
	private String name;
	private String fromdate;
	private String todate;
	private double numberofdays;
	private String leavetype;
	private String manager;
	private double remainingleaves;
	private double utilizedLeaves;
	private String designation;
	private String department;
	private String approvedBy;
	
	
	public Long getEid() {
		return eid;
	}
	public void setEid(Long eid) {
		this.eid = eid;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFromdate() {
		return fromdate;
	}
	public void setFromdate(String fromdate) {
		this.fromdate = fromdate;
	}
	public String getTodate() {
		return todate;
	}
	public void setTodate(String todate) {
		this.todate = todate;
	}

	public double getNumberofdays() {
		return numberofdays;
	}
	public void setNumberofdays(double numberofdays) {
		this.numberofdays = numberofdays;
	}
	public String getLeavetype() {
		return leavetype;
	}
	public void setLeavetype(String leavetype) {
		this.leavetype = leavetype;
	}
	public String getManager() {
		return manager;
	}
	public void setManager(String manager) {
		this.manager = manager;
	}
	
	public double getRemainingleaves() {
		return remainingleaves;
	}
	public void setRemainingleaves(double remainingleaves) {
		this.remainingleaves = remainingleaves;
	}
	
	public double getUtilizedLeaves() {
		return utilizedLeaves;
	}
	public void setUtilizedLeaves(double utilizedLeaves) {
		this.utilizedLeaves = utilizedLeaves;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getApprovedBy() {
		return approvedBy;
	}
	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}
	
	

}
