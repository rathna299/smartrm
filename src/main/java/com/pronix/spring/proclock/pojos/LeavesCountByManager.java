package com.pronix.spring.proclock.pojos;

public class LeavesCountByManager {
	
	private long active;
	
	private long onLeave;
	
	private long approved;
	
	private long pending;
	
	private long declined;

	public long getActive() {
		return active;
	}

	public void setActive(long active) {
		this.active = active;
	}

	public long getOnLeave() {
		return onLeave;
	}

	public void setOnLeave(long onLeave) {
		this.onLeave = onLeave;
	}

	public long getApproved() {
		return approved;
	}

	public void setApproved(long approved) {
		this.approved = approved;
	}

	public long getPending() {
		return pending;
	}

	public void setPending(long pending) {
		this.pending = pending;
	}

	public long getDeclined() {
		return declined;
	}

	public void setDeclined(long declined) {
		this.declined = declined;
	}
	
	

}
