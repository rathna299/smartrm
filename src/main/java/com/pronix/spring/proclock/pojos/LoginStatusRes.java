package com.pronix.spring.proclock.pojos;

import java.util.List;

import com.pronix.spring.proclock.models.DashboardContent;
import com.pronix.spring.proclock.models.Modules;

public class LoginStatusRes {
	
	private String status;
	private String statusCode;
	private String statusMsg;
	private Object userDetails;
	private String userRole;
	private List<Modules> module;
	private List<DashboardContent> dashboard_content;
	public LoginStatusRes() {
		super();
	}
	
	public LoginStatusRes(String status, String statusCode, String statusMsg, Object userDetails, String userRole) {
		super();
		this.status = status;
		this.statusCode = statusCode;
		this.statusMsg = statusMsg;
		this.userDetails = userDetails;
		this.userRole = userRole;
	}
	
	
	public LoginStatusRes(String status, String statusCode, String statusMsg, Object userDetails, String userRole,
			List<Modules> module) {
		super();
		this.status = status;
		this.statusCode = statusCode;
		this.statusMsg = statusMsg;
		this.userDetails = userDetails;
		this.userRole = userRole;
		this.module = module;
	}
	


	public LoginStatusRes(String status, String statusCode, String statusMsg, Object userDetails, String userRole,
			List<Modules> module, List<DashboardContent> dashboard_content) {
		super();
		this.status = status;
		this.statusCode = statusCode;
		this.statusMsg = statusMsg;
		this.userDetails = userDetails;
		this.userRole = userRole;
		this.module = module;
		this.dashboard_content = dashboard_content;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMsg() {
		return statusMsg;
	}
	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}
	public Object getUserDetails() {
		return userDetails;
	}
	public void setUserDetails(Object userDetails) {
		this.userDetails = userDetails;
	}
	public String getUserRole() {
		return userRole;
	}
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public List<Modules> getModule() {
		return module;
	}

	public void setModule(List<Modules> module) {
		this.module = module;
	}

	public List<DashboardContent> getDashboard_content() {
		return dashboard_content;
	}

	public void setDashboard_content(List<DashboardContent> dashboard_content) {
		this.dashboard_content = dashboard_content;
	}


	
	
}
