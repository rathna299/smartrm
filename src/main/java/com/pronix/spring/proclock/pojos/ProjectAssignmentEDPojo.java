package com.pronix.spring.proclock.pojos;

import com.pronix.spring.proclock.models.Project;
import com.pronix.spring.proclock.models.UserDetails;

public class ProjectAssignmentEDPojo {
	
	private Long project_employee_id;
	private boolean enable;
	private String projectAssignedDate;
	private long projectId;
	private long userId;
	
	
	
	public Long getProject_employee_id() {
		return project_employee_id;
	}
	public void setProject_employee_id(Long project_employee_id) {
		this.project_employee_id = project_employee_id;
	}
	public boolean isEnable() {
		return enable;
	}
	public void setEnable(boolean enable) {
		this.enable = enable;
	}
	public String getProjectAssignedDate() {
		return projectAssignedDate;
	}
	public void setProjectAssignedDate(String projectAssignedDate) {
		this.projectAssignedDate = projectAssignedDate;
	}
	public long getProjectId() {
		return projectId;
	}
	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	
	

}