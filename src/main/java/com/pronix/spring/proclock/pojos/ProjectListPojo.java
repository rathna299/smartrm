package com.pronix.spring.proclock.pojos;

import java.util.List;

public class ProjectListPojo {
	private List<Long> projectId;

	public List<Long> getProjectId() {
		return projectId;
	}

	public void setProjectId(List<Long> projectId) {
		this.projectId = projectId;
	}

	public ProjectListPojo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProjectListPojo(List<Long> projectId) {
		super();
		this.projectId = projectId;
	}
	
	
	
}
