package com.pronix.spring.proclock.pojos;

public class ProjectPojo {
	
	private Long projectId;
	private String projectName;
	private String projectCode;
	private String projectVendor;
	private String projectStartDate;
	private String description;
	private boolean enable;
	private String projectEndDate;
	private Long companyId;
	
	
	public ProjectPojo() {
		super();
	}
	
	
	

	public ProjectPojo(String projectName, String projectCode, String projectVendor, String projectStartDate,
			String description, String projectEndDate, Long companyId) {
		super();
		this.projectName = projectName;
		this.projectCode = projectCode;
		this.projectVendor = projectVendor;
		this.projectStartDate = projectStartDate;
		this.description = description;
		this.projectEndDate = projectEndDate;
		this.companyId = companyId;
	}


	public ProjectPojo(Long projectId, String projectName, String projectCode, String projectVendor,
			String projectStartDate, String description, String projectEndDate, Long companyId) {
		super();
		this.projectId = projectId;
		this.projectName = projectName;
		this.projectCode = projectCode;
		this.projectVendor = projectVendor;
		this.projectStartDate = projectStartDate;
		this.description = description;
		this.projectEndDate = projectEndDate;
		this.companyId = companyId;
	}




	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getProjectCode() {
		return projectCode;
	}
	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}
	public String getProjectVendor() {
		return projectVendor;
	}
	public void setProjectVendor(String projectVendor) {
		this.projectVendor = projectVendor;
	}
	public String getProjectStartDate() {
		return projectStartDate;
	}
	public void setProjectStartDate(String projectStartDate) {
		this.projectStartDate = projectStartDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public String getProjectEndDate() {
		return projectEndDate;
	}

	public void setProjectEndDate(String projectEndDate) {
		this.projectEndDate = projectEndDate;
	}

	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	
}
