package com.pronix.spring.proclock.pojos;

import java.util.List;

import com.pronix.spring.proclock.models.Modules;

public class RoleModulePojo {
	
	private String value;
	
	private String description;
	
	private List<Modules> modules;



	public RoleModulePojo(String value, String description) {
		super();
		this.value = value;
		this.description = description;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Modules> getModules() {
		return modules;
	}

	public void setModules(List<Modules> modules) {
		this.modules = modules;
	}
	
	
	
	
	


	
	
}
