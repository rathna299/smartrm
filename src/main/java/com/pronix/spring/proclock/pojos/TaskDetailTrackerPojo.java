package com.pronix.spring.proclock.pojos;

public class TaskDetailTrackerPojo {

	private Long teskdetailtrackerId;

	private String taskComment;

	private String fileName;

	private String fileType;

	private byte[] data;

//	private String priority;

//	private LocalDateTime insertDate;

//	private LocalDateTime updatedDate;
	
	private  double spentHrs;

	private Long taskstatusId;
	
	private Long taskdetailsId;
	
	private Long assignedTo;
	
	private Long resolvedBy;

	public Long getTeskdetailtrackerId() {
		return teskdetailtrackerId;
	}

	public String getTaskComment() {
		return taskComment;
	}

	public String getFileName() {
		return fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public byte[] getData() {
		return data;
	}
	
	

	public double getSpentHrs() {
		return spentHrs;
	}

	public void setSpentHrs(double spentHrs) {
		this.spentHrs = spentHrs;
	}

	public Long getTaskstatusId() {
		return taskstatusId;
	}

	public Long getTaskdetailsId() {
		return taskdetailsId;
	}

	public Long getAssignedTo() {
		return assignedTo;
	}

	public Long getResolvedBy() {
		return resolvedBy;
	}

	public void setTeskdetailtrackerId(Long teskdetailtrackerId) {
		this.teskdetailtrackerId = teskdetailtrackerId;
	}

	public void setTaskComment(String taskComment) {
		this.taskComment = taskComment;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public void setTaskstatusId(Long taskstatusId) {
		this.taskstatusId = taskstatusId;
	}

	public void setTaskdetailsId(Long taskdetailsId) {
		this.taskdetailsId = taskdetailsId;
	}

	public void setAssignedTo(Long assignedTo) {
		this.assignedTo = assignedTo;
	}

	public void setResolvedBy(Long resolvedBy) {
		this.resolvedBy = resolvedBy;
	}
	
	

			
}
