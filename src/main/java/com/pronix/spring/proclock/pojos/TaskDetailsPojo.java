package com.pronix.spring.proclock.pojos;

import java.io.Serializable;
import java.time.LocalDateTime;

public class TaskDetailsPojo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long taskdetailsId;

	private String taskName;

	private String taskDesc;

	private double estimatedHrs;

	private String fileName;

	private String fileType;

	private byte[] data;

	private String priority;
	private String taskReady;
	private String dueDate;

	private LocalDateTime insertDate;
	private LocalDateTime updatedDate;
	private String resolvedDate;
	private String peoplesInvolved;
	private Long resolvedBy;
	private Long modulesId;
	private Long tasktypeId;
	private Long projectId;
	private Long assignedTo;
	private Long createdBy;
	
	public Long getTaskdetailsId() {
		return taskdetailsId;
	}
	public void setTaskdetailsId(Long taskdetailsId) {
		this.taskdetailsId = taskdetailsId;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public String getTaskDesc() {
		return taskDesc;
	}
	public void setTaskDesc(String taskDesc) {
		this.taskDesc = taskDesc;
	}
	public double getEstimatedHrs() {
		return estimatedHrs;
	}
	public void setEstimatedHrs(double estimatedHrs) {
		this.estimatedHrs = estimatedHrs;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public byte[] getData() {
		return data;
	}
	public void setData(byte[] data) {
		this.data = data;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getTaskReady() {
		return taskReady;
	}
	public void setTaskReady(String taskReady) {
		this.taskReady = taskReady;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public LocalDateTime getInsertDate() {
		return insertDate;
	}
	public void setInsertDate(LocalDateTime insertDate) {
		this.insertDate = insertDate;
	}
	public LocalDateTime getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(LocalDateTime updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getResolvedDate() {
		return resolvedDate;
	}
	public void setResolvedDate(String resolvedDate) {
		this.resolvedDate = resolvedDate;
	}
	
	public String getPeoplesInvolved() {
		return peoplesInvolved;
	}
	public void setPeoplesInvolved(String peoplesInvolved) {
		this.peoplesInvolved = peoplesInvolved;
	}
	public Long getResolvedBy() {
		return resolvedBy;
	}
	public void setResolvedBy(Long resolvedBy) {
		this.resolvedBy = resolvedBy;
	}
	public Long getModulesId() {
		return modulesId;
	}
	public void setModulesId(Long modulesId) {
		this.modulesId = modulesId;
	}
	public Long getTasktypeId() {
		return tasktypeId;
	}
	public void setTasktypeId(Long tasktypeId) {
		this.tasktypeId = tasktypeId;
	}
	public Long getProjectId() {
		return projectId;
	}
	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}
	public Long getAssignedTo() {
		return assignedTo;
	}
	public void setAssignedTo(Long assignedTo) {
		this.assignedTo = assignedTo;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	
	
}

