package com.pronix.spring.proclock.pojos;

public class UpdateRoleAndModulePojo {

	private Long role_module_id;
	
	private boolean enable;

	public Long getRole_module_id() {
		return role_module_id;
	}

	public void setRole_module_id(Long role_module_id) {
		this.role_module_id = role_module_id;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}
	
	
	
}
