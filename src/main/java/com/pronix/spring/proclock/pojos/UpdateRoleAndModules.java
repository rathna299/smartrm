package com.pronix.spring.proclock.pojos;

import java.util.List;

public class UpdateRoleAndModules {
	private Long role_id;

	private String role_value;

	private String role_description;
	
	List<UpdateRoleAndModulePojo> updateRoleModulePojo;

	public Long getRole_id() {
		return role_id;
	}

	public void setRole_id(Long role_id) {
		this.role_id = role_id;
	}

	public String getRole_value() {
		return role_value;
	}

	public void setRole_value(String role_value) {
		this.role_value = role_value;
	}

	public String getRole_description() {
		return role_description;
	}

	public void setRole_description(String role_description) {
		this.role_description = role_description;
	}

	public List<UpdateRoleAndModulePojo> getUpdateRoleModulePojo() {
		return updateRoleModulePojo;
	}

	public void setUpdateRoleModulePojo(List<UpdateRoleAndModulePojo> updateRoleModulePojo) {
		this.updateRoleModulePojo = updateRoleModulePojo;
	}
	

}
