package com.pronix.spring.proclock.pojos;

public class UserDashboardData {
	
	private String joiningDate;
	private long countOfPendingTimesheets;
	private long countOfAcceptedTimesheets;
	private long countOfDeclinedTimesheets;
	
	public UserDashboardData() {
		super();
	}
	
	public UserDashboardData(String joiningDate, long countOfPendingTimesheets, long countOfAcceptedTimesheets,
			long countOfRejectedTimesheets) {
		super();
		this.joiningDate = joiningDate;
		this.countOfPendingTimesheets = countOfPendingTimesheets;
		this.countOfAcceptedTimesheets = countOfAcceptedTimesheets;
		this.countOfDeclinedTimesheets = countOfDeclinedTimesheets;
	}
	
	public long getCountOfPendingTimesheets() {
		return countOfPendingTimesheets;
	}
	public void setCountOfPendingTimesheets(long countOfPendingTimesheets) {
		this.countOfPendingTimesheets = countOfPendingTimesheets;
	}
	public long getCountOfAcceptedTimesheets() {
		return countOfAcceptedTimesheets;
	}
	public void setCountOfAcceptedTimesheets(long countOfAcceptedTimesheets) {
		this.countOfAcceptedTimesheets = countOfAcceptedTimesheets;
	}
	
	public long getCountOfDeclinedTimesheets() {
		return countOfDeclinedTimesheets;
	}

	public void setCountOfDeclinedTimesheets(long countOfDeclinedTimesheets) {
		this.countOfDeclinedTimesheets = countOfDeclinedTimesheets;
	}

	public String getJoiningDate() {
		return joiningDate;
	}
	public void setJoiningDate(String joiningDate) {
		this.joiningDate = joiningDate;
	}
	
}
