package com.pronix.spring.proclock.pojos;

public class UserDetailsPojo {
	
	private long userId;
	private String firstName;
	private String lastName;
	private String userName;
	private String emailId;
	private String phoneNo;
	private String dateOfJoining;
	
	public UserDetailsPojo() {
		super();
	}

	//This Constructor is called from the Registration url for the admin 
	public UserDetailsPojo(String firstName, String lastName, String userName, String emailId, String phoneNo,String dateOfJoining) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.userName = userName;
		this.emailId = emailId;
		this.phoneNo = phoneNo;
		this.dateOfJoining = dateOfJoining;
	}
	
	public UserDetailsPojo(String firstName, String lastName, String emailId, String phoneNo,String dateOfJoining) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.emailId = emailId;
		this.phoneNo = phoneNo;
		this.dateOfJoining = dateOfJoining;
	}
	
	public UserDetailsPojo(long userId, String firstName, String lastName, String phoneNo) {
		super();
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNo = phoneNo;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getDateOfJoining() {
		return dateOfJoining;
	}

	public void setDateOfJoining(String dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}
	
	

}
