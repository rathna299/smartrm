package com.pronix.spring.proclock.pojos;

public class UserOnboardCountPojo {
	
	
	
	
	private Long pending;
	private Long approved;
	private Long declined;
	private Long submitted;
	
	
	
	public Long getPending() {
		return pending;
	}
	public void setPending(Long pending) {
		this.pending = pending;
	}
	
	public Long getApproved() {
		return approved;
	}
	public void setApproved(Long approved) {
		this.approved = approved;
	}
	
	public Long getDeclined() {
		return declined;
	}
	public void setDeclined(Long declined) {
		this.declined = declined;
	}
	
	public Long getSubmitted() {
		return submitted;
	}
	public void setSubmitted(Long submitted) {
		this.submitted = submitted;
	}
	
	
	
	

}
