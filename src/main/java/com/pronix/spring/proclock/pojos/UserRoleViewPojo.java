package com.pronix.spring.proclock.pojos;

import java.util.List;

import com.pronix.spring.proclock.models.RefRoles;
import com.pronix.spring.proclock.models.UserDetails;

public class UserRoleViewPojo {
	
	private List<UserDetails> userDetailsList;
	
	private List<RefRoles> rolesList;
	
	

	public UserRoleViewPojo(List<UserDetails> userDetailsList, List<RefRoles> rolesList) {
		super();
		this.userDetailsList = userDetailsList;
		this.rolesList = rolesList;
	}

	public List<UserDetails> getUserDetailsList() {
		return userDetailsList;
	}

	public void setUserDetailsList(List<UserDetails> userDetailsList) {
		this.userDetailsList = userDetailsList;
	}

	public List<RefRoles> getRolesList() {
		return rolesList;
	}

	public void setRolesList(List<RefRoles> rolesList) {
		this.rolesList = rolesList;
	}
	
	
	

}
