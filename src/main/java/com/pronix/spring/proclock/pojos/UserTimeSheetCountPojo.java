package com.pronix.spring.proclock.pojos;

public class UserTimeSheetCountPojo {
	
	
	
	
	private Long pending;
	private Long approved;
	private Long declined;
	private Long semifilled;
	
	
	
	public Long getPending() {
		return pending;
	}
	public void setPending(Long pending) {
		this.pending = pending;
	}
	public Long getApproved() {
		return approved;
	}
	public void setApproved(Long approved) {
		this.approved = approved;
	}
	public Long getDeclined() {
		return declined;
	}
	public void setDeclined(Long declined) {
		this.declined = declined;
	}
	public Long getSemifilled() {
		return semifilled;
	}
	public void setSemifilled(Long semifilled) {
		this.semifilled = semifilled;
	}
	
	
	

}
