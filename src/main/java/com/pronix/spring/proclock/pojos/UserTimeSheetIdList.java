package com.pronix.spring.proclock.pojos;

import java.util.List;

public class UserTimeSheetIdList {
	private List<Long> UserTimeSheetId;

	public UserTimeSheetIdList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public List<Long> getUserTimeSheetId() {
		return UserTimeSheetId;
	}

	public void setUserTimeSheetId(List<Long> userTimeSheetId) {
		UserTimeSheetId = userTimeSheetId;
	}

}
