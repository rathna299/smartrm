package com.pronix.spring.proclock.pojos;

public class UserTimeSheetIdListPojo {
	private Long[] userTimeSheetId;

	public UserTimeSheetIdListPojo() {
		super();
	}

	public UserTimeSheetIdListPojo(Long[] userTimeSheetId) {
		super();
		this.userTimeSheetId = userTimeSheetId;
	}

	public Long[] getUserTimeSheetId() {
		return userTimeSheetId;
	}

	public void setUserTimeSheetId(Long[] userTimeSheetId) {
		this.userTimeSheetId = userTimeSheetId;
	}

	
}
