package com.pronix.spring.proclock.servicerequestModel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.pronix.spring.proclock.models.UserDetails;

@Entity
@Table(name = "department_executors")
public class DepartmentExecutors {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long department_executors_id;

	@ManyToOne
	@JoinColumn(name = "service_department_id", referencedColumnName = "service_department_id")
	private ServiceDepartment serviceDepartment;

	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "user_id")
	private UserDetails executors;

	public DepartmentExecutors() {

	}

	
	public DepartmentExecutors(ServiceDepartment serviceDepartment, UserDetails executors) {
		super();
		this.serviceDepartment = serviceDepartment;
		this.executors = executors;
	}


	public Long getDepartment_executors_id() {
		return department_executors_id;
	}

	public void setDepartment_executors_id(Long department_executors_id) {
		this.department_executors_id = department_executors_id;
	}

	public ServiceDepartment getServiceDepartment() {
		return serviceDepartment;
	}

	public void setServiceDepartment(ServiceDepartment serviceDepartment) {
		this.serviceDepartment = serviceDepartment;
	}

	public UserDetails getExecutors() {
		return executors;
	}

	public void setExecutors(UserDetails executors) {
		this.executors = executors;
	}

}
