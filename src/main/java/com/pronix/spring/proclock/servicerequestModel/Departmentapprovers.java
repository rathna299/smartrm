package com.pronix.spring.proclock.servicerequestModel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.pronix.spring.proclock.models.UserDetails;

@Entity
@Table(name = "department_approvers")
public class Departmentapprovers {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long department_approvers_id;
	
	@ManyToOne
	@JoinColumn(name = "service_department_id",referencedColumnName = "service_department_id")
	private ServiceDepartment serviceDepartment;
	
	@ManyToOne
	@JoinColumn(name = "user_id",referencedColumnName = "user_id")
	private UserDetails approvers;
	
	public Departmentapprovers() {
		
	}
	

	public Departmentapprovers(ServiceDepartment serviceDepartment, UserDetails approvers) {
		super();
		this.serviceDepartment = serviceDepartment;
		this.approvers = approvers;
	}


	public Long getDepartment_approvers_id() {
		return department_approvers_id;
	}

	public void setDepartment_approvers_id(Long department_approvers_id) {
		this.department_approvers_id = department_approvers_id;
	}

	public ServiceDepartment getServiceDepartment() {
		return serviceDepartment;
	}

	public void setServiceDepartment(ServiceDepartment serviceDepartment) {
		this.serviceDepartment = serviceDepartment;
	}

	public UserDetails getApprovers() {
		return approvers;
	}

	public void setApprovers(UserDetails approvers) {
		this.approvers = approvers;
	}

	
	
}
