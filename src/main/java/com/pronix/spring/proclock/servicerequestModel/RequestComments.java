package com.pronix.spring.proclock.servicerequestModel;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "request_comments")
public class RequestComments {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long comment_id;

	@ManyToOne
	@JoinColumn(name = "service_request_id", referencedColumnName = "service_request_id")
	private ServiceRequest serviceRequest;

	private String comment;

	private Long commenter_id;

	private String commentername;

	private LocalDateTime commented_on;

	public RequestComments() {

	}
	

	public RequestComments(ServiceRequest serviceRequest, String comment, Long commenter_id, String commentername,
			LocalDateTime commented_on) {
		super();
		this.serviceRequest = serviceRequest;
		this.comment = comment;
		this.commenter_id = commenter_id;
		this.commentername = commentername;
		this.commented_on = commented_on;
	}


	public Long getComment_id() {
		return comment_id;
	}

	public void setComment_id(Long comment_id) {
		this.comment_id = comment_id;
	}

	public ServiceRequest getServiceRequest() {
		return serviceRequest;
	}

	public void setServiceRequest(ServiceRequest serviceRequest) {
		this.serviceRequest = serviceRequest;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Long getCommenter_id() {
		return commenter_id;
	}

	public void setCommenter_id(Long commenter_id) {
		this.commenter_id = commenter_id;
	}

	public String getCommentername() {
		return commentername;
	}

	public void setCommentername(String commentername) {
		this.commentername = commentername;
	}

	public LocalDateTime getCommented_on() {
		return commented_on;
	}

	public void setCommented_on(LocalDateTime commented_on) {
		this.commented_on = commented_on;
	}

}
