package com.pronix.spring.proclock.servicerequestModel;

import java.util.List;

public class RequestListPojo {

	private ServiceRequest request;

	private RequestTracker requestTracker;

	private List<RequestComments> requestComments;

	public ServiceRequest getRequest() {
		return request;
	}

	public void setRequest(ServiceRequest request) {
		this.request = request;
	}

	public RequestTracker getRequestTracker() {
		return requestTracker;
	}

	public void setRequestTracker(RequestTracker requestTracker) {
		this.requestTracker = requestTracker;
	}

	public List<RequestComments> getRequestComments() {
		return requestComments;
	}

	public void setRequestComments(List<RequestComments> requestComments) {
		this.requestComments = requestComments;
	}

}
