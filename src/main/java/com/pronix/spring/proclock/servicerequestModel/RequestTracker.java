package com.pronix.spring.proclock.servicerequestModel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "request_tracker")
public class RequestTracker {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long request_tracker_id;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "service_request_id", referencedColumnName = "service_request_id")
	private ServiceRequest serviceRequest;

	private Long approver_one_id;

	private String approver_one_status;

	private String approver_one_approvedon;

	private Long approver_two_id;

	private String approver_two_status;

	private String approver_two_approvedon;

	private Long executor_id;

	private String request_status;

	private String executor_updatedon;
	
	private Long status_identity;

	public RequestTracker() {
	}

	public RequestTracker(ServiceRequest serviceRequest, Long approver_one_id, String approver_one_status,Long status_identity) {
		super();
		this.serviceRequest = serviceRequest;
		this.approver_one_id = approver_one_id;
		this.approver_one_status = approver_one_status;
		this.status_identity = status_identity;
	}

	public Long getRequest_tracker_id() {
		return request_tracker_id;
	}

	public void setRequest_tracker_id(Long request_tracker_id) {
		this.request_tracker_id = request_tracker_id;
	}

	public ServiceRequest getServiceRequest() {
		return serviceRequest;
	}

	public void setServiceRequest(ServiceRequest serviceRequest) {
		this.serviceRequest = serviceRequest;
	}

	public Long getApprover_one_id() {
		return approver_one_id;
	}

	public void setApprover_one_id(Long approver_one_id) {
		this.approver_one_id = approver_one_id;
	}

	public String getApprover_one_status() {
		return approver_one_status;
	}

	public void setApprover_one_status(String approver_one_status) {
		this.approver_one_status = approver_one_status;
	}

	public String getApprover_one_approvedon() {
		return approver_one_approvedon;
	}

	public void setApprover_one_approvedon(String approver_one_approvedon) {
		this.approver_one_approvedon = approver_one_approvedon;
	}

	public Long getApprover_two_id() {
		return approver_two_id;
	}

	public void setApprover_two_id(Long approver_two_id) {
		this.approver_two_id = approver_two_id;
	}

	public String getApprover_two_status() {
		return approver_two_status;
	}

	public void setApprover_two_status(String approver_two_status) {
		this.approver_two_status = approver_two_status;
	}

	public String getApprover_two_approvedon() {
		return approver_two_approvedon;
	}

	public void setApprover_two_approvedon(String approver_two_approvedon) {
		this.approver_two_approvedon = approver_two_approvedon;
	}

	public Long getExecutor_id() {
		return executor_id;
	}

	public void setExecutor_id(Long executor_id) {
		this.executor_id = executor_id;
	}

	public String getRequest_status() {
		return request_status;
	}

	public void setRequest_status(String request_status) {
		this.request_status = request_status;
	}

	public String getExecutor_updatedon() {
		return executor_updatedon;
	}

	public void setExecutor_updatedon(String executor_updatedon) {
		this.executor_updatedon = executor_updatedon;
	}

	public Long getStatus_identity() {
		return status_identity;
	}

	public void setStatus_identity(Long status_identity) {
		this.status_identity = status_identity;
	}

	
}
