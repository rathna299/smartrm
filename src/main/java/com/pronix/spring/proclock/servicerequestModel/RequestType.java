package com.pronix.spring.proclock.servicerequestModel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "request_type")
public class RequestType {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long requesttype_id;

	@Size(min = 2,max = 25,message = "name require minimume 2 charactors and max upto 25 charactors")
	private String request_name;

	private String request_description;

	@ManyToOne
	@JoinColumn(name = "service_department_id", referencedColumnName = "service_department_id")
	private ServiceDepartment serviceDepartment;

	public RequestType() {

	}

	public Long getRequesttype_id() {
		return requesttype_id;
	}

	public void setRequesttype_id(Long requesttype_id) {
		this.requesttype_id = requesttype_id;
	}

	public String getRequest_name() {
		return request_name;
	}

	public void setRequest_name(String request_name) {
		this.request_name = request_name;
	}

	public String getRequest_description() {
		return request_description;
	}

	public void setRequest_description(String request_description) {
		this.request_description = request_description;
	}

	public ServiceDepartment getServiceDepartment() {
		return serviceDepartment;
	}

	public void setServiceDepartment(ServiceDepartment serviceDepartment) {
		this.serviceDepartment = serviceDepartment;
	}

}
