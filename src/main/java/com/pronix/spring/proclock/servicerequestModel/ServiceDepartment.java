package com.pronix.spring.proclock.servicerequestModel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "service_department")
public class ServiceDepartment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long service_department_id;
	@Size(min = 2, max = 25)
	private String department_name;

	private String description;

	public ServiceDepartment() {

	}

	public Long getService_department_id() {
		return service_department_id;
	}

	public void setService_department_id(Long service_department_id) {
		this.service_department_id = service_department_id;
	}

	public String getDepartment_name() {
		return department_name;
	}

	public void setDepartment_name(String department_name) {
		this.department_name = department_name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
