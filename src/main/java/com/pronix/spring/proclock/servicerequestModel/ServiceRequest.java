package com.pronix.spring.proclock.servicerequestModel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "service_request")
public class ServiceRequest {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long service_request_id;

	private String employee_name;

	private String email;

	private String phone;

	private Long emp_id;

	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "service_department_id", referencedColumnName = "service_department_id")
	private ServiceDepartment department;

	@ManyToOne
	@JoinColumn(name = "requesttype_id", referencedColumnName = "requesttype_id")
	private RequestType requestType;

	private String ticketId;

	private String description;

	private Date raisedOn;

	private String priority;

	private String status;

	private Date closedDate;

	public ServiceRequest() {

	}

	
	public ServiceRequest(String employee_name, String email, String phone, Long emp_id, ServiceDepartment department,
			RequestType requestType, String description, Date raisedOn, String priority, String status,String ticketId) {
		super();
		this.employee_name = employee_name;
		this.email = email;
		this.phone = phone;
		this.emp_id = emp_id;
		this.department = department;
		this.requestType = requestType;
		this.description = description;
		this.raisedOn = raisedOn;
		this.priority = priority;
		this.status = status;
		this.ticketId = ticketId;
	}


	public Long getService_request_id() {
		return service_request_id;
	}

	public void setService_request_id(Long service_request_id) {
		this.service_request_id = service_request_id;
	}

	public ServiceDepartment getDepartment() {
		return department;
	}

	public void setDepartment(ServiceDepartment department) {
		this.department = department;
	}

	public RequestType getRequestType() {
		return requestType;
	}

	public void setRequestType(RequestType requestType) {
		this.requestType = requestType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getRaisedOn() {
		return raisedOn;
	}

	public void setRaisedOn(Date raisedOn) {
		this.raisedOn = new Date();
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getClosedDate() {
		return closedDate;
	}

	public void setClosedDate(Date closedDate) {
		this.closedDate = closedDate;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = "TK00" + new SimpleDateFormat("ddMMyyyySSS").format(Calendar.getInstance().getTime());
	}

	public String getEmployee_name() {
		return employee_name;
	}

	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Long getEmp_id() {
		return emp_id;
	}

	public void setEmp_id(Long emp_id) {
		this.emp_id = emp_id;
	}

}
