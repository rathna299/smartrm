package com.pronix.spring.proclock.servicerequestModel;

public class TicketApprovalOnePojo {

	private Long tracker_id;

	private Long approver_id;
	private String Status;

	private Long sendType;
	//ActionUserID is id of executor or approver this just based on sendType. if sendType is 1 then it is executor
	private Long ActionUserid;
	
	private String comment;

	public Long getTracker_id() {
		return tracker_id;
	}

	public void setTracker_id(Long tracker_id) {
		this.tracker_id = tracker_id;
	}

	public Long getApprover_id() {
		return approver_id;
	}

	public void setApprover_id(Long approver_id) {
		this.approver_id = approver_id;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public Long getSendType() {
		return sendType;
	}

	public void setSendType(Long sendType) {
		this.sendType = sendType;
	}

	public Long getActionUserid() {
		return ActionUserid;
	}

	public void setActionUserid(Long actionUserid) {
		ActionUserid = actionUserid;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}
