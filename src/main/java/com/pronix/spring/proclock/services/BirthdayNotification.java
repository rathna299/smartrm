package com.pronix.spring.proclock.services;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.pronix.spring.proclock.dao.UserPersonalInfoDao;
import com.pronix.spring.proclock.models.UserPersonalInfo;

@Service
public class BirthdayNotification {

	@Autowired
	private UserPersonalInfoDao userPersonalInfoDao;

	private static final Logger log = LoggerFactory.getLogger(BirthdayNotification.class);
	/*
	 * @Scheduled(cron = "0/30 * * * * ?") public void run() {
	 * log.info("LeaveIncrementService Current time is :: " +
	 * Calendar.getInstance().getTime());
	 * //System.out.println("LeaveIncrementService Current time is :: " +
	 * Calendar.getInstance().getTime()); }
	 */

	// @Scheduled(cron = "[Seconds] [Minutes] [Hours] [Day of month] [Month] [Day of
	// week] [Year]")
	//@Scheduled(cron = "0/30 * * * * ?") // every 30 Seconds
	// @Scheduled(cron = "0 * * ? * *") //every minute
	 @Scheduled(cron = "0 0 0 1 * ?") // every month 1st day at 12 AM ,(change 3rd
	// position hour)
	// @Scheduled(cron = "0 6 55 13 * ?")
	// @Scheduled(cron = "0 1 1 ? * *")
	//@Scheduled(cron = "0 0 17 * * *")
	public boolean dobnotification() {
		log.info("dob Current time is :: " + Calendar.getInstance().getTime());
		List<UserPersonalInfo> userInfoList = null;
		DateFormat df = new SimpleDateFormat("MM-dd");
		Date dateobj = new Date();
		String dob = df.format(dateobj);
		System.out.println("dob MM DD : "+dob);
		userInfoList = userPersonalInfoDao.findByDobEndsWith(dob);
		
		for (UserPersonalInfo userPersonalInfo : userInfoList) {
			
			
			System.out.println("list:"+userPersonalInfo.getDob()+" mobile:"+userPersonalInfo.getMobileNumber());
		}

		return true;
	}
}
