package com.pronix.spring.proclock.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pronix.spring.proclock.dao.LeaveMasterDAO;
import com.pronix.spring.proclock.dao.UserDetailsDAO;
import com.pronix.spring.proclock.pojos.EmployeeCountPojo;
import com.pronix.spring.proclock.pojos.LeaveList;

@Service
public class DashboardService {

	@Autowired
	private UserDetailsDAO userDetailsDAO;

	@Autowired
	private LeaveMasterDAO leaveMasterDAO;

	public Object EmployeeCount() {
		EmployeeCountPojo count = new EmployeeCountPojo();
		long totalActive = userDetailsDAO.findbyCount(true);
		long totalInactive = userDetailsDAO.findbyCount(false);
		long totalEmployee = totalActive + totalInactive;
		count.setTotalEmployees(totalEmployee);
		count.setTotalActiveEmployees(totalActive);
		count.setTotalInactiveEmployee(totalInactive);
		return count;
	}

	public Object getByLeaveStatus(Long userId) {

		LeaveList leaveList = new LeaveList();
		Long pending = leaveMasterDAO.findByStatus(userId, "Pending");
		leaveList.setPendingLeaves(pending);

		Long approved = leaveMasterDAO.findByStatus(userId, "Approved");
		leaveList.setApprovedLeaves(approved);

		Long decliened = leaveMasterDAO.findByStatus(userId, "Declined");
		leaveList.setDeclienedLeaves(decliened);
		Long cancelled = leaveMasterDAO.findByStatus(userId, "Cancelled");
		leaveList.setCancelled(cancelled);

		return leaveList;
	}

	public Object getByLeaveStatusManagerList(Long userId) {
		Map<String, Long> leaveMap = new HashMap<>();
		LeaveList leaveList = new LeaveList();

		List<Object[]> list = leaveMasterDAO.findByManagerId2(userId);

		for (Object[] object : list) {
			leaveMap.put(object[0].toString(), Long.parseLong(object[1].toString()));
		}

		//leaveList.setPendingLeaves(leaveMap.containsKey("Pending") ? leaveMap.get("Pending") : 0);
		//leaveList.setApprovedLeaves(leaveMap.containsKey("Approved") ? leaveMap.get("Approved") : 0);
		//leaveList.setDeclienedLeaves(leaveMap.containsKey("Decliened") ? leaveMap.get("Decliened") : 0);
		return leaveList;
	}
}