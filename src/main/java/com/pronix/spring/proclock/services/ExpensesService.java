package com.pronix.spring.proclock.services;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.pronix.spring.proclock.dao.ProjectDAO;
import com.pronix.spring.proclock.dao.UserDetailsDAO;
import com.pronix.spring.proclock.expenses.DAO.CategoryDAO;
import com.pronix.spring.proclock.expenses.DAO.ExpensesDAO;
import com.pronix.spring.proclock.expenses.DAO.PaymentDAO;
import com.pronix.spring.proclock.expenses.DAO.TripDAO;
import com.pronix.spring.proclock.expenses.models.AddTrip;
import com.pronix.spring.proclock.expenses.models.Category;
import com.pronix.spring.proclock.expenses.models.Expenses;
import com.pronix.spring.proclock.expenses.models.PaymentMode;
import com.pronix.spring.proclock.models.Project;
import com.pronix.spring.proclock.models.UserDetails;
import com.pronix.spring.proclock.pojos.ExpensePojo;
import com.pronix.spring.proclock.pojos.StatusRes;

@Service
public class ExpensesService {

	@Autowired
	private ExpensesDAO expensesDAO;

	@Autowired
	private TripDAO tripDAO;

	@Autowired
	private CategoryDAO categoryDAO;

	@Autowired
	private PaymentDAO paymentDAO;

	@Autowired
	private ProjectDAO projectDAO;

	@Autowired
	private UserDetailsDAO userDetailsDAO;

	public ResponseEntity<Object> addTripDetails(AddTrip addTrip) {
		Optional<AddTrip> checkTripDetails = tripDAO.findByTripName(addTrip.getTripName());

		if (!checkTripDetails.isPresent()) {
			AddTrip tripDetails = tripDAO.save(addTrip);

			tripDetails.setTripName(addTrip.getTripName());
			tripDetails.setTripFromDate(addTrip.getTripFromDate());
			tripDetails.setTripToDate(addTrip.getTripToDate());
			tripDetails.setDescription(addTrip.getDescription());

			tripDAO.save(tripDetails);

			return new ResponseEntity<>(new StatusRes("Success", "000", "Trip Name added Successfully"), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new StatusRes("Failed", "007", "Trip Name already existed"), HttpStatus.OK);
		}
	}

	public ResponseEntity<Object> updateAddTrip(@Valid AddTrip addTrip) {
		Optional<AddTrip> checkTrip = tripDAO.findById(addTrip.getTrip_id());
		Optional<AddTrip> checkTripName = tripDAO.findByTripName(addTrip.getTripName());

		if (checkTrip.isPresent()) {
			if (checkTripName.isPresent() && checkTrip.get().equals(checkTripName.get())
					|| !checkTripName.isPresent()) {

				checkTrip.get().setTripName(addTrip.getTripName());
				checkTrip.get().setTripFromDate(addTrip.getTripFromDate());
				checkTrip.get().setTripToDate(addTrip.getTripToDate());
				checkTrip.get().setDescription(addTrip.getDescription());
				tripDAO.save(checkTrip.get());

				return new ResponseEntity<>(new StatusRes("Success", "000", "Record updated successfully"),
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new StatusRes("Failed", "007", "Trip Name already Existed"), HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<>(new StatusRes("Failed", "007", "Trip details not found"), HttpStatus.OK);
		}
	}

	public ResponseEntity<Object> getTripList() {
		return new ResponseEntity<>(tripDAO.findAll(), HttpStatus.OK);
	}

	public ResponseEntity<Object> addCategory(Category category) {
		Optional<Category> checkCategoryDetails = categoryDAO.findByCategoryName(category.getCategoryName());

		if (!checkCategoryDetails.isPresent()) {
			Category categoryDetails = categoryDAO.save(category);

			categoryDetails.setCategoryName(category.getCategoryName());
			categoryDetails.setDescription(category.getDescription());

			categoryDAO.save(categoryDetails);

			return new ResponseEntity<>(new StatusRes("Success", "000", "Category added Successfully"), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new StatusRes("Failed", "007", "Category already existed"), HttpStatus.OK);
		}
	}

	public ResponseEntity<Object> updateCategory(@Valid Category category) {
		Optional<Category> checkCategory = categoryDAO.findById(category.getCategory_id());
		Optional<Category> checkCategoryName = categoryDAO.findByCategoryName(category.getCategoryName());

		if (checkCategory.isPresent()) {
			if (checkCategoryName.isPresent() && checkCategory.get().equals(checkCategoryName.get())
					|| !checkCategoryName.isPresent()) {

				checkCategory.get().setCategoryName(category.getCategoryName());
				checkCategory.get().setDescription(category.getDescription());
				categoryDAO.save(checkCategory.get());

				return new ResponseEntity<>(new StatusRes("Success", "000", "Record updated successfully"),
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new StatusRes("Failed", "007", "Category Name already Existed"),
						HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<>(new StatusRes("Failed", "007", "Category details not found"), HttpStatus.OK);
		}
	}

	public ResponseEntity<Object> getCategoryList() {
		return new ResponseEntity<>(categoryDAO.findAll(), HttpStatus.OK);
	}

	public ResponseEntity<Object> addPayment(PaymentMode paymentMode) {
		Optional<PaymentMode> checkPaymentDetails = paymentDAO.findByPaymentType(paymentMode.getPaymentType());

		if (!checkPaymentDetails.isPresent()) {
			PaymentMode paymentDetails = paymentDAO.save(paymentMode);

			paymentDetails.setPaymentType(paymentMode.getPaymentType());
			paymentDetails.setDescription(paymentMode.getDescription());

			paymentDAO.save(paymentDetails);

			return new ResponseEntity<>(new StatusRes("Success", "000", "PaymentMode added Successfully"),
					HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new StatusRes("Failed", "007", "PaymentMode already existed"), HttpStatus.OK);
		}
	}

	public ResponseEntity<Object> updatePayment(@Valid PaymentMode paymentMode) {
		Optional<PaymentMode> checkPaymentMode = paymentDAO.findById(paymentMode.getPayment_id());
		Optional<PaymentMode> checkPaymentType = paymentDAO.findByPaymentType(paymentMode.getPaymentType());

		if (checkPaymentMode.isPresent()) {
			if (checkPaymentType.isPresent() && checkPaymentMode.get().equals(checkPaymentType.get())
					|| !checkPaymentType.isPresent()) {

				checkPaymentMode.get().setPaymentType(paymentMode.getPaymentType());
				checkPaymentMode.get().setDescription(paymentMode.getDescription());
				paymentDAO.save(checkPaymentMode.get());

				return new ResponseEntity<>(new StatusRes("Success", "000", "Record updated successfully"),
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new StatusRes("Failed", "007", "PaymentMode Name already Existed"),
						HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<>(new StatusRes("Failed", "007", "Category details not found"), HttpStatus.OK);
		}
	}

	public ResponseEntity<Object> getPaymentList() {
		return new ResponseEntity<>(paymentDAO.findAll(), HttpStatus.OK);
	}

	public ResponseEntity<Object> addExpensesDetails(Expenses expenses, MultipartFile imagefile) throws IOException {
	
		Optional<Category> checkCategoryDetails = categoryDAO.findById(expenses.getCategory().getCategory_id());
		Optional<Project> checkProjectDetails = projectDAO.findById(expenses.getProject_id());
		Optional<PaymentMode> checkPaymentDetails = paymentDAO.findById(expenses.getPaymentMode().getPayment_id());

		if (checkPaymentDetails.isPresent() && checkCategoryDetails.isPresent() && checkProjectDetails.isPresent()) {
			Expenses expensesDetails = expensesDAO.save(expenses);
			expensesDetails.setCreateddate(LocalDateTime.now());
			expensesDetails.setStatus("Pending");

			if (imagefile != null) {
				expensesDetails.setData(imagefile.getBytes());
				expensesDetails.setFileName(imagefile.getOriginalFilename());
				expensesDetails.setFileType(imagefile.getContentType());
			}
			expensesDAO.save(expensesDetails);
			System.out.println("expensesDetails " +expensesDetails);

			return new ResponseEntity<Object>(new StatusRes("Success", "000", "Expenses added Successfully"),
					HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(new StatusRes("Failed", "007", "Expenses Details not found"),
					HttpStatus.OK);
		}
	}

	public ResponseEntity<Object> updateExpensesDetails(Expenses expenses, MultipartFile imagefile) throws IOException {

		Optional<PaymentMode> checkPaymentDetails = paymentDAO.findById(expenses.getPaymentMode().getPayment_id());
		Optional<Category> checkCategoryDetails = categoryDAO.findById(expenses.getCategory().getCategory_id());
		Optional<Project> checkProjectDetails = projectDAO.findById(expenses.getProject_id());
		Optional<Expenses> checkExpensesDetails = expensesDAO.findById(expenses.getExpenses_id());

		if (checkPaymentDetails.isPresent() && checkCategoryDetails.isPresent() && checkProjectDetails.isPresent()
				&& checkExpensesDetails.isPresent()) {

			checkExpensesDetails.get().setModifydate(LocalDateTime.now());
			checkExpensesDetails.get().setExpensesName(expenses.getExpensesName());
			checkExpensesDetails.get().setCategory(expenses.getCategory());
			checkExpensesDetails.get().setProject_id(expenses.getProject_id());
			checkExpensesDetails.get().setExpensesDate(expenses.getExpensesDate());
			checkExpensesDetails.get().setExpensesAmount(expenses.getExpensesAmount());
			checkExpensesDetails.get().setExpensesAdvancedAmount(expenses.getExpensesAdvancedAmount());
			checkExpensesDetails.get().setPaymentMode(expenses.getPaymentMode());
			checkExpensesDetails.get().setPaymentReference(expenses.getPaymentReference());
			checkExpensesDetails.get().setDescription(expenses.getDescription());
			checkExpensesDetails.get().setReimbursable(expenses.getReimbursable());
			// checkExpensesDetails.get().setTrip_id(expenses.getTrip_id());
			checkExpensesDetails.get().setUser_id(expenses.getUser_id());

			if (imagefile != null) {
				checkExpensesDetails.get().setData(imagefile.getBytes());
				checkExpensesDetails.get().setFileName(imagefile.getOriginalFilename());
				checkExpensesDetails.get().setFileType(imagefile.getContentType());
			}
			expensesDAO.save(checkExpensesDetails.get());

			return new ResponseEntity<Object>(new StatusRes("Success", "000", "Expenses updated Successfully"),
					HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(new StatusRes("Failed", "007", "Expenses Details not found"),
					HttpStatus.OK);
		}
	}

	public ResponseEntity<Object> getAllExpensesList() {
		return new ResponseEntity<>(expensesDAO.findAll(), HttpStatus.OK);
	}

//	public ResponseEntity<Object> viewbyUserExpenses(Long user_id) {
//		Optional<Expenses> details = expensesDAO.findByUser_id(user_id);
//		if (details.isPresent()) {
//			return new ResponseEntity<>(details.get(), HttpStatus.OK);
//		} else {
//			return new ResponseEntity<>("{}", HttpStatus.OK);
//		}
//	}

	public List<Expenses> viewbyUserExpenses(Long user_id) {
		return (List<Expenses>) expensesDAO.findByUser_id(user_id);
	}

//	public List<Expenses> reportingManagerExpensesView(Long userId) {
//		List<Expenses> listExpenses = new ArrayList<>();
//		try {
//			List<UserDetails> listManager = userDetailsDAO.findByManagerID(userId);
//			// System.out.println("listManager::"+listManager.size());
//			listManager.forEach(details -> {
//
//				List<Expenses> expenses = expensesDAO.findByUser_ids(details).stream()
//						.filter(n -> n.getStatus().equals("Pending")
//								|| n.getStatus().equals("Approved")
//								|| n.getStatus().equals("Rejected"))
//						.collect(Collectors.toList());
//				//System.out.println("expenses::" + expenses.size());
//				listExpenses.addAl l(expenses);
//			});
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return listExpenses;
//	}
	
	
//	public Object getExpensesByManager(Long reporting_manager_id) {
//		List<Expenses> listExpenses = new ArrayList<>();
//		List<UserDetails> userList = userDetailsDAO.findByManagerIdAndActive(reporting_manager_id, true);
//		for (UserDetails details : userList) {
//			if (details != null) {
//				List<Expenses> timesheeList = expensesDAO.findByUser_id(details.getUserId());
//				listExpenses.addAll(timesheeList);
//			}
//		}
//		return new ResponseEntity<>(listExpenses, HttpStatus.OK);
//	}

	public Object getExpensesByManager(Long reporting_manager_id) {
		List<ExpensePojo> listExpenses = new ArrayList<>();
		
		List<UserDetails> userList = userDetailsDAO.findByManagerIdAndActive(reporting_manager_id, true);
		for (UserDetails details : userList) {
			if (details != null) {
				List<Expenses> expenseList = expensesDAO.findByUser_id(details.getUserId());
				//listExpenses.addAll(expenseList);
				expenseList.forEach(detailslist->{
					ExpensePojo expense=new ExpensePojo();	
					Optional<Project> project=projectDAO.findById(detailslist.getProject_id());
					Optional<Category> category=categoryDAO.findById(detailslist.getCategory().getCategory_id());
					Optional<PaymentMode> paymentMode=paymentDAO.findById(detailslist.getPaymentMode().getPayment_id());
					
					expense.setUserName(details.getFirstName() +" " +details.getLastName());
					expense.setProjectName(project.get().getProjectName());
					expense.setExpensesName(detailslist.getExpensesName());
					expense.setExpensesDate(detailslist.getExpensesDate());
					expense.setCategoryName(category.get().getCategoryName());
					expense.setPaymentType(paymentMode.get().getPaymentType());			
					expense.setStatus(detailslist.getStatus());
					expense.setExpensesAdvancedAmount(detailslist.getExpensesAdvancedAmount());
					expense.setExpensesAmount(detailslist.getExpensesAmount());
					expense.setDescription(detailslist.getDescription());
					expense.setPaymentReference(detailslist.getPaymentReference());
					expense.setComment(detailslist.getComment());
					
					listExpenses.add(expense);
				});				
			}
		}
		return new ResponseEntity<>(listExpenses, HttpStatus.OK);
	}

	public ResponseEntity<Object> approveExpenses(Long expenses_id, String comment) {
		Optional<Expenses> details = expensesDAO.findById(expenses_id);
		if (details.isPresent() && details.get().getStatus().equals("Pending")) {

			details.get().setStatus("Approved");
			details.get().setComment(comment);
			expensesDAO.save(details.get());

			return new ResponseEntity<>(new StatusRes("Success", "000", "Expenses Approved Successfully"),
					HttpStatus.OK);
		} else if (details.get().getStatus().equals("Pending") || details.get().getStatus().equals("Approved")
				|| details.get().getStatus().equals("Declined")) {
			return new ResponseEntity<>(new StatusRes("Failed", "007", "Please check status of this application"),
					HttpStatus.OK);
		}
		return new ResponseEntity<>(new StatusRes("Failed", "007", "Details not found"), HttpStatus.OK);
	}

	public ResponseEntity<Object> declineExpenses(Long expenses_id, String comment) {
		Optional<Expenses> details = expensesDAO.findById(expenses_id);
		if (details.isPresent() && details.get().getStatus().equals("Pending")) {

			details.get().setStatus("Declined");
			details.get().setComment(comment);
			expensesDAO.save(details.get());

			return new ResponseEntity<>(new StatusRes("Success", "000", "Expenses Application Declined Successfully"),
					HttpStatus.OK);
		} else if (details.get().getStatus().equals("Pending") || details.get().getStatus().equals("Approved")
				|| details.get().getStatus().equals("Declined")) {
			return new ResponseEntity<>(new StatusRes("Failed", "007", "Please check status of this application"),
					HttpStatus.OK);
		}
		return new ResponseEntity<>(new StatusRes("Failed", "007", "Details not found"), HttpStatus.OK);
	}

	public ResponseEntity<byte[]> getFile(Long Id) {
		Optional<Expenses> details = expensesDAO.findById(Id);
		if (details.isPresent()) {
			return ResponseEntity.ok().contentType(MediaType.parseMediaType(details.get().getFileType()))
					.body(details.get().getData());
		}
		return null;
	}

	public void downloadZip(Long expenses_id, HttpServletResponse response) {

		Optional<Expenses> details = expensesDAO.findById(expenses_id);

		if (details.isPresent() && details.get().getStatus().equals("Pending")
				|| details.get().getStatus().equals("Approved") || details.get().getStatus().equals("Declined")) {

			List<byte[]> byteList = new ArrayList<byte[]>();
			List<String> fileName = new ArrayList<>();

			byteList.add(details.get().getData());
			fileName.add(details.get().getFileName());

			response.setContentType("application/octet-stream");
			response.setHeader("Content-Disposition",
					"attachment;filename=" + details.get().getExpensesName() + details.get().getExpenses_id() + ".zip");
			response.setStatus(HttpServletResponse.SC_OK);

			try (ZipOutputStream zippedOut = new ZipOutputStream(response.getOutputStream())) {
				for (int i = 0; i < fileName.size(); i++) {
					ZipEntry entry = new ZipEntry(+i + "_" + fileName.get(i));
					zippedOut.putNextEntry(entry);
					zippedOut.write(byteList.get(i), 0, byteList.get(i).length);
					zippedOut.closeEntry();
				}
				zippedOut.finish();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

}
