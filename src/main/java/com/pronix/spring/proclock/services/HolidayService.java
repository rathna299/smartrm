package com.pronix.spring.proclock.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.pronix.spring.proclock.dao.BusinessUnitsDAO;
import com.pronix.spring.proclock.dao.HolidayGroupAndBussinessUnitDAO;
import com.pronix.spring.proclock.dao.HolidaydatesDAO;
import com.pronix.spring.proclock.dao.HolidaygroupDAO;
import com.pronix.spring.proclock.models.BusinessUnits;
import com.pronix.spring.proclock.models.HolidayGroupAndBusinnesUnit;
import com.pronix.spring.proclock.models.Holidaydates;
import com.pronix.spring.proclock.models.HolidaysGroup;
import com.pronix.spring.proclock.onboard.models.Candidate_details;
import com.pronix.spring.proclock.pojos.StatusRes;

@Service
public class HolidayService {

	@Autowired
	private HolidaygroupDAO holidaygroupDAO;

	@Autowired
	private HolidaydatesDAO holidaydatesDAO;

	@Autowired
	private HolidayGroupAndBussinessUnitDAO holidayGroupAndBussinessUnitDAO;

	@Autowired
	private BusinessUnitsDAO businessUnitsDAO;

	public ResponseEntity<Object> CreateGroup(HolidaysGroup holidaysGroup) {
		Optional<HolidaysGroup> group = Optional
				.ofNullable(holidaygroupDAO.findByGroupName(holidaysGroup.getGroupName()));

		if (group.isPresent()) {
			return new ResponseEntity<>(new StatusRes("Failed", "007", "Group Name Already existed"), HttpStatus.OK);
		} else {
			holidaysGroup.setCreateddate(LocalDate.now().toString());
			holidaysGroup.setEnabled(true);
			holidaygroupDAO.save(holidaysGroup);
			return new ResponseEntity<>(new StatusRes("Success", "000", "Submit Successfully"), HttpStatus.OK);
		}
	}

	public ResponseEntity<Object> updategroup(HolidaysGroup holidaysGroup) {
		Optional<HolidaysGroup> group = Optional
				.ofNullable(holidaygroupDAO.findByGroupName(holidaysGroup.getGroupName()));
		if (group.isPresent()) {
			if (group.get().getHolidaygroupid() == holidaysGroup.getHolidaygroupid()) {
				holidaygroupDAO.updategroup(holidaysGroup.getGroupName(), holidaysGroup.getDescription(),
						holidaysGroup.isEnabled(),
						new SimpleDateFormat("dd/MM/yyyy_HH:mm:ss").format(Calendar.getInstance().getTime()),
						holidaysGroup.getHolidaygroupid());
				return new ResponseEntity<>(new StatusRes("Success", "000", "Updated Successfully"), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new StatusRes("Failed", "007", "Group name Already existed"),
						HttpStatus.OK);
			}
		} else {
			holidaygroupDAO.updategroup(holidaysGroup.getGroupName(), holidaysGroup.getDescription(),
					holidaysGroup.isEnabled(),
					new SimpleDateFormat("dd/MM/yyyy_HH:mm:ss").format(Calendar.getInstance().getTime()),
					holidaysGroup.getHolidaygroupid());
			return new ResponseEntity<>(new StatusRes("Success", "000", "Updated Successfully"), HttpStatus.OK);
		}
	}

	public ResponseEntity<Object> getAllHolidayGroup() {
		return new ResponseEntity<>(holidaygroupDAO.findAll(), HttpStatus.OK);
	}

	public ResponseEntity<Object> addholidays(Holidaydates holidaydates) throws ParseException {
		
		DateTimeFormatter simpleDateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate date = LocalDate.parse(holidaydates.getHolidaydate(), simpleDateFormat);
		
		HolidaysGroup group = Optional
				.ofNullable(holidaygroupDAO.findById(holidaydates.getHolidaygroup().getHolidaygroupid())).get()
				.orElseThrow(() -> new RuntimeException("Holiday group Not Found"));
		
		Optional<Holidaydates> checkholidayname = holidaydatesDAO.findByHolidaynameAndHolidaygroupAndHolidayyears(
				holidaydates.getHolidayname(), holidaydates.getHolidaygroup().getHolidaygroupid(),
				""+date.getYear());

		Optional<Holidaydates> checkholidaydate = holidaydatesDAO.findByHolidaydateAndHolidaygroupAndHolidayyear(
				holidaydates.getHolidaydate(), holidaydates.getHolidaygroup().getHolidaygroupid(),
				""+date.getYear());

		if (!checkholidayname.isPresent()) {
				if(!checkholidaydate.isPresent()) {
					
					holidaydates.setCreateddate(LocalDate.now().toString());
					holidaydates.setEnabled(true);
					holidaydates.setHolidayyear("" + date.getYear());
					holidaydatesDAO.save(holidaydates);

					return new ResponseEntity<>(new StatusRes("Success", "000", "Holiday Added successfully"),
							HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new StatusRes("Failed", "007", "Holiday Date Already Existed"),
						HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<>(new StatusRes("Failed", "007", "Holiday Name Already Existed"), HttpStatus.OK);
		}
	}

	public ResponseEntity<Object> getAllHolidayList() {
		return new ResponseEntity<>(holidaydatesDAO.findAll(), HttpStatus.OK);
	}

	public ResponseEntity<Object> getHolidayList(String year, Long group_id) {

		if (year != null && group_id != null) {
			List<Holidaydates> holidaygroupname = holidaydatesDAO.findByHolidaygroup(group_id).stream()
					.filter(group -> group.getHolidayyear().equalsIgnoreCase(year)).collect(Collectors.toList());
			return new ResponseEntity<>(holidaygroupname, HttpStatus.OK);
		}

		else if (year == null && group_id == null) {
			List<Holidaydates> holidaylist = holidaydatesDAO.findAll();
			return new ResponseEntity<>(holidaylist, HttpStatus.OK);
		}
		return null;
	}

	public ResponseEntity<Object> getHolidayListForEmployee(Long businessunit_id, String year) {
		Optional<HolidayGroupAndBusinnesUnit> getlist = Optional
				.ofNullable(holidayGroupAndBussinessUnitDAO.findbyBusinessUnit(businessunit_id));
		if (getlist.isPresent()) {
			HolidaysGroup holidaygroup = holidaygroupDAO.findById(getlist.get().getHolidaysGroup().getHolidaygroupid())
					.orElseThrow(() -> new RuntimeException("No holiday Configured to this business Unit"));
			List<Holidaydates> holidaylist = holidaydatesDAO.findByGroupId(holidaygroup.getHolidaygroupid()).stream()
					.filter(n -> n.getHolidayyear().equals(year)).collect(Collectors.toList());
			return new ResponseEntity<>(holidaylist, HttpStatus.OK);
		}
		return new ResponseEntity<>(new StatusRes("Failed", "007", "Details Not Existed "), HttpStatus.OK);
	}

	public ResponseEntity<Object> mappingHolidayGroupToBusinessUnit(Long HolidayGroupId, Long businessUnitId) {
		HolidaysGroup group = holidaygroupDAO.findById(HolidayGroupId)
				.orElseThrow(() -> new RuntimeException("Holiday group not found"));

		BusinessUnits businessunit = businessUnitsDAO.findById(businessUnitId)
				.orElseThrow(() -> new RuntimeException("Business Unit not found"));
		HolidayGroupAndBusinnesUnit checkholidayGroup = holidayGroupAndBussinessUnitDAO
				.findbyBusinessUnit(businessunit.getBusinessUnitsId());
		if (checkholidayGroup == null) {
			holidayGroupAndBussinessUnitDAO.save(new HolidayGroupAndBusinnesUnit(businessunit, group));
			return new ResponseEntity<>(new StatusRes("Success", "000", "Holiday group mapped Succssfully"),
					HttpStatus.OK);
		}
		return new ResponseEntity<>(new StatusRes("Failed", "007", "Details already Existed"), HttpStatus.OK);
	}

	public ResponseEntity<Object> mappingHolidayGroupToBusinessUnitList() {
		return new ResponseEntity<>(holidayGroupAndBussinessUnitDAO.findAll(), HttpStatus.OK);
	}

	public ResponseEntity<Object> updateHolidayGroupAndBusinessUnit(Long hbId, Long HolidayGroupId,
			Long businessUnitId) {
		Optional<HolidayGroupAndBusinnesUnit> checkData = holidayGroupAndBussinessUnitDAO.findById(hbId);

		Optional<HolidaysGroup> group = holidaygroupDAO.findById(HolidayGroupId);

		Optional<BusinessUnits> businessunit = businessUnitsDAO.findById(businessUnitId);

		HolidayGroupAndBusinnesUnit checkholidayGroup = holidayGroupAndBussinessUnitDAO
				.findbyBusinessUnit(businessunit.get().getBusinessUnitsId());

		if (checkData.get().getBusinessUnits().getBusinessUnitsId() == businessunit.get().getBusinessUnitsId()
				|| checkholidayGroup == null) {
			holidayGroupAndBussinessUnitDAO.updateHolidayGroupAndBusinnesUnit(businessunit.get().getBusinessUnitsId(),
					group.get().getHolidaygroupid(), checkData.get().getId());
			return new ResponseEntity<>(new StatusRes("Success", "000", "Record updated Successfully"), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new StatusRes("Failed", "007", "Details Already Existed"), HttpStatus.OK);
		}
	}

	public ResponseEntity<Object> updateholidays(Holidaydates holidaydates) {
		
		DateTimeFormatter simpleDateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate date = LocalDate.parse(holidaydates.getHolidaydate(), simpleDateFormat);
		
		Optional<Holidaydates> checkdata = holidaydatesDAO.findById(holidaydates.getHoliday_id());

		Optional<Holidaydates> checkholidayname = holidaydatesDAO.findByHolidaynameAndHolidaygroupAndHolidayyears(
				holidaydates.getHolidayname(), holidaydates.getHolidaygroup().getHolidaygroupid(),
				""+date.getYear());

		Optional<Holidaydates> checkholidaydate = holidaydatesDAO.findByHolidaydateAndHolidaygroupAndHolidayyear(
				holidaydates.getHolidaydate(), holidaydates.getHolidaygroup().getHolidaygroupid(),
				""+date.getYear());

		if (checkdata.isPresent()) {
			if (checkholidayname.isPresent() && checkdata.get().equals(checkholidayname.get())
					|| !checkholidayname.isPresent()) {
				if (checkholidaydate.isPresent()
						&& checkholidaydate.get().getHoliday_id() == holidaydates.getHoliday_id()
						|| !checkholidaydate.isPresent()) {

					checkdata.get().setHolidayname(holidaydates.getHolidayname());
					checkdata.get().setHolidaydate(holidaydates.getHolidaydate());
					checkdata.get().setHolidayyear("" + date.getYear());
					checkdata.get().setHolidaygroup(holidaydates.getHolidaygroup());
					checkdata.get().setDescription(holidaydates.getDescription());
					checkdata.get().setModifieddate(LocalDate.now().toString());

					holidaydatesDAO.save(checkdata.get());
					return new ResponseEntity<>(new StatusRes("Success", "000", "Record updated successfully"),
							HttpStatus.OK);
				} else {
					return new ResponseEntity<>(new StatusRes("Failed", "007", "Holiday Date already exist"),
							HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<>(new StatusRes("Failed", "007", "Holiday Name already exist"),
						HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<>(new StatusRes("Failed", "007", "details not found"), HttpStatus.OK);
		}
	}

}
