package com.pronix.spring.proclock.services;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Interval;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.pronix.spring.proclock.dao.BusinessUnitsDAO;
import com.pronix.spring.proclock.dao.HolidayGroupAndBussinessUnitDAO;
import com.pronix.spring.proclock.dao.HolidaydatesDAO;
import com.pronix.spring.proclock.dao.LeaveConfigurationDAO;
import com.pronix.spring.proclock.dao.LeaveDetailsDAO;
import com.pronix.spring.proclock.dao.LeaveMasterDAO;
import com.pronix.spring.proclock.dao.RefRolesDAO;
import com.pronix.spring.proclock.dao.UserContactsDAO;
import com.pronix.spring.proclock.dao.UserDetailsDAO;
import com.pronix.spring.proclock.models.BusinessUnits;
import com.pronix.spring.proclock.models.HolidayGroupAndBusinnesUnit;
import com.pronix.spring.proclock.models.Holidaydates;
import com.pronix.spring.proclock.models.LeaveConfiguration;
import com.pronix.spring.proclock.models.LeaveDetails;
import com.pronix.spring.proclock.models.LeaveMaster;
import com.pronix.spring.proclock.models.RefRoles;
import com.pronix.spring.proclock.models.UserContacts;
import com.pronix.spring.proclock.models.UserDetails;
import com.pronix.spring.proclock.pojos.LeaveMasterPojo;
import com.pronix.spring.proclock.pojos.LeavesCountByManager;
import com.pronix.spring.proclock.pojos.StatusRes;

@Service
public class LeaveIncrementServiceImpl {

	@Autowired
	private UserServices userServices;

	@Autowired
	private LeaveDetailsDAO leaveDetailsDAO;

	@Autowired
	private LeaveMasterDAO leaveMasterDAO;

	@Autowired
	private UserDetailsDAO userDetailsDAO;

	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	private UserContactsDAO userContactsDAO;

	@Autowired
	private LeaveConfigurationDAO configurationDAO;

	@Autowired
	private BusinessUnitsDAO businessUnitsDAO;

	@Autowired
	private RefRolesDAO refRolesDAO;

	@Autowired
	private HolidaydatesDAO holidaydatesDAO;

	@Autowired
	private HolidayGroupAndBussinessUnitDAO groupAndBussinessUnitDAO;

	private static final Logger log = LoggerFactory.getLogger(LeaveIncrementServiceImpl.class);
	/*
	 * @Scheduled(cron = "0/30 * * * * ?") public void run() {
	 * log.info("LeaveIncrementService Current time is :: " +
	 * Calendar.getInstance().getTime());
	 * //System.out.println("LeaveIncrementService Current time is :: " +
	 * Calendar.getInstance().getTime()); }
	 */

	// @Scheduled(cron = "[Seconds] [Minutes] [Hours] [Day of month] [Month] [Day of
	// week] [Year]")
	// @Scheduled(cron = "0/30 * 12 * * ?") // every 30 Seconds
//	 @Scheduled(cron = "0 * * ? * *") //every minute
	// @Scheduled(cron = "0 30 * * * ?")
	@Scheduled(cron = "0 0 0 1 * ?") // every month 1st day at 12 AM ,(change 3rd
	// position hour)
	// @Scheduled(cron = "0 6 55 13 * ?")

	public boolean incrementLeave() {
		log.info("LeaveIncrementService Current time is :: " + Calendar.getInstance().getTime());
		/* Long userId,Integer year */

		Long userId;
		Integer year = Calendar.getInstance().get(Calendar.YEAR);
		System.out.println(year + " PRINT YEAR");
		Double annualLeaves = 0.5;
		LeaveDetails leaveDetails = null;
		boolean result = false;
		try {
			List<UserDetails> userList = userServices.listofEnabledEmployee(true);
			result = false;
			if (userList != null) {
				for (UserDetails userDetails : userList) {
					userId = userDetails.getUserId();
					leaveDetails = leaveDetailsByUserId(userId, year);
					if (leaveDetails != null) {
						log.info("already exists leave details");
					} else {
						leaveDetails = saveLeaveDetails(userDetails);
						log.info("not exists leave details");
					}

					LocalDate dayOfJoining = DateTimeFormat.forPattern("dd-MM-yyyy")
							.parseLocalDate(userDetails.getDateOfJoining());
					LocalDate localDate = new LocalDate();
					Days totaldays = Days.daysBetween(dayOfJoining, localDate.now());
					if (totaldays.getDays() >= 90) {
						leaveDetails.setRemainingAnnualLeaves(leaveDetails.getRemainingAnnualLeaves() + annualLeaves);
						leaveDetails.setActualAnnualLeaves(leaveDetails.getActualAnnualLeaves() + annualLeaves);
						leaveDetailsDAO.save(leaveDetails);
						result = true;
						log.info("Leave Details updated for userid :: " + userDetails.getUserId());
					}
				}
			} else
				System.out.println("user Details is empty ");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

	public LeaveDetails saveLeaveDetails(UserDetails userDetails) {
		LeaveDetails leaveDetails = null;
		Integer year = Calendar.getInstance().get(Calendar.YEAR);
		try {
			leaveDetails = new LeaveDetails();
			leaveDetails.setActualAnnualLeaves(0);
			leaveDetails.setRemainingAnnualLeaves(0);
			leaveDetails.setUtilisedAnnualLeaves(0);
			leaveDetails.setSickLeaves(6);
			leaveDetails.setRemainingSickleaves(6);
			leaveDetails.setUtilizedsickLeaves(0);
			leaveDetails.setLossOfPay(0);
			leaveDetails.setYear(year);
			leaveDetails.setUserDetails(userDetails);
			leaveDetails.setUpdated_on(LocalDateTime.now());
			leaveDetails = leaveDetailsDAO.save(leaveDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return leaveDetails;
	}

	public LeaveDetails leaveDetailsByUserId(Long userId, Integer year) {
		LeaveDetails leaveDetails = null;
		leaveDetails = leaveDetailsDAO.findByLeaveDetails(userId, year);
		return leaveDetails;
	}

	public List<StatusRes> applyLeave(LeaveMasterPojo leaveMasterPojo) {
		List<StatusRes> statusResList = new ArrayList<StatusRes>();
		Optional<LeaveDetails> leaveDetails = leaveDetailsDAO.findByUserId(leaveMasterPojo.getUserId());
		if (leaveDetails.isPresent()) {
			Optional<UserDetails> userDetails = userDetailsDAO.findById(leaveMasterPojo.getUserId());
			UserContacts mangerContact = userContactsDAO.findByUserDetails(userDetails.get().getTempUserDetails());
			LocalDate fromData = DateTimeFormat.forPattern("dd-MM-yyyy").parseLocalDate(leaveMasterPojo.getFromDate());
			LocalDate ToDate = DateTimeFormat.forPattern("dd-MM-yyyy").parseLocalDate(leaveMasterPojo.getToDate());
			long numberOfDays1 = Days.daysBetween(fromData, ToDate).getDays();
			List<LeaveMaster> existingAppliedLeaves = leaveMasterDAO.findByUserDetails(userDetails.get().getUserId());
			for (LeaveMaster existingDetails : existingAppliedLeaves) {
				LocalDate fromDataa = DateTimeFormat.forPattern("dd-MM-yyyy")
						.parseLocalDate(existingDetails.getFromDate());
				LocalDate ToDatee = DateTimeFormat.forPattern("dd-MM-yyyy").parseLocalDate(existingDetails.getToDate());
				long numberOfDaysPresent = Days.daysBetween(fromDataa, ToDatee).getDays();
				for (int i = 0; i <= numberOfDaysPresent; i++) {
					if (fromDataa.plusDays(i).equals(fromData)) {
						statusResList.add(new StatusRes("Failed", "007", "Leave already applied on this date"));
						return statusResList;
					} else if (fromDataa.plusDays(i).equals(ToDate)) {
						statusResList.add(new StatusRes("Failed", "007", "Leave already applied on this date"));
						return statusResList;
					}
				}
			}
			HolidayGroupAndBusinnesUnit holidayGroupAndBusinnesUnit = groupAndBussinessUnitDAO
					.findbyBusinessUnit(userDetails.get().getBusinessUnits().getBusinessUnitsId());
			if (holidayGroupAndBusinnesUnit != null) {
				for(int i=0;i<=numberOfDays1;i++) {
					Holidaydates holidays = holidaydatesDAO.findByholidaydateAndholidayyear(fromData.plusDays(i).toString("yyyy-MM-dd"),
							holidayGroupAndBusinnesUnit.getHolidaysGroup().getHolidaygroupid());
					if(holidays != null) {
						statusResList.add(new StatusRes("Failed", "007", "Leave cannot apply on holidays"));
						return statusResList;
					}
				}
			}

			int weekends = 0;
			String Status = "Pending";
			System.out.println(Status + " Status");

			for (int i = 0; i <= numberOfDays1; i++) {
				if (DateTimeFormat.forPattern("EEEE").print(fromData.plusDays(i)).equals("Saturday")
						|| DateTimeFormat.forPattern("EEEE").print(fromData.plusDays(i)).equals("Sunday")) {
					weekends++;
				}
			}
			long numberOfDays = (numberOfDays1 + 1) - weekends;
			double ActualLeaveCount = leaveDetails.get().getRemainingAnnualLeaves();
			double ActualSickleaveCount = leaveDetails.get().getRemainingSickleaves();
			try {
				if (leaveMasterPojo.getLeaveType() == null) {
					statusResList.add(new StatusRes("Failed", "007", "Leave Type is Empty"));
				}
				if (leaveMasterPojo.getLeaveType().equals("AnualLeaves") && leaveMasterPojo.getDays() == 0) {

					if (numberOfDays > ActualLeaveCount) {
						statusResList.add(new StatusRes("Failed", "007", "Please check your leaves"));
					} else if (numberOfDays <= ActualLeaveCount) {
						LeaveMaster leavemaster = new LeaveMaster(userDetails.get(), leaveMasterPojo.getLeaveType(),
								leaveMasterPojo.getFromDate(), leaveMasterPojo.getToDate(), numberOfDays, Status,
								leaveMasterPojo.getReason(), userDetails.get().getTempUserDetails().getUserId());
						leaveMasterDAO.save(leavemaster);
						statusResList.add(new StatusRes("Success", "000", "Leave application submitted Successfully"));
						sendEmailforLeaveRequest(mangerContact.getEmailId(),
								userDetails.get().getTempUserDetails().getFirstName(), userDetails.get().getFirstName(),
								userDetails.get().getJobTitles().getJobtitle(), leaveMasterPojo.getLeaveType(),
								numberOfDays, leaveMasterPojo.getFromDate() + " To " + leaveMasterPojo.getToDate(),
								leaveMasterPojo.getReason());
					}
				} else if (leaveMasterPojo.getLeaveType().equals("LossOfPay") && numberOfDays != 0
						&& leaveMasterPojo.getDays() == 0) {
					LeaveMaster leavemaster = new LeaveMaster(userDetails.get(), leaveMasterPojo.getLeaveType(),
							leaveMasterPojo.getFromDate(), leaveMasterPojo.getToDate(), numberOfDays, Status,
							leaveMasterPojo.getReason(), userDetails.get().getTempUserDetails().getUserId());
					leaveMasterDAO.save(leavemaster);
					statusResList.add(new StatusRes("Success", "000", "Leave application submitted Successfully"));
					sendEmailforLeaveRequest(mangerContact.getEmailId(),
							userDetails.get().getTempUserDetails().getFirstName(), userDetails.get().getFirstName(),
							userDetails.get().getJobTitles().getJobtitle(), leaveMasterPojo.getLeaveType(),
							numberOfDays, leaveMasterPojo.getFromDate() + " To " + leaveMasterPojo.getToDate(),
							leaveMasterPojo.getReason());

					// Applying off day anual leave

				} else if (leaveMasterPojo.getLeaveType().equals("AnualLeaves") && leaveMasterPojo.getDays() == 0.5
						&& numberOfDays == 1) {
					if (leaveMasterPojo.getDays() > ActualLeaveCount) {
						statusResList.add(new StatusRes("Failed", "007", "Please check your leaves"));
					} else if (leaveMasterPojo.getDays() <= ActualLeaveCount) {
						LeaveMaster leavemaster3 = new LeaveMaster(userDetails.get(), leaveMasterPojo.getLeaveType(),
								leaveMasterPojo.getFromDate(), leaveMasterPojo.getToDate(), leaveMasterPojo.getDays(),
								Status, leaveMasterPojo.getReason(),
								userDetails.get().getTempUserDetails().getUserId());
						leaveMasterDAO.save(leavemaster3);
						statusResList.add(new StatusRes("Success", "000", "Leave application submitted Successfully"));
						sendEmailforLeaveRequest(mangerContact.getEmailId(),
								userDetails.get().getTempUserDetails().getFirstName(), userDetails.get().getFirstName(),
								userDetails.get().getJobTitles().getJobtitle(), leaveMasterPojo.getLeaveType(), 0.5,
								leaveMasterPojo.getFromDate() + " To " + leaveMasterPojo.getToDate(),
								leaveMasterPojo.getReason());
					}

					// Applying off day loss of pay

				} else if (leaveMasterPojo.getLeaveType().equals("LossOfPay") && leaveMasterPojo.getDays() == 0.5
						&& numberOfDays == 1) {
					LeaveMaster leavemaster = new LeaveMaster(userDetails.get(), leaveMasterPojo.getLeaveType(),
							leaveMasterPojo.getFromDate(), leaveMasterPojo.getToDate(), leaveMasterPojo.getDays(),
							Status, leaveMasterPojo.getReason(), userDetails.get().getTempUserDetails().getUserId());
					leaveMasterDAO.save(leavemaster);
					statusResList.add(new StatusRes("Success", "000", "Leave application submitted Successfully"));
					sendEmailforLeaveRequest(mangerContact.getEmailId(),
							userDetails.get().getTempUserDetails().getFirstName(), userDetails.get().getFirstName(),
							userDetails.get().getJobTitles().getJobtitle(), leaveMasterPojo.getLeaveType(), 0.5,
							leaveMasterPojo.getFromDate() + " To " + leaveMasterPojo.getToDate(),
							leaveMasterPojo.getReason());

				} else if (leaveMasterPojo.getLeaveType().equals("LossOfPay")
						|| leaveMasterPojo.getLeaveType().equals("AnualLeaves") && leaveMasterPojo.getDays() == 0.5
								&& numberOfDays != 1) {
					statusResList.add(new StatusRes("Failed", "007", "Leave application Failed"));

				} else if (leaveMasterPojo.getLeaveType() == null || leaveMasterPojo.getLeaveType().isEmpty()) {
					statusResList.add(new StatusRes("Failed", "007", "Leave Type is Empty"));

				} else if (leaveMasterPojo.getLeaveType().equals("SickLeaves") && leaveMasterPojo.getDays() == 0) {
					if (numberOfDays > ActualSickleaveCount) {
						statusResList.add(new StatusRes("Failed", "007", "Please check your leaves"));
					} else if (numberOfDays <= ActualSickleaveCount) {
						LeaveMaster leavemaster = new LeaveMaster(userDetails.get(), leaveMasterPojo.getLeaveType(),
								leaveMasterPojo.getFromDate(), leaveMasterPojo.getToDate(), numberOfDays, Status,
								leaveMasterPojo.getReason(), userDetails.get().getTempUserDetails().getUserId());
						leaveMasterDAO.save(leavemaster);
						statusResList.add(new StatusRes("Success", "000", "Leave application submitted Successfully"));
						sendEmailforLeaveRequest(mangerContact.getEmailId(),
								userDetails.get().getTempUserDetails().getFirstName(), userDetails.get().getFirstName(),
								userDetails.get().getJobTitles().getJobtitle(), leaveMasterPojo.getLeaveType(),
								numberOfDays, leaveMasterPojo.getFromDate() + " To " + leaveMasterPojo.getToDate(),
								leaveMasterPojo.getReason());
					}
				} else if (leaveMasterPojo.getLeaveType().equals("SickLeaves") && leaveMasterPojo.getDays() == 0.5
						&& numberOfDays == 1) {
					if (leaveMasterPojo.getDays() > ActualSickleaveCount) {
						statusResList.add(new StatusRes("Failed", "007", "Please check your leaves"));
					} else if (leaveMasterPojo.getDays() <= ActualSickleaveCount) {
						LeaveMaster leavemaster3 = new LeaveMaster(userDetails.get(), leaveMasterPojo.getLeaveType(),
								leaveMasterPojo.getFromDate(), leaveMasterPojo.getToDate(), leaveMasterPojo.getDays(),
								Status, leaveMasterPojo.getReason(),
								userDetails.get().getTempUserDetails().getUserId());
						leaveMasterDAO.save(leavemaster3);
						statusResList.add(new StatusRes("Success", "000", "Leave application submitted Successfully"));
						sendEmailforLeaveRequest(mangerContact.getEmailId(),
								userDetails.get().getTempUserDetails().getFirstName(), userDetails.get().getFirstName(),
								userDetails.get().getJobTitles().getJobtitle(), leaveMasterPojo.getLeaveType(), 0.5,
								leaveMasterPojo.getFromDate() + " To " + leaveMasterPojo.getToDate(),
								leaveMasterPojo.getReason());
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			statusResList.add(new StatusRes("Failed", "007", "Employee Details not found"));
		}
		return statusResList;
	}

	public List<StatusRes> approveLeave(Long leaveMasterID, String comment) {
		List<StatusRes> statusResList = new ArrayList<StatusRes>();

		LeaveMaster LeaveMasterdetails = leaveMasterDAO.findById(leaveMasterID)
				.orElseThrow(() -> new RuntimeException("Leave details not found"));
		UserContacts userContact = userContactsDAO.findByUserDetails(LeaveMasterdetails.getUserDetails());
		Optional<LeaveDetails> leaveDetails = leaveDetailsDAO
				.findByUserId(LeaveMasterdetails.getUserDetails().getUserId());

		try {
			if (leaveDetails.get().getRemainingAnnualLeaves() >= LeaveMasterdetails.getDays()
					&& LeaveMasterdetails.getLeaveType().equals("AnualLeaves")
					&& LeaveMasterdetails.getStatus().equals("Pending")
					|| leaveDetails.get().getRemainingSickleaves() >= LeaveMasterdetails.getDays()
							&& LeaveMasterdetails.getLeaveType().equals("SickLeaves")
							&& LeaveMasterdetails.getStatus().equals("Pending")) {

				leaveMasterDAO.updateStatus(leaveMasterID, "Approved", comment);
				sendEmailForLeaveApproval(userContact.getEmailId(), LeaveMasterdetails.getUserDetails().getFirstName(),
						LeaveMasterdetails.getUserDetails().getTempUserDetails().getFirstName(),
						LeaveMasterdetails.getLeaveType(), LeaveMasterdetails.getDays(),
						LeaveMasterdetails.getFromDate() + "To" + LeaveMasterdetails.getToDate(),
						LeaveMasterdetails.getReason(), "Your leave approved", "Approved");

				if (LeaveMasterdetails.getLeaveType().equals("AnualLeaves")) {
					double remainingLeaves = leaveDetails.get().getRemainingAnnualLeaves()
							- LeaveMasterdetails.getDays();

					double utilisedLeaves = leaveDetails.get().getUtilisedAnnualLeaves() + LeaveMasterdetails.getDays();

					leaveDetailsDAO.updateLeavese(leaveDetails.get().getLeavedetailId(), remainingLeaves,
							utilisedLeaves);
				} else if (LeaveMasterdetails.getLeaveType().equals("SickLeaves")) {
					double remainingLeaves = leaveDetails.get().getRemainingSickleaves() - LeaveMasterdetails.getDays();

					double utilisedLeaves = leaveDetails.get().getUtilizedsickLeaves() + LeaveMasterdetails.getDays();

					leaveDetailsDAO.updateSickLeavese(leaveDetails.get().getLeavedetailId(), remainingLeaves,
							utilisedLeaves);
				}

				statusResList.add(new StatusRes("Success", "000", "Leave Approved Successfully"));

			} else if (leaveDetails.get().getRemainingAnnualLeaves() <= LeaveMasterdetails.getDays()
					&& LeaveMasterdetails.getLeaveType().equals("AnualLeaves")) {
				statusResList.add(new StatusRes("Failed To Approve", "007", "Person not enough leaves"));

			} else if (LeaveMasterdetails.getStatus().equals("Approved")) {
				statusResList.add(new StatusRes("Failed ", "007", "Leave Application already processed"));
			} else if (LeaveMasterdetails.getLeaveType().equals("LossOfPay")
					&& LeaveMasterdetails.getStatus().equals("Pending")) {
				leaveMasterDAO.updateStatus(leaveMasterID, "Approved", comment);
				leaveDetails.get().setLossOfPay(LeaveMasterdetails.getDays() + leaveDetails.get().getLossOfPay());
				leaveDetailsDAO.save(leaveDetails.get());
				sendEmailForLeaveApproval(userContact.getEmailId(), LeaveMasterdetails.getUserDetails().getFirstName(),
						LeaveMasterdetails.getUserDetails().getTempUserDetails().getFirstName(),
						LeaveMasterdetails.getLeaveType(), LeaveMasterdetails.getDays(),
						LeaveMasterdetails.getFromDate() + "To" + LeaveMasterdetails.getToDate(),
						LeaveMasterdetails.getReason(), "Your leave approved", "Approved");
				statusResList.add(new StatusRes("Success ", "000", "Leave Application approved Successfully"));
			} else {
				statusResList.add(new StatusRes("Failed ", "007", "Leave Application already processed"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return statusResList;
	}

	public List<StatusRes> DeclineLeave(Long leaveMasterID, String comment) {
		List<StatusRes> statusResList = new ArrayList<StatusRes>();
		try {
			Optional<LeaveMaster> LeaveMasterdetails = leaveMasterDAO.findById(leaveMasterID);
			UserContacts userContact = userContactsDAO.findByUserDetails(LeaveMasterdetails.get().getUserDetails());
			if (LeaveMasterdetails.isPresent()) {
				if (LeaveMasterdetails.get().getStatus().equals("Pending")) {
					leaveMasterDAO.updateStatus(leaveMasterID, "Declined", comment);
					statusResList.add(new StatusRes("Success", "000", "Leave Declined Successfully"));
					sendEmailForLeaveDecline(userContact.getEmailId(),
							LeaveMasterdetails.get().getUserDetails().getFirstName(),
							LeaveMasterdetails.get().getUserDetails().getTempUserDetails().getFirstName(),
							LeaveMasterdetails.get().getLeaveType(), LeaveMasterdetails.get().getDays(),
							LeaveMasterdetails.get().getFromDate() + "To" + LeaveMasterdetails.get().getToDate(),
							LeaveMasterdetails.get().getReason(), "Your leave Declined", "Declined",comment);
				} else {
					statusResList.add(new StatusRes("Failed to Decline", "007", "Leave Application Already Processed"));
				}
			} else {
				statusResList.add(new StatusRes("Request failed", "007", "Leave Id not available"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return statusResList;
	}

	public LeaveDetails LeaveDetailsById(Long UserId) {
		LeaveDetails leaveDetails = leaveDetailsDAO.findByLeaveDetails(UserId,
				Calendar.getInstance().get(Calendar.YEAR));
		return leaveDetails;
	}

	public List<LeaveMaster> UserAppliedLeaves(Long userId, String status) {
		return leaveMasterDAO.findByUserId(userId, status);
	}

	public List<LeaveMaster> leavesByStatus(String status) {
		return leaveMasterDAO.findByStatus(status);
	}

	public List<StatusRes> CancelLeave(Long leaveMasterId, String comment) {
		List<StatusRes> statusResList = new ArrayList<StatusRes>();
		LeaveMaster LeaveMasterdetails = leaveMasterDAO.findById(leaveMasterId)
				.orElseThrow(() -> new RuntimeException("Leave details not found"));
		if (LeaveMasterdetails.getStatus().equals("Pending")) {
			leaveMasterDAO.updateStatus(leaveMasterId, "Cancelled", comment);
			statusResList.add(new StatusRes("Success", "000", "Leave Cancelled Successfully"));
		} else {
			statusResList.add(new StatusRes("Failed to Cancel", "007", "Leave Application Already Processed"));
		}

		return statusResList;
	}

	public Optional<Double> leavesRemaining(Long userId) {
		return leaveDetailsDAO.findByUserId1(userId);
	}

	public Optional<Double> leavesUtilized(Long userId) {
		return leaveDetailsDAO.findByUserId2(userId);
	}

	public List<LeaveMaster> leaveDetailsbyManager(Long managerId) {
		return leaveMasterDAO.findByManager(managerId);
	}

	public void sendEmailforLeaveRequest(String email, String managerName, String Employeename, String designation,
			String LeaveType, double dayes, String duration, String Reason) {
		userServices.sendEmail(email, "Reg :: Leave Request - You have recieved a leave request from below Employee ",
				"<!DOCTYPE html><html><head><style>table, th, td {border: 1px solid black;border-collapse: collapse;}</style></head>"
						+ "<body>\r\n"
						+ "<table id=\"Table_01\" style=\"border: 2px solid #062f4f; margin: 10px auto; border-radius: 8px;\" width=\"621\" cellspacing=\"0\" cellpadding=\"0\">"
						+ "<tbody>" + "<tr style=\"background: #062f4f; color: white; height: 39px;\">"
						+ "<td style=\"padding: 20px 10px; font-size: 30px; height: 39px;\">SmarTRM</td>" + "</tr>"
						+ "<tr style=\"height: 36px;\">" + "<td style=\"padding: 20px 10px; height: 36px;\">Dear "
						+ managerName + "</b>,</p>\r\n" + "  <p style=\"padding-left:50px;\">Employee Name. <b>"
						+ Employeename + " " + "</b>,</p>\r\n" + "  <p style=\"padding-left:50px;\">Designation: <b>"
						+ designation + "<b>.</p>\r\n" + "  <p style=\"padding-left:50px;\">Leave Type: <b>" + LeaveType
						+ "<b>.</p>\r\n" + "  <p style=\"padding-left:50px;\">Number of Days: <b>" + dayes
						+ "<b>.</p>\r\n" + "  <p style=\"padding-left:50px;\">Leave Duration: <b>" + duration
						+ "<b>.</p>\r\n" + "  <p style=\"padding-left:50px;\">Reason for Leave: <b>" + Reason
						+ "<b>.</p>\r\n" + "</td>" + "</tr>" + "<tr style=\"height: 33px;\">"
						+ "<td style=\"padding: 20px 40px; text-align: right; height: 33px;\">Thanks and Regards<br /> SmartRM </td>"
						+ "</tr>" + "</tbody>" + "</table>" + "</body>" + "</html>");

	}

	public void sendEmailForLeaveApproval(String email, String name, String approvedBy, String leavetype,
			double numberOfDays, String duration, String reason, String subject, String approvedby) {

		userServices.sendEmail(email, subject,
				"<!DOCTYPE html><html><head><style>table, th, td {border: 1px solid black;border-collapse: collapse;}</style></head>"
						+ "<body>\r\n"
						+ "<table id=\"Table_01\" style=\"border: 2px solid #062f4f; margin: 10px auto; border-radius: 8px;\" width=\"621\" cellspacing=\"0\" cellpadding=\"0\">"
						+ "<tbody>" + "<tr style=\"background: #062f4f; color: white; height: 39px;\">"
						+ "<td style=\"padding: 20px 10px; font-size: 30px; height: 39px;\">SmarTRM</td>" + "</tr>"
						+ "<tr style=\"height: 36px;\">" + "<td style=\"padding: 20px 10px; height: 36px;\">Dear "
						+ name + "</b>,</p>\r\n" + "  <p style=\"padding-left:50px;\">Your leave <b>" + approvedby
						+ " " + approvedBy + " " + "</b>,</p>\r\n"
						+ "  <p style=\"padding-left:50px;\">Leave Type: <b>" + leavetype + "<b>.</p>\r\n"
						+ "  <p style=\"padding-left:50px;\">Number of Days: <b>" + numberOfDays + "<b>.</p>\r\n"
						+ "  <p style=\"padding-left:50px;\">Leave Duration: <b>" + duration + "<b>.</p>\r\n"
						+ "  <p style=\"padding-left:50px;\">Reason for Leave: <b>" + reason + "<b>.</p>\r\n" + "</td>"
						+ "</tr>" + "<tr style=\"height: 33px;\">"
						+ "<td style=\"padding: 20px 40px; text-align: right; height: 33px;\">Thanks and Regards<br /> SmartRM </td>"
						+ "</tr>" + "</tbody>" + "</table>" + "</body>" + "</html>");
	}

	public Object LeavesCountByManager(Long managerId) {
		String status[] = { "Pending", "Approved", "Declined" };
		List<LeaveMaster> leaveData = leaveMasterDAO.findByManager(managerId);
		long pending = leaveData.stream().filter(n -> n.getStatus().equals(status[0])).count();
		long approved = leaveData.stream().filter(n -> n.getStatus().equals(status[1])).count();
		long declined = leaveData.stream().filter(n -> n.getStatus().equals(status[2])).count();
		long active = userDetailsDAO.findByManagerID(managerId).stream().count();
		long onLeave = 0;
		for (LeaveMaster master : leaveData.stream().filter(n -> n.getStatus().equals(status[1]))
				.collect(Collectors.toList())) {
			DateTime fromdate = DateTimeFormat.forPattern("dd-MM-yyyy").parseDateTime(master.getFromDate());
			DateTime todate = DateTimeFormat.forPattern("dd-MM-yyyy").parseDateTime(master.getToDate());
			Interval interval = new Interval(fromdate, todate.plusDays(1));
			boolean checkDate = interval.contains(
					DateTimeFormat.forPattern("dd-MM-yyyy").parseDateTime(LocalDate.now().toString("dd-MM-yyyy")));
			if (checkDate == true) {
				onLeave++;
			}
		}
		LeavesCountByManager totalCount = new LeavesCountByManager();
		totalCount.setActive(active - onLeave);
		totalCount.setOnLeave(onLeave);
		totalCount.setApproved(approved);
		totalCount.setDeclined(declined);
		totalCount.setPending(pending);
		return totalCount;
	}

	public ResponseEntity<Object> addLeaveRule(@Valid LeaveConfiguration configuration) {
		Optional<LeaveConfiguration> existingConfig = configurationDAO.findbyBusinessUnitAndRole(
				configuration.getBusinessUnits().getBusinessUnitsId(), configuration.getRole().getRoleId());
		Optional<BusinessUnits> checkBussinessUnit = businessUnitsDAO
				.findById(configuration.getBusinessUnits().getBusinessUnitsId());
		Optional<RefRoles> checkRole = refRolesDAO.findById(configuration.getRole().getRoleId());
		if (!existingConfig.isPresent() && checkBussinessUnit.isPresent() && checkRole.isPresent()) {
			LeaveConfiguration leavedetails = configurationDAO.save(new LeaveConfiguration(checkBussinessUnit.get(),
					checkRole.get(), configuration.getLeaves(), configuration.getMonths(), LocalDateTime.now(),
					LocalDateTime.now(), configuration.getCreatedby()));
			return new ResponseEntity<>(new StatusRes("Success", "000", "Rule added succussfully", leavedetails),
					HttpStatus.OK);
		}
		return new ResponseEntity<>(new StatusRes("Failed", "007", "Details not found (or) Rule already existed"),
				HttpStatus.OK);
	}

	public ResponseEntity<Object> updateleaveRule(@Valid LeaveConfiguration configuration) {
		Optional<LeaveConfiguration> checkRule = configurationDAO.findById(configuration.getLeave_configuration_id());
		Optional<BusinessUnits> checkBussinessUnit = businessUnitsDAO
				.findById(configuration.getBusinessUnits().getBusinessUnitsId());
		Optional<RefRoles> checkRole = refRolesDAO.findById(configuration.getRole().getRoleId());
		if (checkRule.isPresent() && checkBussinessUnit.isPresent() && checkRole.isPresent()) {
			Optional<LeaveConfiguration> existingConfig = configurationDAO.findbyBusinessUnitAndRole(
					configuration.getBusinessUnits().getBusinessUnitsId(), configuration.getRole().getRoleId());
			if (!existingConfig.isPresent() || existingConfig.get().getLeave_configuration_id() == checkRule.get()
					.getLeave_configuration_id()) {
				checkRule.get().setBusinessUnits(checkBussinessUnit.get());
				checkRule.get().setRole(checkRole.get());
				checkRule.get().setLeaves(configuration.getLeaves());
				checkRule.get().setMonths(configuration.getMonths());
				checkRule.get().setUpdated_on(LocalDateTime.now());
				checkRule.get().setUpdatedby(configuration.getUpdatedby());
				configurationDAO.save(checkRule.get());
				return new ResponseEntity<>(new StatusRes("Success", "000", "Leave Rule Successfully updated"),
						HttpStatus.OK);
			}
			return new ResponseEntity<>(
					new StatusRes("Failed", "007", "Rule already configured with this Business unit and Role"),
					HttpStatus.OK);
		}
		return new ResponseEntity<>(new StatusRes("Failed", "007", "Details not found"), HttpStatus.OK);
	}

	public ResponseEntity<Object> deleteLeaveRule(Long LeaveConfigId) {
		Optional<LeaveConfiguration> checkRule = configurationDAO.findById(LeaveConfigId);
		if (checkRule.isPresent()) {
			configurationDAO.deleteById(LeaveConfigId);
			return new ResponseEntity<>(new StatusRes("Success", "000", "Leave Rule Successfully Deleted"),
					HttpStatus.OK);
		}
		return new ResponseEntity<>(new StatusRes("Failed", "007", "Leave Rule not available"), HttpStatus.OK);
	}

	public ResponseEntity<Object> viewLeaveRules() {
		return new ResponseEntity<>(configurationDAO.findAll(), HttpStatus.OK);
	}

	@Scheduled(cron = "0/30 * * * * ?") // every 30 Seconds
	public ResponseEntity<Object> leaveConfigurationCron() {
		List<LeaveConfiguration> configList = configurationDAO.findAll();
		configList.forEach(config -> {
			List<UserDetails> userlist = userDetailsDAO.findByRoleAndBusinessUnitActiveStatus(
					config.getRole().getRoleId(), config.getBusinessUnits().getBusinessUnitsId(), true);
			for (UserDetails user : userlist) {
				LeaveDetails checkDetails = leaveDetailsByUserId(user.getUserId(),
						Calendar.getInstance().get(Calendar.YEAR));
				if (checkDetails == null) {
					checkDetails = saveLeaveDetails(user);
				}
				LocalDate doj = DateTimeFormat.forPattern("dd-MM-yyyy").parseLocalDate(user.getDateOfJoining());
				Days joiningDays = Days.daysBetween(doj, LocalDate.now());
				if (joiningDays.getDays() >= 90) {
					Long leaveToBeadd = config.getLeaves();
					Long addByDays = config.getMonths() * 30;
					Long daysBeenLastUpdated = ChronoUnit.DAYS.between(checkDetails.getUpdated_on(),
							LocalDateTime.now());
					if (daysBeenLastUpdated == addByDays || daysBeenLastUpdated > addByDays) {
						checkDetails.setActualAnnualLeaves(checkDetails.getActualAnnualLeaves() + leaveToBeadd);
						checkDetails.setRemainingAnnualLeaves(checkDetails.getRemainingAnnualLeaves() + leaveToBeadd);
						checkDetails.setUpdated_on(LocalDateTime.now());
						leaveDetailsDAO.save(checkDetails);
					}
				}
			}
		});
		return null;
	}

	public ResponseEntity<Object> approvedAndPendingLeavesByUserId(Long UserID) {
		List<LeaveMaster> usedLeaves = new ArrayList<>();
		List<LeaveMaster> approved = leaveMasterDAO.findByUserId(UserID, "Approved");
		List<LeaveMaster> pending = leaveMasterDAO.findByUserId(UserID, "Pending");
		usedLeaves.addAll(approved);
		usedLeaves.addAll(pending);
		return new ResponseEntity<>(usedLeaves, HttpStatus.OK);
	}
	public void sendEmailForLeaveDecline(String email, String name, String approvedBy, String leavetype,
			double numberOfDays, String duration, String reason, String subject, String approvedby,String comment) {

		userServices.sendEmail(email, subject,
				"<!DOCTYPE html><html><head><style>table, th, td {border: 1px solid black;border-collapse: collapse;}</style></head>"
						+ "<body>\r\n"
						+ "<table id=\"Table_01\" style=\"border: 2px solid #062f4f; margin: 10px auto; border-radius: 8px;\" width=\"621\" cellspacing=\"0\" cellpadding=\"0\">"
						+ "<tbody>" + "<tr style=\"background: #062f4f; color: white; height: 39px;\">"
						+ "<td style=\"padding: 20px 10px; font-size: 30px; height: 39px;\">SmarTRM</td>" + "</tr>"
						+ "<tr style=\"height: 36px;\">" + "<td style=\"padding: 20px 10px; height: 36px;\">Dear "
						+ name + "</b>,</p>\r\n" + "  <p style=\"padding-left:50px;\">Your leave <b>" + approvedby
						+ " " + approvedBy + " " + "</b>,</p>\r\n"
						+ "  <p style=\"padding-left:50px;\">Leave Type: <b>" + leavetype + "<b>.</p>\r\n"
						+ "  <p style=\"padding-left:50px;\">Number of Days: <b>" + numberOfDays + "<b>.</p>\r\n"
						+ "  <p style=\"padding-left:50px;\">Leave Duration: <b>" + duration + "<b>.</p>\r\n"
						+ "  <p style=\"padding-left:50px;\">Reason for Decline: <b>" + comment + "<b>.</p>\r\n"
						+ "  <p style=\"padding-left:50px;\">Reason for Leave: <b>" + reason + "<b>.</p>\r\n" + "</td>"
						+ "</tr>" + "<tr style=\"height: 33px;\">"
						+ "<td style=\"padding: 20px 40px; text-align: right; height: 33px;\">Thanks and Regards<br /> SmartRM </td>"
						+ "</tr>" + "</tbody>" + "</table>" + "</body>" + "</html>");
	}
	

}