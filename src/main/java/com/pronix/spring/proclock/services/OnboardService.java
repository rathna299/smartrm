package com.pronix.spring.proclock.services;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;

import com.pronix.spring.proclock.DataSeeder;
import com.pronix.spring.proclock.onboard.DAO.Candidate_Education_detailsDAO;
import com.pronix.spring.proclock.onboard.DAO.Candidate_details_DAO;
import com.pronix.spring.proclock.onboard.DAO.Candidate_job_detailsDAO;
import com.pronix.spring.proclock.onboard.DAO.Candidate_personal_detailsDAO;
import com.pronix.spring.proclock.onboard.models.CandidateLoginPojo;
import com.pronix.spring.proclock.onboard.models.Candidate_details;
import com.pronix.spring.proclock.onboard.models.Candidate_education_details;
import com.pronix.spring.proclock.onboard.models.Candidate_job_details;
import com.pronix.spring.proclock.onboard.models.Candidate_personal_details;
import com.pronix.spring.proclock.onboard.models.OfferConfirmPojo;
import com.pronix.spring.proclock.onboard.models.OfferLetter;
import com.pronix.spring.proclock.pojos.StatusRes;
import com.pronix.spring.proclock.utils.Constants;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class OnboardService {

	@Autowired
	private Candidate_details_DAO candidate_details_DAO;

	@Autowired
	private UserServices userServices;

	@Autowired
	private Candidate_personal_detailsDAO candidate_personal_detailsDAO;

	@Autowired
	private Candidate_Education_detailsDAO candidate_Education_detailsDAO;

	@Autowired
	private Candidate_job_detailsDAO candidate_job_detailsDAO;

	public ResponseEntity<Object> addCandidateDetails(Candidate_details candidate_details) {
		Optional<Candidate_details> checkExistingDetails = candidate_details_DAO
				.findByEmail(candidate_details.getEmail());
		if (!checkExistingDetails.isPresent()) {
			Candidate_details details = candidate_details_DAO.save(candidate_details);
			Calendar now = Calendar.getInstance();
			now.add(Calendar.DAY_OF_MONTH, 5);
			details.setStatus("Pending");
			details.setCreateddate(LocalDateTime.now());
			details.setExpirationdate(now.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
			String password = randomAlphaNumeric();
			details.setVerificationtoken(DataSeeder.getMd5(password));
			candidate_details_DAO.save(details);

			userServices.sendEmail(details.getEmail(), "Pronix requested you to submit documents",
					"<!DOCTYPE html><html><head><style>table, th, td {border: 1px solid black;border-collapse: collapse;}</style></head>"
							+ "<body>\r\n"
							+ "<table id=\"Table_01\" style=\"border: 2px solid #062f4f; margin: 10px auto; border-radius: 8px;\" width=\"621\" cellspacing=\"0\" cellpadding=\"0\">"
							+ "<tbody>" + "<tr style=\"background: #062f4f; color: white; height: 39px;\">"
							+ "<td style=\"padding: 20px 10px; font-size: 30px; height: 39px;\">SmarTRM</td>" + "</tr>"
							+ "<tr style=\"height: 36px;\">" + "<td style=\"padding: 20px 10px; height: 36px;\">Dear "
							+ details.getCandidate_name() + "" + "</b></p>\r\n"
							+ "  <p style=\"padding-left:50px;\">UserName: <b>" + details.getEmail() + "<b>.</p>\r\n"
							+ "  <p style=\"padding-left:50px;\">Password: <b>" + password + "<b>.</p>\r\n"
							+ "  <p style=\"padding-left:50px;\">Please login using this Url: <b>" + "" + "/user-login"
							+ "<b>.</p>\r\n" + "</b>.</p>\r\n" + "</td>" + "</tr>" + "<tr style=\"height: 33px;\">"
							+ "<td style=\"padding: 20px 40px; text-align: right; height: 33px;\">Thanks and Regards<br /> Pronix IT Solution Pvt Ltd </td>"
							+ "</tr>" + "</tbody>" + "</table>" + "</body>" + "</html>");

			return new ResponseEntity<>(new StatusRes("Success", "000", "Candidate added Successfully"), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new StatusRes("Failed", "007", "Email already registered"), HttpStatus.OK);
		}
	}

	public ResponseEntity<Object> updateCandidate(@Valid Candidate_details candidate_details) {
		Optional<Candidate_details> checkCandidate = candidate_details_DAO
				.findById(candidate_details.getCandidate_id());
		Optional<Candidate_details> checkEmail = candidate_details_DAO.findByEmail(candidate_details.getEmail());
		if (checkCandidate.isPresent()) {
			if (checkEmail.isPresent() && checkCandidate.get().equals(checkEmail.get()) || !checkEmail.isPresent()) {
				checkCandidate.get().setCandidate_name(candidate_details.getCandidate_name());
				checkCandidate.get().setCandidate_phone(candidate_details.getCandidate_phone());
				checkCandidate.get().setDesignation(candidate_details.getDesignation());
				checkCandidate.get().setEmail(candidate_details.getEmail());
				checkCandidate.get().setExperience(candidate_details.getExperience());
				candidate_details_DAO.save(checkCandidate.get());
				return new ResponseEntity<>(new StatusRes("Success", "000", "Record updated successfully"),
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new StatusRes("Failed", "007", "Email already registered"), HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<>(new StatusRes("Failed", "007", "Candidate details not found"), HttpStatus.OK);
		}
	}

	public ResponseEntity<Object> viewCandidateLists(String status) {
		if (status != null) {
			List<Candidate_details> finalList = candidate_details_DAO.findAll().stream()
					.filter(details -> details.getStatus().equalsIgnoreCase(status)).collect(Collectors.toList());
			return new ResponseEntity<>(finalList, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(candidate_details_DAO.findAll(), HttpStatus.OK);
		}

	}

	private String ALPHA_NUMERIC_STRING = "abcdefghijklmnopqrstuvwxyz0123456789";

	private String randomAlphaNumeric() {
		int count = 10;
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}

	public ResponseEntity<Object> candidatelogin(CandidateLoginPojo candidateLoginPojo) {
		Optional<Candidate_details> checkEmail = candidate_details_DAO.findByEmail(candidateLoginPojo.getUserName());
		if (checkEmail.isPresent() && checkEmail.get().getVerificationtoken()
				.equals(DataSeeder.getMd5(candidateLoginPojo.getPassword()))) {
			if (checkEmail.get().getExpirationdate().isAfter(LocalDateTime.now())) {
				if (checkEmail.get().getStatus().equals("Submitted")
						|| checkEmail.get().getStatus().equals("Declined")) {
					return new ResponseEntity<>(new StatusRes("Failed", "007", "Your application already processed"),
							HttpStatus.OK);
				} else {
					return new ResponseEntity<>(
							new StatusRes("Success", "000", "Successfully logged in", checkEmail.get()), HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<>(new StatusRes("Failed", "007", "Loggin Expired Please contact HR"),
						HttpStatus.OK);
			}
		}
		return new ResponseEntity<>(new StatusRes("Failed", "007", "Please enter valid login credentials"),
				HttpStatus.OK);
	}

	public ResponseEntity<Object> addPersonal(Candidate_personal_details candidate_personal_details,
			MultipartFile aadhar, MultipartFile pancard, MultipartFile passport) throws IOException {
		Optional<Candidate_details> checkCandidate = candidate_details_DAO
				.findById(candidate_personal_details.getCandidate_details().getCandidate_id());
		Optional<Candidate_personal_details> checkpersonal = candidate_personal_detailsDAO
				.findByCandidateId(candidate_personal_details.getCandidate_details().getCandidate_id());
		if (checkCandidate.isPresent() && checkCandidate.get().getStatus().equals("Pending")
				&& !checkpersonal.isPresent()) {
			Candidate_personal_details details = candidate_personal_detailsDAO.save(candidate_personal_details);
			details.setAadhar_file(aadhar.getOriginalFilename());
			details.setAdhartype(aadhar.getContentType());
			details.setAadharbyte(aadhar.getBytes());
			details.setPancard_file(pancard.getOriginalFilename());
			details.setPantype(pancard.getContentType());
			details.setPanbyte(pancard.getBytes());
			if (passport != null) {
				details.setPassport_file(passport.getOriginalFilename());
				details.setPassporttype(passport.getContentType());
				details.setPassportbyte(passport.getBytes());
			}
			candidate_personal_detailsDAO.save(details);
			return new ResponseEntity<>(new StatusRes("Success", "000", "Details saved successfully"), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new StatusRes("Failed", "007", "Details not found"), HttpStatus.OK);
		}
	}

	public ResponseEntity<Object> updatePersonal(Candidate_personal_details candidate_personal_details,
			MultipartFile aadhar, MultipartFile pancard, MultipartFile passport) throws IOException {
		Optional<Candidate_personal_details> checkPersonal = candidate_personal_detailsDAO
				.findById(candidate_personal_details.getPersonal_details_id());
		if (checkPersonal.isPresent() && checkPersonal.get().getCandidate_details().getStatus().equals("Pending")) {
			checkPersonal.get().setFirst_name(candidate_personal_details.getFirst_name());
			checkPersonal.get().setLast_name(candidate_personal_details.getLast_name());
			checkPersonal.get().setMiddle_name(candidate_personal_details.getMiddle_name());
			checkPersonal.get().setAlternate_number(candidate_personal_details.getAlternate_number());
			checkPersonal.get().setFather_name(candidate_personal_details.getFather_name());
			checkPersonal.get().setFather_number(candidate_personal_details.getFather_number());
			checkPersonal.get().setMother_name(candidate_personal_details.getMother_name());
			checkPersonal.get().setPresent_address(candidate_personal_details.getPresent_address());
			checkPersonal.get().setPermanent_address(candidate_personal_details.getPermanent_address());
			checkPersonal.get().setMartial_status(candidate_personal_details.getMartial_status());
			checkPersonal.get().setNationality(candidate_personal_details.getNationality());
			checkPersonal.get().setGender(candidate_personal_details.getGender());
			if (aadhar != null) {
				checkPersonal.get().setAadhar_file(aadhar.getOriginalFilename());
				checkPersonal.get().setAdhartype(aadhar.getContentType());
				checkPersonal.get().setAadharbyte(aadhar.getBytes());
			}
			if (pancard != null) {
				checkPersonal.get().setPancard_file(pancard.getOriginalFilename());
				checkPersonal.get().setPantype(pancard.getContentType());
				checkPersonal.get().setPanbyte(pancard.getBytes());
			}
			if (passport != null) {
				checkPersonal.get().setPassport_file(passport.getOriginalFilename());
				checkPersonal.get().setPassporttype(passport.getContentType());
				checkPersonal.get().setPassportbyte(passport.getBytes());
			}
			if (passport == null) {
				checkPersonal.get().setPassport_file(null);
				checkPersonal.get().setPassporttype(null);
				checkPersonal.get().setPassportbyte(null);
			}

			candidate_personal_detailsDAO.save(checkPersonal.get());
			return new ResponseEntity<>(new StatusRes("Success", "000", "Record updated successfully"), HttpStatus.OK);
		}
		return new ResponseEntity<>(new StatusRes("Failed", "007", "Details not found"), HttpStatus.OK);

	}

	public ResponseEntity<Object> viewPersonalDetails(Long candidate_Id) {
		Optional<Candidate_personal_details> details = candidate_personal_detailsDAO.findByCandidateId(candidate_Id);
		if (details.isPresent()) {
			return new ResponseEntity<>(details.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>("{}", HttpStatus.OK);
		}

	}

	public ResponseEntity<Object> addEducation(Candidate_education_details candidate_education_details,
			MultipartFile marksheet, MultipartFile certificate) throws IOException {
		Optional<Candidate_details> checkCandidate = candidate_details_DAO
				.findById(candidate_education_details.getCandidate_details().getCandidate_id());
		if (checkCandidate.isPresent() && checkCandidate.get().getStatus().equals("Pending")) {
			List<Candidate_education_details> checkExistingQualification = candidate_Education_detailsDAO
					.findbyCandidateId(candidate_education_details.getCandidate_details().getCandidate_id()).stream()
					.filter(check -> check.getHighest_qualification()
							.equalsIgnoreCase(candidate_education_details.getHighest_qualification()))
					.collect(Collectors.toList());

			if (checkExistingQualification.size() > 0) {
				return new ResponseEntity<>(new StatusRes("Filed", "007", "This qualification Already added"),
						HttpStatus.OK);
			}
			Candidate_education_details education_details = candidate_Education_detailsDAO
					.save(candidate_education_details);
			education_details.setMarksheetfile(marksheet.getOriginalFilename());
			education_details.setMarksheettype(marksheet.getContentType());
			education_details.setMarksheetbyte(marksheet.getBytes());
			if (certificate != null) {
				education_details.setCertificates(certificate.getOriginalFilename());
				education_details.setCertificatestype(certificate.getContentType());
				education_details.setCertificatesbye(certificate.getBytes());
			}
			candidate_Education_detailsDAO.save(education_details);
			return new ResponseEntity<>(new StatusRes("Success", "000", "Details saved successfully"), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new StatusRes("Failed", "00", "Details not found"), HttpStatus.OK);
		}
	}

	public ResponseEntity<Object> updateEducation(Candidate_education_details candidate_education_details,
			MultipartFile marksheet, MultipartFile certificate) throws IOException {
		Optional<Candidate_education_details> education_details = candidate_Education_detailsDAO
				.findById(candidate_education_details.getEducational_details_id());
		if (education_details.isPresent()
				&& education_details.get().getCandidate_details().getStatus().equals("Pending")) {
			education_details.get().setHighest_qualification(candidate_education_details.getHighest_qualification());
			education_details.get().setBranch(candidate_education_details.getBranch());
			education_details.get().setPercentage(candidate_education_details.getPercentage());
			education_details.get().setUniversity(candidate_education_details.getUniversity());
			education_details.get().setPass_out_year(candidate_education_details.getPass_out_year());
			if (marksheet != null) {
				education_details.get().setMarksheetfile(marksheet.getOriginalFilename());
				education_details.get().setMarksheettype(marksheet.getContentType());
				education_details.get().setMarksheetbyte(marksheet.getBytes());
			}
			if (certificate != null) {
				education_details.get().setCertificates(certificate.getOriginalFilename());
				education_details.get().setCertificatestype(certificate.getContentType());
				education_details.get().setCertificatesbye(certificate.getBytes());
			}
			candidate_Education_detailsDAO.save(education_details.get());
			return new ResponseEntity<>(new StatusRes("Success", "000", "Record updated successfully"), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new StatusRes("Failed", "007", "Details not found"), HttpStatus.OK);
		}
	}

	public ResponseEntity<Object> viewEducationDetailsList(Long candidate_Id) {
		return new ResponseEntity<>(candidate_Education_detailsDAO.findbyCandidateId(candidate_Id), HttpStatus.OK);
	}

	public ResponseEntity<Object> addJobDetails(Candidate_job_details candidate_job_details, MultipartFile offerletter,
			MultipartFile relievingletter, MultipartFile payslip_one, MultipartFile payslip_two,
			MultipartFile payslip_three) throws IOException {
		Optional<Candidate_details> checkCandidate = candidate_details_DAO
				.findById(candidate_job_details.getCandidate_details().getCandidate_id());
		if (checkCandidate.isPresent() && checkCandidate.get().getStatus().equals("Pending")) {
			Candidate_job_details job_details = candidate_job_detailsDAO.save(candidate_job_details);
			job_details.setOfferletterfile(offerletter.getOriginalFilename());
			job_details.setOfferlettertype(offerletter.getContentType());
			job_details.setOfferletterbyte(offerletter.getBytes());
			job_details.setRelievingletterfile(relievingletter.getOriginalFilename());
			job_details.setRelievinglettertype(relievingletter.getContentType());
			job_details.setRelievingletterbyte(relievingletter.getBytes());
			if (payslip_one != null) {
				job_details.setPayslipfile_one(payslip_one.getOriginalFilename());
				job_details.setPayslipfiletype_one(payslip_one.getContentType());
				job_details.setPayslipfilebyte_one(payslip_one.getBytes());
			}
			if (payslip_two != null) {
				job_details.setPayslipfile_two(payslip_two.getOriginalFilename());
				job_details.setPayslipfiletype_two(payslip_two.getContentType());
				job_details.setPayslipfilebyte_two(payslip_two.getBytes());
			}
			if (payslip_three != null) {
				job_details.setPayslipfile_three(payslip_three.getOriginalFilename());
				job_details.setPayslipfiletype_three(payslip_three.getContentType());
				job_details.setPayslipfilebyte_three(payslip_three.getBytes());
			}
			candidate_job_detailsDAO.save(job_details);
			return new ResponseEntity<>(new StatusRes("Success", "000", "Successfully saved"), HttpStatus.OK);
		}
		return new ResponseEntity<>(new StatusRes("Failed", "007", "Candidate details not found"), HttpStatus.OK);
	}

	public ResponseEntity<Object> updatejob(Candidate_job_details candidate_job_details, MultipartFile offerletter,
			MultipartFile relievingletter, MultipartFile payslip_one, MultipartFile payslip_two,
			MultipartFile payslip_three) throws IOException {
		Optional<Candidate_job_details> job_details = candidate_job_detailsDAO
				.findById(candidate_job_details.getJob_details_id());
		if (job_details.isPresent() && job_details.get().getCandidate_details().getStatus().equals("Pending")) {
			job_details.get().setCompanyname(candidate_job_details.getCompanyname());
			job_details.get().setDesignation(candidate_job_details.getDesignation());
			job_details.get().setFromdate(candidate_job_details.getFromdate());
			job_details.get().setTodate(candidate_job_details.getTodate());
			job_details.get().setEmployee_referance_number(candidate_job_details.getEmployee_referance_number());
			job_details.get().setManager_number(candidate_job_details.getManager_number());
			if (offerletter != null) {
				job_details.get().setOfferletterfile(offerletter.getOriginalFilename());
				job_details.get().setOfferlettertype(offerletter.getContentType());
				job_details.get().setOfferletterbyte(offerletter.getBytes());
			}
			if (relievingletter != null) {
				job_details.get().setRelievingletterfile(relievingletter.getOriginalFilename());
				job_details.get().setRelievinglettertype(relievingletter.getContentType());
				job_details.get().setRelievingletterbyte(relievingletter.getBytes());
			}
			if (payslip_one != null) {
				job_details.get().setPayslipfile_one(payslip_one.getOriginalFilename());
				job_details.get().setPayslipfiletype_one(payslip_one.getContentType());
				job_details.get().setPayslipfilebyte_one(payslip_one.getBytes());
			}
			if (payslip_two != null) {
				job_details.get().setPayslipfile_two(payslip_two.getOriginalFilename());
				job_details.get().setPayslipfiletype_two(payslip_two.getContentType());
				job_details.get().setPayslipfilebyte_two(payslip_two.getBytes());
			}
			if (payslip_three != null) {
				job_details.get().setPayslipfile_three(payslip_three.getOriginalFilename());
				job_details.get().setPayslipfiletype_three(payslip_three.getContentType());
				job_details.get().setPayslipfilebyte_three(payslip_three.getBytes());
			}
			candidate_job_detailsDAO.save(job_details.get());
			return new ResponseEntity<>(new StatusRes("Success", "000", "Record updated successfully"), HttpStatus.OK);
		}
		return new ResponseEntity<>(new StatusRes("Failed", "007", "Details not found"), HttpStatus.OK);
	}

	public ResponseEntity<Object> viewJobDetailslist(Long candidate_Id) {
		return new ResponseEntity<>(candidate_job_detailsDAO.findbyCandidateId(candidate_Id), HttpStatus.OK);
	}

	public ResponseEntity<byte[]> getFile(Long pId, String file) {
		String personalDocs[] = { "aadhar", "pancard", "passport" };
		String educationDocs[] = { "marksheet", "certificates" };
		String jobDocs[] = { "offerletter", "relievingletter", "payslip_one", "payslip_two", "payslip_three" };
		if (file.equalsIgnoreCase(personalDocs[0]) || file.equalsIgnoreCase(personalDocs[1])
				|| file.equalsIgnoreCase(personalDocs[2])) {
			Optional<Candidate_personal_details> personal_details = candidate_personal_detailsDAO.findById(pId);
			if (personal_details.isPresent() && file.equals(personalDocs[0])) {
				return ResponseEntity.ok().contentType(MediaType.parseMediaType(personal_details.get().getAdhartype()))
						.body(personal_details.get().getAadharbyte());

			} else if (personal_details.isPresent() && file.equals(personalDocs[1])) {
				return ResponseEntity.ok().contentType(MediaType.parseMediaType(personal_details.get().getPantype()))
						.body(personal_details.get().getPanbyte());

			} else if (personal_details.isPresent() && file.equals(personalDocs[2])) {
				return ResponseEntity.ok()
						.contentType(MediaType.parseMediaType(personal_details.get().getPassporttype()))
						.body(personal_details.get().getPassportbyte());

			}
		} else if (file.equalsIgnoreCase(educationDocs[0]) || file.equalsIgnoreCase(educationDocs[1])) {
			Optional<Candidate_education_details> education_details = candidate_Education_detailsDAO.findById(pId);
			if (education_details.isPresent() && file.equalsIgnoreCase(educationDocs[0])) {
				return ResponseEntity.ok()
						.contentType(MediaType.parseMediaType(education_details.get().getMarksheettype()))
						.body(education_details.get().getMarksheetbyte());
			} else if (education_details.isPresent() || file.equalsIgnoreCase(educationDocs[1])) {
				return ResponseEntity.ok()
						.contentType(MediaType.parseMediaType(education_details.get().getCertificatestype()))
						.body(education_details.get().getCertificatesbye());
			}

		} else if (file.equalsIgnoreCase(jobDocs[0]) || file.equalsIgnoreCase(jobDocs[1])
				|| file.equalsIgnoreCase(jobDocs[2]) || file.equalsIgnoreCase(jobDocs[3])
				|| file.equalsIgnoreCase(jobDocs[4])) {
			Optional<Candidate_job_details> job_details = candidate_job_detailsDAO.findById(pId);
			if (job_details.isPresent() && file.equalsIgnoreCase(jobDocs[0])) {
				return ResponseEntity.ok().contentType(MediaType.parseMediaType(job_details.get().getOfferlettertype()))
						.body(job_details.get().getOfferletterbyte());
			} else if (job_details.isPresent() && file.equalsIgnoreCase(jobDocs[1])) {
				return ResponseEntity.ok()
						.contentType(MediaType.parseMediaType(job_details.get().getRelievinglettertype()))
						.body(job_details.get().getRelievingletterbyte());
			} else if (job_details.isPresent() && file.equalsIgnoreCase(jobDocs[2])) {
				return ResponseEntity.ok()
						.contentType(MediaType.parseMediaType(job_details.get().getPayslipfiletype_one()))
						.body(job_details.get().getPayslipfilebyte_one());
			} else if (job_details.isPresent() && file.equalsIgnoreCase(jobDocs[3])) {
				return ResponseEntity.ok()
						.contentType(MediaType.parseMediaType(job_details.get().getPayslipfiletype_two()))
						.body(job_details.get().getPayslipfilebyte_two());
			} else if (job_details.isPresent() && file.equalsIgnoreCase(jobDocs[4])) {
				return ResponseEntity.ok()
						.contentType(MediaType.parseMediaType(job_details.get().getPayslipfiletype_three()))
						.body(job_details.get().getPayslipfilebyte_three());
			}
		}
		return null;
	}

	public ResponseEntity<Object> submitApplication(Long candidate_Id) {
		Optional<Candidate_details> checkCandidate = candidate_details_DAO.findById(candidate_Id);
		if (checkCandidate.isPresent() && checkCandidate.get().getStatus().equals("Pending")) {
			Optional<Candidate_personal_details> checkPersonal = candidate_personal_detailsDAO
					.findByCandidateId(candidate_Id);
			List<Candidate_education_details> checkEducation = candidate_Education_detailsDAO
					.findbyCandidateId(candidate_Id);
			if (checkPersonal.isPresent() && !checkEducation.isEmpty()) {
				checkCandidate.get().setStatus("Submitted");
				checkCandidate.get().setSubmittedon(LocalDateTime.now());
				candidate_details_DAO.save(checkCandidate.get());
				return new ResponseEntity<>(new StatusRes("Success", "000", "Application Submitted successfully"),
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new StatusRes("Failed", "007",
						"Please submit your application after adding your personal,Education and job details only"),
						HttpStatus.OK);
			}
		}
		return new ResponseEntity<>(new StatusRes("Failed", "007", "Details not found please your application"),
				HttpStatus.OK);
	}

	public ResponseEntity<Object> approveOffer(OfferConfirmPojo confirmPojo) {
		Optional<Candidate_details> details = candidate_details_DAO.findById(confirmPojo.getCandidate_Id());
		if (details.isPresent() && details.get().getStatus().equals("Submitted")) {
			details.get().setDate_of_joining(confirmPojo.getDate_of_joining());
			details.get().setCtc(confirmPojo.getCtc());
			details.get().setStatus("Approved");
			candidate_details_DAO.save(details.get());

			userServices.sendEmail(details.get().getEmail(), "Notification from pronix",
					"<!DOCTYPE html><html><head><style>table, th, td {border: 1px solid black;border-collapse: collapse;}</style></head>"
							+ "<body>\r\n"
							+ "<table id=\"Table_01\" style=\"border: 2px solid #062f4f; margin: 10px auto; border-radius: 8px;\" width=\"621\" cellspacing=\"0\" cellpadding=\"0\">"
							+ "<tbody>" + "<tr style=\"background: #062f4f; color: white; height: 39px;\">"
							+ "<td style=\"padding: 20px 10px; font-size: 30px; height: 39px;\">SmarTRM</td>" + "</tr>"
							+ "<tr style=\"height: 36px;\">" + "<td style=\"padding: 20px 10px; height: 36px;\">Dear "
							+ details.get().getCandidate_name() + "" + "</b></p>\r\n"
							+ "  <p style=\"padding-left:50px;\">Congratulations !!\r\n <b>"
							+ "  <p style=\"padding-left:50px;\">We are pleased to offer you a position with our company Pronix IT Solutions Pvt. Ltd. to work as a "
							+ details.get().getDesignation()
							+ " at Our Office Gachibowli, Hyderabad.You will be paid a CTC of INR "
							+ details.get().getCtc() + "Laks. Paid Leaves per annum as per start date. "
							+ "  <p style=\"padding-left:50px;\">Your tentative start date for the position is "
							+ details.get().getDate_of_joining()
							+ "  at Our Office Gachibowli, Hyderabad and reporting timing is 2.00 PM IST. You can consider this email as official confirmation for you to start with us. Please acknowledge us with your acceptance on the above.\r\n "
							+ "</b>.</p>\r\n" + "</td>"
							+ "</tr>" + "<tr style=\"height: 33px;\">"
							+ "<td style=\"padding: 20px 40px; text-align: right; height: 33px;\">Thanks and Regards<br /> Pronix IT Solution Pvt Ltd </td>"
							+ "</tr>" + "</tbody>" + "</table>" + "</body>" + "</html>");
			
			return new ResponseEntity<>(new StatusRes("Success", "000", "Offer Confirmation Successful"),
					HttpStatus.OK);
		} else if (details.get().getStatus().equals("Pending") || details.get().getStatus().equals("Approved")
				|| details.get().getStatus().equals("Declined")) {
			return new ResponseEntity<>(new StatusRes("Failed", "007", "Please check status of this application"),
					HttpStatus.OK);
		}
		return new ResponseEntity<>(new StatusRes("Failed", "007", "Details not found"), HttpStatus.OK);
	}

	public ResponseEntity<Object> reminder(Long candidate_Id) {
		Optional<Candidate_details> details = candidate_details_DAO.findById(candidate_Id);
		if (details.isPresent()) {
			userServices.sendEmail(details.get().getEmail(), "Reminder from Pronix",
					"<!DOCTYPE html><html><head><style>table, th, td {border: 1px solid black;border-collapse: collapse;}</style></head>"
							+ "<body>\r\n"
							+ "<table id=\"Table_01\" style=\"border: 2px solid #062f4f; margin: 10px auto; border-radius: 8px;\" width=\"621\" cellspacing=\"0\" cellpadding=\"0\">"
							+ "<tbody>" + "<tr style=\"background: #062f4f; color: white; height: 39px;\">"
							+ "<td style=\"padding: 20px 10px; font-size: 30px; height: 39px;\">SmarTRM</td>" + "</tr>"
							+ "<tr style=\"height: 36px;\">" + "<td style=\"padding: 20px 10px; height: 36px;\">Dear "
							+ details.get().getCandidate_name() + "" + "</b></p>\r\n"
							+ "  <p style=\"padding-left:50px;\">Your account will expiring on: <b>"
							+ details.get().getExpirationdate() + "<b>.</p>\r\n"
							+ "  <p style=\"padding-left:50px;\">Please submit your document before expiration <b>"
							+ "\r\n" + "</b>.</p>\r\n" + "</td>" + "</tr>" + "<tr style=\"height: 33px;\">"
							+ "<td style=\"padding: 20px 40px; text-align: right; height: 33px;\">Thanks and Regards<br /> Pronix IT Solution Pvt Ltd </td>"
							+ "</tr>" + "</tbody>" + "</table>" + "</body>" + "</html>");

			return new ResponseEntity<>(new StatusRes("Success", "000", "Reminder Sent successfully"), HttpStatus.OK);
		}
		return null;
	}

	public ResponseEntity<Object> decline(Long candidate_Id, String comment) {
		Optional<Candidate_details> details = candidate_details_DAO.findById(candidate_Id);
		if (details.isPresent()) {
			if (details.get().getStatus().equals("Submitted")) {
				details.get().setStatus("Declined");
				details.get().setDecline_comment(comment);
				candidate_details_DAO.save(details.get());
				return new ResponseEntity<>(new StatusRes("Success", "000", "Application declined successfully"),
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new StatusRes("Failed", "007",
						"This application cannot be declined, For more details please check application status"),
						HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<>(new StatusRes("Failed", "007", "Details not found"), HttpStatus.OK);
		}

	}

	public ResponseEntity<Object> resubmission(Long candidate_Id, String comment) {
		Optional<Candidate_details> details = candidate_details_DAO.findById(candidate_Id);
		if (details.isPresent()) {
			if (details.get().getStatus().equals("Submitted")) {
				details.get().setStatus("Pending");
				details.get().setResubmission_comment(comment);
				Calendar now = Calendar.getInstance();
				now.add(Calendar.DAY_OF_MONTH, 3);
				String password = randomAlphaNumeric();
				details.get().setVerificationtoken(DataSeeder.getMd5(password));
				details.get()
						.setExpirationdate(now.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
				candidate_details_DAO.save(details.get());
				userServices.sendEmail(details.get().getEmail(), "Please resubmit your Application",
						"<!DOCTYPE html>\r\n" + "<html>\r\n" + "								<body>\r\n"
								+ "								<table id=\"Table_01\" style=\"border: 2px solid #062f4f; margin: 10px auto; border-radius: 8px;\" width=\"621\" cellspacing=\"0\" cellpadding=\"0\">\r\n"
								+ "                                    <tbody>\r\n"
								+ "                                        <tr style=\"background: #062f4f; color: white; height: 39px;\">\r\n"
								+ "                                            <td style=\"padding: 20px 10px; font-size: 30px; height: 39px;\">\r\n"
								+ "                                            SmarTRM\r\n"
								+ "                                            </td>\r\n"
								+ "                                        </tr>\r\n"
								+ "                                        <tr style=\"height: 36px;\">\r\n"
								+ "                                            <td style=\"padding: 20px 10px; height: 36px;\">\r\n"
								+ "                                                Dear <b>"
								+ details.get().getCandidate_name() + "</b> \r\n"
								+ "                                                <p style=\"padding-left:50px;\">"
								+ comment + "</p>\r\n" + "                                            </td>\r\n"
								+ "                                            \r\n"
								+ "                                        </tr>\r\n"
								+ "                                        <hr>\r\n"
								+ "                                        <tr>\r\n"
								+ "                                            <td>\r\n"
								+ "                                                <p style=\"padding-left:50px;\"><b>UserName:</b>"
								+ details.get().getEmail() + "</p>\r\n"
								+ "                                                <p style=\"padding-left:50px;\"><b>Password:</b>"
								+ password + "</p>\r\n"
								+ "                                                <p style=\"padding-left:50px;\"><b>Please login using this URL:</b>"
								+ "Constants.hostnameUI" + "/user-login" + " </p>\r\n"

								+ "                                            </td>\r\n"
								+ "                                        </tr>\r\n"
								+ "                                        <hr>\r\n"
								+ "                                        <tr>\r\n"
								+ "                                            <td style=\"padding: 20px 40px; text-align: right; height: 33px;\">\r\n"
								+ "                                                <b>Thanks and Regards </b><br>Pronix IT Solutions\r\n"
								+ "                                            </td>\r\n"
								+ "                                        </tr>\r\n"
								+ "                                    </tbody>\r\n" + "</table>\r\n"
								+ "    </body>\r\n" + "</html>");
				return new ResponseEntity<>(
						new StatusRes("Success", "000", "Application resubmission request sent successfully"),
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new StatusRes("Failed", "007",
						"This application cannot be resubmitted, For more details please check application status"),
						HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<>(new StatusRes("Failed", "007", "Details not found"), HttpStatus.OK);
		}

	}

	public void downloadZip(Long candidate_Id, HttpServletResponse response) {
		Optional<Candidate_details> checkCandidate = candidate_details_DAO.findById(candidate_Id);
		if (checkCandidate.isPresent() && checkCandidate.get().getStatus().equals("Submitted")
				|| checkCandidate.get().getStatus().equals("Approved")) {
			Optional<Candidate_personal_details> personal = candidate_personal_detailsDAO
					.findByCandidateId(candidate_Id);
			List<Candidate_education_details> education = candidate_Education_detailsDAO
					.findbyCandidateId(candidate_Id);
			List<Candidate_job_details> job = candidate_job_detailsDAO.findbyCandidateId(candidate_Id);
			List<byte[]> byteList = new ArrayList<byte[]>();
			List<String> fileName = new ArrayList<>();
			byteList.add(personal.get().getAadharbyte());
			fileName.add(personal.get().getAadhar_file());

			byteList.add(personal.get().getPanbyte());
			fileName.add(personal.get().getPancard_file());
			
			if (personal.get().getPassport_file() != null) {
				byteList.add(personal.get().getPassportbyte());
				fileName.add(personal.get().getPassport_file());
			}
			for (Candidate_education_details education_details : education) {
				fileName.add(education_details.getMarksheetfile());
				byteList.add(education_details.getMarksheetbyte());

				fileName.add(education_details.getCertificates());
				byteList.add(education_details.getCertificatesbye());
			}

			for (Candidate_job_details job_details : job) {
				fileName.add(job_details.getOfferletterfile());
				byteList.add(job_details.getOfferletterbyte());

				fileName.add(job_details.getRelievingletterfile());
				byteList.add(job_details.getRelievingletterbyte());

				fileName.add(job_details.getPayslipfile_one());
				byteList.add(job_details.getPayslipfilebyte_one());
				try {
					if (job_details.getPayslipfile_two() != null) {
						fileName.add(job_details.getPayslipfile_two());
						byteList.add(job_details.getPayslipfilebyte_two());
					}
				} catch (NullPointerException e) {
					e.printStackTrace();
				}
				try {
					if (job_details.getPayslipfile_three() != null) {
						fileName.add(job_details.getPayslipfile_three());
						byteList.add(job_details.getPayslipfilebyte_three());
					}
				} catch (NullPointerException e) {
					e.printStackTrace();
				}

			}
//			try {
//				if (personal.get().getPassport_file() != null) {
//					byteList.add(personal.get().getPassportbyte());
//					fileName.add(personal.get().getPassport_file());
//				}
//			} catch (NullPointerException e) {
//				e.printStackTrace();
//			}
			response.setContentType("application/octet-stream");
			response.setHeader("Content-Disposition", "attachment;filename=" + checkCandidate.get().getCandidate_name()
					+ checkCandidate.get().getCandidate_id() + ".zip");
			response.setStatus(HttpServletResponse.SC_OK);

			try (ZipOutputStream zippedOut = new ZipOutputStream(response.getOutputStream())) {
				for (int i = 0; i < fileName.size(); i++) {
					ZipEntry entry = new ZipEntry(+i + "_" + fileName.get(i));
					zippedOut.putNextEntry(entry);
					zippedOut.write(byteList.get(i), 0, byteList.get(i).length);
					zippedOut.closeEntry();
				}
				zippedOut.finish();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

	public Object OnbordCount() {
		HashMap<String, Long> count = new HashMap<>();
		long pending = candidate_details_DAO.findByStatus("Pending");
		long approved = candidate_details_DAO.findByStatus("Approved");
		long Submitted = candidate_details_DAO.findByStatus("Submitted");
		long declined = candidate_details_DAO.findByStatus("Declined");
		count.put("pending", pending);
		count.put("Approved", approved);
		count.put("submitted", Submitted);
		count.put("Declined", declined);
		return count;
	}

	public void generateOfferletter(Long id, HttpServletResponse response) throws JRException, IOException {

		List<OfferLetter> letters = new ArrayList<>();
		Optional<Candidate_details> candidate_details = candidate_details_DAO.findById(id);
		if (candidate_details.isPresent() && candidate_details.get().getStatus().equalsIgnoreCase("Approved")) {
			OfferLetter letter = new OfferLetter();
			letter.setDesingation(candidate_details.get().getDesignation());
			letter.setDoj(candidate_details.get().getDate_of_joining());
			letter.setEmail(candidate_details.get().getEmail());
			letter.setFullname(candidate_details.get().getCandidate_name());
			letter.setPhone(candidate_details.get().getCandidate_phone().toString());
			letter.setSalary(candidate_details.get().getCtc());
			letters.add(letter);
			File file = ResourceUtils.getFile("classpath:OfferLetter.jrxml");
			JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
			JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(letters);
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("createdBy", "Java");
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);

			response.setContentType("application/x-pdf");
			response.setHeader("Content-Disposition", "inline; filename=offerletter.pdf");
			final OutputStream outputstream = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, outputstream);
		} else {

		}
	}

}
