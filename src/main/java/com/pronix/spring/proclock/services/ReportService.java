package com.pronix.spring.proclock.services;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.servlet.http.HttpServletResponse;

import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.pronix.spring.proclock.dao.AssetsDao;
import com.pronix.spring.proclock.dao.Assets_employeeDAO;
import com.pronix.spring.proclock.dao.DepartmentDAO;
import com.pronix.spring.proclock.dao.JobTitlesDAO;
import com.pronix.spring.proclock.dao.LeaveDetailsDAO;
import com.pronix.spring.proclock.dao.LeaveMasterDAO;
import com.pronix.spring.proclock.dao.RefStatusDAO;
import com.pronix.spring.proclock.dao.UserContactsDAO;
import com.pronix.spring.proclock.dao.UserDetailsDAO;
import com.pronix.spring.proclock.models.Assets;
import com.pronix.spring.proclock.models.Assets_Employees;
import com.pronix.spring.proclock.models.Department;
import com.pronix.spring.proclock.models.JobTitles;
import com.pronix.spring.proclock.models.LeaveDetails;
import com.pronix.spring.proclock.models.LeaveMaster;
import com.pronix.spring.proclock.models.UserContacts;
import com.pronix.spring.proclock.models.UserDetails;
import com.pronix.spring.proclock.pojos.AssetReport;
import com.pronix.spring.proclock.pojos.EmployeeReport;
import com.pronix.spring.proclock.pojos.LeaveReport;
import com.pronix.spring.proclock.pojos.StatusRes;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;

@Service
public class ReportService {

	@Autowired
	private UserDetailsDAO userDetailsDAO;

	@Autowired
	private UserContactsDAO userContactsDAO;

	@Autowired	
	private LeaveMasterDAO leaveMasterDAO;
	
	@Autowired
	private RefStatusDAO refStatusDAO;

	@Autowired
	private AssetsDao assetsDao;

	@Autowired
	private DepartmentDAO departmentDAO;

	@Autowired
	private JobTitlesDAO jobTitlesDAO;

	@Autowired
	private LeaveDetailsDAO leaveDetailsDAO;

	@Autowired
	private Assets_employeeDAO assets_employeeDAO;

	public List<EmployeeReport> EmployeeReport(boolean enable) {
		List<EmployeeReport> reportlist = new ArrayList<>();
		List<UserDetails> userslist = userDetailsDAO.findByEnabled(enable);
		userslist.forEach(userinfo -> {
			if (userinfo.getTempRefRoles().getRoleId() != 1 && userinfo.getTempRefRoles().getRoleId() != 2) {
				UserContacts contact = userContactsDAO.findByUserDetails(userinfo);
				EmployeeReport userSet = new EmployeeReport();
				userSet.setEmployeeid(userinfo.getUserId());
				userSet.setName(userinfo.getFirstName());
				userSet.setDoj(userinfo.getDateOfJoining());
				userSet.setEmail(contact.getEmailId());
				userSet.setPhone(contact.getPhoneNo());
				userSet.setDesignation(userinfo.getJobTitles().getJobtitle());
				userSet.setDepartment(userinfo.getDepartment().getDepartmentName());
				userSet.setManager(
						userinfo.getTempUserDetails().getFirstName() + userinfo.getTempUserDetails().getLastName());
				userSet.setRole(userinfo.getTempRefRoles().getValue());
				reportlist.add(userSet);
			}

		});

		return reportlist;
	}

	public void generateEmployeeReport(boolean enable, String reportFormat, HttpServletResponse response)
			throws JRException, IOException {
		List<EmployeeReport> reportlist = new ArrayList<>();
		List<UserDetails> userslist = userDetailsDAO.findByEnabled(enable);
		try {
			for (UserDetails userinfo : userslist) {
				if (userinfo.getTempRefRoles().getRoleId() != 1 && userinfo.getTempRefRoles().getRoleId() != 2) {
					UserContacts contact = userContactsDAO.findByUserDetails(userinfo);
					EmployeeReport userSet = new EmployeeReport();
					userSet.setEmployeeid(userinfo.getUserId());
					userSet.setName(userinfo.getFirstName() + userinfo.getLastName());
					userSet.setDoj(userinfo.getDateOfJoining());
					userSet.setEmail(contact.getEmailId());
					userSet.setPhone(contact.getPhoneNo());
					userSet.setDesignation(userinfo.getJobTitles().getJobtitle());
					userSet.setDepartment(userinfo.getDepartment().getDepartmentName());
					userSet.setManager(
							userinfo.getTempUserDetails().getFirstName() + userinfo.getTempUserDetails().getLastName());
					userSet.setRole(userinfo.getTempRefRoles().getValue());
					reportlist.add(userSet);
				}
			}
			if (reportlist.size() > 0) {
				String path = "C:\\Users\\PRONIX7\\Desktop\\report\\employees";
				// load file and compile it
				File file = ResourceUtils.getFile("classpath:employeList.jrxml");
				JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
				JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(reportlist);
				Map<String, Object> parameters = new HashMap<>();
				parameters.put("createdBy", "Java");
				JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
				if (reportFormat.equalsIgnoreCase("pdf")) {
					response.setContentType("application/x-pdf");
					response.setHeader("Content-Disposition", "inline; filename=EmployeeList.pdf");
					final OutputStream outputstream = response.getOutputStream();
					JasperExportManager.exportReportToPdfStream(jasperPrint, outputstream);
				}
				if (reportFormat.equalsIgnoreCase("xls")) {
					response.setContentType("application/x-xls");
					response.setHeader("Content-Disposition", "inline; filename=EmployeeList.xlsx");
					final OutputStream outputstream = response.getOutputStream();
					JRXlsxExporter exportXls = new JRXlsxExporter();

					exportXls.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
					exportXls.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outputstream);
					exportXls.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
					exportXls.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
					exportXls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);

					exportXls.exportReport();
				}
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}

	}

	public ResponseEntity<Object> Leavelist(String fromdate, String todate, String status) {
		List<LeaveReport> leavereport = new ArrayList<>();
		LocalDate from_date = DateTimeFormat.forPattern("dd-MM-yyy").parseLocalDate(fromdate);
		LocalDate to_date = DateTimeFormat.forPattern("dd-MM-yyy").parseLocalDate(todate);
		Days days = Days.daysBetween(from_date, to_date);
		for (int i = 0; i <= days.getDays(); i++) {
			List<LeaveMaster> leavemaster = leaveMasterDAO
					.findbyfromdateAndStatus(from_date.plusDays(i).toString("dd-MM-yyyy"), status);
			for (LeaveMaster master : leavemaster) {
				LeaveReport finalrep = new LeaveReport();
				Department depart = departmentDAO
						.findByDepartmentName(master.getUserDetails().getDepartment().getDepartmentName());
				JobTitles designation = jobTitlesDAO
						.findByJobtitle(master.getUserDetails().getJobTitles().getJobtitle());
				LeaveDetails leaveDetails = leaveDetailsDAO.findByUserId(master.getUserDetails().getUserId()).get();

				finalrep.setEid(master.getUserDetails().getUserId());
				finalrep.setLeavetype(master.getLeaveType());
				finalrep.setFromdate(master.getFromDate());
				finalrep.setTodate(master.getToDate());
				finalrep.setNumberofdays(master.getDays());
				finalrep.setName(master.getUserDetails().getFirstName() + " " + master.getUserDetails().getLastName());
				finalrep.setManager(master.getStatus());
				finalrep.setDepartment(depart.getDepartmentName());
				finalrep.setDesignation(designation.getJobtitle());
				finalrep.setRemainingleaves(
						leaveDetails.getRemainingAnnualLeaves() + leaveDetails.getRemainingSickleaves());
				finalrep.setUtilizedLeaves(
						leaveDetails.getUtilisedAnnualLeaves() + leaveDetails.getUtilizedsickLeaves());
				finalrep.setApprovedBy(master.getUserDetails().getTempUserDetails().getFirstName() + " "
						+ master.getUserDetails().getTempUserDetails().getLastName());

				leavereport.add(finalrep);
			}
		}
		if (leavereport.isEmpty()) {
			return new ResponseEntity<Object>(new StatusRes("failed", "007", "Between Dates No Data"), HttpStatus.OK);
		}
		return new ResponseEntity<Object>(leavereport, HttpStatus.OK);
	}

	public void generateleavereport(String fromdate, String todate, String status, String reportFormat,
			HttpServletResponse response) throws JRException, IOException {
		List<LeaveReport> leavereport = new ArrayList<>();
		try {
			LocalDate from_date = DateTimeFormat.forPattern("dd-MM-yyy").parseLocalDate(fromdate);
			LocalDate to_date = DateTimeFormat.forPattern("dd-MM-yyy").parseLocalDate(todate);
			Days days = Days.daysBetween(from_date, to_date);
			for (int i = 0; i <= days.getDays(); i++) {
				List<LeaveMaster> leavemaster = leaveMasterDAO
						.findbyfromdateAndStatus(from_date.plusDays(i).toString("dd-MM-yyyy"), status);

				for (LeaveMaster master : leavemaster) {
					LeaveReport finalrep = new LeaveReport();
					Department depart = departmentDAO
							.findByDepartmentName(master.getUserDetails().getDepartment().getDepartmentName());
					JobTitles designation = jobTitlesDAO
							.findByJobtitle(master.getUserDetails().getJobTitles().getJobtitle());
					LeaveDetails leaveDetails = leaveDetailsDAO.findByUserId(master.getUserDetails().getUserId()).get();

					finalrep.setEid(master.getUserDetails().getUserId());
					finalrep.setLeavetype(master.getLeaveType());
					finalrep.setFromdate(master.getFromDate());
					finalrep.setTodate(master.getToDate());
					finalrep.setNumberofdays(master.getDays());
					finalrep.setName(
							master.getUserDetails().getFirstName() + " " + master.getUserDetails().getLastName());
					finalrep.setManager(master.getStatus());
					finalrep.setDepartment(depart.getDepartmentName());
					finalrep.setDesignation(designation.getJobtitle());
					finalrep.setRemainingleaves(
							leaveDetails.getRemainingAnnualLeaves() + leaveDetails.getRemainingSickleaves());
					finalrep.setUtilizedLeaves(
							leaveDetails.getUtilisedAnnualLeaves() + leaveDetails.getUtilizedsickLeaves());
					finalrep.setApprovedBy(master.getUserDetails().getTempUserDetails().getFirstName() + " "
							+ master.getUserDetails().getTempUserDetails().getLastName());

					leavereport.add(finalrep);
				}
			}

			// load file and compile it
			if (leavereport.size() > 0) {
				File file = ResourceUtils.getFile("classpath:leaveReport.jrxml");
				JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
				JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(leavereport);
				Map<String, Object> parameters = new HashMap<>();
				parameters.put("createdBy", "Java");
				JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);

				if (reportFormat.equalsIgnoreCase("pdf")) {
					response.setContentType("application/x-pdf");
					response.setHeader("Content-Disposition", "inline; filename=leave.pdf");
					final OutputStream outputstream = response.getOutputStream();
					JasperExportManager.exportReportToPdfStream(jasperPrint, outputstream);
				}
				if (reportFormat.equalsIgnoreCase("xls")) {
					response.setContentType("application/x-xls");
					response.setHeader("Content-Disposition", "inline; filename=leave.xlsx");
					final OutputStream outputstream = response.getOutputStream();
					JRXlsxExporter exportXls = new JRXlsxExporter();

					exportXls.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
					exportXls.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outputstream);
					exportXls.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
					exportXls.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
					exportXls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);

					exportXls.exportReport();
				}
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}

	}

	public List<AssetReport> assetreport() {
		List<AssetReport> assetreport = new ArrayList<>();
		List<Assets> assets = assetsDao.findAll();
		try {
			for (Assets asset : assets) {
				if (asset.isEnable() == true) {
					AssetReport report = new AssetReport();

					List<Assets_Employees> userassets = assets_employeeDAO.findByAssets(asset.getAssetId());

					for (Assets_Employees userassetlist : userassets) {
						UserDetails assigneduserinfo = userDetailsDAO
								.findById(userassetlist.getUserDetails().getUserId()).get();

						report.setAssignedTo(assigneduserinfo.getFirstName() + " " + assigneduserinfo.getLastName());
						report.setCreatedBy(userassetlist.getAssets().getUserDetails().getFirstName() + " "
								+ userassetlist.getAssets().getUserDetails().getLastName());
						report.setDescription(userassetlist.getAssets().getProductDesc());
						report.setProductId(userassetlist.getAssets().getProductId());
						report.setProductmodel(userassetlist.getAssets().getProductModel());
						report.setProductnumber(userassetlist.getAssets().getProductNumber());
						report.setProducttype(userassetlist.getAssets().getProductType());

						assetreport.add(report);
					}
				} else if (asset.isEnable() == false) {
					AssetReport report = new AssetReport();
					report.setAssignedTo("Not Assigned");
					report.setCreatedBy(
							asset.getUserDetails().getFirstName() + " " + asset.getUserDetails().getLastName());
					report.setDescription(asset.getProductDesc());
					report.setProductId(asset.getProductId());
					report.setProductmodel(asset.getProductModel());
					report.setProductnumber(asset.getProductNumber());
					report.setProducttype(asset.getProductType());
					assetreport.add(report);
				}
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}
		return assetreport;
	}

	public void generateAssetreport(String reportFormat, HttpServletResponse response) throws JRException, IOException {
		List<AssetReport> assetreport = new ArrayList<>();
		List<Assets> assets = assetsDao.findAll();

		try {
			for (Assets asset : assets) {
				if (asset.isEnable() == true) {
					AssetReport report = new AssetReport();

					List<Assets_Employees> userassets = assets_employeeDAO.findByAssets(asset.getAssetId());

					for (Assets_Employees userassetlist : userassets) {
						UserDetails assigneduserinfo = userDetailsDAO
								.findById(userassetlist.getUserDetails().getUserId()).get();

						report.setAssignedTo(assigneduserinfo.getFirstName() + " " + assigneduserinfo.getLastName());
						report.setCreatedBy(userassetlist.getAssets().getUserDetails().getFirstName() + " "
								+ userassetlist.getAssets().getUserDetails().getLastName());
						report.setDescription(userassetlist.getAssets().getProductDesc());
						report.setProductId(userassetlist.getAssets().getProductId());
						report.setProductmodel(userassetlist.getAssets().getProductModel());
						report.setProductnumber(userassetlist.getAssets().getProductNumber());
						report.setProducttype(userassetlist.getAssets().getProductType());

						assetreport.add(report);
					}
				} else if (asset.isEnable() == false) {
					AssetReport report = new AssetReport();
					report.setAssignedTo("Not Assigned");
					report.setCreatedBy(
							asset.getUserDetails().getFirstName() + " " + asset.getUserDetails().getLastName());
					report.setDescription(asset.getProductDesc());
					report.setProductId(asset.getProductId());
					report.setProductmodel(asset.getProductModel());
					report.setProductnumber(asset.getProductNumber());
					report.setProducttype(asset.getProductType());
					assetreport.add(report);
				}
			}

			if (assetreport.size() > 0) {
				File file = ResourceUtils.getFile("classpath:AssetReport.jrxml");
				JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
				JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(assetreport);
				Map<String, Object> parameters = new HashMap<>();
				parameters.put("createdBy", "Java");
				JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);

				if (reportFormat.equalsIgnoreCase("pdf")) {
					response.setContentType("application/x-pdf");
					response.setHeader("Content-Disposition", "inline; filename=AssetReport.pdf");
					final OutputStream outputstream = response.getOutputStream();
					JasperExportManager.exportReportToPdfStream(jasperPrint, outputstream);
				}
				if (reportFormat.equalsIgnoreCase("xls")) {
					response.setContentType("application/x-xls");
					response.setHeader("Content-Disposition", "inline; filename=AssetReport.xlsx");
					final OutputStream outputstream = response.getOutputStream();
					JRXlsxExporter exportXls = new JRXlsxExporter();

					exportXls.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
					exportXls.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outputstream);
					exportXls.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
					exportXls.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
					exportXls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);

					exportXls.exportReport();
				}
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}

	}

	public ResponseEntity<Object> userLeavelist(String fromdate, String todate, String status, Long userId) {
		List<LeaveReport> leavereport = new ArrayList<>();
		LocalDate from_date = DateTimeFormat.forPattern("dd-MM-yyy").parseLocalDate(fromdate);
		LocalDate to_date = DateTimeFormat.forPattern("dd-MM-yyy").parseLocalDate(todate);
		Days days = Days.daysBetween(from_date, to_date);

		for (int i = 0; i <= days.getDays(); i++) {
			List<LeaveMaster> leavemaster = leaveMasterDAO.findbyfromdateAndStatusAndUserDetails(
					from_date.plusDays(i).toString("dd-MM-yyyy"), status, userId);

			for (LeaveMaster master : leavemaster) {
				LeaveReport finalrep = new LeaveReport();
				Department depart = departmentDAO
						.findByDepartmentName(master.getUserDetails().getDepartment().getDepartmentName());
				JobTitles designation = jobTitlesDAO
						.findByJobtitle(master.getUserDetails().getJobTitles().getJobtitle());
				LeaveDetails leaveDetails = leaveDetailsDAO.findByUserId(master.getUserDetails().getUserId()).get();

				finalrep.setEid(master.getUserDetails().getUserId());
				finalrep.setLeavetype(master.getLeaveType());
				finalrep.setFromdate(master.getFromDate());
				finalrep.setTodate(master.getToDate());
				finalrep.setNumberofdays(master.getDays());
				finalrep.setName(master.getUserDetails().getFirstName() + " " + master.getUserDetails().getLastName());
				finalrep.setManager(master.getStatus());
				finalrep.setDepartment(depart.getDepartmentName());
				finalrep.setDesignation(designation.getJobtitle());
				finalrep.setRemainingleaves(
						leaveDetails.getRemainingAnnualLeaves() + leaveDetails.getRemainingSickleaves());
				finalrep.setUtilizedLeaves(
						leaveDetails.getUtilisedAnnualLeaves() + leaveDetails.getUtilizedsickLeaves());
				finalrep.setApprovedBy(master.getUserDetails().getTempUserDetails().getFirstName() + " "
						+ master.getUserDetails().getTempUserDetails().getLastName());

				leavereport.add(finalrep);
			}
		}
		if (leavereport.isEmpty()) {
			return new ResponseEntity<Object>(new StatusRes("failed", "007", "Between Dates No Data"), HttpStatus.OK);
		}
		return new ResponseEntity<Object>(leavereport, HttpStatus.OK);
	}

	public void generateuserleavereport(String fromdate, String todate, String status, Long userId, String reportFormat,
			HttpServletResponse response) throws JRException, IOException {
		List<LeaveReport> leavereport = new ArrayList<>();
		try {
			LocalDate from_date = DateTimeFormat.forPattern("dd-MM-yyy").parseLocalDate(fromdate);
			LocalDate to_date = DateTimeFormat.forPattern("dd-MM-yyy").parseLocalDate(todate);
			Days days = Days.daysBetween(from_date, to_date);
			for (int i = 0; i <= days.getDays(); i++) {
				List<LeaveMaster> leavemaster = leaveMasterDAO.findbyfromdateAndStatusAndUserDetails(
						from_date.plusDays(i).toString("dd-MM-yyyy"), status, userId);

				for (LeaveMaster master : leavemaster) {
					LeaveReport finalrep = new LeaveReport();
					Department depart = departmentDAO
							.findByDepartmentName(master.getUserDetails().getDepartment().getDepartmentName());
					JobTitles designation = jobTitlesDAO
							.findByJobtitle(master.getUserDetails().getJobTitles().getJobtitle());
					LeaveDetails leaveDetails = leaveDetailsDAO.findByUserId(master.getUserDetails().getUserId()).get();

					finalrep.setEid(master.getUserDetails().getUserId());
					finalrep.setLeavetype(master.getLeaveType());
					finalrep.setFromdate(master.getFromDate());
					finalrep.setTodate(master.getToDate());
					finalrep.setNumberofdays(master.getDays());
					finalrep.setName(
							master.getUserDetails().getFirstName() + " " + master.getUserDetails().getLastName());
					finalrep.setManager(master.getStatus());
					finalrep.setDepartment(depart.getDepartmentName());
					finalrep.setDesignation(designation.getJobtitle());
					finalrep.setRemainingleaves(
							leaveDetails.getRemainingAnnualLeaves() + leaveDetails.getRemainingSickleaves());
					finalrep.setUtilizedLeaves(
							leaveDetails.getUtilisedAnnualLeaves() + leaveDetails.getUtilizedsickLeaves());
					finalrep.setApprovedBy(master.getUserDetails().getTempUserDetails().getFirstName() + " "
							+ master.getUserDetails().getTempUserDetails().getLastName());

					leavereport.add(finalrep);
				}
			}
			// System.out.println("leavereport.size :: "+leavereport.size());
			if (leavereport.size() > 0) {
				// load file and compile it
				File file = ResourceUtils.getFile("classpath:leaveReport.jrxml");
				JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
				JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(leavereport);
				Map<String, Object> parameters = new HashMap<>();
				parameters.put("createdBy", "Java");
				JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);

				if (reportFormat.equalsIgnoreCase("pdf")) {
					response.setContentType("application/x-pdf");
					response.setHeader("Content-Disposition", "inline; filename=leave.pdf");
					final OutputStream outputstream = response.getOutputStream();
					JasperExportManager.exportReportToPdfStream(jasperPrint, outputstream);
				}
				if (reportFormat.equalsIgnoreCase("xls")) {
					response.setContentType("application/x-xls");
					response.setHeader("Content-Disposition", "inline; filename=leave.xlsx");
					final OutputStream outputstream = response.getOutputStream();
					JRXlsxExporter exportXls = new JRXlsxExporter();

					exportXls.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
					exportXls.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outputstream);
					exportXls.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
					exportXls.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
					exportXls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);

					exportXls.exportReport();
				}
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}
	}

	public ResponseEntity<Object> rmLeavelist(String fromdate, String todate, String status, Long userId) {
		List<LeaveReport> leavereport = new ArrayList<>();

		List<UserDetails> listManager = userDetailsDAO.findByManagerID(userId);
		// System.out.println("listManager::"+listManager.size());
		listManager.forEach(details -> {

			LocalDate from_date = DateTimeFormat.forPattern("dd-MM-yyy").parseLocalDate(fromdate);
			LocalDate to_date = DateTimeFormat.forPattern("dd-MM-yyy").parseLocalDate(todate);
			Days days = Days.daysBetween(from_date, to_date);
			for (int i = 0; i <= days.getDays(); i++) {
				List<LeaveMaster> leavemaster = leaveMasterDAO.findbyfromdateAndStatusAndUserDetails(
						from_date.plusDays(i).toString("dd-MM-yyyy"), status, details.getUserId());

				for (LeaveMaster master : leavemaster) {
					LeaveReport finalrep = new LeaveReport();
					Department depart = departmentDAO
							.findByDepartmentName(master.getUserDetails().getDepartment().getDepartmentName());
					JobTitles designation = jobTitlesDAO
							.findByJobtitle(master.getUserDetails().getJobTitles().getJobtitle());
					LeaveDetails leaveDetails = leaveDetailsDAO.findByUserId(master.getUserDetails().getUserId()).get();

					finalrep.setEid(master.getUserDetails().getUserId());
					finalrep.setLeavetype(master.getLeaveType());
					finalrep.setFromdate(master.getFromDate());
					finalrep.setTodate(master.getToDate());
					finalrep.setNumberofdays(master.getDays());
					finalrep.setName(
							master.getUserDetails().getFirstName() + " " + master.getUserDetails().getLastName());
					finalrep.setManager(master.getStatus());
					finalrep.setDepartment(depart.getDepartmentName());
					finalrep.setDesignation(designation.getJobtitle());
					finalrep.setRemainingleaves(
							leaveDetails.getRemainingAnnualLeaves() + leaveDetails.getRemainingSickleaves());
					finalrep.setUtilizedLeaves(
							leaveDetails.getUtilisedAnnualLeaves() + leaveDetails.getUtilizedsickLeaves());
					finalrep.setApprovedBy(master.getUserDetails().getTempUserDetails().getFirstName() + " "
							+ master.getUserDetails().getTempUserDetails().getLastName());

					leavereport.add(finalrep);
				}
			}
		});
		if (leavereport.isEmpty()) {
			return new ResponseEntity<Object>(new StatusRes("failed", "007", "Between Dates No Data"), HttpStatus.OK);
		}
		return new ResponseEntity<Object>(leavereport, HttpStatus.OK);
	}

	public void generateRmleavereport(String fromdate, String todate, String status, Long userId, String reportFormat,
			HttpServletResponse response) throws JRException, IOException {

		List<LeaveReport> leavereport = new ArrayList<>();

		try {
			List<UserDetails> listManager = userDetailsDAO.findByManagerID(userId);
			// System.out.println("listManager::"+listManager.size());
			listManager.forEach(details -> {

				LocalDate from_date = DateTimeFormat.forPattern("dd-MM-yyy").parseLocalDate(fromdate);
				LocalDate to_date = DateTimeFormat.forPattern("dd-MM-yyy").parseLocalDate(todate);
				Days days = Days.daysBetween(from_date, to_date);
				for (int i = 0; i <= days.getDays(); i++) {
					List<LeaveMaster> leavemaster = leaveMasterDAO.findbyfromdateAndStatusAndUserDetails(
							from_date.plusDays(i).toString("dd-MM-yyyy"), status, details.getUserId());

					for (LeaveMaster master : leavemaster) {
						LeaveReport finalrep = new LeaveReport();
						Department depart = departmentDAO
								.findByDepartmentName(master.getUserDetails().getDepartment().getDepartmentName());
						JobTitles designation = jobTitlesDAO
								.findByJobtitle(master.getUserDetails().getJobTitles().getJobtitle());
						LeaveDetails leaveDetails = leaveDetailsDAO.findByUserId(master.getUserDetails().getUserId())
								.get();

						finalrep.setEid(master.getUserDetails().getUserId());
						finalrep.setLeavetype(master.getLeaveType());
						finalrep.setFromdate(master.getFromDate());
						finalrep.setTodate(master.getToDate());
						finalrep.setNumberofdays(master.getDays());
						finalrep.setName(
								master.getUserDetails().getFirstName() + " " + master.getUserDetails().getLastName());
						finalrep.setManager(master.getStatus());
						finalrep.setDepartment(depart.getDepartmentName());
						finalrep.setDesignation(designation.getJobtitle());
						finalrep.setRemainingleaves(
								leaveDetails.getRemainingAnnualLeaves() + leaveDetails.getRemainingSickleaves());
						finalrep.setUtilizedLeaves(
								leaveDetails.getUtilisedAnnualLeaves() + leaveDetails.getUtilizedsickLeaves());
						finalrep.setApprovedBy(master.getUserDetails().getTempUserDetails().getFirstName() + " "
								+ master.getUserDetails().getTempUserDetails().getLastName());

						leavereport.add(finalrep);
					}
				}
			});
			// load file and compile it
			if (leavereport.size() > 0) {
				File file = ResourceUtils.getFile("classpath:leaveReport.jrxml");
				JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
				JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(leavereport);
				Map<String, Object> parameters = new HashMap<>();
				parameters.put("createdBy", "Java");
				JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);

				if (reportFormat.equalsIgnoreCase("pdf")) {
					response.setContentType("application/x-pdf");
					response.setHeader("Content-Disposition", "inline; filename=leave.pdf");
					final OutputStream outputstream = response.getOutputStream();
					JasperExportManager.exportReportToPdfStream(jasperPrint, outputstream);
				}
				if (reportFormat.equalsIgnoreCase("xls")) {
					response.setContentType("application/x-xls");
					response.setHeader("Content-Disposition", "inline; filename=leave.xlsx");
					final OutputStream outputstream = response.getOutputStream();
					JRXlsxExporter exportXls = new JRXlsxExporter();

					exportXls.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
					exportXls.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outputstream);
					exportXls.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
					exportXls.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
					exportXls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);

					exportXls.exportReport();
				}
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}
	}

}
