package com.pronix.spring.proclock.services;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pronix.spring.proclock.dao.BusinessUnitsDAO;
import com.pronix.spring.proclock.dao.CompanyDAO;
import com.pronix.spring.proclock.dao.DepartmentDAO;
import com.pronix.spring.proclock.dao.ModuleDAO;
import com.pronix.spring.proclock.dao.ProjectDAO;
import com.pronix.spring.proclock.dao.Project_employeeDAO;
import com.pronix.spring.proclock.dao.RefRolesDAO;
import com.pronix.spring.proclock.dao.RolesModuleDAO;
import com.pronix.spring.proclock.dao.UserDetailsDAO;
import com.pronix.spring.proclock.dao.UserRolesDAO;
import com.pronix.spring.proclock.models.BusinessUnits;
import com.pronix.spring.proclock.models.Company;
import com.pronix.spring.proclock.models.Department;
import com.pronix.spring.proclock.models.Modules;
import com.pronix.spring.proclock.models.Project;
import com.pronix.spring.proclock.models.Project_Employees;
import com.pronix.spring.proclock.models.RefRoles;
import com.pronix.spring.proclock.models.RolesModules;
import com.pronix.spring.proclock.models.UserDetails;
import com.pronix.spring.proclock.models.UserRoles;
import com.pronix.spring.proclock.pojos.BusinessUnitsPojo;
import com.pronix.spring.proclock.pojos.DepartmentPojo;
import com.pronix.spring.proclock.pojos.EmpProjectPojo;
import com.pronix.spring.proclock.pojos.ProjectAssignmentEDPojo;
import com.pronix.spring.proclock.pojos.RoleModulePojo;
import com.pronix.spring.proclock.pojos.StatusRes;
import com.pronix.spring.proclock.pojos.UpdateRoleAndModulePojo;
import com.pronix.spring.proclock.pojos.UpdateRoleAndModules;
import com.pronix.spring.proclock.pojos.UserRolePojo;
import com.pronix.spring.proclock.pojos.UserRoleViewPojo;

@Service
public class RolesService {

	@Autowired
	private UserRolesDAO userRolesDAO;
	@Autowired
	private UserDetailsDAO userDetailsDAO;
	@Autowired
	private RefRolesDAO refRolesDAO;

	@Autowired
	private ModuleDAO moduleDAO;

	@Autowired
	private RolesModuleDAO rolesModuleDAO;

	@Autowired
	private BusinessUnitsDAO businessUnitsDAO;

	@Autowired
	private CompanyDAO companyDAO;

	@Autowired
	private DepartmentDAO departmentDAO;

	@Autowired
	private ProjectDAO projectDAO;

	@Autowired
	private Project_employeeDAO projectEmployeeDAO;
	private static final Logger log = LoggerFactory.getLogger(RolesService.class);

	public Object AddingRoles(RoleModulePojo role_module_pojo) {
		List<StatusRes> status = new ArrayList<StatusRes>();
		RefRoles temp_roles_details = refRolesDAO.findByValue(role_module_pojo.getValue());
		try {
			if (role_module_pojo.getValue() != null && role_module_pojo.getDescription() != null
					&& temp_roles_details == null) {
				RefRoles roles = new RefRoles(role_module_pojo.getValue(), role_module_pojo.getDescription());
				refRolesDAO.save(roles);
				for (Modules modules : role_module_pojo.getModules()) {
					Optional<Modules> modules_info = moduleDAO.findById(modules.getModulesId());
					RefRoles roles_details = refRolesDAO.findByValue(roles.getValue());
					RolesModules rolemodule = new RolesModules(modules_info.get(), roles_details, modules.isEnabled());
					rolesModuleDAO.save(rolemodule);
					status.add((new StatusRes("Success", "000", "Role created sucessfully")));
				}
			} else {
				status.add((new StatusRes("Failed", "007", "Role details not found or Role already existed")));
			}
		} catch (Exception e) {
			e.printStackTrace();
			status.add((new StatusRes("Failed", "007", "Role details not found")));
		}
		return status;
	}

	public List<RolesModules> getRoleModule(Long role_id) {
		List<RolesModules> rolemodules = rolesModuleDAO.findByroleId(role_id);
		return rolemodules;
	}

	public Object updateRoleAndModule(UpdateRoleAndModules updateRoleAndModules) {
		List<StatusRes> status = new ArrayList<StatusRes>();
		RefRoles roles = refRolesDAO.findById(updateRoleAndModules.getRole_id())
				.orElseThrow(() -> new RuntimeException("Role Not Found"));
		RefRoles checkRoleValue = refRolesDAO.findByValue(updateRoleAndModules.getRole_value());
		if (updateRoleAndModules.getRole_value().isEmpty()) {
			status.add((new StatusRes("Failed", "007", "Role value Should not be empty")));
		} else {
			if (checkRoleValue == null) {
				refRolesDAO.save(new RefRoles(updateRoleAndModules.getRole_id(), updateRoleAndModules.getRole_value(),
						updateRoleAndModules.getRole_description()));
				status.add((new StatusRes("Sucess", "000", "Role updated Successfully")));
			} else if (checkRoleValue != null) {
				if (checkRoleValue.getRoleId() == roles.getRoleId()) {
					refRolesDAO.save(new RefRoles(updateRoleAndModules.getRole_id(),
							updateRoleAndModules.getRole_value(), updateRoleAndModules.getRole_description()));
					status.add((new StatusRes("Sucess", "000", "Role updated Successfully")));
				} else {
					status.add((new StatusRes("Failed", "007", "Role Name Already Existed")));
				}
			}
		}
		for (UpdateRoleAndModulePojo tempUpdateRoleAndModulePojo : updateRoleAndModules.getUpdateRoleModulePojo()) {
			if (tempUpdateRoleAndModulePojo.getRole_module_id() != null) {
				rolesModuleDAO.updateRoleModule(tempUpdateRoleAndModulePojo.getRole_module_id(),
						tempUpdateRoleAndModulePojo.isEnable());
				status.add((new StatusRes("Success", "000", "module updated successfully")));
			} else {
				status.add((new StatusRes("Failed", "007", "role_module id not found details not found")));
			}
		}
		return status;
	}

	public Object userRoleMapping(UserRolePojo userRolePojo) {
		UserDetails userDetails = null;
		RefRoles refRoles = null;
		UserRoles userRoles = null;
		try {
			userDetails = userDetailsDAO.findById(userRolePojo.getUserId()).get();
			refRoles = refRolesDAO.findById(userRolePojo.getRoleId()).get();
			userRoles = new UserRoles(refRoles, userDetails);

			userRolesDAO.save(userRoles);
		} catch (Exception e) {
			log.error("Exception in method userRoleMapping:" + e.getMessage());
		}
		if (userDetails != null && refRoles != null) {
			return new StatusRes("success", "000", "userId : " + userRolePojo.getUserId() + ", Mapped to Role : "
					+ userRolePojo.getRoleId() + "Successfully Role Mapped to User", null);
		} else
			return new StatusRes("failed", "007", "userId : " + userRolePojo.getUserId() + ", Mapped to Role : "
					+ userRolePojo.getRoleId() + " Failed to Map User Role", null);
	}

	public StatusRes updateUserRole(UserRolePojo userRolePojo) {
		UserRoles userRole = userRolesDAO.findById(userRolePojo.getId()).get();

		if (userRole != null) {
			userRolesDAO.updateRoles(userRolePojo.getId(), userRolePojo.getRoleId());

			return new StatusRes("success", "000", "UserRoles with ID : " + userRolePojo.getId()
					+ ", UserRoles Roles : " + userRolePojo.getRoleId() + " updated successfully", null);
		} else
			return new StatusRes("failed", "007", "UserRoles with ID : " + userRolePojo.getId() + ", UserRoles Roles : "
					+ userRolePojo.getRoleId() + " Not Found", null);
	}

	public List<UserRoles> viewUserRoles() {
		return (List<UserRoles>) userRolesDAO.findAll();
	}

	public UserRoleViewPojo userRolesConfig() {
		List<UserDetails> userDetailsList = (List<UserDetails>) userDetailsDAO.findAll();
		List<RefRoles> rolesList = (List<RefRoles>) refRolesDAO.findAll();
		UserRoleViewPojo userRolesList = new UserRoleViewPojo(userDetailsList, rolesList);
		return userRolesList;
	}

	public List<StatusRes> addRoleslist(List<RefRoles> refRoles) {
		List<StatusRes> statusResList = new ArrayList<>();
		for (RefRoles roles : refRoles) {

			RefRoles addrole = refRolesDAO.findByDescriptionAndValue(roles.getDescription(), roles.getValue());

			if (addrole != null)
				statusResList.add(new StatusRes("failed", "007", "Role Description : " + roles.getDescription()
						+ ", Role Value : " + roles.getValue() + " Already Existed", null));
			else {
				refRolesDAO.save(new RefRoles(roles.getDescription(), roles.getValue()));

				statusResList.add(new StatusRes("success", "000", "Role Description : " + roles.getDescription()
						+ ", Role Value : " + roles.getValue() + " Stored Successfully", null));
			}
		}
		return statusResList;
	}

	public List<RefRoles> viewRolesList() {
		List<RefRoles> finalist = new ArrayList<>();
		List<RefRoles> list = refRolesDAO.findAll();
		list.forEach(getOne -> {
			if (getOne.getRoleId() != 1) {
				finalist.add(getOne);
			}
		});
		return finalist;

	}

	public List<StatusRes> addModules(List<Modules> modules) {
		List<StatusRes> statusResList = new ArrayList<>();

		for (Modules module : modules) {

			if (module.getValue().isEmpty() && module.getDescription().isEmpty() || module.getValue().isEmpty()
					|| module.getDescription().isEmpty()) {
				statusResList.add(new StatusRes("failed", "007", "please provide all details"));
			} else {

				Modules addmodule = moduleDAO.findByDescriptionAndValue(module.getDescription(), module.getValue());

				if (addmodule != null)
					statusResList.add(new StatusRes("failed", "007", "Module Value : " + module.getValue()
							+ ", Module Description : " + module.getDescription() + " Already Existed", null));
				else {
					moduleDAO.save(module);

					List<RefRoles> refRoles = refRolesDAO.findAll();

					for (RefRoles tempRefRoles : refRoles) {

						RolesModules tempRolesModules = new RolesModules(module, tempRefRoles, false);
						rolesModuleDAO.save(tempRolesModules);
					}
					statusResList.add(new StatusRes("success", "000", "Module Value : " + module.getValue()
							+ ", Module Description : " + module.getDescription() + " Stored Successfully", null));
				}
			}
		}
		return statusResList;
	}

	public List<Modules> viewModuleList() {
		return (List<Modules>) moduleDAO.findAll();
	}

	public List<StatusRes> updateModule(Modules modules) {
		List<StatusRes> statusResList = new ArrayList<>();
		try {
			Modules module = moduleDAO.findById(modules.getModulesId())
					.orElseThrow(() -> new RuntimeException("updateModule not found"));

			Modules moduleName = moduleDAO.findByValue(modules.getValue());
			Modules description = moduleDAO.findByDescription(modules.getDescription());

			if (description == null && moduleName == null) {
				moduleDAO.updateModules(modules.getModulesId(), modules.getValue(), modules.getDescription(),
						modules.isEnabled());

				statusResList.add(new StatusRes("Success", "000", "updated successfull"));

			} else if (description == null || moduleName == null) {
				if (moduleName == null) {
					if (description.getModulesId() == modules.getModulesId()) {
						moduleDAO.updateModules(modules.getModulesId(), modules.getValue(), modules.getDescription(),
								modules.isEnabled());
						statusResList.add(new StatusRes("Success", "000", " Modules updated successfull"));
					} else {
						statusResList.add(new StatusRes("Failed", "007", " Modules Code Already Existed"));
					}
				} else if (description == null) {
					if (moduleName.getModulesId() == modules.getModulesId()) {
						moduleDAO.updateModules(modules.getModulesId(), modules.getValue(), modules.getDescription(),
								modules.isEnabled());
						statusResList.add(new StatusRes("Success", "000", " Modules updated successfull"));
					} else {
						statusResList.add(new StatusRes("Failed", "007", " Modules Name Already Existed"));
					}
				}
			} else if (moduleName != null && description != null) {
				if (modules.getModulesId() == modules.getModulesId()
						&& modules.getModulesId() == modules.getModulesId()) {
					moduleDAO.updateModules(modules.getModulesId(), modules.getValue(), modules.getDescription(),
							modules.isEnabled());
					statusResList.add(new StatusRes("Success", "000", " Modules updated successfull"));
				} else {
					statusResList.add(new StatusRes("Failed", "007", " Modules Already Existed"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return statusResList;
	}

	public List<StatusRes> addBusinessUnits(List<BusinessUnitsPojo> businessUnitsPojo) {
		List<StatusRes> statusResList = new ArrayList<>();

		for (BusinessUnitsPojo businessUnits : businessUnitsPojo) {

			if (businessUnits.getBusinessName().isEmpty() && businessUnits.getBusinessCode().isEmpty()
					|| businessUnits.getBusinessName().isEmpty() || businessUnits.getBusinessCode().isEmpty()) {
				statusResList.add(new StatusRes("failed", "007", "please provide all details"));
			} else {
				Optional<Company> company = companyDAO.findById(businessUnits.getCompanyId());
				BusinessUnits units = businessUnitsDAO.findByBusinessCodeAndBusinessName(
						businessUnits.getBusinessCode(), businessUnits.getBusinessName());
				BusinessUnits unitsname = businessUnitsDAO.findByBusinessName(businessUnits.getBusinessName());
				BusinessUnits unitscode = businessUnitsDAO.findByBusinessCode(businessUnits.getBusinessCode());

				if (units != null || unitsname != null || unitscode != null)
					statusResList.add(new StatusRes(
							"failed", "007", "businessUnits title : " + businessUnits.getBusinessName()
									+ ", businessUnits code : " + businessUnits.getBusinessCode() + " Already Existed",
							null));
				else {
					businessUnitsDAO
							.save(new BusinessUnits(businessUnits.getBusinessName(), businessUnits.getBusinessCode(),
									businessUnits.getBusinessStartDate(), businessUnits.getCountry(),
									businessUnits.getState(), businessUnits.getCity(), company.get()));

					statusResList.add(new StatusRes("success", "000", "Bussiness units added sucessfully", null));
				}
			}
		}
		return statusResList;
	}

	public List<StatusRes> updateBusinessUnits(BusinessUnitsPojo businessUnitsPojo) {
		List<StatusRes> statusResList = new ArrayList<>();
		try {
			BusinessUnits units = businessUnitsDAO.findById(businessUnitsPojo.getBusinessUnitsId())
					.orElseThrow(() -> new RuntimeException("department not found"));

			BusinessUnits businessCode = businessUnitsDAO.findByBusinessCode(businessUnitsPojo.getBusinessCode());
			BusinessUnits businessName = businessUnitsDAO.findByBusinessName(businessUnitsPojo.getBusinessName());

			if (businessCode == null && businessName == null) {
				businessUnitsDAO.updateBusinessUnit(businessUnitsPojo.getBusinessUnitsId(),
						businessUnitsPojo.getBusinessCode(), businessUnitsPojo.getBusinessName(),
						businessUnitsPojo.getCompanyId());

				statusResList.add(new StatusRes("Success", "000", "Businessunits updated successfully"));

			} else if (businessCode == null || businessName == null) {
				if (businessName == null) {
					if (businessCode.getBusinessUnitsId() == businessUnitsPojo.getBusinessUnitsId()) {
						businessUnitsDAO.updateBusinessUnit(businessUnitsPojo.getBusinessUnitsId(),
								businessUnitsPojo.getBusinessCode(), businessUnitsPojo.getBusinessName(),
								businessUnitsPojo.getCompanyId());

						statusResList.add(new StatusRes("Success", "000", "Businessunits updated successfully"));
					} else {
						statusResList.add(new StatusRes("Failed", "007", " BusinessUnits Code Already Existed"));
					}
				} else if (businessCode == null) {
					if (businessName.getBusinessUnitsId() == businessUnitsPojo.getBusinessUnitsId()) {
						businessUnitsDAO.updateBusinessUnit(businessUnitsPojo.getBusinessUnitsId(),
								businessUnitsPojo.getBusinessCode(), businessUnitsPojo.getBusinessName(),
								businessUnitsPojo.getCompanyId());
						statusResList.add(new StatusRes("Success", "000", "Businessunits updated successfully"));
					} else {
						statusResList.add(new StatusRes("Failed", "007", " BusinessUnits Name Already Existed"));
					}
				}
			} else if (businessCode != null && businessName != null) {
				if (businessCode.getBusinessUnitsId() == businessUnitsPojo.getBusinessUnitsId()
						&& businessName.getBusinessUnitsId() == businessUnitsPojo.getBusinessUnitsId()) {

					businessUnitsDAO.updateBusinessUnits(businessUnitsPojo.getBusinessUnitsId(),
							businessUnitsPojo.getBusinessCode(), businessUnitsPojo.getBusinessName(),
							businessUnitsPojo.getCompanyId(), businessUnitsPojo.getBusinessStartDate(),
							businessUnitsPojo.getCountry(), businessUnitsPojo.getState(), businessUnitsPojo.getCity());
					statusResList.add(new StatusRes("Success", "000", "Businessunits updated successfully"));
				} else {
					statusResList.add(new StatusRes("Failed", "007", " Details Already Existed"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return statusResList;
	}

	public List<BusinessUnits> viewBusinessUnit() {
		return (List<BusinessUnits>) businessUnitsDAO.findAll();
	}

	public List<StatusRes> addDepartment(List<DepartmentPojo> departmentPojo) {
		List<StatusRes> statusResList = new ArrayList<>();

		for (DepartmentPojo department : departmentPojo) {

			if (department.getDepartmentName().isEmpty() && department.getDepartmentCode().isEmpty()
					|| department.getDepartmentName().isEmpty() || department.getDepartmentCode().isEmpty()) {
				statusResList.add(new StatusRes("failed", "007", "please provide all details"));
			} else {
				BusinessUnits businessUnits = businessUnitsDAO.findById(department.getBusinessUnitsId()).get();
				Department departments = departmentDAO.findByDepartmentNameAndDepartmentCode(
						department.getDepartmentName(), department.getDepartmentCode());

				if (departments != null)
					statusResList.add(new StatusRes(
							"failed", "007", "Department Name : " + businessUnits.getBusinessName()
									+ ", Department code : " + businessUnits.getBusinessCode() + " Already Existed",
							null));
				else {
					departmentDAO.save(new Department(department.getDepartmentName(), department.getDepartmentCode(),
							businessUnits));

					statusResList.add(new StatusRes("success", "000", "Department Added Sucessfully", null));
				}
			}
		}
		return statusResList;
	}

	public List<StatusRes> updateDepartment(DepartmentPojo departmentPojo) {
		List<StatusRes> statusResList = new ArrayList<>();
		try {
			Department department = departmentDAO.findById(departmentPojo.getDepartmentId())
					.orElseThrow(() -> new RuntimeException("department not found"));

			Department departmentCode = departmentDAO.findByDepartmentCode(departmentPojo.getDepartmentCode());
			Department departmentName = departmentDAO.findByDepartmentName(departmentPojo.getDepartmentName());

			if (departmentCode == null && departmentName == null) {
				departmentDAO.updateDepartment(departmentPojo.getDepartmentId(), departmentPojo.getDepartmentCode(),
						departmentPojo.getDepartmentName(), departmentPojo.getBusinessUnitsId());

				statusResList.add(new StatusRes("Success", "000", " Title updated successfull"));

			} else if (departmentCode == null || departmentName == null) {
				if (departmentName == null) {
					if (departmentCode.getDepartmentId() == departmentPojo.getDepartmentId()) {
						departmentDAO.updateDepartment(departmentPojo.getDepartmentId(),
								departmentPojo.getDepartmentCode(), departmentPojo.getDepartmentName(),
								departmentPojo.getBusinessUnitsId());
						statusResList.add(new StatusRes("Success", "000", " Department updated successfull"));
					} else {
						statusResList.add(new StatusRes("Failed", "007", " Department Code Already Existed"));
					}
				} else if (departmentCode == null) {
					if (departmentName.getDepartmentId() == departmentPojo.getDepartmentId()) {
						departmentDAO.updateDepartment(departmentPojo.getDepartmentId(),
								departmentPojo.getDepartmentCode(), departmentPojo.getDepartmentName(),
								departmentPojo.getBusinessUnitsId());
						statusResList.add(new StatusRes("Success", "000", " Department updated successfull"));
					} else {
						statusResList.add(new StatusRes("Failed", "007", " Department Name Already Existed"));
					}
				}
			} else if (departmentCode != null && departmentName != null) {
				if (departmentCode.getDepartmentId() == departmentPojo.getDepartmentId()
						&& departmentName.getDepartmentId() == departmentPojo.getDepartmentId()) {
					departmentDAO.updateDepartment(departmentPojo.getDepartmentId(), departmentPojo.getDepartmentCode(),
							departmentPojo.getDepartmentName(), departmentPojo.getBusinessUnitsId());
					statusResList.add(new StatusRes("Success", "000", " Department updated successfull"));
				} else {
					statusResList.add(new StatusRes("Failed", "007", " Details Already Existed"));
				}
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}
		return statusResList;
	}

	public List<Department> viewDepartments() {
		return (List<Department>) departmentDAO.findAll();
	}

	public List<RefRoles> viewRefRoles() {
		return (List<RefRoles>) refRolesDAO.findAll();
	}

	public List<BusinessUnits> viewBusinessUnits(Long companyId) {
		return businessUnitsDAO.findBusinessUnitsByCompany(companyDAO.findById(companyId).get());
	}

	public List<Department> viewDepartment(Long businessUnitsId) {
		return departmentDAO.findDepartmentByBusinessUnits(businessUnitsDAO.findById(businessUnitsId).get());
	}

	public Object assignEmployeeToProject(EmpProjectPojo empPojo) {
		List<StatusRes> statusResList = new ArrayList<>();
		Project project = projectDAO.findById(empPojo.getProject_id())
				.orElseThrow(() -> new RuntimeException("Project Not Found"));
		if (empPojo.getEmployeeList().isEmpty()) {
			statusResList.add(new StatusRes("Failed", "007", "You should add atleast one employee"));
		}
		for (Long details : empPojo.getEmployeeList()) {

			UserDetails tempdetails = userDetailsDAO.findById(details)
					.orElseThrow(() -> new RuntimeException("Employee Not Found"));
						
			Long projectsEmployeeCount = projectEmployeeDAO.findCount(details);
			if(projectsEmployeeCount==null) {
				projectsEmployeeCount=(long) 0;
			}
		
			List<Project_Employees> duplicateEmp = projectEmployeeDAO.findByEmployee(details, empPojo.getProject_id());
			
			LocalDate projEndDate = DateTimeFormat.forPattern("yyyy-MM-dd")
					.parseLocalDate(project.getProjectEndDate());
			LocalDate getAssingedDate = DateTimeFormat.forPattern("yyyy-MM-dd")
					.parseLocalDate(empPojo.getProjectAssignedDate());
			
			if (getAssingedDate.isAfter(projEndDate)) {
				return new StatusRes("Failed", "003","Project Period Ended");
			}else if(projectsEmployeeCount>=2) {
				return new StatusRes("Failed", "007", "Employee Already assigned to two Project ");
			}
			
			if (duplicateEmp.isEmpty()) {
				Project_Employees proEmployee = new Project_Employees(empPojo.getProjectAssignedDate(), project,
						tempdetails, true);
				projectEmployeeDAO.save(proEmployee);
				return new StatusRes("Success", "000","Employee assigned succesfully");
			} else {
				return new StatusRes("Failed", "007", "Employee Already assigned to this Project ");
			}
		}
		return statusResList;
	}

	public Object employeeProjectED(Long project_employee_id, boolean status) {

		Optional<Project_Employees> proEmp = projectEmployeeDAO.findById(project_employee_id);
		System.out.println("proEmp " + proEmp);
		if (proEmp.isPresent()) {
			proEmp.get().setEnable(status);
			projectEmployeeDAO.save(proEmp.get());
		}
		if (status = true) {
			return new StatusRes("Success", "000", "Project Successfully Enabled");
		} else {
			return new StatusRes("Success", "000", "Project Successfully Disabled");
		}
	}

	public Object employeeEDProject(ProjectAssignmentEDPojo paedpojo) {
		Optional<Project_Employees> proEmp = projectEmployeeDAO.findById(paedpojo.getProject_employee_id());
		
		List<Project_Employees> projectList=projectEmployeeDAO.findByEmployeeAndEnable(proEmp.get().getUserDetails().getUserId(),true);
		if(projectList.size()>1 && paedpojo.isEnable()==true) {			
			return new StatusRes("Failed", "007", "User Already Assigned with Two Projects");
		}
		
		if (proEmp.isPresent()) {
			proEmp.get().setEnable(paedpojo.isEnable());
			projectEmployeeDAO.save(proEmp.get());
			if (proEmp.get().isEnable() == true) {
				return new StatusRes("success", "000", "Project Successfully Enabled", null);
			} else {
				return new StatusRes("success", "000", "Project Successfully Disabled", null);
			}
		}
		return new StatusRes("Failed", "007", "Details not found", null);
	}

	public List<Project_Employees> assignEmployeeToProjectView() {
		return (List<Project_Employees>) projectEmployeeDAO.findAll();
	}

}
