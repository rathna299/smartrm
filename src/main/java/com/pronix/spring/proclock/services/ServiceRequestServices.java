package com.pronix.spring.proclock.services;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import com.pronix.spring.proclock.ServiceRequestDAO.DepartmentApproversDAO;
import com.pronix.spring.proclock.ServiceRequestDAO.DepartmentExcecutorDAO;
import com.pronix.spring.proclock.ServiceRequestDAO.RequestCommentsDAO;
import com.pronix.spring.proclock.ServiceRequestDAO.RequestTrackerDAO;
import com.pronix.spring.proclock.ServiceRequestDAO.RequestTypeDAO;
import com.pronix.spring.proclock.ServiceRequestDAO.ServiceDepartmentDAO;
import com.pronix.spring.proclock.ServiceRequestDAO.ServiceRequestDAO;
import com.pronix.spring.proclock.dao.RolesModuleDAO;
import com.pronix.spring.proclock.dao.UserContactsDAO;
import com.pronix.spring.proclock.dao.UserDetailsDAO;
import com.pronix.spring.proclock.models.RolesModules;
import com.pronix.spring.proclock.models.UserContacts;
import com.pronix.spring.proclock.models.UserDetails;
import com.pronix.spring.proclock.pojos.StatusRes;
import com.pronix.spring.proclock.servicerequestModel.DepartmentExecutors;
import com.pronix.spring.proclock.servicerequestModel.Departmentapprovers;
import com.pronix.spring.proclock.servicerequestModel.RequestComments;
import com.pronix.spring.proclock.servicerequestModel.RequestListPojo;
import com.pronix.spring.proclock.servicerequestModel.RequestTracker;
import com.pronix.spring.proclock.servicerequestModel.RequestType;
import com.pronix.spring.proclock.servicerequestModel.ServiceDepartment;
import com.pronix.spring.proclock.servicerequestModel.ServiceRequest;
import com.pronix.spring.proclock.servicerequestModel.TicketApprovalOnePojo;
import com.pronix.spring.proclock.servicerequestModel.TicketPojo;

import rx.functions.Actions;

@Service
public class ServiceRequestServices {

	private static final Logger loggger = LoggerFactory.getLogger(ServiceRequestServices.class);

	@Autowired
	private ServiceRequestDAO serviceRequestDAO;

	@Autowired
	private ServiceDepartmentDAO serviceDepartmentDAO;

	@Autowired
	private RequestTypeDAO requestTypeDAO;

	@Autowired
	private DepartmentApproversDAO departmentApproversDAO;

	@Autowired
	private DepartmentExcecutorDAO departmentExecutors;

	@Autowired
	private UserDetailsDAO userDetailsDAO;

	@Autowired
	private UserContactsDAO userContactsDAO;

	@Autowired
	private RequestTrackerDAO requestTrackerDAO;

	@Autowired
	private RequestCommentsDAO requestCommentsDAO;

	@Autowired
	private RolesModuleDAO rolesModuleDAO;

	public ResponseEntity<Object> raiseARequest(TicketPojo ticket) {
		Optional<ServiceDepartment> checkDepartment = serviceDepartmentDAO.findById(ticket.getDepartment_id());
		Optional<RequestType> checkRequest = requestTypeDAO.findByDepartmentAndRequestId(ticket.getRequest_id(),
				ticket.getDepartment_id());
		Optional<UserDetails> employee = userDetailsDAO.findById(ticket.getEmp_id());
		if (employee.get().getTempRefRoles().getValue().equalsIgnoreCase("Management")
				|| employee.get().getTempRefRoles().getValue().equalsIgnoreCase("SuperAdmin")) {
			return new ResponseEntity<>(
					new StatusRes("Failed", "007", "Super admin and Management people will not have access to create tickets"),
					HttpStatus.OK);
		}
		UserContacts emp_contacts = userContactsDAO.findByUserDetails(employee.get());
//		Optional<Departmentapprovers> checkApprover = departmentApproversDAO
//				.findbyApproverIdAndDepartmentId(ticket.getApprover_id(), ticket.getDepartment_id());
		if (checkDepartment.isPresent() && checkRequest.isPresent() && employee.isPresent()) {
			String ticketId = "TK00" + new SimpleDateFormat("ddMMyyyySSS").format(Calendar.getInstance().getTime());
			ServiceRequest request = serviceRequestDAO.save(new ServiceRequest(
					employee.get().getFirstName() + " " + employee.get().getLastName(), emp_contacts.getEmailId(),
					emp_contacts.getPhoneNo(), ticket.getEmp_id(), checkDepartment.get(), checkRequest.get(),
					ticket.getDescription(), new Date(), ticket.getPriority(), "Open", ticketId));
			long statusIdentity = 1;
			RequestTracker tracker = requestTrackerDAO.save(new RequestTracker(request,
					employee.get().getTempUserDetails().getUserId(), "Pending", statusIdentity));
			return new ResponseEntity<>(new StatusRes("Success", "000", "Request created successfully"),
					HttpStatus.CREATED);
		}
		return new ResponseEntity<>(new StatusRes("Failed", "007", "Details not found"), HttpStatus.OK);
	}

	public ResponseEntity<Object> addServiceDepartment(@Validated ServiceDepartment department) {
		Optional<ServiceDepartment> checkDepartment = serviceDepartmentDAO.findAll().stream()
				.filter(n -> n.getDepartment_name().equalsIgnoreCase(department.getDepartment_name())).findFirst();
		if (!checkDepartment.isPresent()) {
			serviceDepartmentDAO.save(department);
			loggger.info("Department created Successfully: " + department.getDepartment_name());
			return new ResponseEntity<>(new StatusRes("Success", "000", "Department created Successfully"),
					HttpStatus.CREATED);
		} else {
			loggger.info("Department name already in use: " + department.getDepartment_name());
			return new ResponseEntity<>(new StatusRes("Failed", "007", "Details already existed"),
					HttpStatus.OK);
		}
	}

	public ResponseEntity<Object> updateServiceDepartment(@Validated ServiceDepartment department) {
		Optional<ServiceDepartment> checkDepartment = serviceDepartmentDAO
				.findById(department.getService_department_id());
		if (checkDepartment.isPresent()) {
			Optional<ServiceDepartment> checkDepartmentName = serviceDepartmentDAO
					.findByDepartmentName(department.getDepartment_name());
			if (checkDepartmentName.isPresent() && checkDepartmentName.get()
					.getService_department_id() == checkDepartment.get().getService_department_id()) {
				serviceDepartmentDAO.save(department);
				return new ResponseEntity<>(new StatusRes("Success", "000", "Department updated successfully"),
						HttpStatus.OK);
			} else if (!checkDepartmentName.isPresent()) {
				serviceDepartmentDAO.save(department);
				return new ResponseEntity<>(new StatusRes("Success", "000", "Department updated successfully"),
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new StatusRes("Failed", "007", "Department name already in use"),
						HttpStatus.BAD_GATEWAY);
			}
		} else {
			return new ResponseEntity<>(new StatusRes("Failed", "007", "Details Not found"), HttpStatus.NOT_FOUND);
		}
	}

	public ResponseEntity<Object> addRequestType(@Validated RequestType requestType) {

		Optional<ServiceDepartment> checkDepartment = serviceDepartmentDAO
				.findById(requestType.getServiceDepartment().getService_department_id());
		if (checkDepartment.isPresent()) {
			Optional<RequestType> checkRequestType = requestTypeDAO.findAll().stream()
					.filter(n -> n.getRequest_name().equalsIgnoreCase(requestType.getRequest_name())).findFirst();
			if (!checkRequestType.isPresent()) {
				requestTypeDAO.save(requestType);
				return new ResponseEntity<>(new StatusRes("Success", "000", "Request Created Successfully"),
						HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>(new StatusRes("Failed", "007", "Duplicate content found"),
						HttpStatus.OK);
			}
		}
		return new ResponseEntity<>(new StatusRes("Failed", "007", "Department is not available"),
				HttpStatus.OK);
	}

	public ResponseEntity<Object> updateRequestType(@Validated RequestType requestType) {
		Optional<RequestType> checkRequestType = requestTypeDAO.findById(requestType.getRequesttype_id());
		Optional<ServiceDepartment> checkDepartment = serviceDepartmentDAO
				.findById(requestType.getServiceDepartment().getService_department_id());
		if (checkRequestType.isPresent() && checkDepartment.isPresent()) {
			Optional<RequestType> checkRequestName = requestTypeDAO.findbyRequstName(requestType.getRequest_name());
			if (checkRequestName.isPresent()
					&& checkRequestName.get().getRequesttype_id() == checkRequestType.get().getRequesttype_id()) {
				requestTypeDAO.save(requestType);
				return new ResponseEntity<>(new StatusRes("Success", "000", "Request updated Successfully"),
						HttpStatus.OK);
			} else if (!checkRequestName.isPresent()) {
				requestTypeDAO.save(requestType);
				return new ResponseEntity<>(new StatusRes("Success", "000", "Request updated Successfully"),
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new StatusRes("Failed", "007", "Request name already in use"),
						HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<>(new StatusRes("Failed", "007", "Request or department is not available"),
					HttpStatus.NOT_FOUND);
		}
	}

	public ResponseEntity<Object> mapApprovers(Long approver_id, Long department_id) {
		Optional<Departmentapprovers> checkMap = departmentApproversDAO.findbyApproverIdAndDepartmentId(approver_id,
				department_id);
		if (!checkMap.isPresent()) {
			ServiceDepartment department = serviceDepartmentDAO.findById(department_id)
					.orElseThrow(() -> new RuntimeException("Department not found"));
			UserDetails approver = userDetailsDAO.findById(approver_id)
					.orElseThrow(() -> new RuntimeException("Approver not found"));
			departmentApproversDAO.save(new Departmentapprovers(department, approver));
			return new ResponseEntity<>(new StatusRes("Success", "000", "Approver mapped Successfully"), HttpStatus.OK);
		}
		return new ResponseEntity<>(new StatusRes("Failed", "007", "User Already Assigned"), HttpStatus.OK);
	}

	public ResponseEntity<Object> mapExecutors(Long executor_id, Long department_id) {
		Optional<DepartmentExecutors> checkExecutor = departmentExecutors.findByExecutorAndDepartment(executor_id,
				department_id);
		if (!checkExecutor.isPresent()) {
			UserDetails executor = userDetailsDAO.findById(executor_id)
					.orElseThrow(() -> new RuntimeException("Executor not found"));
			ServiceDepartment department = serviceDepartmentDAO.findById(department_id)
					.orElseThrow(() -> new RuntimeException("Department not found"));
			departmentExecutors.save(new DepartmentExecutors(department, executor));
			return new ResponseEntity<>(new StatusRes("Success", "000", "Executor mapped Successfully"), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new StatusRes("Failed", "007", "User Already Assigned"),
					HttpStatus.OK);
		}
	}
	// ***Status identity definition
	// Status Identity 1 = request pending at approver one
	// status Identity 2 = request pending at approver two
	// Status Identity 3 = request pending at executor
	// Status Identity 4 = request approved by executor
	// Status Identity 5 = request declined by approver one
	// Status Identity 6 = request declined by approved two

	public ResponseEntity<Object> approveTicket(TicketApprovalOnePojo approvalOne) {
		// ActionUserID is id of executor or approver this just based on sendType. if
		// sendType is 1 then it is executor
		Optional<RequestTracker> checkRequest = requestTrackerDAO.findById(approvalOne.getTracker_id());
		if (checkRequest.isPresent()) {
			// status at first approval
			if (checkRequest.get().getStatus_identity() == 1) {
				// send type one means, we sending ticket to executor
				if (approvalOne.getSendType() == 1) {
					Optional<DepartmentExecutors> checkExecutor = departmentExecutors.findByExecutorAndDepartment(
							approvalOne.getActionUserid(),
							checkRequest.get().getServiceRequest().getDepartment().getService_department_id());
					if (checkExecutor.isPresent()) {
						Optional<UserDetails> approverOneInfo = userDetailsDAO
								.findById(checkRequest.get().getApprover_one_id());
						long statusIndentity = 3;
						requestTrackerDAO.updateExecutor("Approved", new Date().toString(),
								checkExecutor.get().getExecutors().getUserId(), "Pending", statusIndentity,
								checkRequest.get().getRequest_tracker_id());
						requestCommentsDAO.save(new RequestComments(checkRequest.get().getServiceRequest(),
								approvalOne.getComment(), checkRequest.get().getApprover_one_id(),
								approverOneInfo.get().getFirstName(), LocalDateTime.now()));
						return new ResponseEntity<>(
								new StatusRes("Success", "000", "Ticket is approved and submitted to Executor"),
								HttpStatus.OK);
					} else {
						return new ResponseEntity<>(
								new StatusRes("Failed", "007", "Executor details not found in the table"),
								HttpStatus.OK);
					}
				} else if (approvalOne.getSendType() == 2) {
					Optional<Departmentapprovers> checkApprover = departmentApproversDAO
							.findbyApproverIdAndDepartmentId(approvalOne.getActionUserid(),
									checkRequest.get().getServiceRequest().getDepartment().getService_department_id());
					if (checkApprover.isPresent()) {
						Optional<UserDetails> approverOneInfo = userDetailsDAO
								.findById(checkRequest.get().getApprover_one_id());
						long statusIndentity = 2;
						requestTrackerDAO.updateApprover("Approved", new Date().toString(),
								checkApprover.get().getApprovers().getUserId(), "Pending", statusIndentity,
								checkRequest.get().getRequest_tracker_id());
						requestCommentsDAO.save(new RequestComments(checkRequest.get().getServiceRequest(),
								approvalOne.getComment(), checkRequest.get().getApprover_one_id(),
								approverOneInfo.get().getFirstName(), LocalDateTime.now()));
						return new ResponseEntity<>(
								new StatusRes("Success", "000", "Ticket is approved and submitted to approver"),
								HttpStatus.OK);
					} else {
						return new ResponseEntity<>(
								new StatusRes("Failed", "007", "Approver details not found in the table"),
								HttpStatus.OK);
					}
				}
				// status at second approval
			} else if (checkRequest.get().getStatus_identity() == 2) {
				if (approvalOne.getSendType() == 1) {
					Optional<DepartmentExecutors> checkExecutor = departmentExecutors.findByExecutorAndDepartment(
							approvalOne.getActionUserid(),
							checkRequest.get().getServiceRequest().getDepartment().getService_department_id());
					if (checkExecutor.isPresent()) {
						Optional<UserDetails> approverTwoInfo = userDetailsDAO
								.findById(checkRequest.get().getApprover_two_id());
						long statusIndentity = 3;
						requestTrackerDAO.updateExecutorBySecondApprover("Approved", new Date().toString(),
								checkExecutor.get().getExecutors().getUserId(), "Pending", statusIndentity,
								checkRequest.get().getRequest_tracker_id());
						requestCommentsDAO.save(new RequestComments(checkRequest.get().getServiceRequest(),
								approvalOne.getComment(), approverTwoInfo.get().getUserId(),
								approverTwoInfo.get().getFirstName(), LocalDateTime.now()));
						return new ResponseEntity<>(
								new StatusRes("Success", "000", "Ticket is approved and submitted to executor"),
								HttpStatus.OK);
					}
				} else {
					return new ResponseEntity<>(new StatusRes("Failed", "0007", "Please send Executor details"),
							HttpStatus.OK);
				}
			}
		}
		return new ResponseEntity<>(new StatusRes("Failed", "007", "Request failed details not found"),
				HttpStatus.OK);
	}

	public ResponseEntity<Object> declineRequest(Long ticketId, String comment) {
		Optional<RequestTracker> checkRequest = requestTrackerDAO.findById(ticketId);
		if (checkRequest.isPresent()) {
			if (checkRequest.get().getStatus_identity() == 1) {
				Optional<UserDetails> approverOneInfo = userDetailsDAO
						.findById(checkRequest.get().getApprover_one_id());
				long statusIndentity = 5;
				requestTrackerDAO.declineByApproverOne("Declined", new Date().toString(), statusIndentity, ticketId);
				requestCommentsDAO.save(new RequestComments(checkRequest.get().getServiceRequest(), comment,
						checkRequest.get().getApprover_one_id(), approverOneInfo.get().getFirstName(),
						LocalDateTime.now()));
				serviceRequestDAO.updateStatus("Closed", new Date(),
						checkRequest.get().getServiceRequest().getService_request_id());
				return new ResponseEntity<>(new StatusRes("Success", "000", "Successfully declined by approver one"),
						HttpStatus.OK);
			} else if (checkRequest.get().getStatus_identity() == 2) {
				Optional<UserDetails> approverTwoInfo = userDetailsDAO
						.findById(checkRequest.get().getApprover_one_id());
				long statusIndentity = 6;
				requestTrackerDAO.declineByApproverTwo("Declined", new Date().toString(), statusIndentity, ticketId);
				serviceRequestDAO.updateStatus("Closed", new Date(),
						checkRequest.get().getServiceRequest().getService_request_id());
				requestCommentsDAO.save(new RequestComments(checkRequest.get().getServiceRequest(), comment,
						checkRequest.get().getApprover_two_id(), approverTwoInfo.get().getFirstName(),
						LocalDateTime.now()));
				return new ResponseEntity<>(new StatusRes("Success", "000", "Successfully declined by approver two"),
						HttpStatus.OK);
			}
		}
		return new ResponseEntity<>(new StatusRes("Failed", "007", "Ticket details not found"), HttpStatus.OK);
	}

	public ResponseEntity<Object> CloseTicketByExecutor(Long ticketTrackingId, String comment) {
		Optional<RequestTracker> checkRequest = requestTrackerDAO.findById(ticketTrackingId);
		if (checkRequest.isPresent()) {
			if (checkRequest.get().getStatus_identity() == 3) {
				Optional<UserDetails> executorInfo = userDetailsDAO.findById(checkRequest.get().getExecutor_id());
				long statusIndentity = 4;
				requestTrackerDAO.closedByExecutor("Closed", new Date().toString(), statusIndentity,
						checkRequest.get().getRequest_tracker_id());
				serviceRequestDAO.updateStatus("Closed", new Date(),
						checkRequest.get().getServiceRequest().getService_request_id());
				requestCommentsDAO.save(new RequestComments(checkRequest.get().getServiceRequest(), comment,
						executorInfo.get().getUserId(), executorInfo.get().getFirstName(), LocalDateTime.now()));
				return new ResponseEntity<>(new StatusRes("Success", "000", "Ticket successfully closed by executor"),
						HttpStatus.OK);
			}
			return new ResponseEntity<>(
					new StatusRes("Failed", "007", "Ticket already processed or Ticket already processed"),
					HttpStatus.OK);
		}
		return new ResponseEntity<>(new StatusRes("Failed", "007", "Ticket details not found"), HttpStatus.OK);
	}

	public ResponseEntity<Object> ticketslistByApprover(Long approverID, String status) {
		List<RequestListPojo> finallist = new ArrayList<>();
		if (status.equalsIgnoreCase("All")) {
			List<RequestTracker> reqList = requestTrackerDAO.findbyApproverOneId(approverID);
			reqList.forEach(reqtracket -> {
				Optional<ServiceRequest> serviceReq = serviceRequestDAO
						.findById(reqtracket.getServiceRequest().getService_request_id());
				List<RequestComments> comments = requestCommentsDAO
						.findByIdServiceRequestId(serviceReq.get().getService_request_id());
				RequestListPojo requestlist = new RequestListPojo();
				requestlist.setRequest(serviceReq.get());
				requestlist.setRequestTracker(reqtracket);
				requestlist.setRequestComments(comments);
				finallist.add(requestlist);
			});
			List<RequestTracker> reqList1 = requestTrackerDAO.findbyApproverTwoId(approverID);
			reqList1.forEach(reqtracket -> {
				Optional<ServiceRequest> serviceReq = serviceRequestDAO
						.findById(reqtracket.getServiceRequest().getService_request_id());
				List<RequestComments> comments = requestCommentsDAO
						.findByIdServiceRequestId(serviceReq.get().getService_request_id());
				RequestListPojo requestlist = new RequestListPojo();
				requestlist.setRequest(serviceReq.get());
				requestlist.setRequestTracker(reqtracket);
				requestlist.setRequestComments(comments);
				finallist.add(requestlist);
			});
			return new ResponseEntity<>(finallist, HttpStatus.OK);
		} else {
			List<RequestTracker> reqList = requestTrackerDAO.findByApproverOneAndStatus(approverID, status);
			reqList.forEach(reqtracket -> {
				Optional<ServiceRequest> serviceReq = serviceRequestDAO
						.findById(reqtracket.getServiceRequest().getService_request_id());
				List<RequestComments> comments = requestCommentsDAO
						.findByIdServiceRequestId(serviceReq.get().getService_request_id());
				RequestListPojo requestlist = new RequestListPojo();
				requestlist.setRequest(serviceReq.get());
				requestlist.setRequestTracker(reqtracket);
				requestlist.setRequestComments(comments);
				finallist.add(requestlist);
			});
			List<RequestTracker> reqList1 = requestTrackerDAO.findbyApproverTwoAndStatus(approverID, status);
			reqList1.forEach(reqtracket -> {
				Optional<ServiceRequest> serviceReq = serviceRequestDAO
						.findById(reqtracket.getServiceRequest().getService_request_id());
				List<RequestComments> comments = requestCommentsDAO
						.findByIdServiceRequestId(serviceReq.get().getService_request_id());
				RequestListPojo requestlist = new RequestListPojo();
				requestlist.setRequest(serviceReq.get());
				requestlist.setRequestTracker(reqtracket);
				requestlist.setRequestComments(comments);
				finallist.add(requestlist);
			});
			return new ResponseEntity<>(finallist, HttpStatus.OK);
		}
	}

	public ResponseEntity<Object> getServiceDepartments() {
		List<ServiceDepartment> departmentList = serviceDepartmentDAO.findAll();
		return new ResponseEntity<>(departmentList, HttpStatus.OK);
	}

	public ResponseEntity<Object> getRequestTypes(Long departmentId) {
		List<RequestType> requestList = requestTypeDAO.findByDepartment(departmentId);
		return new ResponseEntity<>(requestList, HttpStatus.OK);
	}

	public ResponseEntity<Object> getRequestTypeList() {
		List<RequestType> list = requestTypeDAO.findAll();
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

	public ResponseEntity<Object> getApprovers(Long departmentId) {
		List<Departmentapprovers> approverList = departmentApproversDAO.findbyDepartmentId(departmentId);
		List<UserDetails> getdetails = new ArrayList<>();
		approverList.forEach(approver -> {
			getdetails.add(approver.getApprovers());
		});
		return new ResponseEntity<>(getdetails, HttpStatus.OK);
	}

	public ResponseEntity<Object> getExecutors(Long departmentId) {
		List<UserDetails> executorList = new ArrayList<>();
		List<DepartmentExecutors> departmentList = departmentExecutors.findByDepartment(departmentId);
		departmentList.forEach(executor -> {
			executorList.add(executor.getExecutors());
		});
		return new ResponseEntity<>(executorList, HttpStatus.OK);
	}

	public ResponseEntity<Object> ticketListByExecutors(Long executorId, String status) {
		List<RequestListPojo> finallist = new ArrayList<>();
		if (status.equalsIgnoreCase("All")) {
			List<RequestTracker> reqList = requestTrackerDAO.findbyExecutorId(executorId);
			reqList.forEach(reqtracket -> {
				Optional<ServiceRequest> serviceReq = serviceRequestDAO
						.findById(reqtracket.getServiceRequest().getService_request_id());
				List<RequestComments> comments = requestCommentsDAO
						.findByIdServiceRequestId(serviceReq.get().getService_request_id());
				RequestListPojo requestlist = new RequestListPojo();
				requestlist.setRequest(serviceReq.get());
				requestlist.setRequestTracker(reqtracket);
				requestlist.setRequestComments(comments);
				finallist.add(requestlist);
			});
			return new ResponseEntity<>(finallist, HttpStatus.OK);
		} else {
			List<RequestTracker> reqList = requestTrackerDAO.findByExecutorAndStatus(executorId, status);
			reqList.forEach(reqtracket -> {
				Optional<ServiceRequest> serviceReq = serviceRequestDAO
						.findById(reqtracket.getServiceRequest().getService_request_id());
				List<RequestComments> comments = requestCommentsDAO
						.findByIdServiceRequestId(serviceReq.get().getService_request_id());
				RequestListPojo requestlist = new RequestListPojo();
				requestlist.setRequest(serviceReq.get());
				requestlist.setRequestTracker(reqtracket);
				requestlist.setRequestComments(comments);
				finallist.add(requestlist);
			});
			return new ResponseEntity<>(finallist, HttpStatus.OK);
		}
	}

	public ResponseEntity<Object> ticketList(Long empId) {
		List<RequestListPojo> finallist = new ArrayList<>();
		List<ServiceRequest> requestList = serviceRequestDAO.findByEmp_id(empId);
		requestList.forEach(request -> {
			RequestTracker trackingDetails = requestTrackerDAO.findByServiceRequest(request);
			List<RequestComments> comments = requestCommentsDAO
					.findByIdServiceRequestId(request.getService_request_id());
			RequestListPojo requestlist = new RequestListPojo();
			requestlist.setRequest(request);
			requestlist.setRequestComments(comments);
			requestlist.setRequestTracker(trackingDetails);
			finallist.add(requestlist);
		});
		return new ResponseEntity<>(finallist, HttpStatus.OK);
	}

	public ResponseEntity<Object> ticketCount(Long empId) {
		List<ServiceRequest> requestList = serviceRequestDAO.findByEmp_id(empId);
		Long pendingTickets = requestList.stream().filter(request -> request.getStatus().equalsIgnoreCase("Open"))
				.count();
		long approved_tickets = 0;
		for (ServiceRequest request : requestList) {
			Optional<RequestTracker> trackingDetails = requestTrackerDAO
					.findbyServiceRequestAndRequestStatus(request.getService_request_id(), "Closed");
			if (trackingDetails.isPresent()) {
				approved_tickets++;
			}
		}
		Long totalraised = requestList.stream().count();
		Map<String, Long> response = new HashMap<>();
		response.put("total_requests", totalraised);
		response.put("pending_request", pendingTickets);
		response.put("Approved_request", approved_tickets);
		response.put("declined_request", totalraised - pendingTickets - approved_tickets);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	public ResponseEntity<Object> tickectCountByApprover(Long ApproverId) {
		Long reqList1_pend = requestTrackerDAO.findByApproverOneAndStatusCount(ApproverId, "Pending");
		Long reqList1_Approved = requestTrackerDAO.findByApproverOneAndStatusCount(ApproverId, "Approved");
		Long reqList1_Declined = requestTrackerDAO.findByApproverOneAndStatusCount(ApproverId, "Declined");
		Long reqList2_pending = requestTrackerDAO.findbyApproverTwoAndStatusCount(ApproverId, "Pending");
		Long reqList2_Approved = requestTrackerDAO.findbyApproverTwoAndStatusCount(ApproverId, "Approved");
		Long reqList2_Declined = requestTrackerDAO.findbyApproverTwoAndStatusCount(ApproverId, "Declined");
		Map<String, Long> response = new HashMap<>();
		response.put("Total", reqList1_pend + reqList1_Approved + reqList1_Declined + reqList2_pending
				+ reqList2_Approved + reqList2_Declined);
		response.put("Pending", reqList1_pend + reqList2_pending);
		response.put("Approved", reqList1_Approved + reqList2_Approved);
		response.put("Declined", reqList1_Declined + reqList2_Declined);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	public ResponseEntity<Object> ticketCountByExecutor(Long executorId) {
		Long pedning = requestTrackerDAO.findByExecutorAndStatusCount(executorId, "Pending");
		Long closed = requestTrackerDAO.findByExecutorAndStatusCount(executorId, "Closed");
		Map<String, Long> response = new HashMap<>();
		response.put("Total", pedning + closed);
		response.put("Pending", pedning);
		response.put("Closed", closed);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	public ResponseEntity<Object> executorList() {
		List<UserDetails> executors = new ArrayList<>();
		List<RolesModules> roleList = rolesModuleDAO.findByModuleActiveStatus((long) 11, true);
		roleList.forEach(roles -> {
			List<UserDetails> executorList = userDetailsDAO.findByRoleAndActiveStatus(roles.getRefRoles().getRoleId(),
					true);
			executors.addAll(executorList);
		});
		return new ResponseEntity<>(executors, HttpStatus.OK);
	}

	public ResponseEntity<Object> ApproverList() {
		List<UserDetails> approverList = new ArrayList<>();
		List<RolesModules> roleList = rolesModuleDAO.findByModuleActiveStatus((long) 10, true);
		roleList.forEach(roles -> {
			List<UserDetails> approvers = userDetailsDAO.findByRoleAndActiveStatus(roles.getRefRoles().getRoleId(),
					true);
			approverList.addAll(approvers);
		});
		return new ResponseEntity<>(approverList, HttpStatus.OK);
	}
}
