package com.pronix.spring.proclock.services;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.pronix.spring.proclock.dao.ModuleDAO;
import com.pronix.spring.proclock.dao.ProjectDAO;
import com.pronix.spring.proclock.dao.UserDetailsDAO;
import com.pronix.spring.proclock.models.Modules;
import com.pronix.spring.proclock.models.Project;
import com.pronix.spring.proclock.models.UserDetails;
import com.pronix.spring.proclock.pojos.StatusRes;
import com.pronix.spring.proclock.pojos.TaskDetailTrackerPojo;
import com.pronix.spring.proclock.pojos.TaskDetailsPojo;
import com.pronix.spring.proclock.taskmanagement.DAO.TaskDetailsDAO;
import com.pronix.spring.proclock.taskmanagement.DAO.TaskDetailsFileDAO;
import com.pronix.spring.proclock.taskmanagement.DAO.TaskDetailsTrackerDAO;
import com.pronix.spring.proclock.taskmanagement.DAO.TaskStatusDAO;
import com.pronix.spring.proclock.taskmanagement.DAO.TaskTypeDAO;
import com.pronix.spring.proclock.taskmanagement.models.TaskDetails;
import com.pronix.spring.proclock.taskmanagement.models.TaskDetailsFile;
import com.pronix.spring.proclock.taskmanagement.models.TaskDetailsTracker;
import com.pronix.spring.proclock.taskmanagement.models.TaskStatus;
import com.pronix.spring.proclock.taskmanagement.models.TaskTrackerPojo;
import com.pronix.spring.proclock.taskmanagement.models.TaskType;
import com.pronix.spring.proclock.taskmanagement.models.TrackerFileDetails;

@Service
public class TaskManagementService {

	@Autowired
	private TaskTypeDAO taskTypeDAO;

	@Autowired
	private TaskStatusDAO taskStatusDAO;

	@Autowired
	private ProjectDAO projectDAO;

	@Autowired
	private ModuleDAO moduleDAO;

	@Autowired
	private TaskDetailsDAO taskDetailsDAO;

	@Autowired
	private TaskDetailsTrackerDAO taskDetailsTrackerDAO;

	@Autowired
	private TaskDetailsFileDAO taskDetailsFileDAO;

	@Autowired
	private UserDetailsDAO userDetailsDAO;

	public ResponseEntity<Object> addTaskType(TaskType taskType) {
		Optional<TaskType> checkTaskTypeDetails = taskTypeDAO.findByTasktypeName(taskType.getTasktypeName());

		if (!checkTaskTypeDetails.isPresent()) {
			TaskType taskTypeDetails = taskTypeDAO.save(taskType);

			taskTypeDetails.setTasktypeName(taskType.getTasktypeName());
			taskTypeDetails.setDescription(taskType.getDescription());

			taskTypeDAO.save(taskTypeDetails);

			return new ResponseEntity<>(new StatusRes("Success", "000", "TaskType added Successfully"), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new StatusRes("Failed", "007", "TaskType already existed"), HttpStatus.OK);
		}
	}

	public ResponseEntity<Object> updateTaskType(@Valid TaskType taskType) {
		Optional<TaskType> checkTaskType = taskTypeDAO.findById(taskType.getTasktype_id());
		Optional<TaskType> checkTasktypeName = taskTypeDAO.findByTasktypeName(taskType.getTasktypeName());

		if (checkTaskType.isPresent()) {
			if (checkTasktypeName.isPresent() && checkTaskType.get().equals(checkTasktypeName.get())
					|| !checkTasktypeName.isPresent()) {

				checkTaskType.get().setTasktypeName(taskType.getTasktypeName());
				checkTaskType.get().setDescription(taskType.getDescription());
				taskTypeDAO.save(checkTaskType.get());

				return new ResponseEntity<>(new StatusRes("Success", "000", "Record updated successfully"),
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new StatusRes("Failed", "007", "Task Type Name already Existed"),
						HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<>(new StatusRes("Failed", "007", "Task Type details not found"), HttpStatus.OK);
		}
	}

	public ResponseEntity<Object> getTasktypeList() {
		return new ResponseEntity<>(taskTypeDAO.findAll(), HttpStatus.OK);
	}

	public ResponseEntity<Object> addTaskStatus(TaskStatus taskstatus) {
		Optional<TaskStatus> checkTaskStatusDetails = taskStatusDAO
				.findByTaskstatusType(taskstatus.getTaskstatusType());

		if (!checkTaskStatusDetails.isPresent()) {
			TaskStatus taskStatusDetails = taskStatusDAO.save(taskstatus);

			taskStatusDetails.setTaskstatusType(taskstatus.getTaskstatusType());
			taskStatusDetails.setDescription(taskstatus.getDescription());

			taskStatusDAO.save(taskStatusDetails);

			return new ResponseEntity<>(new StatusRes("Success", "000", "Task Status added Successfully"),
					HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new StatusRes("Failed", "007", "Task Status already existed"), HttpStatus.OK);
		}
	}

	public ResponseEntity<Object> updateTaskStatus(@Valid TaskStatus taskstatus) {
		Optional<TaskStatus> checkTaskStatus = taskStatusDAO.findById(taskstatus.getTaskstatus_id());
		Optional<TaskStatus> taskstatusType = taskStatusDAO.findByTaskstatusType(taskstatus.getTaskstatusType());

		if (checkTaskStatus.isPresent()) {
			if (taskstatusType.isPresent() && checkTaskStatus.get().equals(taskstatusType.get())
					|| !taskstatusType.isPresent()) {

				checkTaskStatus.get().setTaskstatusType(taskstatus.getTaskstatusType());
				checkTaskStatus.get().setDescription(taskstatus.getDescription());
				taskStatusDAO.save(checkTaskStatus.get());

				return new ResponseEntity<>(new StatusRes("Success", "000", "Record updated successfully"),
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new StatusRes("Failed", "007", "Task Status Name already Existed"),
						HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<>(new StatusRes("Failed", "007", "Task Status details not found"), HttpStatus.OK);
		}
	}

	public ResponseEntity<Object> getTaskStatusList() {
		return new ResponseEntity<>(taskStatusDAO.findAll(), HttpStatus.OK);
	}

	public ResponseEntity<Object> getTaskStatusLists() {

		List<TaskStatus> listData = new ArrayList<>();
		List<TaskStatus> list = taskStatusDAO.findAll();
		for (TaskStatus status : list) {
			if (status.getTaskstatus_id() != 1) {
				listData.add(status);
			}
		}
		return new ResponseEntity<>(listData, HttpStatus.OK);
	}

	public ResponseEntity<Object> addTaskDetails(TaskDetailsPojo taskDetails, MultipartHttpServletRequest imagefile)
			throws IOException {

		TaskDetails details = new TaskDetails();

		Optional<TaskDetails> checkTaskNameDetails = taskDetailsDAO.findByTaskName(taskDetails.getTaskName());
		Optional<TaskType> checkTaskType = taskTypeDAO.findById(taskDetails.getTasktypeId());
		Optional<Project> checkProjectDetails = projectDAO.findById(taskDetails.getProjectId());
		Optional<Modules> checkModules = moduleDAO.findById(taskDetails.getModulesId());
		Optional<UserDetails> createdUserDetails = userDetailsDAO.findById(taskDetails.getCreatedBy());
		Optional<UserDetails> assignedUserDetails = userDetailsDAO.findById(taskDetails.getAssignedTo());

		BeanUtils.copyProperties(taskDetails, details, "taskdetailsId");

		details.setTaskType(checkTaskType.get());
		details.setProject(checkProjectDetails.get());
		details.setModules(checkModules.get());
		details.setUserDetails(createdUserDetails.get());
		details.setAssignedTo(assignedUserDetails.get());
		details.setInsertDate(LocalDateTime.now());
		details.setPeoplesInvolved(createdUserDetails.get().getUserId() + "");

		TaskStatus taskStatusDetail = new TaskStatus();
		taskStatusDetail.setTaskstatus_id(1L);
		details.setTaskStatus(taskStatusDetail);

		if (taskDetails.getResolvedBy() != null) {
			Optional<UserDetails> resolvedUserDetails = userDetailsDAO.findById(taskDetails.getResolvedBy());
			details.setUserResolveDetails(resolvedUserDetails.get());
		}

		if (checkModules.isPresent() && checkTaskType.isPresent() && checkProjectDetails.isPresent()) {
			TaskDetails taskData = taskDetailsDAO.save(details);
			taskData.setInsertDate(LocalDateTime.now());

			taskDetailsDAO.save(taskData);

			TaskDetailsTracker trackerDetails = new TaskDetailsTracker();

			TaskStatus taskStatusDetails = new TaskStatus();

			taskStatusDetails.setTaskstatus_id(1L);
			trackerDetails.setAssignedTo(assignedUserDetails.get());
			trackerDetails.setTaskStatus(taskStatusDetails);
			trackerDetails.setTaskDetails(taskData);
			trackerDetails.setInsertDate(LocalDateTime.now());
			trackerDetails.setUserDetails(createdUserDetails.get());

			taskDetailsTrackerDAO.save(trackerDetails);

			for (Iterator<String> it = imagefile.getFileNames(); it.hasNext();) {
				MultipartFile multipartFile = imagefile.getFile(it.next());
				TaskDetailsFile detailsFile = new TaskDetailsFile();
				detailsFile.setTaskDetails(taskData);
				detailsFile.setInsertDate(LocalDateTime.now());
				detailsFile.setUserDetails(createdUserDetails.get());

				detailsFile.setData(multipartFile.getBytes());
				detailsFile.setFileName(multipartFile.getOriginalFilename());
				detailsFile.setFileType(multipartFile.getContentType());

				taskDetailsFileDAO.save(detailsFile);
			}
			return new ResponseEntity<Object>(new StatusRes("Success", "000", "Task added Successfully"),
					HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(new StatusRes("Failed", "007", "Task Details not found"), HttpStatus.OK);
		}
	}

	public ResponseEntity<Object> updateTaskDetails(TaskDetailsPojo taskDetails, MultipartHttpServletRequest imagefile)
			throws IOException {

		TaskDetails details = new TaskDetails();

		Optional<TaskDetails> checkTaskNameDetails = taskDetailsDAO.findByTaskName(taskDetails.getTaskName());
		Optional<TaskType> checkTaskType = taskTypeDAO.findById(taskDetails.getTasktypeId());
		Optional<Project> checkProjectDetails = projectDAO.findById(taskDetails.getProjectId());
		Optional<Modules> checkModules = moduleDAO.findById(taskDetails.getModulesId());
		Optional<UserDetails> createdUserDetails = userDetailsDAO.findById(taskDetails.getCreatedBy());
		Optional<UserDetails> assignedUserDetails = userDetailsDAO.findById(taskDetails.getAssignedTo());
		Optional<TaskDetails> checkTaskID = taskDetailsDAO.findById(taskDetails.getTaskdetailsId());
		// Optional<TaskDetails> checkCreatedID =
		// taskDetailsDAO.findByUserDetails(taskDetails.getCreatedBy());

		BeanUtils.copyProperties(taskDetails, details);

		details.setTaskType(checkTaskType.get());
		details.setProject(checkProjectDetails.get());
		details.setModules(checkModules.get());
		details.setUserDetails(createdUserDetails.get());
		details.setAssignedTo(assignedUserDetails.get());
		details.setPeoplesInvolved(createdUserDetails.get().getUserId() + "");
		details.setInsertDate(LocalDateTime.now());

		TaskStatus taskStatusDetail = new TaskStatus();
		taskStatusDetail.setTaskstatus_id(1L);
		details.setTaskStatus(taskStatusDetail);

		if (taskDetails.getResolvedBy() != null) {
			Optional<UserDetails> resolvedUserDetails = userDetailsDAO.findById(taskDetails.getResolvedBy());
			details.setUserResolveDetails(resolvedUserDetails.get());
		}

		// if (checkCreatedID.isPresent()) {
		if (checkModules.isPresent() && checkTaskType.isPresent() && checkProjectDetails.isPresent()) {
			TaskDetails taskData = taskDetailsDAO.save(details);
			taskData.setUpdatedDate(LocalDateTime.now());
			taskData.setInsertDate(LocalDateTime.now());

			taskDetailsDAO.save(taskData);

			TaskDetailsTracker trackerDetails = new TaskDetailsTracker();
			TaskStatus taskStatusDetails = new TaskStatus();
			taskStatusDetails.setTaskstatus_id(1L);

			trackerDetails.setAssignedTo(assignedUserDetails.get());
			trackerDetails.setTaskStatus(taskStatusDetails);
			trackerDetails.setTaskDetails(taskData);
			trackerDetails.setUpdatedDate(LocalDateTime.now());
			trackerDetails.setInsertDate(LocalDateTime.now());
			trackerDetails.setUserDetails(createdUserDetails.get());

			taskDetailsTrackerDAO.save(trackerDetails);

			for (Iterator<String> it = imagefile.getFileNames(); it.hasNext();) {
				MultipartFile multipartFile = imagefile.getFile(it.next());
				TaskDetailsFile detailsFile = new TaskDetailsFile();
				detailsFile.setTaskDetails(taskData);
				detailsFile.setUpdatedDate(LocalDateTime.now());
				detailsFile.setInsertDate(LocalDateTime.now());
				detailsFile.setUserDetails(createdUserDetails.get());

				detailsFile.setData(multipartFile.getBytes());
				detailsFile.setFileName(multipartFile.getOriginalFilename());
				detailsFile.setFileType(multipartFile.getContentType());

				taskDetailsFileDAO.save(detailsFile);
			}
			return new ResponseEntity<Object>(new StatusRes("Success", "000", " Update Task added Successfully"),
					HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(new StatusRes("Failed", "007", "Task Details not found"), HttpStatus.OK);
		}
//		} else {
//			return new ResponseEntity<Object>(new StatusRes("Failed", "007", "Task Details created User Only Modify"),
//					HttpStatus.OK);
//		}
	}

	public ResponseEntity<Object> taskDetailsList() {
		return new ResponseEntity<>(taskDetailsDAO.findAll(), HttpStatus.OK);
	}

	public List<TaskDetails> viewbyUserTasks(Long assigned_to) {
		return (List<TaskDetails>) taskDetailsDAO.findByAssignedTo(assigned_to);
	}

	public ResponseEntity<Object> updateTaskDetailsTracker(TaskTrackerPojo taskDetailsTracker,
			MultipartHttpServletRequest imagefile) throws IOException {

		Optional<TaskDetails> checkTaskDetailsDetails = taskDetailsDAO.findById(taskDetailsTracker.getTaskdetailsId());
		Optional<TaskDetailsTracker> checkTrackerId = taskDetailsTrackerDAO
				.findById(taskDetailsTracker.getTeskdetailtrackerId());

		if (checkTrackerId.get().getTaskStatus().getTaskstatus_id() == 2) {
			return new ResponseEntity<>(new StatusRes("Failed", "007", "Already Status Resolved"), HttpStatus.OK);
		}

		if (checkTrackerId.isPresent() && checkTaskDetailsDetails.isPresent()) {
			checkTrackerId.get().setSpentHrs(taskDetailsTracker.getSpentHrs());
			checkTrackerId.get().setUpdatedDate(LocalDateTime.now());
			checkTaskDetailsDetails.get().setUpdatedDate(LocalDateTime.now());

			taskDetailsTrackerDAO.save(checkTrackerId.get());
			taskDetailsDAO.save(checkTaskDetailsDetails.get());

			for (Iterator<String> it = imagefile.getFileNames(); it.hasNext();) {
				MultipartFile multipartFile = imagefile.getFile(it.next());
				TaskDetailsFile detailsFile = new TaskDetailsFile();
				detailsFile.setTaskDetailsTracker(checkTrackerId.get());
				detailsFile.setUpdatedDate(LocalDateTime.now());

				detailsFile.setData(multipartFile.getBytes());
				detailsFile.setFileName(multipartFile.getOriginalFilename());
				detailsFile.setFileType(multipartFile.getContentType());

				taskDetailsFileDAO.save(detailsFile);
			}
			return new ResponseEntity<Object>(new StatusRes("Success", "000", "Task added Successfully"),
					HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(new StatusRes("Failed", "007", "Task Details not found"), HttpStatus.OK);
		}
	}

	public ResponseEntity<Object> resolvedTaskDetailsTracker(TaskDetailTrackerPojo taskDetailsTracker,
			MultipartHttpServletRequest imagefile) throws IOException {

		TaskDetailsTracker details = new TaskDetailsTracker();

		Optional<TaskStatus> checkTaskStatus = taskStatusDAO.findById(taskDetailsTracker.getTaskstatusId());
		Optional<TaskDetails> checkTaskDetails = taskDetailsDAO.findById(taskDetailsTracker.getTaskdetailsId());
		Optional<UserDetails> createdAssignedDetails = userDetailsDAO.findById(taskDetailsTracker.getAssignedTo());
		Optional<UserDetails> assignedCommentByDetails = userDetailsDAO.findById(taskDetailsTracker.getResolvedBy());
		List<TaskDetailsTracker> checkTrackerId = taskDetailsTrackerDAO
				.findBytaskDetails(checkTaskDetails.get().getTaskdetailsId());

		TaskDetailsTracker checkTracker = checkTrackerId.get(checkTrackerId.size() - 1);
//		if (checkTracker.getTaskStatus().getTaskstatus_id() == 2) {
//			return new ResponseEntity<>(new StatusRes("Failed", "007", "Already Status Resolved"), HttpStatus.OK);
//		}

		if (checkTaskStatus.isPresent() && checkTaskDetails.isPresent() && createdAssignedDetails.isPresent()) {
			details.setTaskComment(taskDetailsTracker.getTaskComment());
			details.setTaskDetails(checkTaskDetails.get());
			details.setTaskStatus(checkTaskStatus.get());
			details.setAssignedTo(createdAssignedDetails.get());
			details.setResolvedBy(assignedCommentByDetails.get());
			details.setInsertDate(LocalDateTime.now());
			details.setUpdatedDate(LocalDateTime.now());
			details.setSpentHrs(taskDetailsTracker.getSpentHrs());
			checkTaskDetails.get().setUpdatedDate(LocalDateTime.now());
			checkTaskDetails.get().setTaskStatus(details.getTaskStatus());

			if (!checkTaskDetails.get().getPeoplesInvolved().contains(taskDetailsTracker.getResolvedBy().toString())) {
				checkTaskDetails.get().setPeoplesInvolved(
						checkTaskDetails.get().getPeoplesInvolved() + "," + taskDetailsTracker.getResolvedBy());
			}

			details.setUserDetails(assignedCommentByDetails.get());
			details.setResolvedDate(LocalDateTime.now());

			if (taskDetailsTracker.getTaskstatusId() == 2 || taskDetailsTracker.getTaskstatusId() == 4
					|| taskDetailsTracker.getTaskstatusId() == 5) {

				Optional<UserDetails> userResolveDetails = userDetailsDAO.findById(taskDetailsTracker.getResolvedBy());
				details.setResolvedBy(userResolveDetails.get());

				checkTaskDetails.get().setTaskdetailsId(taskDetailsTracker.getTaskdetailsId());
				checkTaskDetails.get().setUserResolveDetails(assignedCommentByDetails.get());
				details.setResolvedDate(LocalDateTime.now());
				checkTaskDetails.get().setAssignedTo(createdAssignedDetails.get());
				checkTaskDetails.get().setTaskStatus(details.getTaskStatus());
				checkTaskDetails.get().setResolvedDate(LocalDateTime.now());

				if (!checkTaskDetails.get().getPeoplesInvolved()
						.contains(taskDetailsTracker.getResolvedBy().toString())) {
					checkTaskDetails.get().setPeoplesInvolved(
							checkTaskDetails.get().getPeoplesInvolved() + "," + taskDetailsTracker.getResolvedBy());
				}
			}

			taskDetailsTrackerDAO.save(details);
			taskDetailsDAO.save(checkTaskDetails.get());

			for (Iterator<String> it = imagefile.getFileNames(); it.hasNext();) {
				MultipartFile multipartFile = imagefile.getFile(it.next());
				TaskDetailsFile detailsFile = new TaskDetailsFile();
				detailsFile.setTaskDetailsTracker(details);
				detailsFile.setInsertDate(LocalDateTime.now());
				// detailsFile.setTaskDetailsTracker(details);
				detailsFile.setUserDetails(assignedCommentByDetails.get());
				detailsFile.setTaskDetails(details.getTaskDetails());

				detailsFile.setData(multipartFile.getBytes());
				detailsFile.setFileName(multipartFile.getOriginalFilename());
				detailsFile.setFileType(multipartFile.getContentType());

				taskDetailsFileDAO.save(detailsFile);
			}
			return new ResponseEntity<Object>(
					new StatusRes("Success", "000", "Task Details Tracker updated Successfully"), HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(new StatusRes("Failed", "007", "Task Details Tracker Details not found"),
					HttpStatus.OK);
		}
	}

	public ResponseEntity<Object> taskDetailsTrackerList() {
		return new ResponseEntity<>(taskDetailsTrackerDAO.findAll(), HttpStatus.OK);
	}

	public ResponseEntity<Object> taskDetailsByID(Long taskID) {
		Map<String, Object> map = new HashMap<String, Object>();

		List<TaskDetailsFile> details = taskDetailsFileDAO.findByTaskDetails(taskID);
		map.put("TaskFiles", details);

		Optional<TaskDetails> taskDetails = taskDetailsDAO.findById(taskID);
		map.put("TaskDetails", taskDetails);

		return new ResponseEntity<>(map, HttpStatus.OK);
	}

	public ResponseEntity<Object> taskDetailsTrackerByID(Long taskDetailsId) {

		List<Object> list = new ArrayList<Object>();

		List<TrackerFileDetails> detailsList = new ArrayList<TrackerFileDetails>();

		List<TaskDetailsTracker> taskDetailsTracker = taskDetailsTrackerDAO.findBytaskDetails(taskDetailsId);

		taskDetailsTracker.forEach(action -> {
			TrackerFileDetails details = new TrackerFileDetails();
			List<TaskDetailsFile> fileDetails = taskDetailsFileDAO
					.findByTaskDetailsTracker(action.getTaskdetailtrackerId());
//				details.setDetailsTracker(action);
//				details.setFiles(fileDetails);	
//				detailsList.add(details);			

			// if(fileDetails.size() > 0) {
			details.setDetailsTracker(action);
			details.setFiles(fileDetails);
			detailsList.add(details);
			// }
		});

		list.add(taskDetailsTracker);

		return new ResponseEntity<>(detailsList, HttpStatus.OK);
	}

	public ResponseEntity<byte[]> getTaskDetailsFile(Long Id) {
		Optional<TaskDetailsFile> details = taskDetailsFileDAO.findById(Id);
		if (details.isPresent()) {
			return ResponseEntity.ok().contentType(MediaType.parseMediaType(details.get().getFileType()))
					.body(details.get().getData());
		}
		return null;
	}

	public ResponseEntity<byte[]> getTaskDetailsTrackerFile(Long Id) {
		Optional<TaskDetailsFile> details = taskDetailsFileDAO.findById(Id);
		if (details.isPresent()) {
			return ResponseEntity.ok().contentType(MediaType.parseMediaType(details.get().getFileType()))
					.body(details.get().getData());
		}
		return null;
	}

	public void downloadZip(Long taskdetailsfileId, HttpServletResponse response) {

		Optional<TaskDetailsFile> taskDetails = taskDetailsFileDAO.findById(taskdetailsfileId);

		if (taskDetails.isPresent()) {

			List<byte[]> byteList = new ArrayList<byte[]>();
			List<String> fileName = new ArrayList<>();

			byteList.add(taskDetails.get().getData());
			fileName.add(taskDetails.get().getFileName());

			response.setContentType("application/octet-stream");
			response.setHeader("Content-Disposition", "attachment;filename=" + taskDetails.get().getFileName()
					+ taskDetails.get().getTaskdetailsfileId() + ".zip");
			response.setStatus(HttpServletResponse.SC_OK);

			try (ZipOutputStream zippedOut = new ZipOutputStream(response.getOutputStream())) {
				for (int i = 0; i < fileName.size(); i++) {
					ZipEntry entry = new ZipEntry(+i + "_" + fileName.get(i));
					zippedOut.putNextEntry(entry);
					zippedOut.write(byteList.get(i), 0, byteList.get(i).length);
					zippedOut.closeEntry();
				}
				zippedOut.finish();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

//	public ResponseEntity<Object> taskDetailsTrackerByIDs(Long taskDetailsID) {
//		return new ResponseEntity<>(taskDetailsTrackerDAO.findBytaskDetails(taskDetailsID), HttpStatus.OK);
//	}
//	
//	public ResponseEntity<Object> taskDetailsByIDs(Long taskID) {
//		return new ResponseEntity<>(taskDetailsDAO.findById(taskID), HttpStatus.OK);
//	}

	public ResponseEntity<Object> deleteByFiles(long taskdetailsfileId) {
		taskDetailsFileDAO.deleteById(taskdetailsfileId);
		Optional<TaskDetailsFile> fileData = taskDetailsFileDAO.findById(taskdetailsfileId);
		if (fileData == null) {
			return new ResponseEntity<Object>(new StatusRes("Failed", "007", "Unexpected error occured"),
					HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(new StatusRes("Success", "000", "file Data deleted successfully"),
					HttpStatus.OK);
		}
	}

	public ResponseEntity<Object> deleteByTaskFiles(List<Long> taskdetailsfileId) {	

		for (Long fileID : taskdetailsfileId) {
			Optional<TaskDetailsFile> fileData = taskDetailsFileDAO.findById(fileID);
			if (fileData.isPresent()) {
				taskDetailsFileDAO.deleteById(fileID);
			} else {
				return new ResponseEntity<>(new StatusRes("Failed", "007", "Unexpected Error Occured"),
						HttpStatus.OK);
			}
		}
		return new ResponseEntity<>(new StatusRes("Success", "000", "Files Deleted Successfully"),
				HttpStatus.OK);
	}

}
