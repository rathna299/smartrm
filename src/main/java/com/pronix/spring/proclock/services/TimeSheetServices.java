package com.pronix.spring.proclock.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.pronix.spring.proclock.dao.Project_employeeDAO;
import com.pronix.spring.proclock.dao.UserDetailsDAO;
import com.pronix.spring.proclock.models.Project_Employees;
import com.pronix.spring.proclock.models.UserDetails;
import com.pronix.spring.proclock.pojos.StatusRes;
import com.pronix.spring.proclock.timesheet.DAO.TimeSheet_DAO;
import com.pronix.spring.proclock.timesheet.models.Timesheet_details;

@Service
public class TimeSheetServices {

	@Autowired
	private TimeSheet_DAO timeSheet_DAO;

	@Autowired
	private UserDetailsDAO userDetailsDAO;

	@Autowired
	private Project_employeeDAO project_employeeDAO;

	public ResponseEntity<Object> insertDayTimesheet(List<Timesheet_details> timesheet_details) {
		boolean save = false;
		try {
			for (Timesheet_details details : timesheet_details) {
				Optional<Timesheet_details> checkExisting = timeSheet_DAO.findByYearMonthWeekDay(
						details.getUserDetails().getUserId(), details.getTs_year(), details.getTs_month(),
						details.getTs_week(), details.getProject().getProjectId());
				if (!checkExisting.isPresent()) {
					save = true;
				}
			}
			if (save == true) {
				timesheet_details.forEach(timesheet -> {
					timesheet.setWeek_status("Saved");
					timeSheet_DAO.save(timesheet);
				});
				return new ResponseEntity<>(new StatusRes("Success", "000", "Timesheet Saved Successfully"),
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new StatusRes("Failed", "007", "Hours already present in this week"),
						HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public ResponseEntity<Object> updateDayTimeSheet(List<Timesheet_details> timesheet_details) {
		try {
			boolean update = true;
			for (Timesheet_details details : timesheet_details) {
				Optional<Timesheet_details> checkExisting = timeSheet_DAO.findByYearMonthWeekDay(
						details.getUserDetails().getUserId(), details.getTs_year(), details.getTs_month(),
						details.getTs_week(), details.getProject().getProjectId());
				if (checkExisting.isPresent()) {
					update = true;
				} else {
					// return new ResponseEntity<>(new StatusRes("Failed", "000", "Details not
					// found"), HttpStatus.OK);
				}
			}
			if (update == true) {
				timesheet_details.forEach(updateRecord -> {
					timeSheet_DAO.save(updateRecord);
				});
				return new ResponseEntity<>(new StatusRes("Success", "000", "Timesheet updated Successfully"),
						HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public ResponseEntity<Object> timeSheetList(String status, long year, long month) {
		List<Timesheet_details> userTimesheetList = new ArrayList<>();
		if (status != null) {
			List<Timesheet_details> details = timeSheet_DAO.findByStatusAndYearAndMonth(status, year, month);
			details.forEach(detail -> {
				Project_Employees checkStatus = project_employeeDAO.findByEmployeeAndProject(
						detail.getUserDetails().getUserId(), detail.getProject().getProjectId());
				if (checkStatus != null) {
					detail.getProject().setEnable(checkStatus.isEnable());
					userTimesheetList.add(detail);
				} else {
					userTimesheetList.add(detail);
				};
				
			});
			return new ResponseEntity<>(userTimesheetList, HttpStatus.OK);
		}
		return new ResponseEntity<>(timeSheet_DAO.findAll(), HttpStatus.OK);
	}

	public Object getTimeSheetByManager(Long reporting_manager_id, String status, long year, long month) {
		List<Timesheet_details> userTimesheetList = new ArrayList<>();
		List<UserDetails> userList = userDetailsDAO.findByManagerIdAndActive(reporting_manager_id, true);
		for (UserDetails details : userList) {
			if (status != null) {
				List<Timesheet_details> timesheeList = timeSheet_DAO.findByUserAndStatusAndYearAndMonth(status,
						details.getUserId(), year, month);
				timesheeList.forEach(detail -> {
					Project_Employees checkStatus = project_employeeDAO.findByEmployeeAndProject(
							detail.getUserDetails().getUserId(), detail.getProject().getProjectId());
					if (checkStatus != null) {
						detail.getProject().setEnable(checkStatus.isEnable());
						userTimesheetList.add(detail);
					} else {
						userTimesheetList.add(detail);
					}
				});

			}
		}
		return new ResponseEntity<>(userTimesheetList, HttpStatus.OK);
	}

	public Object getMonthTimeSheetByUser(Long user_id, String status, long year, long month) {
		if (status != null) {
			return new ResponseEntity<>(timeSheet_DAO.findByUserAndStatusAndYearAndMonth(status, user_id, year, month),
					HttpStatus.OK);
		} else {
			return new ResponseEntity<>(timeSheet_DAO.findByUserAndYearAndMonth(user_id, year, month), HttpStatus.OK);
		}
	}

	public Object getWeekTimeSheetByUser(Long user_id, long year, long month, long week) {
		List<Timesheet_details> finalList = new ArrayList<>();
		List<Timesheet_details> details = timeSheet_DAO.findByUserAndYearAndMonthAndWeek(user_id, year, month, week);
		details.forEach(detail -> {
			Project_Employees checkStatus = project_employeeDAO
					.findByEmployeeAndProject(detail.getUserDetails().getUserId(), detail.getProject().getProjectId());
			if (checkStatus != null) {
				detail.getProject().setEnable(checkStatus.isEnable());
				finalList.add(detail);
			} else {
				finalList.add(detail);
			}
		});
		return new ResponseEntity<>(finalList, HttpStatus.OK);
	}

	public Object getCountByReportingManager(Long reporting_manager_id, long year, long month) {
		HashMap<String, Long> count = new HashMap<>();
		long submitCount = 0;
		long savedCount = 0;
		long approvedCount = 0;
		long rejectedcount = 0;
		List<UserDetails> userList = userDetailsDAO.findByManagerIdAndActive(reporting_manager_id, true);
		for (UserDetails details : userList) {
			Long submitted = timeSheet_DAO.findByTimesheetCountByManager("Submitted", details.getUserId(), month, year)
					.stream().count();
			submitCount = submitCount + submitted;
			Long approved = timeSheet_DAO.findByTimesheetCountByManager("Approved", details.getUserId(), month, year)
					.stream().count();
			approvedCount = approvedCount + approved;
			Long Rejected = timeSheet_DAO.findByTimesheetCountByManager("Declined", details.getUserId(), month, year)
					.stream().count();
			rejectedcount = rejectedcount + Rejected;
			Long saved = timeSheet_DAO.findByTimesheetCountByManager("Saved", details.getUserId(), month, year).stream()
					.count();
			savedCount = savedCount + saved;
		}
		count.put("submitted", submitCount);
		count.put("approved", approvedCount);
		count.put("saved", savedCount);
		count.put("declined", rejectedcount);
		return new ResponseEntity<>(count, HttpStatus.OK);
	}

	public Object getCountByAllUsers(long year, long month) {
		HashMap<String, Long> count = new HashMap<>();

		Long submitted = timeSheet_DAO.findByTimesheetCountByAllUsers("Submitted", month, year).stream().count();

		Long approved = timeSheet_DAO.findByTimesheetCountByAllUsers("Approved", month, year).stream().count();

		Long Rejected = timeSheet_DAO.findByTimesheetCountByAllUsers("Declined", month, year).stream().count();

		Long saved = timeSheet_DAO.findByTimesheetCountByAllUsers("Saved", month, year).stream().count();

		count.put("submitted", submitted);
		count.put("approved", approved);
		count.put("saved", saved);
		count.put("declined", Rejected);
		return new ResponseEntity<>(count, HttpStatus.OK);
	}

}
