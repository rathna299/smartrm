package com.pronix.spring.proclock.services;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.apache.catalina.Manager;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.pronix.spring.proclock.dao.AssetsDao;
import com.pronix.spring.proclock.dao.Assets_employeeDAO;
import com.pronix.spring.proclock.dao.BusinessUnitsDAO;
import com.pronix.spring.proclock.dao.CompanyDAO;
import com.pronix.spring.proclock.dao.DashboardContentDAO;
import com.pronix.spring.proclock.dao.DepartmentDAO;
import com.pronix.spring.proclock.dao.JobTitlesDAO;
import com.pronix.spring.proclock.dao.LeaveDetailsDAO;
import com.pronix.spring.proclock.dao.LoginLogsDAO;
import com.pronix.spring.proclock.dao.ModuleDAO;
import com.pronix.spring.proclock.dao.ProjectDAO;
import com.pronix.spring.proclock.dao.Project_employeeDAO;
import com.pronix.spring.proclock.dao.RefRolesDAO;
import com.pronix.spring.proclock.dao.RefStatusDAO;
import com.pronix.spring.proclock.dao.RolesModuleDAO;
import com.pronix.spring.proclock.dao.TimeSheetUnitqueDAO;
import com.pronix.spring.proclock.dao.UserContactsDAO;
import com.pronix.spring.proclock.dao.UserDetailsDAO;
import com.pronix.spring.proclock.dao.UserPasswordDAO;
import com.pronix.spring.proclock.dao.UserPersonalInfoDao;
import com.pronix.spring.proclock.dao.UserRolesDAO;
import com.pronix.spring.proclock.models.Assets;
import com.pronix.spring.proclock.models.Assets_Employees;
import com.pronix.spring.proclock.models.BusinessUnits;
import com.pronix.spring.proclock.models.Company;
import com.pronix.spring.proclock.models.DashboardContent;
import com.pronix.spring.proclock.models.Department;
import com.pronix.spring.proclock.models.JobTitles;
import com.pronix.spring.proclock.models.LeaveDetails;
import com.pronix.spring.proclock.models.Logs;
import com.pronix.spring.proclock.models.Modules;
import com.pronix.spring.proclock.models.Project;
import com.pronix.spring.proclock.models.Project_Employees;
import com.pronix.spring.proclock.models.RefRoles;
import com.pronix.spring.proclock.models.RolesModules;
import com.pronix.spring.proclock.models.UserContacts;
import com.pronix.spring.proclock.models.UserDetails;
import com.pronix.spring.proclock.models.UserPassword;
import com.pronix.spring.proclock.models.UserPersonalInfo;
import com.pronix.spring.proclock.models.UserRoles;
import com.pronix.spring.proclock.pojos.AssetsEmployeePojo;
import com.pronix.spring.proclock.pojos.AssetsPojo;
import com.pronix.spring.proclock.pojos.CompanyPojo;
import com.pronix.spring.proclock.pojos.DashboardPojo;
import com.pronix.spring.proclock.pojos.Employee;
import com.pronix.spring.proclock.pojos.EmployeeMultiple;
import com.pronix.spring.proclock.pojos.LeaveDetailsPojo;
import com.pronix.spring.proclock.pojos.LoginDetailsPojo;
import com.pronix.spring.proclock.pojos.LoginStatusRes;
import com.pronix.spring.proclock.pojos.ProjectPojo;
import com.pronix.spring.proclock.pojos.RemoveContentPojo;
import com.pronix.spring.proclock.pojos.ResetPassword;
import com.pronix.spring.proclock.pojos.StatusRes;
import com.pronix.spring.proclock.pojos.UserDetailsPojo;
import com.pronix.spring.proclock.utils.Constants;

@Service
public class UserServices {

	@Autowired
	private UserContactsDAO userContactsDAO;

	@Autowired
	private UserDetailsDAO userDetailsDAO;

	@Autowired
	private UserPasswordDAO userPasswordDAO;

	@Autowired
	private UserRolesDAO userRolesDAO;

	@Autowired
	private RefRolesDAO refRolesDAO;

	@Autowired
	private RefStatusDAO refStatusDAO;

	@Autowired
	private CompanyDAO companyDAO;

	@Autowired
	private ProjectDAO projectDAO;

	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	private AssetsDao assetsDao;

	@Autowired
	private LeaveDetailsDAO leaveDetailsDAO;

	@Autowired
	private JobTitlesDAO jobTitlesDAO;

	@Autowired
	private BusinessUnitsDAO businessUnitsDAO;

	@Autowired
	private DepartmentDAO departmentDAO;

	@Autowired
	private RolesModuleDAO rolesModuleDAO;

	@Autowired
	private ModuleDAO moduleDAO;

	@Autowired
	private Project_employeeDAO project_employeeDAO;

	@Autowired
	private DashboardContentDAO dashboardContentDAO;

	@Autowired
	private UserPersonalInfoDao userPersonalInfoDao;

	@Autowired
	private TimeSheetUnitqueDAO timeSheetUnitqueDAO;

	@Autowired
	private LeaveIncrementServiceImpl leaveIncrementServiceImpl;

	@Autowired
	private Assets_employeeDAO assets_employeeDAO;

	@Autowired
	private LoginLogsDAO loginLogsDAO;

	@Value("{documents.onboarding.Path}")
	private String documentsPath;

	private static final String ALPHA_NUMERIC_STRING = "abcdefghijklmnopqrstuvwxyz0123456789";
	
	private static final Logger log = LoggerFactory.getLogger(UserServices.class);

	private static final Pattern PASSWORD_PATTERN = Pattern
			.compile("((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%!]).{8,16})");

	public StatusRes assetAssigntoUser(AssetsEmployeePojo assetsemp) {
		Optional<UserDetails> userDetails = userDetailsDAO.findById(assetsemp.getUserId());
		Optional<Assets> asset = assetsDao.findById(assetsemp.getAssetId());

		if (asset.isPresent() && asset.get().isEnable() == false) {

			assets_employeeDAO
					.save(new Assets_Employees(assetsemp.getAssignToUserDate(), asset.get(), userDetails.get()));

			asset.get().setEnable(true);
			assetsDao.save(asset.get());

			return new StatusRes("Success", "000", "Asset assigned Sucessfully", null);
		} else if (asset.get().isEnable() == true) {
			return new StatusRes("Failed", "007", " already assigned to User", null);
		} else
			return new StatusRes("Failed", "007", " Not Found", null);
	}

	public List<StatusRes> addProductslist(List<AssetsPojo> assetsList) {
		List<StatusRes> statusResList = new ArrayList<>();

		for (AssetsPojo assets : assetsList) {

			if (assets.getProductNumber().isEmpty() || assets.getProductId().isEmpty()
					|| assets.getProductNumber().isEmpty() && assets.getProductId().isEmpty()) {
				statusResList.add(new StatusRes("failed", "007", "please provide all details"));
			} else {
				UserDetails userDetails = userDetailsDAO.findById(assets.getUserId()).get();
				Assets asset = assetsDao.findByProductNumberAndProductId(assets.getProductNumber(),
						assets.getProductId());
				if (asset != null)
					statusResList.add(new StatusRes("failed", "007", "Product ID : " + assets.getProductId()
							+ ", Product Number : " + assets.getProductNumber() + " Already Existed", null));
				else {
					assetsDao.save(new Assets(assets.getProductType(), assets.getProductNumber(),
							assets.getProductDesc(), assets.getProductId(), assets.getProductModel(),
							assets.getProductMake(), false, userDetails));

					statusResList.add(new StatusRes("success", "000", "Assets added sucessfully", null));
				}
			}
		}
		return statusResList;
	}

	public List<StatusRes> updateProduct(AssetsPojo assets) {
		List<StatusRes> statusResList = new ArrayList<>();
		try {
			Assets asset = assetsDao.findById(assets.getAssetId())
					.orElseThrow(() -> new RuntimeException("Job Title not found"));
			Assets productNumber = assetsDao.findByProductNumber(assets.getProductNumber());
			Assets productId = assetsDao.findByProductId(assets.getProductId());
			UserDetails userDetails = userDetailsDAO.findById(assets.getUserId()).get();

			if (productId == null && productNumber == null) {
				
				asset.setAssetId(assets.getAssetId());
				asset.setProductType(assets.getProductType());
				asset.setProductNumber(assets.getProductNumber());
				asset.setProductDesc(assets.getProductDesc());
				asset.setProductId(assets.getProductId());
				asset.setProductModifyDate();
				asset.setProductModel(assets.getProductModel());
				asset.setUserDetails(userDetails);
				assetsDao.save(asset);

				statusResList.add(new StatusRes("Success", "000", " assets updated successfull"));

			} else if (productId == null || productNumber == null) {
				if (productNumber == null) {
					if (productId.getAssetId() == assets.getAssetId()) {
						
						asset.setAssetId(assets.getAssetId());
						asset.setProductType(assets.getProductType());
						asset.setProductNumber(assets.getProductNumber());
						asset.setProductDesc(assets.getProductDesc());
						asset.setProductId(assets.getProductId());
						asset.setProductModifyDate();
						asset.setProductModel(assets.getProductModel());
						asset.setUserDetails(userDetails);
						assetsDao.save(asset);
						
						statusResList.add(new StatusRes("Success", "000", " assets updated successfull"));
					} else {
						statusResList.add(new StatusRes("Failed", "007", " assets Code Already Existed"));
					}
				} else if (productId == null) {
					if (productNumber.getAssetId() == assets.getAssetId()) {
						
						asset.setAssetId(assets.getAssetId());
						asset.setProductType(assets.getProductType());
						asset.setProductNumber(assets.getProductNumber());
						asset.setProductDesc(assets.getProductDesc());
						asset.setProductId(assets.getProductId());
						asset.setProductModifyDate();
						asset.setProductModel(assets.getProductModel());
						asset.setUserDetails(userDetails);
						assetsDao.save(asset);

						statusResList.add(new StatusRes("Success", "000", " assets updated successfull"));
					} else {
						statusResList.add(new StatusRes("Failed", "007", " assets Name Already Existed"));
					}
				}
			} else if (productId != null && productNumber != null) {
				if (productId.getAssetId() == assets.getAssetId()
						&& productNumber.getAssetId() == assets.getAssetId()) {
				
					asset.setAssetId(assets.getAssetId());
					asset.setProductType(assets.getProductType());
					asset.setProductNumber(assets.getProductNumber());
					asset.setProductDesc(assets.getProductDesc());
					asset.setProductId(assets.getProductId());
					asset.setProductModifyDate();
					asset.setProductModel(assets.getProductModel());
					asset.setUserDetails(userDetails);
					assetsDao.save(asset);

					statusResList.add(new StatusRes("Success", "000", " assets updated successfull"));
				} else {
					statusResList.add(new StatusRes("Failed", "007", " Details Already Existed"));
				}
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}
		return statusResList;
	}

	public List<Assets_Employees> viewProducts(Long userId) {
		return (List<Assets_Employees>) assets_employeeDAO.findByUserid(userId);
	}

	public List<Assets> viewListProducts(String status) {
		if (status != null) {
			if (status.equalsIgnoreCase("assigned")) {
				return (List<Assets>) assetsDao.findAll().stream().filter(n -> n.isEnable() == true)
						.collect(Collectors.toList());
			} else if (status.equalsIgnoreCase("unassigned")) {
				return (List<Assets>) assetsDao.findAll().stream().filter(n -> n.isEnable() == false)
						.collect(Collectors.toList());
			}
		} else {
			return (List<Assets>) assetsDao.findAll();
		}
		return null;
	}

	public StatusRes deleteByAssets(long assetId) {
		assetsDao.deleteById(assetId);
		Optional<Assets> asset = assetsDao.findById(assetId);
		if (asset == null) {
			return new StatusRes("Success", "000", "Assets deleted successfully");
		} else {
			return new StatusRes("failed", "007", "unexpected error occured");
		}
	}

	public Object viewEmployeeList(Long userId) {
		List<Object> contactData = new ArrayList<>();
		Optional<UserDetails> userDetails = userDetailsDAO.findById(userId);
		UserContacts userContact = userContactsDAO.findByUserDetails(userDetails.get());
		contactData.add(userContact);
		contactData.add(userDetails);
		return contactData;
	}

	public List<UserDetails> viewListofEmployee() {
		return (List<UserDetails>) userDetailsDAO.findAll();
	}

	public List<Assets> viewProductTypeList() {
		return (List<Assets>) assetsDao.findAll();
	}

	public Object registerUser(UserDetailsPojo userDetailsPojo) {
		List<StatusRes> statusResList = new ArrayList<StatusRes>();
		if (userPasswordDAO.findByUserName(userDetailsPojo.getUserName()) != null) {
			statusResList.add(new StatusRes("failed", "011", "UserName is Duplicated"));
		}
		if (userContactsDAO.findByEmailId(userDetailsPojo.getEmailId()) != null) {
			statusResList.add(new StatusRes("failed", "012", "EmailId is Duplicated"));
		}
		if (userContactsDAO.findByPhoneNo(userDetailsPojo.getPhoneNo()) != null) {
			statusResList.add(new StatusRes("failed", "013", "Phone Number is Duplicated"));
		}
		if (statusResList.size() == 0) {
			try {
				// userContactsDAO.findByEmailId(userDetailsPojo.getEmailId());
				RefRoles refRoles = null;
				refRoles = refRolesDAO.findByValue("Super Admin");
				if (refRoles == null) {
					refRolesDAO.save(new RefRoles("Super Admin", "Admin User"));
					refRoles = refRolesDAO.findByValue("Super Admin");
				}
				String password = randomAlphaNumeric();
				UserDetails userDetails = new UserDetails(userDetailsPojo.getFirstName(), userDetailsPojo.getLastName(),
						getMd5(userDetailsPojo.getUserName()), false, refRoles, userDetailsPojo.getDateOfJoining());
				userDetailsDAO.save(userDetails);
				UserPassword userPassword = new UserPassword(userDetailsPojo.getUserName(), getMd5(password),
						userDetails, null);
				UserContacts userContacts = new UserContacts(userDetailsPojo.getEmailId(), userDetailsPojo.getPhoneNo(),
						userDetails);
				UserRoles userRoles = new UserRoles(refRoles, userDetails);
				userPasswordDAO.save(userPassword);
				userContactsDAO.save(userContacts);
				userRolesDAO.save(userRoles);

				sendEmail(userDetailsPojo.getEmailId(), "Email Verification for SmarTRM", "	<body>\r\n"
						+ "<table id=\"Table_01\" style=\"border: 2px solid #062f4f; margin: 10px auto; border-radius: 8px;\" width=\"621\" cellspacing=\"0\" cellpadding=\"0\">"
						+ "<tbody>" + "<tr style=\"background: #062f4f; color: white; height: 39px;\">"
						+ "<td style=\"padding: 20px 10px; font-size: 30px; height: 39px;\">SmarTRM</td>" + "</tr>"
						+ "<tr style=\"height: 36px;\">" + "<td style=\"padding: 20px 10px; height: 36px;\">Dear "
						+ userDetailsPojo.getFirstName() + ",</p>"
						+ "  <p style=\"padding: 20px 10px;height: 15px;\">SmarTRM Heartly Welcomes You.</p>"
						+ "<p style=\"padding: 0px 20px; height: 25px;\">Mr. " + userDetailsPojo.getFirstName()
						+ " You are the admin and You can start with this.</p>"
						+ "  <p style=\"padding-left:50px;\">Your Login Credentials</p>\r\n"
						+ "  <p style=\"padding-left:100px; color:red;\">Email : " + userDetailsPojo.getEmailId()
						+ "</p>\r\n" + "  <p style=\"padding-left:100px;color:red;\">Password : " + password
						+ "</p>\r\n"
						+ "  <p style=\"padding-left:50px;\">Please confirm this email by clicking on this</p>"
						+ "  <a href=" + Constants.hostnameAPI + Constants.confirmEmail
						+ getMd5(userDetailsPojo.getUserName())
						+ " target=\"_blank\"style=\"background-color: #4CAF50;\r\n" + "  color: white;width:100px;\r\n"
						+ "  padding: 15px 25px;\r\n"
						+ "  text-align: center;display: inline-block;text-decoration: none;display: block;\r\n"
						+ "  margin-left: auto;\r\n" + "  margin-right: auto;\">Confirm Email</a>\r\n" + "</td>"
						+ "</tr>" + "<tr style=\"height: 33px;\">"
						+ "<td style=\"padding: 20px 40px; text-align: right; height: 33px;\">Thanks and Regards<br /> SmarTRM</td>"
						+ "</tr>" + "</tbody>" + "</table>" + "</body>");

				statusResList.add(new StatusRes("success", "000",
						"Registration Successful, Activation link sent to the email Successfully",
						userDetailsDAO.findById(userPassword.getUserDetails().getUserId())));
			} catch (DataIntegrityViolationException e) {
				String x[] = e.getMostSpecificCause().getMessage().split("\'");
				if (userDetailsPojo.getUserName().equals(x[1]))
					return new StatusRes("failed", "011", "UserName is Duplicated");
				else if (userDetailsPojo.getEmailId().equals(x[1]))
					return new StatusRes("failed", "012", "EmailId is Duplicated");
				else if (userDetailsPojo.getPhoneNo().equals(x[1]))
					return new StatusRes("failed", "013", "Phone Number is Duplicated");
				else
					return new StatusRes("failed", "004", "Mobile Number or EmailId or Username is already found");

			} catch (Exception e) {
				return new StatusRes("failed", "009", "Mobile Number or EmailId or Username is already found");
			}

		}
		return statusResList;
	}

	public Object login(LoginDetailsPojo loginDetailsPojo, HttpServletRequest request) {
		UserContacts userContacts = userContactsDAO.findByEmailId(loginDetailsPojo.getEmailId());
		List<Modules> modulelist = new ArrayList<>();
		List<DashboardContent> contentlist = new ArrayList<>();
		if (userContacts != null) {
			if (userContacts.getUserDetails().isEnabled()) {

				LocalDate doj = DateTimeFormat.forPattern("dd-MM-yyyy")
						.parseLocalDate(userContacts.getUserDetails().getDateOfJoining());
				LocalDate todayDate = new LocalDate();
				if (todayDate.isBefore(doj)) {
					return new StatusRes("failed", "003",
							"Your date of joing is in Future -> " + userContacts.getUserDetails().getDateOfJoining());
				}
				UserPassword userPassword = userPasswordDAO.findByUserId(userContacts.getUserDetails().getUserId());

				if (userPassword.getUserPassword().equals(getMd5(loginDetailsPojo.getPassword()))) {
					UserDetails userDetails = userDetailsDAO.findById(userPassword.getUserDetails().getUserId()).get();
					// UserRoles userRoles = userRolesDAO.findByUserDetails(userDetails);
					Optional<RefRoles> rolename = refRolesDAO.findById(userDetails.getTempRefRoles().getRoleId());
					List<RolesModules> role_module = rolesModuleDAO.findByRoleWithEnables(rolename.get().getRoleId(),
							true);
					List<DashboardContent> contents = dashboardContentDAO.findByuserdetails(userDetails);
					for (RolesModules rolelist : role_module) {
						Modules module = moduleDAO.findById(rolelist.getModules().getModulesId()).get();
						modulelist.add(module);
					}
					for (DashboardContent content : contents) {
						contentlist.add(content);
					}
					String IpAddress = getIpAddr(request);
					loginLogs(userDetails, userContacts, IpAddress);
					return new LoginStatusRes("success", "000", "Your login is Successful", userDetails,
							rolename.get().getValue(), modulelist, contentlist);
				} else
					return new StatusRes("failed", "003", "Password Wrong");
			} else
				return new StatusRes("failed", "009", "Your Email is not yet Confirmed");
		} else
			return new StatusRes("failed", "001", "Email is not Registered");
	}

	public StatusRes forgotPassword(String email) {
		UserContacts userContacts = userContactsDAO.findByEmailId(email);
		if (userContacts != null) {
			String password = randomAlphaNumeric();
			UserPassword userPassword = userPasswordDAO.findByUserId(userContacts.getUserDetails().getUserId());
			userPassword.setUserPassword(getMd5(password));
			userPasswordDAO.save(userPassword);
			sendEmail(userContacts.getEmailId(), "Whoops! Let's Reset your Password.", "	<body>\r\n"
					+ "<table id=\"Table_01\" style=\"border: 2px solid #062f4f; margin: 10px auto; border-radius: 8px;\" width=\"621\" cellspacing=\"0\" cellpadding=\"0\">"
					+ "<tbody>" + "<tr style=\"background: #062f4f; color: white; height: 39px;\">"
					+ "<td style=\"padding: 20px 10px; font-size: 30px; height: 39px;\">SmarTRM</td>" + "</tr>"
					+ "<tr style=\"height: 36px;\">" + "<td style=\"padding: 20px 10px; height: 36px;\">Dear "
					+ userContacts.getUserDetails().getFirstName() + ",</p>\r\n"
					+ "  <p style=\"padding: 20px 10px;height: 15px;\"> Welcomes You SmarTRM.</p>"
					+ "  <p style=\"padding-left:50px;\">You Reset Your Password.</p>\r\n"
					+ "  <p style=\"padding-left:50px;\">Your Login Credentials</p>\r\n"
					+ "  <p style=\"padding-left:50px;\">Email : " + email + "</p>\r\n"
					+ "  <p style=\"padding-left:50px;\">Password : " + password + "</p>\r\n"
					+ "  <p style=\"padding-left:50px;\">If you didn't sign up for an account, Please ignore this mail</p>\r\n"
					+ "</td>" + "</tr>" + "<tr style=\"height: 33px;\">"
					+ "<td style=\"padding: 20px 40px; text-align: right; height: 33px;\">Thanks and Regards<br /> SmarTRM</td>"
					+ "</tr>" + "</tbody>" + "</table>" + "</body>");

			return new StatusRes("success", "000", " Activation link sent to the email Successfully");
		} else
			return new StatusRes("failed", "001", "Email is not Registered");
	}

	public StatusRes resetPassword(ResetPassword resetPassword) {
		UserPassword userPassword = userPasswordDAO.findByUserId(resetPassword.getUserId());
		if (userPassword != null) {
			userPasswordDAO.save(new UserPassword(userPassword.getId(), userPassword.getUserName(),
					getMd5(resetPassword.getNewPassword()), userPassword.getUserDetails(),
					userPassword.getCreatedByUserDetails(), userPassword.getCreatedDate()));
			return new StatusRes("success", "000", "ok");
		} else
			return new StatusRes("failed", "002", "User Not Found");
	}

	public StatusRes changePassword(ResetPassword changePassword) {
		UserPassword userPassword = userPasswordDAO.findByUserId(changePassword.getUserId());

		if (userPassword != null) {
			if (userPassword.getUserPassword().equals(getMd5(changePassword.getOldPassword()))) {
				if (changePassword.getOldPassword().equals(changePassword.getNewPassword())) {
					return new StatusRes("failed", "003", "old password should not be same as new password");
				} else {
					if (changePassword.getNewPassword() != null) {
						Matcher matcher = PASSWORD_PATTERN.matcher(changePassword.getNewPassword());
						if (!matcher.matches()) {
							return new StatusRes("failed", "003", "Upper Lower Special Digits password dont match");
						}
					}
					userPassword.setUserPassword(getMd5(changePassword.getNewPassword()));
					userPasswordDAO.save(userPassword);
					return new StatusRes("success", "000", "password changed successfully");
				}
			} else
				return new StatusRes("failed", "003", "Old Password Wrong");
		} else
			return new StatusRes("failed", "002", "User Not Found");
	}

	public List<RefRoles> getRoles() {
		List<RefRoles> finalist = new ArrayList<>();
		List<RefRoles> list = refRolesDAO.findAll();
		list.forEach(getOne -> {
			if (getOne.getRoleId() != 1) {
				finalist.add(getOne);
			}
		});
		return finalist;
	}

	public StatusRes addCompany(CompanyPojo company) {
		UserDetails userDetails = userDetailsDAO.findById(company.getUserId()).get();
		Company companydata = companyDAO.findByAddressLine1(company.getAddressLine1());

		if (companydata != null)
			return new StatusRes("failed", "007", "Company Address Already Existed");
		else {
			companyDAO.save(new Company(company.getCompanyName(), company.getCountry(), company.getAddressLine1(),
					company.getAddressLine2(), company.getCity(), company.getState(), company.getPostalCode(),
					company.getPhoneNumber(), company.getEmailId(), company.getWebsite(), company.getDomain(),
					company.getStartedDate(), userDetails));

			return new StatusRes("success", "000", "New-Company added sucessfully");
		}
	}

	public StatusRes updateCompany(CompanyPojo company) {
		UserDetails userDetails = userDetailsDAO.findById(company.getUserId()).get();
		Company companydata = companyDAO.findByAddressLine1(company.getAddressLine1());

		try {
			companyDAO.save(new Company(company.getCompanyId(), company.getCompanyName(), company.getCountry(),
					company.getAddressLine1(), company.getAddressLine2(), company.getCity(), company.getState(),
					company.getPostalCode(), company.getPhoneNumber(), company.getEmailId(), company.getWebsite(),
					company.getDomain(), company.getStartedDate(), userDetails));

			return new StatusRes("success", "000", "Updated sucessfully");
		} catch (Exception e) {
			return new StatusRes("failed", "007",
					"company with Address :" + company.getAddressLine1() + "  already there", null);
		}
	}
	
	public List<Company> viewCompanies() {
		return (List<Company>) companyDAO.findAll();
	}

	public StatusRes addProject(ProjectPojo projectPojo) {
		Company company = companyDAO.findById(projectPojo.getCompanyId()).get();
		List<Project> projectList = projectDAO.findProjectsByCompany(company);

		int count = 0;
		for (Project project : projectList) {
			if (project.getProjectName().equals(projectPojo.getProjectName())
					&& project.getProjectCode().equals(projectPojo.getProjectCode())
					&& project.getProjectVendor().equals(projectPojo.getProjectVendor())
					&& project.getProjectStartDate().equals(projectPojo.getProjectStartDate())
					&& project.getDescription().equals(projectPojo.getDescription())
					&& project.getProjectEndDate().equals(projectPojo.getProjectEndDate())) {
				count++;
			} else if (project.getProjectCode().toUpperCase().equals(projectPojo.getProjectCode().toUpperCase())) {
				count++;
			}
		}
		try {
			if (count == 0) {
				projectDAO.save(new Project(projectPojo.getProjectName(), projectPojo.getProjectCode(),
						projectPojo.getProjectVendor(), projectPojo.getProjectStartDate(), projectPojo.getDescription(),
						projectPojo.getProjectEndDate(), company));
				return new StatusRes("success", "000", "Project added Successfully", null);
			} else
				return new StatusRes("failed", "012", "Project already found", null);
		} catch (Exception e) {
			return new StatusRes("failed", "007", "project code already found", e.getMessage());
		}
	}

	public StatusRes updateProject(ProjectPojo projectPojo) {
		Company company = companyDAO.findById(projectPojo.getCompanyId()).get();
		List<Project> projectList = projectDAO.findProjectsByCompany(company);
		int count = 0;
		for (Project project : projectList) {
			if (project.getProjectCode().toUpperCase().equals(projectPojo.getProjectCode().toUpperCase())
					&& project.getProjectId() != projectPojo.getProjectId()) {
				count++;
			} else if (project.getProjectCode().toUpperCase().equals(projectPojo.getProjectCode().toUpperCase())
					&& project.getProjectId() == projectPojo.getProjectId()) {
				count--;
			}
		}
		if (count == 0 || count == -1) {
			projectDAO.save(new Project(projectPojo.getProjectId(), projectPojo.getProjectName(),
					projectPojo.getProjectCode(), projectPojo.getProjectVendor(), projectPojo.getProjectStartDate(),
					projectPojo.getDescription(), projectPojo.getProjectEndDate(), company));
			return new StatusRes("success", "000", "Project updated Successfully", null);
		} else
			return new StatusRes("failed", "012", "Project code already found ", null);
	}

	public List<Project> viewProject() {
		List<Project> projectList = new ArrayList<Project>();
		List<Project> project = (List<Project>) projectDAO.findAll();
		for (Project projectData : project) {
			if (projectData.getProjectId() != 1) {
				projectList.add(projectData);
			}
		}
		return projectList;
	}
	
	public List<Project> viewProjects(Long companyId) {
		return projectDAO.findProjectsByCompany(companyDAO.findById(companyId).get());
	}

	public List<StatusRes> addEmployee(EmployeeMultiple employee) {
		List<StatusRes> statusResList = new ArrayList<StatusRes>();
		String email = employee.getEmailId();
		if (userContactsDAO.findByEmailId(employee.getEmailId()) != null) {
			statusResList.add(new StatusRes("failed", "012", "EmailId is Duplicated"));
		}
		if (userContactsDAO.findByPhoneNo(employee.getPhoneNo()) != null) {
			statusResList.add(new StatusRes("failed", "013", "Phone Number is Duplicated"));
		}
		String password;
		UserDetails userDetails;
		UserPassword userPassword;
		UserContacts userContacts;
		UserDetails adminUserDetails;
		String adminName;
		Manager manager = null;
		Project project = null;
		try {
			if (statusResList.isEmpty()) {
				password = randomAlphaNumeric();
				RefRoles refRoles = refRolesDAO.findById(employee.getRole_id()).get();
				if (refRoles.getValue().contains("Management")) {

					JobTitles jobtitles = jobTitlesDAO.findById(employee.getJob_title_id()).get();
					Company tempCompany = companyDAO.findById(employee.getCompanyId()).get();
					if (jobtitles != null && tempCompany != null) {
						userDetails = new UserDetails(employee.getFirstName(), employee.getLastName(),
								getMd5(employee.getEmailId()), true, refRoles, tempCompany, null, null, null,
								jobtitles, employee.getDateOfJoining());
						userPassword = new UserPassword(employee.getFirstName() + System.currentTimeMillis(),
								getMd5(password), userDetails, userDetailsDAO.findById(employee.getCreatedBy()).get());
						userContacts = new UserContacts(employee.getEmailId(), employee.getPhoneNo(), userDetails);
						userDetailsDAO.save(userDetails);
						userPasswordDAO.save(userPassword);
						userContactsDAO.save(userContacts);

						adminUserDetails = userDetailsDAO.findById(employee.getCreatedBy()).get();
						adminName = adminUserDetails.getFirstName() + " " + adminUserDetails.getLastName();
						addEmployeeEmail(email, adminName, password, employee, userDetails);
						statusResList.add(new StatusRes("Success", "000",
								"Employee added successfully and link sent to the mail."));

					} else {
						statusResList.add(new StatusRes("failed", "013", "Given detials are not found"));
					}

				} else {
					Company tempCompany = companyDAO.findById(employee.getCompanyId()).get();
					BusinessUnits businessUnits = businessUnitsDAO.findById(employee.getBusinessUnit_id()).get();
					Department department = departmentDAO.findById(employee.getDepartment_id()).get();
					JobTitles jobtitles = jobTitlesDAO.findById(employee.getJob_title_id()).get();
					UserDetails manager_details = userDetailsDAO.findById(employee.getManagerId()).get();
					if (tempCompany != null && businessUnits != null && department != null && jobtitles != null
							&& manager_details != null) {
						userDetails = new UserDetails(employee.getFirstName(), employee.getLastName(),
								getMd5(employee.getEmailId()), true, refRoles, tempCompany, businessUnits, department,
								manager_details, jobtitles, employee.getDateOfJoining());
						userPassword = new UserPassword(employee.getFirstName() + System.currentTimeMillis(),
								getMd5(password), userDetails, userDetailsDAO.findById(employee.getCreatedBy()).get());
						userContacts = new UserContacts(employee.getEmailId(), employee.getPhoneNo(), userDetails);
						userDetailsDAO.save(userDetails);
						userPasswordDAO.save(userPassword);
						userContactsDAO.save(userContacts);

						// sending confirm email
						adminUserDetails = userDetailsDAO.findById(employee.getCreatedBy()).get();
						adminName = adminUserDetails.getFirstName() + " " + adminUserDetails.getLastName();
						addEmployeeEmail(email, adminName, password, employee, manager_details);

						statusResList.add(
								new StatusRes("Success", "000", "Employee added succesfully and link sent to Email"));

					} else {
						statusResList.add(new StatusRes("failed", "013", "Given detials are not found"));
					}
				}
			}
		} catch (Exception e) {
			log.info("Exception in method addEmployee:" + e.getMessage());
			statusResList.add(new StatusRes("failed", "013", "Given detials are not found"));
		}

		return statusResList;
	}

	private void addEmployeeEmail(String email, String adminName, String password, EmployeeMultiple employee,
			UserDetails manager) {
		sendEmail(email, "Employee Email Invitation from SmarTRM", "	<body>\r\n"
				+ "<table id=\"Table_01\" style=\"border: 2px solid #062f4f; margin: 10px auto; border-radius: 8px;\" width=\"621\" cellspacing=\"0\" cellpadding=\"0\">"
				+ "<tbody>" + "<tr style=\"background: #062f4f; color: white; height: 39px;\">"
				+ "<td style=\"padding: 20px 10px; font-size: 30px; height: 39px;\">SmarTRM</td>" + "</tr>"
				+ "<tr style=\"height: 36px;\">" + "<td style=\"padding: 10px 7px; height: 20px;\">Dear "
				+ employee.getFirstName() + " " + employee.getLastName() + ",</p>\r\n"
				+ "  <p style=\"padding: 10px 0px;height: 15px;\"> Welcomes You SmarTRM.</p>"
				+ "  <p style=\"padding-left:50px;\">" + adminName
				+ " have created an Employee Account for You</p>\r\n"
//				+ "  <p style=\"padding-left:50px;\">You have been Assigned with Project : " + "</p>\r\n"
				+ "  <p style=\"padding-left:50px;\">You have been Assigned with Manager : " + manager.getFirstName()
				+ "</p>\r\n" + "  <p style=\"padding-left:50px;\">Your Login credentials</p>\r\n"
				+ "  <p style=\"padding-left:50px;\">Email : " + employee.getEmailId() + "</p>\r\n"
				+ "  <p style=\"padding-left:50px;\">Password : " + password + "</p>\r\n"
//				+ "  <p style=\"padding-left:50px;\">Please confirm your Email address to start using SmarTRM</p>\r\n"
//				+ "  <a href=" + Constants.hostnameAPI + Constants.confirmEmail + getMd5(employee.getEmailId())
//				+ " target=\"_blank\"style=\"background-color: #4CAF50;\r\n" + "  color: white;width:100px;\r\n"
//				+ "  padding: 15px 25px;\r\n"
//				+ "  text-align: center;display: inline-block;text-decoration: none;display: block;\r\n"
//				+ "  margin-left: auto;\r\n" + "  margin-right: auto;\">Confirm Email</a>\r\n"
//				+ "  <p style=\"padding-left:50px;\">If you didn't sign up for an account, Please ignore this mail</p>\r\n"
				+ "</td>" + "</tr>" + "<tr style=\"height: 33px;\">"
				+ "<td style=\"padding: 20px 40px; text-align: right; height: 33px;\">Thanks and Regards<br /> SmarTRM</td>"
				+ "</tr>" + "</tbody>" + "</table>" + "</body>");
	}

	@SuppressWarnings("unused")
	public List<StatusRes> updateEmployee(EmployeeMultiple employee) {
		List<StatusRes> statusResList = new ArrayList<StatusRes>();
		try {
			UserDetails userdetails = userDetailsDAO.findById(employee.getUserId()).get();
			RefRoles refRoles = refRolesDAO.findById(employee.getRole_id()).get();
			Company company = companyDAO.findById(employee.getCompanyId()).get();
			JobTitles jobtiles = jobTitlesDAO.findById(employee.getJob_title_id()).get();
			UserContacts usercontacts = userContactsDAO.findByUserDetails(userdetails);
			UserContacts user_Email = userContactsDAO.findByEmailId(employee.getEmailId());
			if (refRoles != null && refRoles.getRoleId() == 2 && userdetails != null && company != null
					&& jobtiles != null && usercontacts != null) {
				if (user_Email == null || user_Email.getUserDetails() == userdetails) {
					userDetailsDAO.save(new UserDetails(userdetails.getUserId(), employee.getFirstName(),
							employee.getLastName(), userdetails.getVerificationToken(), userdetails.isEnabled(),
							refRoles, company, null, null, null, jobtiles, employee.getDateOfJoining()));
					userContactsDAO.save(new UserContacts(usercontacts.getId(), employee.getEmailId(),
							employee.getPhoneNo(), userdetails));
					statusResList.add(new StatusRes("Success", "000", "updated successfully"));
					return statusResList;
				}
			}
			BusinessUnits businessunit = businessUnitsDAO.findById(employee.getBusinessUnit_id()).get();
			Department department = departmentDAO.findById(employee.getDepartment_id()).get();
			UserDetails manager = userDetailsDAO.findById(employee.getManagerId()).get();

			if (userdetails != null && refRoles != null && company != null && businessunit != null && department != null
					&& manager != null && jobtiles != null && usercontacts != null) {

				if (user_Email == null) {
					userDetailsDAO.save(
							new UserDetails(userdetails.getUserId(), employee.getFirstName(), employee.getLastName(),
									userdetails.getVerificationToken(), userdetails.isEnabled(), refRoles, company,
									businessunit, department, manager, jobtiles, employee.getDateOfJoining()));
					UserDetails temp_userdetails = userDetailsDAO.findById(employee.getUserId()).get();
					userContactsDAO.save(new UserContacts(usercontacts.getId(), employee.getEmailId(),
							employee.getPhoneNo(), temp_userdetails));
					statusResList.add(new StatusRes("Success", "000", "updated successfully"));

				} else if (user_Email.getId() != usercontacts.getId()) {
					statusResList.add(new StatusRes("Filed", "007", "Details already existed"));
				} else {
					userDetailsDAO.save(
							new UserDetails(userdetails.getUserId(), employee.getFirstName(), employee.getLastName(),
									userdetails.getVerificationToken(), userdetails.isEnabled(), refRoles, company,
									businessunit, department, manager, jobtiles, employee.getDateOfJoining()));
					UserDetails temp_userdetails = userDetailsDAO.findById(employee.getUserId()).get();
					userContactsDAO.save(new UserContacts(usercontacts.getId(), employee.getEmailId(),
							employee.getPhoneNo(), temp_userdetails));

					statusResList.add(new StatusRes("Success", "000", "updated successfully"));
				}
			} else {
				statusResList.add(new StatusRes("Filed", "007", "details not found"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return statusResList;
	}

	public void updateEmployeeEmail(String emailId, Long id, Manager manager, Project project,
			EmployeeMultiple employee, String adminName) {
		userContactsDAO.updateEmail(emailId, id);
		userDetailsDAO.updateToken(employee.getUserId(), getMd5(emailId));
		String password = randomAlphaNumeric();
		userPasswordDAO.updatePassword(employee.getUserId(), getMd5(password));
		sendEmail(employee.getEmailId(), "Email Invitation from SmarTRM",

				"<body>\r\n"
						+ "<table id=\"Table_01\" style=\"border: 2px solid #062f4f; margin: 10px auto; border-radius: 8px;\" width=\"621\" cellspacing=\"0\" cellpadding=\"0\">"
						+ "<tbody>" + "<tr style=\"background: #062f4f; color: white; height: 39px;\">"
						+ "<td style=\"padding: 20px 10px; font-size: 30px; height: 39px;\">SmarTRM</td>" + "</tr>"
						+ "<tr style=\"height: 36px;\">" + "  <td style=\"padding: 10px 7px; height: 20px;\">Dear "
						+ employee.getFirstName()
						+ "  <p style=\"padding: 10px 0px;height: 15px;\"> Welcomes You SmarTRM.</p>"
						+ "  <p style=\"padding-left:50px;\">Mr. " + adminName
						+ " have created an Employee Account for You</p>\r\n"
						+ "  <p style=\"padding-left:50px;\">You have been Assigned with Project : "
						+ project.getProjectName() + "</p>\r\n"
						+ "  <p style=\"padding-left:50px;\">your EmailId has Updated </p>\r\n"
						+ "  <p style=\"padding-left:50px;\">Email : " + employee.getEmailId() + "</p>\r\n"
						+ " <p style=\"padding-left:50px;\">Password : " + password
						+ "  <p style=\"padding-left:50px;\">Please confirm your Email address to start using SmarTRM</p>\r\n"
						+ "  <a href=" + Constants.hostnameAPI + Constants.confirmEmail + getMd5(employee.getEmailId())
						+ " target=\"_blank\"style=\"background-color: #4CAF50;\r\n" + "  color: white;width:100px;\r\n"
						+ "  padding: 15px 25px;\r\n"
						+ "  text-align: center;display: inline-block;text-decoration: none;display: block;\r\n"
						+ "  margin-left: auto;\r\n" + "  margin-right: auto;\">Confirm Email</a>\r\n"
						+ "  <p style=\"padding-left:50px;\">If you didn't sign up for an account, Please ignore this mail</p>\r\n"
						+ "</td>" + "</tr>" + "<tr style=\"height: 33px;\">"
						+ "<td style=\"padding: 20px 40px; text-align: right; height: 33px;\">Thanks and Regards<br /> SmarTRM</td>"
						+ "</tr>" + "</tbody>" + "</table>" + "</body>");
	}

	public List<Employee> viewEmployees(Long userId) {
		List<Employee> employeeList = new ArrayList<>();
		List<UserPassword> userPasswordList = userPasswordDAO
				.findByCreatedByUserDetails(userDetailsDAO.findById(userId).get());
		for (UserPassword userPassword : userPasswordList) {
			UserDetails userDetails = userPassword.getUserDetails();
			UserContacts userContacts = userContactsDAO.findByUserDetails(userDetails);
			Employee e = new Employee(userDetails.getUserId(), userDetails.getFirstName(), userDetails.getLastName(),
					userContacts.getEmailId(), userContacts.getPhoneNo(), userDetails.getTempCompany().getCompanyId(),
					userDetails.getTempCompany().getCompanyName(), userDetails.getTempUserDetails().getFirstName(),
					userDetails.getDateOfJoining());
			employeeList.add(e);
		}
		return employeeList;
	}

	// list of Enabled Employee
	public List<Employee> viewEmployees(String name) {
		List<Employee> employeeList = new ArrayList<>();
		List<UserDetails> tempuserlist = userDetailsDAO.findByEnabled(true);
		try {
			if (name != null) {
				List<UserDetails> filterlist = userDetailsDAO.findByFirstNameAndLastName(name);
				for (UserDetails userDetails : filterlist) {
					if (userDetails.getUserId() == 1
							|| userDetails.getTempRefRoles().getValue().equalsIgnoreCase("Management")) {

						if (userDetails.getTempRefRoles().getValue().equalsIgnoreCase("Management")) {
							UserContacts userContacts = userContactsDAO.findByUserDetails(userDetails);
							Employee e = new Employee(userDetails.getUserId(), userDetails.getFirstName(),
									userDetails.getLastName(), userContacts.getEmailId(), userContacts.getPhoneNo(),
									userDetails.getTempCompany().getCompanyId(),
									userDetails.getTempCompany().getCompanyName(), null,
									userDetails.getDateOfJoining());
							employeeList.add(e);
						}
					} else {
						UserContacts userContacts = userContactsDAO.findByUserDetails(userDetails);
						Employee e = new Employee(userDetails.getUserId(), userDetails.getFirstName(),
								userDetails.getLastName(), userContacts.getEmailId(), userContacts.getPhoneNo(),
								userDetails.getTempCompany().getCompanyId(),
								userDetails.getTempCompany().getCompanyName(),
								userDetails.getTempUserDetails().getFirstName(), userDetails.getDateOfJoining());
						employeeList.add(e);
					}
				}
				return employeeList;
			} else {
				for (UserDetails userDetails : tempuserlist) {
					if (userDetails.getUserId() == 1
							|| userDetails.getTempRefRoles().getValue().equalsIgnoreCase("Management")) {

						if (userDetails.getTempRefRoles().getValue().equalsIgnoreCase("Management")) {
							UserContacts userContacts = userContactsDAO.findByUserDetails(userDetails);
							Employee e = new Employee(userDetails.getUserId(), userDetails.getFirstName(),
									userDetails.getLastName(), userContacts.getEmailId(), userContacts.getPhoneNo(),
									userDetails.getTempCompany().getCompanyId(),
									userDetails.getTempCompany().getCompanyName(), null,
									userDetails.getDateOfJoining());
							employeeList.add(e);
						}

					} else {
						UserContacts userContacts = userContactsDAO.findByUserDetails(userDetails);
						Employee e = new Employee(userDetails.getUserId(), userDetails.getFirstName(),
								userDetails.getLastName(), userContacts.getEmailId(), userContacts.getPhoneNo(),
								userDetails.getTempCompany().getCompanyId(),
								userDetails.getTempCompany().getCompanyName(),
								userDetails.getTempUserDetails().getFirstName(), userDetails.getDateOfJoining());
						employeeList.add(e);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return employeeList;
	}

	private String getMd5(String input) {
		try {
			// Static getInstance method is called with hashing MD5
			MessageDigest md = MessageDigest.getInstance("MD5");
			// digest() method is called to calculate message digest
			// of an input digest() return array of byte
			byte[] messageDigest = md.digest(input.getBytes());
			// Convert byte array into signum representation
			BigInteger no = new BigInteger(1, messageDigest);
			// Convert message digest into hex value
			String hashtext = no.toString(16);
			while (hashtext.length() < 32) {
				hashtext = "0" + hashtext;
			}
			return hashtext;
		}
		// For specifying wrong message digest algorithms
		catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	public void sendEmail(String mailTo, String subject, String message) {
		// Session session = Session.getDefaultInstance(props);
		MimeMessage mimeMessage = javaMailSender.createMimeMessage();
		MimeMessageHelper helper;
		try {
			helper = new MimeMessageHelper(mimeMessage, true);
			helper.setFrom(new InternetAddress("timeclock@avkore.com"));
			helper.setTo(mailTo);
			helper.setSubject(subject);
			helper.setText("ok", message);
		} catch (MessagingException e1) {
			e1.printStackTrace();
		}
		javaMailSender.send(mimeMessage);
	}

	public String confirmEmail(String verificationToken) {
		UserDetails userDetails = userDetailsDAO.findByVerificationToken(verificationToken);
		if (userDetails != null && userDetails.isEnabled() == false) {
			userDetails.setEnabled(true);
			userDetailsDAO.save(userDetails);
			leaveIncrementServiceImpl.saveLeaveDetails(userDetails);
			return "success";
		} else
			return "Request already processed or not found";
	}

	private String randomAlphaNumeric() {
		int count = 10;
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}

	private String[] storeFileAndGetFilePathArray(String FilePath, MultipartFile[] attachedList) {
		String fileNameArr[] = new String[attachedList.length];
		File file = null;
		try {
			for (int i = 0; i < attachedList.length; i++) {
				MultipartFile multipartFile = attachedList[i];
				if (multipartFile != null) {
					fileNameArr[i] = FilePath + File.separator + multipartFile.getOriginalFilename();
					file = new File(fileNameArr[i]);
					multipartFile.transferTo(file);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileNameArr;
	}

	private String storeFileAndGetFilePath(String filePath, MultipartFile pan) {
		String fileName = null;
		File file = null;
		try {
			fileName = filePath + File.separator + pan.getOriginalFilename();
			file = new File(fileName);
			pan.transferTo(file);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileName;
	}

	// LEAVE MODULE
	public List<UserDetails> listofEnabledEmployee(boolean enabled) {
		return (List<UserDetails>) userDetailsDAO.findByEnabled(enabled);
	}

	// LEAVE MODULE
	public StatusRes updateleaveDetailsByUserId(LeaveDetailsPojo leaveData) {
		// UserDetails userDetails =
		// userDetailsDAO.findById(leaveData.getUserId()).get();
		LeaveDetails leaveDetails = leaveDetailsDAO.findById(leaveData.getLeavedetailId()).get();
		Double annualLeaves = 0.5;
		if (leaveDetails != null) {
			leaveDetailsDAO.save(new LeaveDetails(leaveData.getLeavedetailId(), leaveData.getYear(),
					leaveDetails.getSickLeaves(), leaveDetails.getRemainingSickleaves(),
					leaveData.getUtilizedsickLeaves(), leaveData.getActualAnnualLeaves() + annualLeaves,
					leaveData.getRemainingAnnualLeaves() + annualLeaves, leaveData.getUtilisedAnnualLeaves(),
					leaveData.getLossOfPay()));

			return new StatusRes("success", "000",
					"LeaveDetails with Leave Details ID : " + leaveData.getLeavedetailId() + ", Annual Leaves : "
							+ leaveDetails.getSickLeaves() + " updated successfully",
					null);
		} else
			return new StatusRes("failed", "007", "LeaveDetails with Leave Details ID : " + leaveData.getLeavedetailId()
					+ ", Annual Leaves  Not Found", null);
	}

	public List<StatusRes> addJobTitle(List<JobTitles> jobTitles) {
		List<StatusRes> statusResList = new ArrayList<>();

		for (JobTitles titles : jobTitles) {
			JobTitles addtitles = jobTitlesDAO.findByJobtitle(titles.getJobtitle());
			JobTitles jobcode = jobTitlesDAO.findByJobtitlecode(titles.getJobtitlecode());

			if (jobcode != null && addtitles != null || jobcode != null || addtitles != null)
				statusResList.add(new StatusRes("failed", "007", "Job title : " + titles.getJobtitle()
						+ ", Job Title code : " + titles.getJobtitlecode() + " Already Existed", null));
			else {
				jobTitlesDAO.save(titles);
				statusResList.add(new StatusRes("success", "000", "Job Title added sucessfully", null));
			}
		}
		return statusResList;
	}

	@SuppressWarnings("unused")
	public List<StatusRes> updateJobTitles(JobTitles jobTitles) {
		List<StatusRes> statusResList = new ArrayList<>();
		try {
			int count = 0;
			JobTitles title = jobTitlesDAO.findById(jobTitles.getJobTitlesId())
					.orElseThrow(() -> new RuntimeException("Job Title not found"));

			JobTitles TitleCode = jobTitlesDAO.findByJobtitlecode(jobTitles.getJobtitlecode());
			JobTitles titleName = jobTitlesDAO.findByJobtitle(jobTitles.getJobtitle());

			if (TitleCode == null && titleName == null) {
				count++;
			} else if (TitleCode == null || titleName == null) {
				if (titleName == null) {
					if (TitleCode.getJobTitlesId() == jobTitles.getJobTitlesId()) {
						count++;
					} else {
						statusResList.add(new StatusRes("Failed", "007", " Title Code Already Existed"));
					}
				} else if (TitleCode == null) {
					if (titleName.getJobTitlesId() == jobTitles.getJobTitlesId()) {
						count++;
					} else {
						statusResList.add(new StatusRes("Failed", "007", " Title Name Already Existed"));
					}
				}
			} else if (TitleCode != null && titleName != null) {
				if (TitleCode.getJobTitlesId() == jobTitles.getJobTitlesId()
						&& titleName.getJobTitlesId() == jobTitles.getJobTitlesId()) {
					count++;
				} else {
					statusResList.add(new StatusRes("Failed", "007", " Details Already Existed"));
				}
			}
			if (count != 0) {
				jobTitlesDAO.updateTitles(jobTitles.getJobTitlesId(), jobTitles.getJobtitlecode(),
						jobTitles.getJobtitle(), jobTitles.getJobdescription());
				statusResList.add(new StatusRes("Success", "000", "Job Title Updated  sucessfully"));
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}
		return statusResList;
	}

	public List<JobTitles> viewJobTitles() {
		return (List<JobTitles>) jobTitlesDAO.findAll();
	}

	public List<Employee> ReportingManagerList(long PresentUserID) {
		List<Employee> employeeList = new ArrayList<>();
		List<UserDetails> tempuserlist = userDetailsDAO.findByEnabled(true);
		try {
			for (UserDetails userDetails : tempuserlist) {
				if (userDetails.getUserId() == 1 || userDetails.getUserId() == PresentUserID
						|| userDetails.getTempUserDetails().getUserId() == PresentUserID) {

				} else {
					UserContacts userContacts = userContactsDAO.findByUserDetails(userDetails);
					Employee e = new Employee(userDetails.getUserId(), userDetails.getFirstName(),
							userDetails.getLastName(), userContacts.getEmailId(), userContacts.getPhoneNo(),
							userDetails.getTempCompany().getCompanyId(), userDetails.getTempCompany().getCompanyName(),
							userDetails.getTempUserDetails().getFirstName(), userDetails.getDateOfJoining());
					employeeList.add(e);
				}
			}
		} catch (Exception e) {
		}
		return employeeList;
	}

	public ResponseEntity<Object> addDashboardContent(DashboardPojo dashboardContent) {
		UserDetails details = userDetailsDAO.findById(dashboardContent.getUser_id())
				.orElseThrow(() -> new RuntimeException("User not found"));
		List<DashboardContent> availableContent = dashboardContentDAO.findByuserdetails(details);
		availableContent.forEach(delByOneByOne -> {
			dashboardContentDAO.deleteById(delByOneByOne.getDashboard_id());
		});
		for (Long dashpojo : dashboardContent.getModule_id()) {
			dashboardContentDAO.save(new DashboardContent(dashpojo, details));
		}
		return new ResponseEntity<>(new StatusRes("Success", "000", "Successfully updated"), HttpStatus.OK);
	}

	public Object removeDashboardContent(RemoveContentPojo contentId) {
		for (Long Cid : contentId.getContentid()) {
			DashboardContent content = dashboardContentDAO.findById(Cid)
					.orElseThrow(() -> new RuntimeException("Content not found"));
			dashboardContentDAO.deleteById(Cid);
		}
		return "Succesfully removed from dashboard";
	}

	// ADD Profile
	public Object userPersonalInfos(UserPersonalInfo userPersonalInfo, MultipartFile imagefile) throws IOException {
		boolean status = false;
		try {
			userPersonalInfo.setData(imagefile.getBytes());
			userPersonalInfo.setFileName(imagefile.getName());
			userPersonalInfo.setFileType(imagefile.getContentType());

			if (userPersonalInfo != null) {
				userPersonalInfoDao.save(userPersonalInfo);
				status = true;
			} else {
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (status)
			return new StatusRes("success", "000", "Stored Successfully");
		else
			return new StatusRes("failed", "007", "Stored Failed");
	}

	public Object userPersonalInfosView(Long userId) {
		return userPersonalInfoDao.findByUserIds(userId);
	}

	public StatusRes assetUserReturn(AssetsEmployeePojo assetsemp) {
		Optional<UserDetails> userDetails = userDetailsDAO.findById(assetsemp.getUserId());
		Optional<Assets> asset = assetsDao.findById(assetsemp.getAssetId());
		Assets_Employees empassets = assets_employeeDAO.findById(assetsemp.getAssets_employees_id()).get();

		if (asset != null) {

			assets_employeeDAO.save(new Assets_Employees(empassets.getAssets_employees_id(),
					empassets.getAssignToUserDate(), assetsemp.getAssetsUserReturnDate(), assetsemp.getComment(),
					asset.get(), userDetails.get()));

			asset.get().setEnable(false);
			assetsDao.save(asset.get());

			return new StatusRes("success", "000", "User return Successfully", null);
		} else
			return new StatusRes("failed", "007", " Not Found", null);
	}

	public List<Employee> viewProjectsByEmployees(Long userId) {

		List<Employee> employeeList = new ArrayList<>();
		UserDetails userDetail = userDetailsDAO.findById(userId).get();
		List<Project_Employees> projectAssignment = project_employeeDAO.findAllByUserDetails(userDetail);

		for (Project_Employees projassign : projectAssignment) {
			UserDetails userDetails = projassign.getUserDetails();
			UserContacts userContacts = userContactsDAO.findByUserDetails(userDetails);
			Employee e = new Employee(userDetails.getUserId(), userDetails.getFirstName(), userDetails.getLastName(),
					userContacts.getEmailId(), userContacts.getPhoneNo(), projassign.getProject().getProjectId(),
					projassign.getProject().getProjectName(), projassign.getProject().getProjectVendor(),
					projassign.getProjectAssignedDate(), 0, projassign.isEnable());
			employeeList.add(e);
		}
		return employeeList;

	}

	public List<UserPersonalInfo> userPersonalInfosList() {
		return (List<UserPersonalInfo>) userPersonalInfoDao.findAll();
	}

	public Object updateUserPersonalInfos(UserPersonalInfo userPersonalInfo, MultipartFile imagefile)
			throws IOException {

		boolean status = false;

		try {
			Optional<UserPersonalInfo> userPersonalInfo1 = userPersonalInfoDao
					.findById(userPersonalInfo.getUserPersonalInfoId());
			if (userPersonalInfo1.isPresent()) {
				userPersonalInfo1.get().setEmail(userPersonalInfo.getEmail());
				userPersonalInfo1.get().setMobileNumber(userPersonalInfo.getMobileNumber());
				userPersonalInfo1.get().setEmergencyNumber(userPersonalInfo.getEmergencyNumber());
				userPersonalInfo1.get().setDob(userPersonalInfo.getDob());
				userPersonalInfo1.get().setPancard(userPersonalInfo.getPancard());
				userPersonalInfo1.get().setAdhaarcard(userPersonalInfo.getAdhaarcard());
				userPersonalInfo1.get().setPassport(userPersonalInfo.getPassport());
				userPersonalInfo1.get().setFatherName(userPersonalInfo.getFatherName());
				userPersonalInfo1.get().setMotherName(userPersonalInfo.getMotherName());
				userPersonalInfo1.get().setParentContact(userPersonalInfo.getParentContact());
				userPersonalInfo1.get().setMartialStatus(userPersonalInfo.getMartialStatus());
				userPersonalInfo1.get().setGender(userPersonalInfo.getGender());
				userPersonalInfo1.get().setPermanentAddress(userPersonalInfo.getPermanentAddress());
				userPersonalInfo1.get().setPresentAddress(userPersonalInfo.getPresentAddress());
				if (imagefile != null) {
					userPersonalInfo1.get().setData(imagefile.getBytes());
					userPersonalInfo1.get().setFileName(imagefile.getName());
					userPersonalInfo1.get().setFileType(imagefile.getContentType());
					userPersonalInfo1.get().setImage(imagefile.getOriginalFilename());
				}
				status = true;
				userPersonalInfoDao.save(userPersonalInfo1.get());
				return new StatusRes("success", "000", "Updated Successfully");
			} else
				return new StatusRes("failed", "007", "Stored Failed");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public List<Assets_Employees> viewassignedlist() {
		return (List<Assets_Employees>) assets_employeeDAO.findAll();
	}

	public void loginLogs(UserDetails userDetails, UserContacts userContacts, String ip) {

		try {
			loginLogsDAO.save(new Logs(userDetails.getFirstName() + " " + userDetails.getLastName(),
					userContacts.getEmailId(), userDetails.getTempRefRoles().getValue(), ip));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<Logs> loginLogsUser() {
		return (List<Logs>) loginLogsDAO.findAll();
	}

	public String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}
	
	// RM And Management list for employees
	public List<UserDetails> listofEmployeeRM() {
		return (List<UserDetails>) userDetailsDAO.findByroleId();
	}
}