package com.pronix.spring.proclock.taskmanagement.DAO;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.pronix.spring.proclock.taskmanagement.models.TaskDetails;

public interface TaskDetailsDAO extends JpaRepository<TaskDetails, Long> {
	
	
	Optional<TaskDetails> findByTaskName(String taskName);
		
	@Query(value = "Select * from task_details where assigned_to =?1", nativeQuery = true)
	List<TaskDetails> findByAssignedTo(Long assignedTo);
	
//	@Query(value = "SELECT td.*,tdt.*\r\n" + 
//			"FROM task_details td \r\n" + 
//			"right JOIN taskdetail_tracker tdt\r\n" + 
//			"ON tdt.assigned_to = td.assigned_to where td.assigned_to=?1", nativeQuery = true)
//	List<TaskDetails> findByAssignedTo(Long assignedTo);
	
	//Optional<TaskDetails> findByUserDetails(Long userId);
	
	@Query(value = "Select * from task_details where taskstatus_id=1 and created_by =?1", nativeQuery = true)
	Optional<TaskDetails> findByUserDetails(long userDetails);
}
