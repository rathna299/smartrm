package com.pronix.spring.proclock.taskmanagement.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pronix.spring.proclock.taskmanagement.models.TaskDetailsFile;

public interface TaskDetailsFileDAO extends JpaRepository<TaskDetailsFile, Long> {

	@Query(value = "select * from task_details_files where taskdetails_id = ?1 and taskdetailtracker_id is null", nativeQuery = true)
	List<TaskDetailsFile> findByTaskDetails(Long taskDetails);

	@Query(value = "select * from task_details_files where taskdetailtracker_id = ?1 and taskdetailtracker_id is not null", nativeQuery = true)
	List<TaskDetailsFile> findByTaskDetailsTracker(Long taskDetailsTracker);

}
