package com.pronix.spring.proclock.taskmanagement.DAO;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pronix.spring.proclock.taskmanagement.models.TaskDetailsTracker;

public interface TaskDetailsTrackerDAO extends JpaRepository<TaskDetailsTracker, Long> {
	
	
	@Query(value = "Select * from taskdetail_tracker where taskdetails_id=?1", nativeQuery = true)
	List<TaskDetailsTracker> findBytaskDetails(Long taskDetails);
	
	
	@Query(value = "Select * from taskdetail_tracker where taskdetails_id=?1", nativeQuery = true)
	Optional<TaskDetailsTracker> findBytaskDetail1s(Long taskDetails);
	
}
