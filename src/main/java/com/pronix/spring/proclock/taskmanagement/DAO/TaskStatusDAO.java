package com.pronix.spring.proclock.taskmanagement.DAO;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pronix.spring.proclock.taskmanagement.models.TaskStatus;

public interface TaskStatusDAO extends JpaRepository<TaskStatus, Long> {
	
	Optional<TaskStatus> findByTaskstatusType(String taskstatusType);
	

}
