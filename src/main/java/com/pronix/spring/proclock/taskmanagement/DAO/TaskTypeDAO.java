package com.pronix.spring.proclock.taskmanagement.DAO;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pronix.spring.proclock.taskmanagement.models.TaskType;

public interface TaskTypeDAO extends JpaRepository<TaskType, Long> {
	
	Optional<TaskType> findByTasktypeName(String tasktypeName);
	

}
