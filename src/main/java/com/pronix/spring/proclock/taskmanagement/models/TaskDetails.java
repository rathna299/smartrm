package com.pronix.spring.proclock.taskmanagement.models;

import java.time.LocalDateTime;
import java.util.Arrays;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pronix.spring.proclock.models.Modules;
import com.pronix.spring.proclock.models.Project;
import com.pronix.spring.proclock.models.UserDetails;

@Entity
@Table(name = "task_details")
public class TaskDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long taskdetailsId;
	
	private String taskName;
	
	private String taskDesc;

	private double estimatedHrs;
	
	private String priority;					
	private String taskReady;
	private String dueDate;
	
	private LocalDateTime insertDate;
	private LocalDateTime updatedDate;
	private LocalDateTime resolvedDate;
	
	private String peoplesInvolved;
	
	
	@ManyToOne
	@JoinColumn(name = "resolved_by",referencedColumnName = "user_id")
	private UserDetails userResolveDetails;
	
	@ManyToOne
	@JoinColumn(name = "modules_id",referencedColumnName = "modules_id")
	private Modules modules;
	
	@ManyToOne
	@JoinColumn(name = "tasktype_id",referencedColumnName = "tasktype_id")
	private TaskType taskType;
	
	@ManyToOne
	@JoinColumn(name = "project_id",referencedColumnName = "project_id")
	private Project project;
	
	@ManyToOne
	@JoinColumn(name = "taskstatus_id", referencedColumnName = "taskstatus_id")
	private TaskStatus taskStatus;
	
	@ManyToOne
	@JoinColumn(name = "assigned_to",referencedColumnName = "user_id")
	private UserDetails assignedTo;
	
	@ManyToOne
	@JoinColumn(name = "created_by",referencedColumnName = "user_id")
	private UserDetails userDetails;
	
	
	
	public TaskDetails() {
		super();
	}



	public Long getTaskdetailsId() {
		return taskdetailsId;
	}



	public void setTaskdetailsId(Long taskdetailsId) {
		this.taskdetailsId = taskdetailsId;
	}



	public String getTaskName() {
		return taskName;
	}



	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}



	public String getTaskDesc() {
		return taskDesc;
	}



	public void setTaskDesc(String taskDesc) {
		this.taskDesc = taskDesc;
	}



	public double getEstimatedHrs() {
		return estimatedHrs;
	}



	public void setEstimatedHrs(double estimatedHrs) {
		this.estimatedHrs = estimatedHrs;
	}


	public String getPriority() {
		return priority;
	}



	public void setPriority(String priority) {
		this.priority = priority;
	}



	public String getTaskReady() {
		return taskReady;
	}



	public void setTaskReady(String taskReady) {
		this.taskReady = taskReady;
	}



	public String getDueDate() {
		return dueDate;
	}



	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}



	public LocalDateTime getInsertDate() {
		return insertDate;
	}



	public void setInsertDate(LocalDateTime insertDate) {
		this.insertDate = insertDate;
	}



	public LocalDateTime getUpdatedDate() {
		return updatedDate;
	}



	public void setUpdatedDate(LocalDateTime updatedDate) {
		this.updatedDate = updatedDate;
	}



	public LocalDateTime getResolvedDate() {
		return resolvedDate;
	}



	public String getPeoplesInvolved() {
		return peoplesInvolved;
	}



	public void setPeoplesInvolved(String peoplesInvolved) {
		this.peoplesInvolved = peoplesInvolved;
	}



	public void setResolvedDate(LocalDateTime resolvedDate) {
		this.resolvedDate = resolvedDate;
	}



	public UserDetails getUserResolveDetails() {
		return userResolveDetails;
	}



	public void setUserResolveDetails(UserDetails userResolveDetails) {
		this.userResolveDetails = userResolveDetails;
	}



	public Modules getModules() {
		return modules;
	}



	public void setModules(Modules modules) {
		this.modules = modules;
	}



	public TaskType getTaskType() {
		return taskType;
	}



	public void setTaskType(TaskType taskType) {
		this.taskType = taskType;
	}



	public Project getProject() {
		return project;
	}

	


	public TaskStatus getTaskStatus() {
		return taskStatus;
	}



	public void setTaskStatus(TaskStatus taskStatus) {
		this.taskStatus = taskStatus;
	}



	public void setProject(Project project) {
		this.project = project;
	}



	public UserDetails getAssignedTo() {
		return assignedTo;
	}



	public void setAssignedTo(UserDetails assignedTo) {
		this.assignedTo = assignedTo;
	}



	public UserDetails getUserDetails() {
		return userDetails;
	}



	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}

	
}
