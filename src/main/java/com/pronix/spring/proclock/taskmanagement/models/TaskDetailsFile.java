package com.pronix.spring.proclock.taskmanagement.models;

import java.time.LocalDateTime;
import java.util.Arrays;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pronix.spring.proclock.models.Modules;
import com.pronix.spring.proclock.models.Project;
import com.pronix.spring.proclock.models.UserDetails;

@Entity
@Table(name = "task_details_files")
public class TaskDetailsFile {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long taskdetailsfileId;
	
	private String fileName;

	private String fileType;
	
	@JsonIgnore
	@Lob
	private byte[] data;
	
	private LocalDateTime insertDate;
	private LocalDateTime updatedDate;
	
	@ManyToOne
	@JoinColumn(name = "taskdetailsId",referencedColumnName = "taskdetailsId")
	private TaskDetails taskDetails;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "taskdetailtrackerId",referencedColumnName = "taskdetailtrackerId")
	private TaskDetailsTracker taskDetailsTracker;
	
	@ManyToOne
	@JoinColumn(name = "created_by",referencedColumnName = "user_id")
	private UserDetails userDetails;
	
	
	
	public TaskDetailsFile() {
		super();
	}



	public Long getTaskdetailsfileId() {
		return taskdetailsfileId;
	}



	public void setTaskdetailsfileId(Long taskdetailsfileId) {
		this.taskdetailsfileId = taskdetailsfileId;
	}



	public String getFileName() {
		return fileName;
	}



	public void setFileName(String fileName) {
		this.fileName = fileName;
	}



	public String getFileType() {
		return fileType;
	}



	public void setFileType(String fileType) {
		this.fileType = fileType;
	}



	public byte[] getData() {
		return data;
	}



	public void setData(byte[] data) {
		this.data = data;
	}



	public LocalDateTime getInsertDate() {
		return insertDate;
	}



	public void setInsertDate(LocalDateTime insertDate) {
		this.insertDate = insertDate;
	}



	public LocalDateTime getUpdatedDate() {
		return updatedDate;
	}



	public void setUpdatedDate(LocalDateTime updatedDate) {
		this.updatedDate = updatedDate;
	}



	public TaskDetails getTaskDetails() {
		return taskDetails;
	}



	public void setTaskDetails(TaskDetails taskDetails) {
		this.taskDetails = taskDetails;
	}

	


	public TaskDetailsTracker getTaskDetailsTracker() {
		return taskDetailsTracker;
	}



	public void setTaskDetailsTracker(TaskDetailsTracker taskDetailsTracker) {
		this.taskDetailsTracker = taskDetailsTracker;
	}



	public UserDetails getUserDetails() {
		return userDetails;
	}



	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}


	
}
