package com.pronix.spring.proclock.taskmanagement.models;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.pronix.spring.proclock.models.UserDetails;

@Entity
@Table(name = "taskdetail_tracker")
public class TaskDetailsTracker {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long taskdetailtrackerId;

	private String taskComment;

	private double spentHrs;

	private LocalDateTime insertDate;

	private LocalDateTime updatedDate;
	
	private LocalDateTime resolvedDate;

	@ManyToOne
	@JoinColumn(name = "taskstatus_id", referencedColumnName = "taskstatus_id")
	private TaskStatus taskStatus;

	@ManyToOne
	@JoinColumn(name = "taskdetailsId", referencedColumnName = "taskdetailsId")
	private TaskDetails taskDetails;

	@ManyToOne
	@JoinColumn(name = "assigned_to", referencedColumnName = "user_id")
	private UserDetails assignedTo;

	@ManyToOne
	@JoinColumn(name = "resolved_by", referencedColumnName = "user_id")
	private UserDetails resolvedBy;
	
	@ManyToOne
	@JoinColumn(name = "created_by",referencedColumnName = "user_id")
	private UserDetails userDetails;

	public TaskDetailsTracker() {
		super();
	}

	public Long getTaskdetailtrackerId() {
		return taskdetailtrackerId;
	}

	public void setTaskdetailtrackerId(Long taskdetailtrackerId) {
		this.taskdetailtrackerId = taskdetailtrackerId;
	}

	public String getTaskComment() {
		return taskComment;
	}

	public void setTaskComment(String taskComment) {
		this.taskComment = taskComment;
	}

	public double getSpentHrs() {
		return spentHrs;
	}

	public void setSpentHrs(double spentHrs) {
		this.spentHrs = spentHrs;
	}

	public LocalDateTime getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(LocalDateTime insertDate) {
		this.insertDate = insertDate;
	}

	public LocalDateTime getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(LocalDateTime updatedDate) {
		this.updatedDate = updatedDate;
	}
	

	public LocalDateTime getResolvedDate() {
		return resolvedDate;
	}


	public void setResolvedDate(LocalDateTime resolvedDate) {
		this.resolvedDate = resolvedDate;
	}

	public TaskStatus getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(TaskStatus taskStatus) {
		this.taskStatus = taskStatus;
	}

	public TaskDetails getTaskDetails() {
		return taskDetails;
	}

	public void setTaskDetails(TaskDetails taskDetails) {
		this.taskDetails = taskDetails;
	}

	public UserDetails getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(UserDetails assignedTo) {
		this.assignedTo = assignedTo;
	}

	public UserDetails getResolvedBy() {
		return resolvedBy;
	}

	public void setResolvedBy(UserDetails resolvedBy) {
		this.resolvedBy = resolvedBy;
	}


	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}
	
	

}
