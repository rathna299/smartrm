package com.pronix.spring.proclock.taskmanagement.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.pronix.spring.proclock.models.Project;
import com.pronix.spring.proclock.models.UserDetails;

@Entity
@Table(name = "taskstatus")
public class TaskStatus {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long taskstatus_id;

	private String taskstatusType;

	private String description;

	public TaskStatus() {
		super();
	}
	
	
	public TaskStatus(String taskstatusType, String description) {
		super();
		this.taskstatusType = taskstatusType;
		this.description = description;
	}

	

	public Long getTaskstatus_id() {
		return taskstatus_id;
	}


	public void setTaskstatus_id(Long taskstatus_id) {
		this.taskstatus_id = taskstatus_id;
	}


	public String getTaskstatusType() {
		return taskstatusType;
	}

	public void setTaskstatusType(String taskstatusType) {
		this.taskstatusType = taskstatusType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
	

}
