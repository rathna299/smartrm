package com.pronix.spring.proclock.taskmanagement.models;

import java.time.LocalDateTime;

public class TaskTrackerPojo {
	private Long teskdetailtrackerId;

	private double spentHrs;

	private String fileName;

	private String fileType;

	private byte[] data;

	private String priority;

	private LocalDateTime updatedDate;

	//private String dueDate;

	private Long taskdetailsId;

	public Long getTeskdetailtrackerId() {
		return teskdetailtrackerId;
	}

	public double getSpentHrs() {
		return spentHrs;
	}

	public String getFileName() {
		return fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public byte[] getData() {
		return data;
	}

	public String getPriority() {
		return priority;
	}

	public LocalDateTime getUpdatedDate() {
		return updatedDate;
	}

//	public String getDueDate() {
//		return dueDate;
//	}
//	
//	public void setDueDate(String dueDate) {
//		this.dueDate = dueDate;
//	}

	public Long getTaskdetailsId() {
		return taskdetailsId;
	}

	public void setTeskdetailtrackerId(Long teskdetailtrackerId) {
		this.teskdetailtrackerId = teskdetailtrackerId;
	}

	public void setSpentHrs(double spentHrs) {
		this.spentHrs = spentHrs;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public void setUpdatedDate(LocalDateTime updatedDate) {
		this.updatedDate = updatedDate;
	}

	

	public void setTaskdetailsId(Long taskdetailsId) {
		this.taskdetailsId = taskdetailsId;
	}

}
