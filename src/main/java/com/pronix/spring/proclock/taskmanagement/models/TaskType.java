package com.pronix.spring.proclock.taskmanagement.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.pronix.spring.proclock.models.Project;
import com.pronix.spring.proclock.models.UserDetails;

@Entity
@Table(name = "tasktype")
public class TaskType {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long tasktype_id;

	private String tasktypeName;

	private String description;

	public Long getTasktype_id() {
		return tasktype_id;
	}

	public void setTasktype_id(Long tasktype_id) {
		this.tasktype_id = tasktype_id;
	}

	public String getTasktypeName() {
		return tasktypeName;
	}

	public void setTasktypeName(String tasktypeName) {
		this.tasktypeName = tasktypeName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	

}

	
