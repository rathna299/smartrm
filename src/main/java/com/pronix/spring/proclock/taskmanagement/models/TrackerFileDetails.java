package com.pronix.spring.proclock.taskmanagement.models;

import java.io.Serializable;
import java.util.List;

public class TrackerFileDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private TaskDetailsTracker detailsTracker;
	private List<TaskDetailsFile> files;
	public TaskDetailsTracker getDetailsTracker() {
		return detailsTracker;
	}
	public void setDetailsTracker(TaskDetailsTracker detailsTracker) {
		this.detailsTracker = detailsTracker;
	}
	public List<TaskDetailsFile> getFiles() {
		return files;
	}
	public void setFiles(List<TaskDetailsFile> files) {
		this.files = files;
	}
	
	

	
}
