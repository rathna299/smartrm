package com.pronix.spring.proclock.timesheet.DAO;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pronix.spring.proclock.timesheet.models.Timesheet_details;

public interface TimeSheet_DAO extends JpaRepository<Timesheet_details, Long> {

	@Query(value = "select * from timesheet_details where user_id =?1", nativeQuery = true)
	List<Timesheet_details> findbyUserId(Long userId);

	@Query(value = "select * from timesheet_details where user_id =?1 and ts_year =?2 and ts_month =?3 and ts_week =?4 and project_id =?5", nativeQuery = true)
	Optional<Timesheet_details> findByYearMonthWeekDay(long user_id, long year, long month, long week, long project_id);

	@Query(value = "select * from timesheet_details where week_status =?1", nativeQuery = true)
	List<Timesheet_details> findByStatus(String status);

	@Query(value = "select * from timesheet_details where week_status =?1 and user_id =?2 and ts_year =?3 and ts_month =?4", nativeQuery = true)
	List<Timesheet_details> findByUserAndStatusAndYearAndMonth(String status, Long user_id, long year, long month);

	@Query(value = "select * from timesheet_details where user_id =?1 and ts_year =?2 and ts_month =?3 and ts_week =?4", nativeQuery = true)
	List<Timesheet_details> findByUserAndYearAndMonthAndWeek(Long user_id, long year, long month, long week);

	@Query(value = "select * from timesheet_details where week_status =?1 and ts_year =?2 and ts_month =?3", nativeQuery = true)
	List<Timesheet_details> findByStatusAndYearAndMonth(String status, long year, long month);

	@Query(value = "select * from timesheet_details where user_id =?1 and ts_year =?2 and ts_month =?3", nativeQuery = true)
	List<Timesheet_details> findByUserAndYearAndMonth(Long user_id, long year, long month);

	@Query(value = "SELECT * FROM timesheet_details where week_status= ?1 and user_id = ?2 and ts_month = ?3 and ts_year =?4 group by ts_week", nativeQuery = true)
	List<Timesheet_details> findByTimesheetCountByManager(String weekstatus,long user_id,long ts_month,long ts_year);
	
	@Query(value = "SELECT * FROM timesheet_details where week_status= ?1 and ts_month = ?2 and ts_year =?3 group by ts_week", nativeQuery = true)
	List<Timesheet_details> findByTimesheetCountByAllUsers(String weekstatus,long ts_month,long ts_year);
}
