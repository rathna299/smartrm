package com.pronix.spring.proclock.timesheet.models;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.pronix.spring.proclock.models.Project;
import com.pronix.spring.proclock.models.UserDetails;

@Entity
@Table(name = "timesheet_details")
public class Timesheet_details {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long timesheet_details_id;

	private long ts_year;

	private long ts_month;

	private long ts_week;

	private LocalDate sun_date;

	private double sun_duration;

	private String sun_note;

	private String sun_status;

	private String sun_reject_note;

	private LocalDate mon_date;

	private double mon_duration;

	private String mon_note;

	private String mon_status;

	private String mon_reject_note;

	private LocalDate tue_date;

	private double tue_duration;

	private String tue_note;

	private String tue_status;

	private String tue_reject_note;

	private LocalDate wed_date;

	private double wed_duration;

	private String wed_note;

	private String wed_status;

	private String wed_reject_note;

	private LocalDate thu_date;

	private double thu_duration;

	private String thu_note;

	private String thu_status;

	private String thu_reject_note;

	private LocalDate fri_date;

	private double fri_duration;

	private String fri_note;

	private String fri_status;

	private String fri_reject_note;

	private LocalDate sat_date;

	private double sat_duration;

	private String sat_note;

	private String sat_status;

	private String sat_reject_note;

	private double week_duration;

	private String week_status;

	private String week_note;

	private String week_reject_note;

	@ManyToOne
	@JoinColumn(name = "project_id", referencedColumnName = "project_id")
	private Project project;

	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "user_id")
	private UserDetails userDetails;

	public long getTimesheet_details_id() {
		return timesheet_details_id;
	}

	public long getTs_year() {
		return ts_year;
	}

	public long getTs_month() {
		return ts_month;
	}

	public long getTs_week() {
		return ts_week;
	}

	public LocalDate getSun_date() {
		return sun_date;
	}

	public double getSun_duration() {
		return sun_duration;
	}

	public String getSun_note() {
		return sun_note;
	}

	public String getSun_status() {
		return sun_status;
	}

	public String getSun_reject_note() {
		return sun_reject_note;
	}

	public LocalDate getMon_date() {
		return mon_date;
	}

	public double getMon_duration() {
		return mon_duration;
	}

	public String getMon_note() {
		return mon_note;
	}

	public String getMon_status() {
		return mon_status;
	}

	public String getMon_reject_note() {
		return mon_reject_note;
	}

	public LocalDate getTue_date() {
		return tue_date;
	}

	public double getTue_duration() {
		return tue_duration;
	}

	public String getTue_note() {
		return tue_note;
	}

	public String getTue_status() {
		return tue_status;
	}

	public String getTue_reject_note() {
		return tue_reject_note;
	}

	public LocalDate getWed_date() {
		return wed_date;
	}

	public double getWed_duration() {
		return wed_duration;
	}

	public String getWed_note() {
		return wed_note;
	}

	public String getWed_status() {
		return wed_status;
	}

	public String getWed_reject_note() {
		return wed_reject_note;
	}

	public LocalDate getThu_date() {
		return thu_date;
	}

	public double getThu_duration() {
		return thu_duration;
	}

	public String getThu_note() {
		return thu_note;
	}

	public String getThu_status() {
		return thu_status;
	}

	public String getThu_reject_note() {
		return thu_reject_note;
	}

	public LocalDate getFri_date() {
		return fri_date;
	}

	public double getFri_duration() {
		return fri_duration;
	}

	public String getFri_note() {
		return fri_note;
	}

	public String getFri_status() {
		return fri_status;
	}

	public String getFri_reject_note() {
		return fri_reject_note;
	}

	public LocalDate getSat_date() {
		return sat_date;
	}

	public double getSat_duration() {
		return sat_duration;
	}

	public String getSat_note() {
		return sat_note;
	}

	public String getSat_status() {
		return sat_status;
	}

	public String getSat_reject_note() {
		return sat_reject_note;
	}

	public double getWeek_duration() {
		return week_duration;
	}

	public String getWeek_status() {
		return week_status;
	}

	public String getWeek_note() {
		return week_note;
	}

	public String getWeek_reject_note() {
		return week_reject_note;
	}

	public Project getProject() {
		return project;
	}

	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setTimesheet_details_id(long timesheet_details_id) {
		this.timesheet_details_id = timesheet_details_id;
	}

	public void setTs_year(long ts_year) {
		this.ts_year = ts_year;
	}

	public void setTs_month(long ts_month) {
		this.ts_month = ts_month;
	}

	public void setTs_week(long ts_week) {
		this.ts_week = ts_week;
	}

	public void setSun_date(LocalDate sun_date) {
		this.sun_date = sun_date;
	}

	public void setSun_duration(double sun_duration) {
		this.sun_duration = sun_duration;
	}

	public void setSun_note(String sun_note) {
		this.sun_note = sun_note;
	}

	public void setSun_status(String sun_status) {
		this.sun_status = sun_status;
	}

	public void setSun_reject_note(String sun_reject_note) {
		this.sun_reject_note = sun_reject_note;
	}

	public void setMon_date(LocalDate mon_date) {
		this.mon_date = mon_date;
	}

	public void setMon_duration(double mon_duration) {
		this.mon_duration = mon_duration;
	}

	public void setMon_note(String mon_note) {
		this.mon_note = mon_note;
	}

	public void setMon_status(String mon_status) {
		this.mon_status = mon_status;
	}

	public void setMon_reject_note(String mon_reject_note) {
		this.mon_reject_note = mon_reject_note;
	}

	public void setTue_date(LocalDate tue_date) {
		this.tue_date = tue_date;
	}

	public void setTue_duration(double tue_duration) {
		this.tue_duration = tue_duration;
	}

	public void setTue_note(String tue_note) {
		this.tue_note = tue_note;
	}

	public void setTue_status(String tue_status) {
		this.tue_status = tue_status;
	}

	public void setTue_reject_note(String tue_reject_note) {
		this.tue_reject_note = tue_reject_note;
	}

	public void setWed_date(LocalDate wed_date) {
		this.wed_date = wed_date;
	}

	public void setWed_duration(double wed_duration) {
		this.wed_duration = wed_duration;
	}

	public void setWed_note(String wed_note) {
		this.wed_note = wed_note;
	}

	public void setWed_status(String wed_status) {
		this.wed_status = wed_status;
	}

	public void setWed_reject_note(String wed_reject_note) {
		this.wed_reject_note = wed_reject_note;
	}

	public void setThu_date(LocalDate thu_date) {
		this.thu_date = thu_date;
	}

	public void setThu_duration(double thu_duration) {
		this.thu_duration = thu_duration;
	}

	public void setThu_note(String thu_note) {
		this.thu_note = thu_note;
	}

	public void setThu_status(String thu_status) {
		this.thu_status = thu_status;
	}

	public void setThu_reject_note(String thu_reject_note) {
		this.thu_reject_note = thu_reject_note;
	}

	public void setFri_date(LocalDate fri_date) {
		this.fri_date = fri_date;
	}

	public void setFri_duration(double fri_duration) {
		this.fri_duration = fri_duration;
	}

	public void setFri_note(String fri_note) {
		this.fri_note = fri_note;
	}

	public void setFri_status(String fri_status) {
		this.fri_status = fri_status;
	}

	public void setFri_reject_note(String fri_reject_note) {
		this.fri_reject_note = fri_reject_note;
	}

	public void setSat_date(LocalDate sat_date) {
		this.sat_date = sat_date;
	}

	public void setSat_duration(double sat_duration) {
		this.sat_duration = sat_duration;
	}

	public void setSat_note(String sat_note) {
		this.sat_note = sat_note;
	}

	public void setSat_status(String sat_status) {
		this.sat_status = sat_status;
	}

	public void setSat_reject_note(String sat_reject_note) {
		this.sat_reject_note = sat_reject_note;
	}

	public void setWeek_duration(double week_duration) {
		this.week_duration = week_duration;
	}

	public void setWeek_status(String week_status) {
		this.week_status = week_status;
	}

	public void setWeek_note(String week_note) {
		this.week_note = week_note;
	}

	public void setWeek_reject_note(String week_reject_note) {
		this.week_reject_note = week_reject_note;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}

}
