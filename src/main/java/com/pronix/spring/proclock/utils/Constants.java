package com.pronix.spring.proclock.utils;

public class Constants {

//	public static final String hostnameAPI = "http://183.82.123.57:1919";
//
//	public static final String hostnameUI = "http://183.82.123.57:1920";
//
////	public static final String hostnameAPI = "https://proclockapidev.azurewebsites.net";
//
////	public static final String hostnameUI = "https://proclockui.azurewebsites.net";
//
//	public static final String offerconfirm = hostnameUI+"/candidatedetails/";
//	
//	public static final String resubmitform = hostnameUI+"/resubmitform/";
//
//	public static final String genearetePasswordUI = "/generatepsd";
//
//	public static final String confirmEmail = "/proClock/confirmEmail/";
//
//	public static final String approve = "/proClock/approve/";
//
//	public static final String reject = "/proClock/reject/";
//
//	public static String confirmManagerEmail = "/proClock/confirmManagerEmail/";
//
//	public static String hrUI = "onboarding/offeredUserVerification";
	
	
	public static final String hostnameAPI = "Host";

	public static final String hostnameUI = "HostUI";

//	public static final String hostnameAPI = "https://proclockapidev.azurewebsites.net";

//	public static final String hostnameUI = "https://proclockui.azurewebsites.net";

	public static final String offerconfirm = hostnameUI+"//";
	
	public static final String resubmitform = hostnameUI+"//";

	public static final String genearetePasswordUI = "/";

	public static final String confirmEmail = "/proClock//";

	public static final String approve = "/proClock//";

	public static final String reject = "/proClock/reject/";

	public static String confirmManagerEmail = "/proClock/confirmManagerEmail/";

	public static String hrUI = "onboarding/offeredUserVerification";
	

}
